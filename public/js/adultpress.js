/* global eval, extContact, invoiceListTableID */
var MonthFormat = 'm/Y';
var DateFormat = 'd/m/Y';
var DateTimeFormat = 'd/m/Y H:i';
var deleted_customers_list;
$('#invemailCk').change(invMethodCheck);
$('#invaddressCk').change(invMethodCheck);
$('#invpoboxCk').change(invMethodCheck);
function invMethodCheck() {
  if ($('#invemailCk').is(':checked') === false && $('#invaddressCk').is(':checked') === false && $('#invpoboxCk').is(':checked') === false) {
    $("#updateAddabnButton").prop('disabled', true);
  } else {
    $("#updateAddabnButton").prop('disabled', false);
  }
}

var SOURCEROOT = document.location.origin;
if (document.location.hostname === 'localhost' || document.location.hostname === '127.0.0.1') {
  //var SOURCEROOT = (document.location.hostname === 'localhost') ? "http://localhost" : "http://127.0.0.1";
  var PUBLICDIRECTORY = '/';
} else {
  //var SOURCEROOT = "http://crm.adultpress.com.au";
  var PUBLICDIRECTORY = '/public/';
}

window.onload = function setUp() {
//*** progress bar
//*******************
  $("#invemailCk").change(function () {
    if (this.checked) {
      $("#invemailDiv").css("display", "block");
    } else {
      $("#invemailDiv").css("display", "none");
    }
  });
  $("#invaddressCk").change(function () {
    if (this.checked) {
      $("#invaddressDiv").css("display", "block");
    } else {
      $("#invaddressDiv").css("display", "none");
    }
  });
  $("#invpoboxCk").change(function () {
    if (this.checked) {
      $("#invpoboxDiv").css("display", "block");
    } else {
      $("#invpoboxDiv").css("display", "none");
    }
  });
// Document ROOT!
  var SOURCEROOT = document.location.origin;
  if (document.location.hostname === 'localhost' || document.location.hostname === '127.0.0.1') {
    //var SOURCEROOT = (document.location.hostname === 'localhost') ? "http://localhost" : "http://127.0.0.1";
    var PUBLICDIRECTORY = '/';
  } else {
    //var SOURCEROOT = "http://crm.adultpress.com.au";
    var PUBLICDIRECTORY = '/public/';
  }
//**************
  $(".Editor-editor").css("font-size", "1em");
  //***********
  if (typeof ($("#selectedUsers").val()) !== 'undefined' && typeof ($("#selectedUsers").val()) !== null)
    $('.selectpicker').selectpicker('val', $("#selectedUsers").val().split(','));
  $('.selectpicker').selectpicker();
  //********  row in duplicaterd table is clickable ************
  $(".clickable-row").click(function () {    
    var $th = $(this);    
    window.open($th.attr('data-href'), $th.attr('data-target'));    
  });
  //******************
// Customer form
// SET UP - disable all fields but business type
  function setAll() {
    $("#bnameDiv").css("display", "block");
    $("#bphoneDiv").css("display", "block");
    $("#bemailDiv").css("display", "block");
    $("#abnDiv").css("display", "block");
    $("#firstnameDiv").css("display", "block");
    $("#lastnameDiv").css("display", "block");
    $("#phoneDiv").css("display", "block");
    $("#positionDiv").css("display", "block");
    $("#emailDiv").css("display", "block");
    $("#websiteDiv").css("display", "block");
    $("#extendDiv").css("display", "block");
    $("#addressDiv").css("display", "block");
    $("#postcodeDiv").css("display", "block");
    $("#accountmanagerDiv").css("display", "block");
    $("#mpmDiv").css("display", "block");
    $("#brothelDiv").css("display", "block");
    $("#additionalContactsDiv").css("display", "block");
  }

  function setNull() {
    $("#bnameDiv").css("display", "none");
    $("#bphoneDiv").css("display", "none");
    $("#bemailDiv").css("display", "none");
    $("#abnDiv").css("display", "none");
    $("#firstnameDiv").css("display", "none");
    $("#lastnameDiv").css("display", "none");
    $("#phoneDiv").css("display", "none");
    $("#positionDiv").css("display", "none");
    $("#emailDiv").css("display", "none");
    $("#websiteDiv").css("display", "none");
    $("#extendDiv").css("display", "none");
    $("#addressDiv").css("display", "none");
    $("#postcodeDiv").css("display", "none");
    $("#accountmanagerDiv").css("display", "none");
    $("#mpmDiv").css("display", "none");
    $("#brothelDiv").css("display", "none");
    $("#additionalContactsDiv").css("display", "none");
  }

  if ($("#businessTypeSelect").val() === null) {
    setNull();
  } else {
    setForm();
  }
  $("#businessTypeSelect").change(setForm);
  function setForm() {
    setNull();
    var selectedOption = $("#businessTypeSelect option:selected").text();
    switch (selectedOption) {
      case "Escort":
        setAll();
        $("#abnDiv").css("display", "none");
        $("#positionDiv").css("display", "none");
        $("#extendDiv").css("display", "none");
        $("#addressDiv").css("display", "none");
        $("#mpmDiv").css("display", "none");
        $("#brothelDiv").css("display", "none");
        $("#additionalContactsDiv").css("display", "none");
        break;
      case "Brothel":
      case "Agency":
      case "Parlour":
        setAll();
        $("#mpmDiv").css("display", "none");
        break;
      case "Advertisment":
        setAll();
        break;
      case "Member":
        setAll();
        break;
      default:
        setAll();
    }
  }

//************************
//Form validation POSITION
  if ($("#positionForm").length !== 0) {
    $("#positionForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        position: {
          required: true,
          regex: /^[a-zA-Z0-9 -]{5,20}$/
        },
        status: {
          required: true
        }
      }
    });
  }
  if ($("#websitesForm").length !== 0) {
    $("#websitesForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        website: {
          required: true,
          regex: /^[a-zA-Z0-9 -]{5,20}$/
        },
        status: {
          required: true
        }
      }
    });
  }
  if ($("#statusForm").length !== 0) {
    $("#statusForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        orderid: {
          required: true,
          regex: /^[0-9]{1,6}$/
        },
        name: {
          required: true,
          regex: /^[a-zA-Z0-9 -]{3,30}$/
        },
        status: {
          required: true
        }
      }
    });
  }
  if ($("#typeForm").length !== 0) {
    $("#typeForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        type: {
          required: true,
          regex: /^[a-zA-Z0-9 -]{5,20}$/
        },
        status: {
          required: true
        }
      }
    });
  }

  if ($('input[name=gstcalcalculator]').length > 0) {
    $('input[name=gstcalcalculator]').on('input', function () {
      var value = parseFloat($('input[name=gstcalcalculator]').val());
      $('input[name=orderamount]').val(parseFloat(value - (value / 11)).toFixed(2))
    });
    $('.gstcalcalculator-info').click(function () {
      window.alert("Enter total amount, the subtotal will automatically be calculated");
    });
  }

//Form validation registrationForm
  if ($("#registrationForm").length !== 0) {
    $("#registrationForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        name: {
          regex: /^[a-zA-Z0-9 -]{2,50}$/,
          required: true
        },
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          minlength: 6
        },
        password_confirmation: {
          required: true,
          equalTo: "#password"
        },
        status: {
          required: true
        }
      }
    });
  }
//Form validation registrationForm
  if ($("#changeAccManagers").length !== 0) {
    $("#changeAccManagers").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        accFrom: {
          required: true
        },
        accTo: {
          required: true
        }
      }
    });
  }
  if ($("#userEditForm").length !== 0) {
    $("#userEditForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        name: {
          regex: /^[a-zA-Z0-9 -]{2,50}$/,
          required: true
        },
        email: {
          required: true,
          email: true
        },
        password: {
          minlength: 6
        },
        password_confirmation: {
          equalTo: "#password"
        },
        status: {
          required: true
        }
      }
    });
  }
  //********** Customer form validation
  if ($("#customer-form").length !== 0) {
    $("#customer-form").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        bname: {
          required: true,
          minlength: 2,
          maxlength: 30
        },
        bphone: {
          require_from_group: [1, ".contact-group"],
          regex: /^([0-9 ]{1,12})$/
        },
        bemail: {
          require_from_group: [1, ".contact-group"],
          minlength: 5,
          regex: /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i,
          email: true
        },
        abn: {
          regex: /^[0-9]{2}[ ][0-9]{3}[ ][0-9]{3}[ ][0-9]{3}$/
        },
        email: {
          require_from_group: function (element) {
            if ($("#businessTypeSelect option:selected").text() !== "Escort") {
              return [1, ".pcontact-group"];
            } else {
              return [0, ".pcontact-group"];
            }
          },
          regex: /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i,
          minlength: 5,
          email: true
        },
        phone: {
          require_from_group: function (element) {
            if ($("#businessTypeSelect option:selected").text() !== "Escort") {
              return [1, ".pcontact-group"];
            } else {
              return [0, ".pcontact-group"];
            }
          },
          minlength: 5,
          regex: /^([0-9 ]{1,12})$/
        },
        firstname: {
          required: function (element) {
            var status = false;
            if ($("#businessTypeSelect option:selected").text() !== "Escort") {
              status = true;
            }
            return status;
          }
        },
        searchPostcode: {
          required: true,
          minlength: 6,
          equalTo: "#LockSearch"
        },
        address: {
          required: function (element) {
            var status = false;
            if ($("#businessTypeSelect option:selected").text() !== "Escort") {
              var status = true;
            }
            return status;
          }
        },
        btype: {
          required: true
        },
        account_manager: {
          required: true
        }
      },
      submitHandler: function (form) {
        form.submit();
      },
      debug: true
    });
  }
  $("#facebook").change(facebookcheck);
  function facebookcheck() {
    $("#facebook").val($("#facebook").val().replace(/ /g, ''));
    facebookregex = /(http:\/\/|https:\/\/)?(www.)?facebook.com\/[a-zA-Z0-9./]{1,100}/;
    if ($("#facebook").val().match(facebookregex) !== null) {
      $("#facebook").val($("#facebook").val().match(facebookregex)[0]);
    } else {
      $("#facebook").val('');
    }
    if (facebookregex.test($("#facebook").val()) === false && $("#facebook").val() !== '') {
      $("#facebookerr").html('Please correct the facebook link');
    } else {
      $("#facebookerr").html('');
    }
  }

  $("#twitter").change(twittercheck);
  function twittercheck() {
    $("#twitter").val($("#twitter").val().replace(/ /g, ''));
    twitterregex = /(http:\/\/|https:\/\/)?(www.)?twitter.com\/[@a-zA-Z0-9./]{1,100}/;
    if ($("#twitter").val().match(twitterregex) !== null) {
      $("#twitter").val($("#twitter").val().match(twitterregex)[0]);
    } else {
      $("#twitter").val('');
    }
    if (twitterregex.test($("#twitter").val()) === false && $("#twitter").val() !== '') {
      $("#twittererr").html('Please correct the facebook link');
    } else {
      $("#twittererr").html('');
    }
  }

  $("#instagram").change(instagramcheck);
  function instagramcheck() {
    $("#instagram").val($("#instagram").val().replace(/ /g, ''));
    instagramregex = /(http:\/\/|https:\/\/)?(www.)?instagram.com\/[@a-zA-Z0-9./]{1,100}/;
    if ($("#instagram").val().match(instagramregex) !== null) {
      $("#instagram").val($("#instagram").val().match(instagramregex)[0]);
    } else {
      $("#instagram").val('');
    }
    if (twitterregex.test($("#instagram").val()) === false && $("#instagram").val() !== '') {
      $("#instagramerr").html('Please correct the facebook link');
    } else {
      $("#instagramerr").html('');
    }
  }

  //*************** deduplicate Form
  if ($("#deduplicateForm").length !== 0) {

    $("#deduplicateForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        findBy: {
          required: true
        }
      }
    });
  }
  //*************** Import Form
  if ($("#importForm").length !== 0) {
    $("#importForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        account_manager: {
          required: true
        },
        btype: {
          required: true
        },
        customersFile: {
          required: true
        }
      }
    });
  }
//*************** Customer Status Form
  if ($("#customerStatusForm").length !== 0) {

    $("#customerStatusForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        cstatus: {
          required: true
        }
      }
    });
  }
//*************** Addabn  Form
  if ($("#addabnForm").length !== 0) {
    $("#addabnForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        invname: {
          required: true,
          regex: /^[A-Za-z0-9 &\’./_\-`\'()]{3,50}$/
        },
        invabn: {
          required: function () {
            if ($("#btype").val() !== 'Escort')
              return true;
            return false;
          },
          regex: /^[0-9]{2}[ ][0-9]{3}[ ][0-9]{3}[ ][0-9]{3}$/
        },
        unit: {
          regex: /^[A-Za-z0-9 -/]{1,20}$/
        },
        number: {
          required: true,
          regex: /^[A-Za-z0-9 -/]{1,20}$/
        },
        street: {
          required: true,
          regex: /^[A-Za-z0-9 -]{2,30}$/
        },
        searchPostcode: {
          required: true,
          minlength: 6,
          equalTo: "#LockSearch"
        },
        searchSttype: {
          required: true
        },
        invemail: {
          regex: /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i,
          minlength: 5,
          email: true,
          required: true
        },
        invpoboxno: {
          regex: /^[0-9 -]{2,30}$/,
          required: true
        },
        invpoboxpostcode: {
          required: true,
          minlength: 6,
          equalTo: "#invpoboxpostcodeSearch"
        }
      }
    });
  }
//*************** Invoice  Form
  if ($("#invoiceForm").length !== 0) {
    $("#invoiceForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        created_at: {
          required: true
        },
        myob: {
          regex: /^[0-9a-zA-Z]{1,20}$/
        },
        orderamount: {
          regex: /^-?([0-9]{1,10}[.][0-9]{0,2}|[0-9]{1,10})$/
        },
        position: {
          regex: /^[0-9]{1,5}$/
        },
        startdate: "required",
        frequency: "required",
        expirydate: "required",
        duedate: "required",
        payment_method: "required"
      }
    });
  }
  //*************** Payment  Form
  if ($("#paymentForm").length !== 0) {
    $maxPayable = eval($("#invoiceTotalAmount").val()) - eval($("#invoicePaidAmount").val());
    $("#paymentForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        method: {
          required: true
        },
        amount: {
          required: true,
          regex: /^\d{1,20}[.]\d{2}$/
        },
        created_at: {
          required: true,
        }
      }
    });
  }
  // $( "#paymentForm").on("submit", function(e){
  //   e.preventDefault();
  //   var data = $( this ).serialize();
  //   console.log( data, '>>>>>>data')
  //   $.ajax({
  //     url: '/public/payment',
  //     data: data,
  //     method: 'POST'
  //   })
  //     .then( response => console.log( response ))
  // })
  //*************** Invoice Description  Form
  if ($("#orderoptionForm").length !== 0) {
    $("#orderoptionForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        orderoption: {
          required: true,
          regex: /^[0-9a-zA-Z ]{3,20}$/
        }
      }
    });
  }
  //*************** Invoice order page
  if ($("#orderpageForm").length !== 0) {
    $("#orderpageForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        orderpage: {
          required: true,
          regex: /^[0-9a-zA-Z ]{2,30}$/
        }
      }
    });
  }
  //*************** Payment Report  Form
  if ($("#paymentReportForm").length !== 0) {
    $("#paymentReportForm").validate({
      errorPlacement: function (error, element) {
        if (element.is(":radio")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else if (element.is(":checkbox")) {
          error.appendTo('#' + element.attr('name') + '_multiError');
        } else {
          error.appendTo(element.parent());
        }
      },
      rules: {
        reportfrom: {
          required: true
        },
        reportto: {
          required: true
        }
      }
    });
  }
  //** Fix the bug for js validator one of either fields! - form-group
  jQuery.validator.addMethod("require_from_group", function (value, element, options) {
    var numberRequired = options[0];
    var selector = options[1];
    var fields = $(selector, element.form);
    var filled_fields = fields.filter(function () {
      // it's more clear to compare with empty string
      return $(this).val() !== "";
    });
    var empty_fields = fields.not(filled_fields);
    // we will mark only first empty field as invalid
    if (filled_fields.length < numberRequired && empty_fields[0] === element) {
      return false;
    }
    return true;
    // {0} below is the 0th item in the options field
  }, jQuery.validator.format("Please fill out at least {0} of these fields."));
  // Custome error messages
  jQuery.extend(jQuery.validator.messages, {
    require_from_group: "One type of contact is required! Either Phone or Email"
  });
  // END form validation
  $("#txtEditor").Editor();
  /* Postcode autocomplete */
  //var hostname = $('<a>').prop('href', url).prop('hostname');
  $(function () {
    $("#SearchPostcodes").autocomplete({
      source: SOURCEROOT + PUBLICDIRECTORY + "/postcodes",
      dataType: "json",
      minLength: 3,
      select: function (event, ui) {
        $('#q').val(ui.item.value);
        $('#LockSearch').val(ui.item.value);
        var str = ui.item.value;
        var array = str.split(', ');
        $("#postcode").val(array[0]);
        $("#suburb").val(array[1]);
        $("#state").val(array[2]);
      }
    });
  });
  $(function () {
    $("#invpoboxpostcodesearch").autocomplete({
      source: SOURCEROOT + PUBLICDIRECTORY + "/postcodes",
      dataType: "json",
      minLength: 3,
      select: function (event, ui) {
        $('#q').val(ui.item.value);
        $('#invpoboxpostcodeSearch').val(ui.item.value);
        var str = ui.item.value;
        var array = str.split(', ');
        $("#invpoboxpostcode").val(array[0]);
        $("#invpoboxsuburb").val(array[1]);
        $("#invpoboxstate").val(array[2]);
      }
    });
  });
  $(function () {
    $("#searchSttype").autocomplete({
      source: SOURCEROOT + PUBLICDIRECTORY + "/streettypes",
      dataType: "json",
      minLength: 1,
      select: function (event, ui) {
        $('#searchSttype').val(ui.item.value);
      }
    });
  });
  $(function () {
    $("#bname").autocomplete({
      source: SOURCEROOT + PUBLICDIRECTORY + "/customersname",
      dataType: "json",
      minLength: 1,
      select: function (event, ui) {
        $('#bname').val(ui.item.value);
      }
    });
  });
  $(function () {
    $("#bphone").autocomplete({
      source: SOURCEROOT + PUBLICDIRECTORY + "/customersphone",
      dataType: "json",
      minLength: 1,
      select: function (event, ui) {
        $('#bphone').val(ui.item.value);
      }
    });
  });
  $(function () {
    $("#bemail").autocomplete({
      source: SOURCEROOT + PUBLICDIRECTORY + "/customersemail",
      dataType: "json",
      minLength: 1,
      select: function (event, ui) {
        $('#bemail').val(ui.item.value);
      }
    });
  });
  $('#users_list').DataTable(
    { 
      "dom":  "<'row'<'col-sm-12 text-right'f>>" +
              "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +
              "<'row'<'col-sm-12'i>>" +
              "rt" +
              "<'row'<'col-sm-12 text-right'p>>",
      "language": { "lengthMenu": "Display _MENU_ records per page" },
      "iDisplayLength": 100 
    });
  $("#users_list_length select").addClass("form-control");
  $("#users_list_filter input").addClass("form-control search");
  $('#transactionsListTable').DataTable({
    "dom":    "<'row'<'col-sm-12 text-right'f>>" +
                        "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +              
                        "<'row'<'col-sm-12'i>>" +
                        "rt" +
                        "<'row'<'col-sm-12 text-right'p>>",
    "language": { "lengthMenu": "Display _MENU_ records per page" },
    "order": [[0, "desc"]]
  });
  // ******** fix datatable titles
  //     //*************************
  $("#transactionsListTable_length select").prop("class", "form-control");
  $("#transactionsListTable_filter input").prop("class", "form-control");
  var invoiceTable = $(".invoiveListTable").DataTable(
    {
      "dom":  "<'row'<'col-sm-12 text-right'f>>" +
              "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +
              "<'row'<'col-sm-12'i>>" +
              "rt" +
              "<'row'<'col-sm-12 text-right'p>>",
      "language": { "lengthMenu": "Display _MENU_ records per page" },
      "iDisplayLength": 100,
      "aLengthMenu": [[10, 20, 50, 100, 500, -1], [10, 20, 50, 100, 500, "All"]],
      "order": [[1, "desc"]],
      "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        var eVal = function (i) {
          return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
        };
        // total_salary over all pages
        total_invoiced = api.column(5).data().reduce(function (a, b) {
          return eVal(a) + eVal(b);
        }, 0);

        total_paid = api.column(6).data().reduce(function (a, b) {
          return eVal(a) + eVal(b);
        }, 0);

        total_payable = api.column(7).data().reduce(function (a, b) {
          return eVal(a) + eVal(b);
        }, 0);

        total_page_invoiced = api.column(5, { page: 'current' }).data().reduce(function (a, b) {
          return eVal(a) + eVal(b);
        }, 0);

        total_page_paid = api.column(6, { page: 'current' }).data().reduce(function (a, b) {
          return eVal(a) + eVal(b);
        }, 0);

        total_page_payable = api.column(7, { page: 'current' }).data().reduce(function (a, b) {
          return eVal(a) + eVal(b);
        }, 0);

        total_page_invoiced = parseFloat(total_page_invoiced);
        total_invoiced = parseFloat(total_invoiced);
        // Update footer
        $('#total_page_invoiced').html('$' + addCommas(total_page_invoiced.toFixed(2)));
        $("#total_invoiced").html('$' + addCommas(total_invoiced.toFixed(2)));

        total_page_paid = parseFloat(total_page_paid);
        total_paid = parseFloat(total_paid);
        // Update footer
        $('#total_page_paid').html('$' + addCommas(total_page_paid.toFixed(2)));
        $("#total_paid").html('$' + addCommas(total_paid.toFixed(2)));

        total_page_payable = parseFloat(total_page_payable);
        total_payable = parseFloat(total_payable);
        // Update footer
        $('#total_page_payable').html('$' + addCommas(total_page_payable.toFixed(2)));
        $("#total_payable").html('$' + addCommas(total_payable.toFixed(2)));
      }
    }
  );
  $(".invoiveListTable_filter input").addClass("form-control search");
  $(".invoiveListTable_length select").addClass("form-control");
  $(".dataTables_filter input").addClass("form-control");
  $(".dataTables_length select").addClass("form-control");
  $('#duplicated_list').DataTable(
    {
      "dom":  "<'row'<'col-sm-12 text-right'f>>" +
              "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +
              "<'row'<'col-sm-12'i>>" +
              "rt" +
              "<'row'<'col-sm-12 text-right'p>>",
      "language": { "lengthMenu": "Display _MENU_ records per page" },
      "iDisplayLength": 100,
      "order": [[5, "asc"]],
      "aLengthMenu": [[10, 20, 50, 100, 500, -1], [10, 20, 50, 100, 500, "All"]]
    }
  );
  // ******** fix datatable titles for customers table
  //     //*************************
  $("#duplicated_list_length select").prop("class", "form-control");
  $("#duplicated_list_filter input").prop("class", "form-control");
//************ second table for tasks
  var tasks_list = $('#tasks_list').DataTable(
    {
      "dom":  "<'row'<'col-sm-12 text-right'f>>" +
              "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +
              "<'row'<'col-sm-12'i>>" +
              "rt" +
              "<'row'<'col-sm-12 text-right'p>>",
      "language": { "lengthMenu": "Display _MENU_ records per page" },
      "iDisplayLength": 100,
      "order": [[5, "asc"]],
      "aLengthMenu": [[10, 20, 50, 100, 500, -1], [10, 20, 50, 100, 500, "All"]]
    }
  );
  // ******** fix datatable titles for customers table
  //     //*************************
  $("#tasks_list_length select").prop("class", "form-control");
  $("#tasks_list_filter input").prop("class", "form-control");
  $(".taskfilterhead").each(function (i) {
    if ([1, 4].includes(i)) {
      var select = $('<select class=' + 'form-control' + '><option value="">- All -</option></select>')
        .appendTo($(this).empty())
        .on('change', function () {
          var term = $(this).val();
          tasks_list.column(i).search(term, false, false).draw();
        });
      tasks_list.column(i).data().unique().sort().each(function (d, j) {
        select.append('<option value="' + d + '">' + d + '</option>')
      });
    }
    if ([3].includes(i)) {
      var account_manager = $('#manager_name').html();
      var select = $('<select class=' + 'form-control' + '><option value="">- All -</option><option selected value="' + account_manager + '">' + account_manager + '</option></select>')
        .appendTo($(this).empty())
        .on('change', function () {
          var term = $(this).val();
          tasks_list.column(i).search(term, false, false).draw();
        });
      tasks_list.column(i).search(account_manager, false, false).draw();
      tasks_list.column(i).data().unique().sort().each(function (d, j) {
        if (d !== account_manager) {
          select.append('<option value="' + d + '">' + d + '</option>')
        }
      });
    }
  });
  if ($('button[name=customerRadiusSearch]').length > 0) {
    $('button[name=customerRadiusSearch]').click(function () {
      var postcode = $('select[name=postcode]').val();
      var btn = $(this);
      if (postcode !== '-1') {
        btn.attr('disabled', true);
        var data = {
          postcode: postcode,
          radius: $('select[name=radius]').val(),
          _token: $('input[name="_token"]').val()
        }
        customers_list.clear().draw();
        $.post(SOURCEROOT + PUBLICDIRECTORY + 'customerslistrefined', data)
          .done(function (response) {
            var customers = JSON.parse(response);
            console.log( customers, '>>>>customers')
            $.each(customers, function (i, value) {
              customers_list.row.add(value).draw(false);
            });
            customers_list.draw();
            btn.attr('disabled', false);
          })
          .fail(error => console.log(error))
      } else {
        location.reload();
      } 
    })
  }

  // ******** fix datatable titles for customers table
  //     //*************************
  $("#tasks_list_length select").prop("class", "form-control");
  $("#tasks_list_filter input").prop("class", "form-control");

  function cbDropdown(column) {
    return $('<ul>', {
      'class': 'cb-dropdown'
    }).appendTo($('<div>', {
      'class': 'cb-dropdown-wrap'
    }).appendTo(column));
  }
//************ first table fore customers
  var customers_list = $('#customers_list').DataTable(
    {  
      //live ajax path /public/customerslist
      "dom":  "<'row'<'col-sm-12 text-right'f>>" +              
              "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +              
              "<'row'<'col-sm-12'i>>" +
              "rt" +
              "<'row'<'col-sm-12 text-right'p>>",
      "language": { "lengthMenu": "Display _MENU_ records per page" },
      "ajax": SOURCEROOT + PUBLICDIRECTORY + 'customerslist',
      "columns": [
        {
          "data": "id",
          "fnCreatedCell": function (nTd, id) {
            $(nTd).html("<a target='_blank' href=' " + SOURCEROOT + PUBLICDIRECTORY + "customer/" + id + "'>" + id + "</a>");
          },
        },
        { "data": "btype" },
        {
          "data": "bname",
          "fnCreatedCell": function (nTd, id, row) {
            $(nTd).html("<a target='_blank' href=' " + SOURCEROOT + PUBLICDIRECTORY + "customer/" + row.id + "'>" + row.bname + "</a>");
          },
        },
        { "data": "bphone" },
        {
          "data": "bemail",
          "fnCreatedCell": function (nTd, bemail) {
            $(nTd).html("<a href='mailto://" + bemail + "'>" + bemail + "</a>");
          },
        },
        { "data": "suburb" },
        { "data": "state" },
        { "data": "manager" },
        { "data": "statusname" },
        { "data": "dueDatetime" }/*,
        { "data": "email", "visible": false },
        { "data": "firstname", "visible": false },
        { "data": "lastname", "visible": false },
        { "data": "phone", "visible": false },
        { "data": "abn", "visible": false},
        { "data": "website", "visible": false},
        {"data": "address", "visible": false },
          {"data": "postcode", "visible": false }*/
      ],
      "iDisplayLength": 100,
      "aLengthMenu": [[10, 20, 50, 100, 500, -1], [10, 20, 50, 100, 500, "All"]],
      "initComplete": function () {     
        $(".filterhead").each(function (i) {
          if ([1, 5, 6, 7, 8].includes(i)) {            
            var select = $('<select class="column' + i + ' form-control"><option value="">- All -</option></select>')
              .appendTo($(this).empty())
              .on('change', function () {
                var term = $(this).val();
                customers_list.column(i).search(term, false, false).draw();
              });
            if(i == 6) {  
              customers_list.column(i).data().unique().sort().each(function (d, j) {
                if(['ACT', 'NSW', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA'].includes(d))
                  select.append('<option value="' + d + '">' + d + '</option>');
              });
            } else {
              customers_list.column(i).data().unique().sort().each(function (d, j) {
                if(!['', null].includes(d))
                  select.append('<option value="' + d + '">' + d + '</option>');
              });
            }
          }
        });       
      }
    });

  $("#customers_list_length select").prop("class", "form-control");
  $("#customers_list_filter input").prop("class", "form-control");

  deleted_customers_list = $('#deleted_customers_table').DataTable({
    "dom":    "<'row'<'col-sm-12 text-right'f>>" +
              "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +              
              "<'row'<'col-sm-12'i>>" +
              "rt" +
              "<'row'<'col-sm-12 text-right'p>>",
    "language": { "lengthMenu": "Display _MENU_ records per page" },
    "iDisplayLength": 100,
    "aLengthMenu": [[10, 20, 50, 100, 500, -1], [10, 20, 50, 100, 500, "All"]],
    "initComplete": function () {
      deleted_customers_button_functions();
    }
  });
  // ******** fix datatable titles for customers table
  //     //*************************
  $("#deleted_customers_table_length select").prop("class", "form-control");
  $("#deleted_customers_table_filter input").prop("class", "form-control");



//****************************
  if ($("#cvsButton").length !== 0) {
    var buttons = new $.fn.dataTable.Buttons(customers_list, {
      buttons: ['csvHtml5']
    }).container().appendTo($('#cvsButton'));
  }
  $("#cvsButton a span").html("Export");

  if ($("#csvButton").length !== 0) {
    var buttons = new $.fn.dataTable.Buttons(invoiceTable, {
      buttons: [{
        extend: 'csvHtml5',
        text: 'Export',
        exportOptions: {
          modifier: {
            search: 'none'
          }
        }
      }
      ]
    }).container().appendTo($('#csvButton'));
  }
  // ******** fix datatable titles for customers table
  if (typeof extContact !== "undefined" && extContact !== '' && extContact !== null) {
    var count = extContact['fname'].length;
    for (i = 0; i < count; i++) {
      $("#additionalContactsWrapper ol").append($(".addContactTemplate").html());
      $("ol li:nth-child(" + eval(i + 1) + ") #fname").val(extContact['fname'][i]);
      $("ol li:nth-child(" + eval(i + 1) + ") #lname").val(extContact['lname'][i]);
      $("ol li:nth-child(" + eval(i + 1) + ") #phone").val(extContact['phone'][i]);
      //$("ol li:nth-child(" + eval(i+1) + ") #position").val(extContact['position'][i]);
      $("ol li:nth-child(" + eval(i + 1) + ") #position option[value='" + extContact['position'][i] + "']").attr('selected', 'selected');
      $("ol li:nth-child(" + eval(i + 1) + ") #email").val(extContact['email'][i]);
      $("#additionalContactsWrapper ol li:nth-child(" + eval(i + 1) + ")").attr('id', 'addContact' + eval(i + 1));
      $("#additionalContactsWrapper ol li:nth-child(" + eval(i + 1) + ") .removeButton").attr('onclick', 'removeContact(' + eval(i + 1) + ')');
    }
  }
//************* END ADDitional contact **************/
//**  Export to CSV
//**  END Export to CSV
  if (undefined !== $("#orders").val() && $("#orders").val().length) {
    if ($("#orders").val().length > 1) {
      ordersjson = $.parseJSON($("#orders").val());
    }
  }
  if ($("#ordersTable tbody tr").length <= 1) {
    $("#ordersTable tbody tr .btn:nth-child(2)").css("display", "none");
  } else {
    $("#ordersTable tbody tr .btn:nth-child(2)").css("display", "inline-block");
  }

  if ($('#mpmChecklist').length > 0) {
    $('#mpmChecklist').change(function () {
      if ($('#mpmChecklist:checked').length > 0) {
        $('.mpmchecklistdetails').removeClass('hidden')
      } else {
        $('.mpmchecklistdetails').addClass('hidden')
      }
    });
    $('#bChecklist').change(function () {
      if ($('#bChecklist:checked').length > 0) {
        $('.bchecklistdetails').removeClass('hidden')
      } else {
        $('.bchecklistdetails').addClass('hidden')
      }
    });
    $('#abchecklist').change(function () {
      if ($('#abchecklist:checked').length > 0) {
        $('.abchecklistdetails').removeClass('hidden')
      } else {
        $('.abchecklistdetails').addClass('hidden')
      }
    });
  }

  if ($('.myob-company-search').length > 0) {
    $('.myob-company-search').click(function () {
      $('.myob-company-search-table-container').html('');
      var btn = $(this);
      btn.attr('disabled', true);
      $.ajax({
        type: "GET",
        url: SOURCEROOT + PUBLICDIRECTORY + 'tools/myob/getAllCustomers',
        success: function (response) {
          $('.myob-company-search-table-container').html(response).promise().done(function () {
            $('#myob-customers-table').DataTable({
              "dom":    "<'row'<'col-sm-12 text-right'f>>" +
                        "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +              
                        "<'row'<'col-sm-12'i>>" +
                        "rt" +
                        "<'row'<'col-sm-12 text-right'p>>",
              "language": { "lengthMenu": "Display _MENU_ records per page" }              
            });
            $('.myob-company-search-table-container').removeClass('hidden');
            btn.attr('disabled', false);
            myobCustomersRowClick();
            close_popup_click_handler();
          });
          // ******** fix datatable titles
          //     //*************************
          $("#myob-customers-table_length select").prop("class", "form-control");
          $("#myob-customers-table_filter input").prop("class", "form-control");
        }
      });
    });

    function myobCustomersRowClick() {
      $('#myob-customers-table tbody').on('click', 'tr', function () {
        var uid = $(this).find('input[name=uid]').val();
        if (uid !== undefined && uid.length > 0) {
          $('.myob-company-search-table-container').addClass('hidden');
          $('.myob-company-search-table-container').html('');
          $('#invmyobisInput').val(uid);
        }
      });
    }

  }

  if ($('#brothels_account_uid').length > 0) {
    $('button[name=brothels_account_uid_search]').click(function () {
      $('.myob_search_container').html('');
      var btn = $(this);
      btn.attr('disabled', true);
      $.ajax({
        type: "GET",
        url: SOURCEROOT + PUBLICDIRECTORY + 'tools/myob/getAllAccounts',
        success: function (response) {
          $('.myob_search_container').html(response).promise().done(function () {
            $('#myob-accounts-table').DataTable({
              "dom":    "<'row'<'col-sm-12 text-right'f>>" +
                        "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +              
                        "<'row'<'col-sm-12'i>>" +
                        "rt" +
                        "<'row'<'col-sm-12 text-right'p>>",
              "language": { "lengthMenu": "Display _MENU_ records per page" }              
            });
            $('.myob_search_container').removeClass('hidden');
            btn.attr('disabled', false);
            myobAccountsBrothelsRowClick();
          });
          // ******** fix datatable titles
          //     //*************************
          $("#myob-accounts-table_length select").prop("class", "form-control");
          $("#myob-accounts-table_filter input").prop("class", "form-control");
        }
      });
    });

    $('button[name=mpm_account_uid_search]').click(function () {
      $('.myob_search_container').html('');
      var btn = $(this);
      btn.attr('disabled', true);
      $.ajax({
        type: "GET",
        url: SOURCEROOT + PUBLICDIRECTORY + 'tools/myob/getAllAccounts',
        success: function (response) {
          $('.myob_search_container').html(response).promise().done(function () {
            close_popup_click_handler();
            $('#myob-accounts-table').DataTable({
              "dom":    "<'row'<'col-sm-12 text-right'f>>" +
                        "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +              
                        "<'row'<'col-sm-12'i>>" +
                        "rt" +
                        "<'row'<'col-sm-12 text-right'p>>",
              "language": { "lengthMenu": "Display _MENU_ records per page" }              
            });
            $('.myob_search_container').removeClass('hidden');
            btn.attr('disabled', false);
            myobAccountsMPMRowClick();
          });
          // ******** fix datatable titles
          //     //*************************
          $("#myob-accounts-table_length select").prop("class", "form-control");
          $("#myob-accounts-table_filter input").prop("class", "form-control");
        }
      });
    });

    $('button[name=myob_tax_code_uid_search]').click(function () {
      $('.myob_search_container').html('');
      var btn = $(this);
      btn.attr('disabled', true);
      $.ajax({
        type: "GET",
        url: SOURCEROOT + PUBLICDIRECTORY + 'tools/myob/getAllTaxes',
        success: function (response) {
          $('.myob_search_container').html(response).promise().done(function () {
            close_popup_click_handler();
            $('#myob-taxes-table').DataTable({
              "dom":    "<'row'<'col-sm-12 text-right'f>>" +
                        "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +              
                        "<'row'<'col-sm-12'i>>" +
                        "rt" +
                        "<'row'<'col-sm-12 text-right'p>>",
              "language": { "lengthMenu": "Display _MENU_ records per page" }              
            });
            $('.myob_search_container').removeClass('hidden');
            btn.attr('disabled', false);
            myobTaxesRowClick();
          });
          // ******** fix datatable titles
          //     //*************************
          $("#myob-taxes-table_length select").prop("class", "form-control");
          $("#myob-taxes-table_filter input").prop("class", "form-control");
        }
      });
    });

    function myobAccountsBrothelsRowClick() {
      $('#myob-accounts-table tbody').on('click', 'tr', function () {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var uid = $(this).find('input[name=uid]').val();
        if (uid !== undefined && uid.length > 0) {
          $.post(SOURCEROOT + PUBLICDIRECTORY + 'tools/myob/updateBrothelsAccountUid', {
            value: uid,
            _token: CSRF_TOKEN
          }, function () {
            $('.myob_search_container').addClass('hidden');
            $('.myob_search_container').html('');
            $('#brothels_account_uid').val(uid);
          })
        }
      });
    }

    function myobAccountsMPMRowClick() {
      $('#myob-accounts-table tbody').on('click', 'tr', function () {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var uid = $(this).find('input[name=uid]').val();
        if (uid !== undefined && uid.length > 0) {
          $.post(SOURCEROOT + PUBLICDIRECTORY + 'tools/myob/updateMPMAccountUid', {
            value: uid,
            _token: CSRF_TOKEN
          }, function () {
            $('.myob_search_container').addClass('hidden');
            $('.myob_search_container').html('');
            $('#mpm_account_uid').val(uid);
          })
        }
      });
    }


    function myobTaxesRowClick() {
      $('#myob-taxes-table tbody').on('click', 'tr', function () {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var uid = $(this).find('input[name=uid]').val();
        if (uid !== undefined && uid.length > 0) {
          $.post(SOURCEROOT + PUBLICDIRECTORY + 'tools/myob/updateTaxUid', {
            value: uid,
            _token: CSRF_TOKEN
          }, function () {
            $('.myob_search_container').addClass('hidden');
            $('.myob_search_container').html('');
            $('#myob_tax_code_uid').val(uid);
          })
        }
      });
    }
  }

  function close_popup_click_handler() {
    $('.close-popup').click(function () {
      $('.popup').addClass('hidden');
      $('.popup').html('');
    });
  }

  if ($('#company-files-table').length > 0) {
    $('#company-files-table').DataTable({
      "dom":    "<'row'<'col-sm-12 text-right'f>>" +
                "<'row'<'col-sm-6 text-left'l><'col-sm-6 text-right'p>>" +              
                "<'row'<'col-sm-12'i>>" +
                "rt" +
                "<'row'<'col-sm-12 text-right'p>>",
      "language": { "lengthMenu": "Display _MENU_ records per page" }              
    });
    // ******** fix datatable titles
    //     //*************************
    $("#company-files-table_length select").prop("class", "form-control");
    $("#company-files-table_filter input").prop("class", "form-control");
    $('#company-files-table tbody').on('click', 'tr', function () {
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var uid = $(this).find('input[name=companyUID]').val();
      var url = $(this).find('input[name=companyURL]').val();
      if (uid !== undefined && uid.length > 0 && url !== undefined && url.length > 0) {
        $.post(SOURCEROOT + PUBLICDIRECTORY + 'tools/myob/updateCompanyFile', {
          uid: uid,
          url: url,
          _token: CSRF_TOKEN
        }, function () {
          $('.company-files-table-container').addClass('hidden');
          $('.company-files-table-container').html('');
        })
      }
    });
    $('.close-popup').click(function () {
      $('.company-files-table-container').addClass('hidden');
      $('.company-files-table-container').html('');
    });
  }

  if ($('button[name=myob_admin_password_button]').length > 0) {
    $('button[name=myob_admin_password_button]').click(function () {
      var button = $(this);
      button.attr('disabled', true);
      var password = $('#myob_admin_password')
      if (password !== undefined && password.length > 0) {
        var data = {
          _token: $('meta[name="csrf-token"]').attr('content'),
          newPassword: password.val()
        }
        $.post(SOURCEROOT + PUBLICDIRECTORY + 'tools/myob/updatePassword', data, function (response) {
          if (response) {
            $('.adminPasswordChangeInfo').html(response);
          } else {
            password.val('');
          }
          button.attr('disabled', false);
        })
      }
    });
  }

};

function myobSearchAjax() {

}

// END WINDOW ON LOAD
$("#sameInvEmail").click(function () {
  $("#invemail").val($("#workingEmail").val());
});
$("#sameInvABN").click(function () {
  $("#invABNInput").val($("#workingABN").val());
});
$("#sameInvName").click(function () {
  $("#invNameInput").val($("#workingName").val());
});
$("#sameInvAdd").click(function () {
  $("#invUnitInput").val($("#workingUnit").val());
  $("#invNumberInput").val($("#workingNumber").val());
  $("#invStreetInput").val($("#workingStreet").val());
  $("#searchSttype").val($("#workingSttype").val());
  $("#SearchPostcodes").val($("#workingPostcode").val() + ', ' + $("#workingSuburb").val() + ', ' + $("#workingState").val());
  $("#LockSearch").val($("#SearchPostcodes").val());
  $("#suburb").val($("#workingSuburb").val());
  $("#state").val($("#workingState").val());
  $("#postcode").val($("#workingPostcode").val());
});
$("input.abnInput").change(abnCheck);
$("input.abnInput").keyup(abnCheck);
$.validator.addMethod(
  "regex",
  function (value, element, regexp) {
    var re = new RegExp(regexp);
    return this.optional(element) || re.test(value);
  },
  "Please check your input."
);
$(".datePicker").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'

});
$("#startdate").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'
});
$("#expirydate").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'
});
$("#duedate").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'
});
$("#orderstartdate").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'
});
$("#orderexpirydate").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'
});
$("#orderduedate").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'
});
$("#dueDate").datetimepicker({
  format: DateTimeFormat,
  step: 30,
  minDate: '2010/01/01',
  minTime: '08:00'
});
$("#dueDateFrom").datetimepicker({
  format: DateFormat,
  minDate: '2010/01/01',
  timepicker: false
});
$("#dueDateTo").datetimepicker({
  format: DateFormat,
  minDate: '2010/01/01',
  timepicker: false
});
$("#monthfrom").datetimepicker({
  format: DateFormat,
  minDate: '2010/01/01',
  timepicker: false
});
$("#monthto").datetimepicker({
  format: DateFormat,
  minDate: '2010/01/01',
  timepicker: false
});
$("#reportfrom").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'
});
$("#reportto").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'
});
$("#invoiceCreatedAt").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'
});
$("#paymentCreatedAt").datetimepicker({
  format: DateFormat,
  timepicker: false,
  minDate: '2010/01/01'
});
function setSelectedValues() {
  $("#selectedUsers").val($(".selectpicker").val());
}
function editNote(id) {
//console.log(id);
  $("#txtEditor" + id).toggle();
  $("#txtEditor" + id).Editor();
  $("#editNote" + id).toggle();
  $("#updateNote" + id).toggle();
  $("#cancelUpdate" + id).toggle();
  $("#txtEditor" + id).Editor("setText", $("#note" + id).html());
  $("#note" + id).toggle();
}
// ************* Time out messages after 5 sec
$(function () {
  $('.systemmsg').show();
  setTimeout(function () {
    $('.systemmsg').fadeOut('slow');
  }, 5000);
});
//* Customer page tabs ***
//** set up  **//////
$("#tabBtnGroup span").prop("class", "btn btn-group btn-info btn-margin");
$(".customerContentContainer").css("display", "none");
$("#tabBtn1").prop("class", "btn btn-group btn-warning btn-margin");
$("#customerContainer1").css("display", "block");
//******* Toggle on click **///////
$("#tabBtn1").click(customerTabToggle(1));
$("#tabBtn2").click(customerTabToggle(2));
$("#tabBtn8").click(customerTabToggle(8));
$("#tabBtn3").click(customerTabToggle(3));
$("#tabBtn4").click(customerTabToggle(4));
$("#tabBtn5").click(customerTabToggle(5));
$("#tabBtn6").click(customerTabToggle(6));
$("#tabBtn7").click(customerTabToggle(7));
$("#tabBtn9").click(customerTabToggle(9));
function customerTabToggle(i) {
  $("#tabBtn" + i).click(function () {
    $("#tabBtnGroup span").prop("class", "btn btn-group btn-info btn-margin");
    $(".customerContentContainer").css("display", "none");
    $("#tabBtn" + i).prop("class", "btn btn-group btn-warning btn-margin");
    $("#customerContainer" + i).css("display", "block");
  });
}
//************************************
//************* ADDitional contact **************/
$("#addContactButton").click(addContactDiv);
$("#businessTypeSelect").change(setType);
function setType() {
  var type = $("#businessTypeSelect option:selected").text();
  $("#SelectedBusinessType").html(type);
}

function removeContact(i) {
//alert(i);
  $("#addContact" + i).remove();
  //this.parents().eq(3).remove();
}
function addContactDiv() {
  $("#additionalContactsWrapper ol").append($(".addContactTemplate").html());
  counts = $(".removeButton").length;
  for (i = 1; i <= counts; i++) {
// $("#removeButton").attr('id', 'addContact' + i );
    $("#additionalContactsWrapper ol li:nth-child(" + i + ")").attr('id', 'addContact' + i);
    $("#additionalContactsWrapper ol li:nth-child(" + i + ") .removeButton").attr('onclick', 'removeContact(' + i + ')');
  }
}
//$(".sweetDeleteButton").click(DeleteButton);
function DeleteButton(formid) {
  swal({
      title: "Are you sure?",
      text: "You will not be able to recover this customer",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete the customer file!',
      closeOnConfirm: false
    },
    function () {
      setTimeout(swal("Deleted!", "Customer file has been deleted!", "success"), 3000);
      $('#customerDeleteForm' + formid).submit();
      //$('#customerDeleteForm' + formid).submit();
    });
}
$(".DeleteButtonOption").click(DeleteButtonOption);
function DeleteButtonOption() {
  swal({
      title: "Are you sure?",
      text: "You will not be able to recover this action!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete this item!',
      closeOnConfirm: false
    },
    function () {
      swal("Deleted!", "Item has been deleted!", "success");
      setTimeout($('#OptionsDeleteForm').submit(), 1000);
    });
}
function invoiceDeleteButton() {
  $.post(SOURCEROOT + PUBLICDIRECTORY + 'invoice/checkMyobInvoice', {
    _token: $('input[name=_token]').val(),
    invoice_id: $('input[name=invoiceid]').val()
  }, function (response) {
    if (response) {
      swal({
          title: "Associated MYOB Invoice",
          text: "Please enter password to continue",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true,
          animation: "slide-from-top",
          inputPlaceholder: "Password"
        },
        function (inputValue) {
          $.post(SOURCEROOT + PUBLICDIRECTORY + 'tools/myob/checkPassword', {
            _token: $('input[name=_token]').val(),
            password: inputValue
          }, function (response) {
            if (response) {
              deleteBox()
            } else {
              swal("Wrong password entered")
            }
          });
        });
    } else {
      deleteBox();
    }
  });
  function deleteBox() {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this action!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete this item!',
        closeOnConfirm: false
      },
      function () {
        swal("Deleted!", "Item has been deleted!", "success");
        setTimeout($('#invoiceDeleteForm').submit(), 1000);
      });
  }
}

function invoiceCancelButton() {
  swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, cancel this invoice',
      closeOnConfirm: false
    },
    function () {
      $('#cancelInvoiceButton').submit();
    });
}

function DeletePaymentButton(paymentid) {
  swal({
      title: "Are you sure?",
      text: "You will not be able to recover this action!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete this item!',
      closeOnConfirm: false
    },
    function () {
      swal("Deleted!", "Item has been deleted!", "success");
      setTimeout($('#paymentDeleteForm' + paymentid).submit(), 1000);
    });
}
$("#paymentmount").keydown(function (event) {
  if (event.keyCode === 13) {
    $("#paymentmount").val(fixdecimals('paymentmount'));
    return true;
  }
});
$("#paymentmount").focusout(function () {
  $("#paymentmount").val(fixdecimals('paymentmount'));
  return true;
});
$("#addorderbutton").click(addorder);
$("#orderamount, #orderpage, #orderposition").keydown(function (event) {
  if (event.keyCode === 13) {
    $("#orderamount").val(fixdecimals('orderamount'));
    event.preventDefault();
    addorder();
    return false;
  }
});
$("#orderamount, #orderpage, #orderposition").focusout(function () {
  $("#orderamount").val(fixdecimals('orderamount'));
  event.preventDefault();
  return false;
});
function fixdecimals(id) {
  var amount = $("#" + id).val();
  var amountRegmatch1 = new RegExp('^-?[0-9]{1,8}$');
  var amountRegmatch2 = new RegExp('^-?[0-9]{1,8}[.]$');
  var amountRegmatch3 = new RegExp('^-?[0-9]{1,8}[.][0-9]{1}$');
  if (amountRegmatch1.test(amount)) {
    amount = amount + '.00';
  }
  if (amountRegmatch2.test(amount)) {
    amount = amount + '00';
  }
  if (amountRegmatch3.test(amount)) {
    amount = amount + '0';
  }
  return amount;
}
var ordersjson = {};
function addorder() {
  if (ordervalidation()) {
    var amountRegmatch = new RegExp('^-?([0-9]{1,10}|[0-9]{1,10}[.][0-9]{0,2})$');
    if (eval($("#websites").val()) > 0 && $("#websites").val() !== null && amountRegmatch.test($("#orderamount").val())) {
      var orderid = $("#websites").val() + '-' + $("#orderpage option:selected").attr("extstr");
      var editting_order = $('.editing').attr('id');
      if (editting_order) {
        editting_order = editting_order.split('orderrow');
        editting_order = editting_order[editting_order.length - 1];
        removeorder(editting_order);
        $('#ordersTable tbody tr').each(function () {
          if ($(this).hasClass('editing')) {
            $(this).removeClass('editing');
          }
        })
      }
      $("#ordersTable tbody").append("<tr id=orderrow" + orderid + "><td>" + $("#websites option:selected").text() + "</td><td>" + $("#orderoption").val() + "</td><td>" + $("#orderpage").val() + "</td><td>" + $("#orderposition").val() + "</td><td>" + $("#orderstartdate").val() + "</td><td>" + $("#orderfrequency").val() + "</td><td>" + $("#orderexpirydate").val() + "</td><td>" + $("#orderduedate").val() + "</td><td class='amountColumn'>$</td><td class='amountColumn rowordervalue'>" + $("#orderamount").val() + "</td><td class='amountColumn text-right'><span class='btn btn-primary btn-sm glyphicon glyphicon-edit' onclick=editorder('" + orderid + "')></span><span class='btn btn-danger btn-sm glyphicon glyphicon-remove' onclick=removeorder('" + orderid + "')></span></td></tr>");
      settotals(eval($("#orderamount").val()));
      ordersjson[orderid] = $("#websites option:selected").text() + ',' + $("#orderoption").val() + ',' + eval($("#orderamount").val()).toFixed(2) + ',' + $("#orderpage").val() + ',' + $("#orderposition").val() + ',' + $("#orderstartdate").val() + ',' + $("#orderfrequency").val() + ',' + $("#orderexpirydate").val() + ',' + $("#orderduedate").val() + ',' + $('#from-myob-account').val();
      $(".ordersInputs input").val('');
      $(".ordersInputs select").val('');
      $("#orders").val(JSON.stringify(ordersjson));
      JSON.stringify(ordersjson);
      if ($("#ordersTable tbody tr").length <= 1) {
        $("#ordersTable tbody tr .btn:nth-child(2)").css("display", "none");
      } else {
        $("#ordersTable tbody tr .btn:nth-child(2)").css("display", "inline-block");
      }
    }
  }
}
function removeorder(orderid) {
  if ($("#orderrow" + orderid).length !== 0) {
    delete ordersjson[orderid];
    //ordersjson.splice(orderid, 1);
    $("#orders").val(JSON.stringify(ordersjson));
    var invoicesubtotal = eval($("#invoicesubtotal").html().replace(',', ''));
    invoicesubtotal -= eval($("#orderrow" + orderid + " .rowordervalue").html());
    $("#invoicesubtotal").html(eval(invoicesubtotal).toFixed(2));
    $("#ordersTable #orderrow" + orderid).remove();
    setGSTandtotal();
    if ($("#ordersTable tbody tr").length <= 1) {
      $("#ordersTable tbody tr .btn:nth-child(2)").css("display", "none");
    } else {
      $("#ordersTable tbody tr .btn:nth-child(2)").css("display", "inline-block");
    }
  }
}

function editorder(orderid) {
  var row = $('#' + orderid);
  $('#ordersTable tbody tr').each(function () {
    if ($(this).hasClass('editing')) {
      $(this).removeClass('editing');
    }
    if ($(this).attr('id') === 'orderrow' + orderid) {
      $(this).addClass('editing');
    }
  })
  setOptionId('websites', $("#orderrow" + orderid + " td:nth-child(1)").html());
  $("#orderamount").val($("#orderrow" + orderid + " td:nth-child(10)").html());
  //setOptionId('websites',$("#orderrow" + orderid +" td:nth-child(1)").html());
  setOptionId('orderoption', $("#orderrow" + orderid + " td:nth-child(2)").html());
  setOptionId('orderpage', $("#orderrow" + orderid + " td:nth-child(3)").html());
  $("#orderposition").val($("#orderrow" + orderid + " td:nth-child(4)").html());
  $("#orderstartdate").val($("#orderrow" + orderid + " td:nth-child(5)").html());
  setOptionId('orderfrequency', $("#orderrow" + orderid + " td:nth-child(6)").html());
  $("#orderexpirydate").val($("#orderrow" + orderid + " td:nth-child(7)").html());
  $("#orderduedate").val($("#orderrow" + orderid + " td:nth-child(8)").html());
  $("#from-myob-account").val($("#orderrow" + orderid + " input[name=myob_account]").val());
}
function setOptionId(selectId, optionText) {
  $("#" + selectId + " > option").each(function () {
    if (this.text === optionText) {
      $("#" + selectId).val(this.value);
    }
    ;
  });
}

function settotals(ordervalue) {
  var invoicesubtotal = eval($("#invoicesubtotal").html().replace(',', ''));
  invoicesubtotal += eval(ordervalue);
  $("#invoicesubtotal").html(eval(invoicesubtotal).toFixed(2));
  setGSTandtotal();
}
function setGSTandtotal() {
  $("#invoicegst").html(eval(eval($("#invoicesubtotal").html().replace(',', '')) / 10).toFixed(2));
  $("#invoicetotal").html((eval($("#invoicesubtotal").html().replace(',', '')) + eval($("#invoicegst").html().replace(',', ''))).toFixed(2));
}
function ordervalidation() {
  $("span.error").html('');
  if ($("#websites").val() < 1) {
    $("#websites").siblings(".error").html("Please select a website");
    return false;
  }
  amountPreg = /^-?([0-9]{1,10}[.][0-9]{0,2}|[0-9]{1,10})$/;
  if (!amountPreg.test($("#orderamount").val())) {
    $("#orderamount").siblings(".error").html("Amount format 0.00");
    return false;
  }
  if (!$.isNumeric($("#orderposition").val()) && $("#orderposition").val() !== '') {
    $("#orderposition").siblings(".error").html("Just numeric");
    return false;
  } else if ($("#orderpage").val() === '' && $("#orderposition").val() !== '') {
    $("#orderpage").siblings(".error").html("Required with position");
    return false;
  }

  if ($("#orderoption option:selected").text() !== "SMS campaign" && $("#websites option:selected").text() === "Brothels" && $("#orderposition").val() === '') {

    if ($("#websites  option:selected").text() === "Brothels" && $("#orderpage").val() === '') {
      $("#orderpage").siblings(".error").html("Required");
      return false;
    }
    if (!$.isNumeric($("#orderposition").val()) || $("#orderposition").val() === '') {
      $("#orderposition").siblings(".error").html("Required");
      return false;
    }
    return false;
  }
  if ($("#orderstartdate").val() === '') {
    $("#orderstartdateerror").html("Required");
    return false;
  }
  if ($("#orderfrequency").val() === '') {
    $("#orderfrequencyerror").html("Required");
    return false;
  }
  if ($("#orderexpirydate").val() === '') {
    $("#orderexpirydateerror").html("Required");
    return false;
  }
  if ($("#orderduedate").val() === '') {
    $("#orderduedateerror").html("Required");
    return false;
  }
  return true;
}
//************* expiry and due date
$("#startdate").change(setexpiryduedate);
$("#frequency").change(setexpiryduedate);
function setexpiryduedate() {
  if ($("#frequency").val() !== null && $("#startdate").val() !== '') {
    switch ($("#frequency").val()) {
      case 'Weekly':
        $("#expirydate").val(addDaysfromtoday($("#startdate").val(), 7));
        break;
      case 'Fortnightly':
        $("#expirydate").val(addDaysfromtoday($("#startdate").val(), 14));
        break;
      case 'Monthly':
        $("#expirydate").val(addMonthsfromtoday($("#startdate").val(), 1));
        break;
      case 'Quarterly':
        $("#expirydate").val(addMonthsfromtoday($("#startdate").val(), 3));
        break;
      case 'Bi-annually':
        $("#expirydate").val(addMonthsfromtoday($("#startdate").val(), 6));
        break;
      case 'Annually':
        $("#expirydate").val(addMonthsfromtoday($("#startdate").val(), 12));
        break;
    }
    $("#duedate").val(addDaysfromtoday($("#expirydate").val(), -6));
  }
}
//************* expiry and due date
$("#orderstartdate").change(setorderexpiryduedate);
$("#orderfrequency").change(setorderexpiryduedate);
function setorderexpiryduedate() {
  if ($("#orderfrequency").val() !== null && $("#orderstartdate").val() !== '' && $("#orderfrequency").val() !== '') {
    switch ($("#orderfrequency").val()) {
      case 'Weekly':
        $("#orderexpirydate").val(addDaysfromtoday($("#orderstartdate").val(), 7));
        break;
      case 'Fortnightly':
        $("#orderexpirydate").val(addDaysfromtoday($("#orderstartdate").val(), 14));
        break;
      case 'Monthly':
        $("#orderexpirydate").val(addMonthsfromtoday($("#orderstartdate").val(), 1));
        break;
      case 'Quarterly':
        $("#orderexpirydate").val(addMonthsfromtoday($("#orderstartdate").val(), 3));
        break;
      case 'Bi-annually':
        $("#orderexpirydate").val(addMonthsfromtoday($("#orderstartdate").val(), 6));
        break;
      case 'Annually':
        $("#orderexpirydate").val(addMonthsfromtoday($("#orderstartdate").val(), 12));
        break;
    }
    $("#orderduedate").val(addDaysfromtoday($("#orderexpirydate").val(), -6));
  }
}
function addDaysfromtoday(string, int) {
  var inputDate = string.split('/');
  var startdate = new Date(inputDate[2], inputDate[1] - 1, inputDate[0]);
  //console.log(startdate);
  var finishdate = new Date(inputDate[2], inputDate[1] - 1, inputDate[0]);
  //console.log(finishdate);
  finishdate.setDate(finishdate.getDate() + int - 1);
  var dd = finishdate.getDate();
  var mm = finishdate.getMonth() + 1;
  var y = finishdate.getFullYear();
  var FormattedDate = dd + '/' + mm + '/' + y;
  return FormattedDate;
}
function addMonthsfromtoday(string, int) {
  var inputDate = string.split('/');
  var startdate = new Date(inputDate[2], inputDate[1] - 1, inputDate[0]);
  var finishdate = new Date(inputDate[2], inputDate[1] - 1, inputDate[0]);
  finishdate.setMonth(finishdate.getMonth() + int);
  finishdate.setDate(finishdate.getDate() - 1);
  var dd = finishdate.getDate();
  var mm = finishdate.getMonth() + 1;
  var y = finishdate.getFullYear();
  var FormattedDate = dd + '/' + mm + '/' + y;
  return FormattedDate;
}
function abnCheck() {
  var abnCk = /^(\d{1,2}|\d{2}[ ]|\d{2}[ ]\d|\d{2}[ ]\d{1,3}|\d{2}[ ]\d{3}[ ]|\d{2}[ ]\d{3}[ ]\d{1,3}|\d{2}[ ]\d{3}[ ]\d{3}[ ]|\d{2}[ ]\d{3}[ ]\d{3}[ ]\d{1,3}|\d{2}[ ]\d{3}[ ]\d{3}[ ]\d{1,3})$/;
  if (!abnCk.test($(this).val()) || $(this).val() === '') {
    $(this).val($(this).val().slice(0, -1));
  }
  var abnCk = /^(\d{2}|\d{2}[ ]\d{3}|\d{2}[ ]\d{3}[ ]\d{3})$/;
  if (abnCk.test($(this).val())) {
    $(this).val($(this).val() + ' ');
  }
}
$("input.bphoneInput").change(phoneCheck);
$("input.bphoneInput").keyup(phoneCheck);
function phoneCheck() {
  str = $(this).val();
  str = str.replace(/\D/g, '');
  strlen = str.length;
  formatedstr = '';
  if (strlen > 1) {
    if (str[0] === '0' && str[1] === '4') {
      if (5 <= strlen && strlen < 8) {
        formatedstr = str.substring(0, 4) + ' ' + str.substring(4, strlen);
      } else if (8 <= strlen) {
        formatedstr = str.substring(0, 4) + ' ' + str.substring(4, 7) + ' ' + str.substring(7, 10);
      } else {
        formatedstr = str;
      }
    } else if (str[0] === '0' && str[1] !== '4') {
      if (3 <= strlen && strlen < 6) {
        formatedstr = str.substring(0, 2) + ' ' + str.substring(2, strlen);
      } else if (6 <= strlen) {
        formatedstr = str.substring(0, 2) + ' ' + str.substring(2, 6) + ' ' + str.substring(6, 10);
      } else {
        formatedstr = str;
      }
    } else if (str[0] !== '0' && str[0] !== '1' && strlen <= 8) {

      if (5 <= strlen) {
        formatedstr = str.substring(0, 4) + ' ' + str.substring(4, strlen);
      } else {
        formatedstr = str;
      }
    } else if (str[0] === '1' && (str[1] === '3' || str[1] === '8')) {
      if (5 <= strlen && strlen < 8) {
        formatedstr = str.substring(0, 4) + ' ' + str.substring(4, strlen);
      } else if (8 <= strlen) {
        formatedstr = str.substring(0, 4) + ' ' + str.substring(4, 7) + ' ' + str.substring(7, 10);
      } else {
        formatedstr = str;
      }
    } else {
      formatedstr = str;
    }
  } else {
    formatedstr = str;
  }
  $(this).val(formatedstr);
}
$("#cstatuses").change(setExpDays);
function setExpDays() {
  var intRegex = /^\d+$/;
  var selectedStatus = $("#cstatuses").val();
  var expdays = $("#cstatuses option:selected").attr("expdays");
  //alert(intRegex.test(expdays));
//    if (!intRegex.test(expdays))
//    {
//        return;
//    }
  var today = new Date();
  var d = ("0" + today.getDate()).slice(-2);
  var m = ("0" + (today.getMonth() + 1)).slice(-2);
  var y = today.getFullYear();
  var formattedToday = d + '/' + m + '/' + y;
  var duedate = addDaysfromtoday(formattedToday, eval(eval(expdays) + eval(1)));
  duedate = duedate + ' 08:00';
  if (selectedStatus === '9999' || selectedStatus === '1000' || selectedStatus === '9000' || selectedStatus === '9200') {
    $("#dueDate").val('');
    $('.due-date-wrapper').addClass('hidden');
  } else {
    $("#dueDate").val(duedate);
    if ($('.due-date-wrapper').hasClass('hidden')) {
      $('.due-date-wrapper').removeClass('hidden');
    }
  }
}

function addCommas(nStr) {
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}

function deleted_customers_button_functions() {
  $('button[name=deleted_c_restore]').click(function () {
    var dBtn = $('button[name=deleted_c_delete]'), rbtn = $(this);
    rbtn.attr('disabled', true);
    dBtn.attr('disabled', true);
    var data = {
      id: $(this).val(),
      _token: $('input[name="_token"]').val()
    }
    $.post(SOURCEROOT + PUBLICDIRECTORY + 'customer/restoreDeletedCustomer', data, function (result) {
      if (result) {
        deleted_customers_list
          .row($(this).parents('tr'))
          .remove()
          .draw();
      } else {
        rbtn.attr('disabled', false);
        dBtn.attr('disabled', false);
        alert('There was a problem please try again')
      }
    });
  });
  $('button[name=deleted_c_delete]').click(function () {
    var dBtn = $(this), rbtn = $('button[name=deleted_c_restore]');
    rbtn.attr('disabled', true);
    dBtn.attr('disabled', true);
    var data = {
      id: $(this).val(),
      _token: $('input[name="_token"]').val()
    }
    $.post(SOURCEROOT + PUBLICDIRECTORY + 'customer/deleteDeletedCustomer', data, function (result) {
      if (result) {
        deleted_customers_list
          .row($(this).parents('tr'))
          .remove()
          .draw();
      } else {
        rbtn.attr('disabled', false);
        dBtn.attr('disabled', false);
        alert('There was a problem please try again')
      }
    });
  });
}

$(document).ready(function() {  
  setInterval(function() {
    setUpDatatable();
  }, 500);
});

function setUpDatatable(){
  if($('.dataTables_info').length) {
    if($('.dataTables_info').text().indexOf('Showing 0 to 0') >= 0){
      $('.dataTables_length').hide();
      $('.dataTables_info').hide();
      $('.dataTables_paginate').hide();      
    } else {
      $('.dataTables_length').show();
      $('.dataTables_info').show();
      $('.dataTables_paginate').show();
    }
  }
  if($('.dataTables_paginate').length) {
    if($('.previous').first().hasClass('disabled') && $('.next').first().hasClass('disabled')) {
      $('.dataTables_paginate').hide();  
    }
  }
}