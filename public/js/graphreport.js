/* global eval, parseFloat */

var labels = [];
var payments = [];
var invoiced = [];
var respond;
var SOURCEROOT = '';

if (document.location.hostname === 'localhost') {
    SOURCEROOT = "http://localhost/laravelCRM";
} else {
    SOURCEROOT = "http://crm.adultpress.com.au";
}

$.get(SOURCEROOT + "/public/reports/monthlypayments", function (data) {
    length = data.length;

    for (i = 0; i < length; i++) {
        labels[i] = data[i]['month'];
        payments[i] = data[i]['amount'];
    }

});

//console.log(payments);

$.get(SOURCEROOT + "/public/reports/monthlyinvoiced", function (data) {
    length = data.length;
    for (i = 0; i < length; i++) {
        invoiced[i] = data[i]['amount'];
    }

});


//console.log(labels);
var data = {
    labels: labels,
    datasets: [{
            label: 'Paid',
            data: payments,
            lineTension: 0.3,
            backgroundColor: "rgba(75,192,75,0.1)",
            borderColor: "rgba(75,192,75,0.8)"
        },
        {
            label: 'Invoiced',
            data: invoiced,
            lineTension: 0.3,
            backgroundColor: "rgba(192,75,75,0.1)",
            borderColor: "rgba(192,75,75,0.8)"

        }],
    borderWidth: 1,
    lineTension: 0
};

var context = $('#graphReport');

var graphReport = new Chart(context, {
    type: 'line',
    data: data
});


