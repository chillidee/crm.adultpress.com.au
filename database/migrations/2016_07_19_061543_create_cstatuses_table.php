<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCstatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cstatuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('statusid');
            $table->dateTime('datetime');
            $table->dateTime('dueDatetime');
            $table->integer('userid');
            $table->integer('customerid');
            $table->tinyInteger('latest');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cstatuses');
    }
}
