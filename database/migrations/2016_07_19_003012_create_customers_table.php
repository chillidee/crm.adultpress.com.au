<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bname')->required();
            $table->string('bphone');
            $table->string('bemail');
            $table->string('abn');
            $table->string('website');
            $table->string('address');
            $table->string('suburb');
            $table->string('state');
            $table->string('postcode');
            $table->string('firstname');
            $table->string('lastname');
            $table->integer('position');                    // positions table id
            $table->string('phone');
            $table->string('email');
            $table->string('additional_contacts');
            $table->integer('account_manager');             // user table id
            $table->integer('btype')->unsigned();           // types table id
            $table->integer('mpm_user_id');         
            $table->integer('mpm_type_id');         
            $table->integer('brothel_id');     
            $table->string('extend');                       //facebook, twitter ...
            $table->timestamps();           
        });
    }

}
