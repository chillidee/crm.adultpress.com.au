<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddabnsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('addabns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customerid');
            $table->string('unit', 20);
            $table->string('number', 20);
            $table->string('street', 50);
            $table->string('sttype', 20);
            $table->string('suburb', 50);
            $table->string('state', 10);
            $table->string('postcode', 10);

            $table->string('invname', 100);
            $table->string('abn', 20);

            $table->string('status', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('addabns');
    }

}
