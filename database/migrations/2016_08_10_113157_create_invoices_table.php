<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('addabnid');
            $table->integer('customerid');
            $table->integer('userid');
            $table->integer('myob');
            $table->date('startdate');
            $table->date('expirydate');
            $table->date('duedate');
            $table->string('frequency',20);
            $table->string('description', 300);
            $table->string('payment_method', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('invoices');
    }

}
