<?PHP

use App\Customer;
?>
@extends('layouts.app')

@section('content')
<script>
    $(document).ready(function() {
        $('select').on('change', function (e) {            
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;    
            if(valueSelected != '-1' || valueSelected != '999999') {
                $('#clearFilters').show();
            } else {
                $('#clearFilters').hide();
            }
        });
        $('#clearFilters').on('click', function (e) {        
            $('#postcode').val("-1");        
            $('#radius').val("999999");
            $('#clearFilters').hide();
        });        
    });
    function hideFilters() {
        $('.postcode-search-wrapper').hide();
        $('#hide-filters').hide();
        $('#tableFilters').hide();
        $('#show-filters').show();
    }
    function showFilters() {
        $('.postcode-search-wrapper').show();
        $('#hide-filters').show();
        $('#tableFilters').show();
        $('#show-filters').hide();
    }
</script>
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Customers
                    <div style="display: inline-block; margin: 0" class="pull-right">                        
                        <a id="hide-filters" class="btn btn-sm btn-primary pull-right" style="display:none" onclick="hideFilters();">Hide Filters</a>
                        <a id="show-filters" class="btn btn-sm btn-primary pull-right" onclick="showFilters();">Show Filters</a>
                        <button id="cvsButton" class="btn btn-sm btn-primary pull-right" style="margin: 0 5px;"></button>
                        <a class="btn btn-sm btn-success pull-right" href="{{url('/')}}/customer/create">Add</a>                       
                    </div>
                </div>
                <div class="panel-body">
                    <div class="postcode-search-wrapper" style="display:none">
                        <p>Filters</p>
                        <label>Suburb</label>
                        <select id="postcode" name="postcode">
                            <option value="-1"></option>
                            <?php foreach ($postcodes as $postcode => $suburb): ?>
                                <option value="<?php echo $postcode ?>"><?php echo $suburb . ' - ' . $postcode ?></option>
                            <?php endforeach; ?>
                        </select>
                        <label class="radius-label">Radius</label>
                        <select id="radius" name="radius">
                            <option value="999999"></option>
                            <?php foreach ($radiuses as $radius): ?>
                                <option value="<?php echo $radius ?>"><?php echo $radius ?>km</option>
                            <?php endforeach; ?>
                        </select>
                        {{ csrf_field() }}
                        <div class="filter-button-wrapper text-center">
                            <button style="margin-bottom: 5px; display:none;" class="btn btn-sm btn-warning" id="clearFilters">Clear Filters</button>
                            <button style="margin-bottom: 5px;" class="btn btn-sm btn-primary" name="customerRadiusSearch">Search</button>
                        </div>
                    </div>
                    <table id="customers_list" class="table table-bordered">
                        <thead>
                            <tr id="tableFilters">
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <!--<th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>-->
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Bus Name</th>
                                <th>Bus Phone</th>
                                <th>Bus Email</th>
                                <th>Suburb</th>
                                <th>State</th>
                                <th>Acc Manager</th>
                                <th>Status</th>
                                <th>Account Expiry</th>
                                <!--<th>Email</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Phone</th>
                                <th>ABN</th>
                                <th>Website</th>
                                <th>Address</th>
                                <th>Post Code</th>-->
                            </tr>
                        </thead>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
