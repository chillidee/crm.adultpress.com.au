@extends('layouts.app')

@section('content')

<?PHP
$filteruser = isset($_GET['user']) ? $_GET['user'] : Auth::user()->id;
$filterFrom = isset($_GET['dueDateFrom']) ? $_GET['dueDateFrom'] : date(DateFormat);
$filterTo = isset($_GET['dueDateTo']) ? $_GET['dueDateTo'] : date(DateFormat);
$selectedUsers = isset($_GET['selectedUsers']) ? $_GET['selectedUsers'] : Auth::user()->name;
?>
<script>
    $(document).ready(function() {
        toggleClearFilter();
        $('#user, #dueDateFrom, #dueDateTo').on('change', function (e) {         
            toggleClearFilter();
        });
        $('#clearFilters').on('click', function (e) {     
            $('.bs-deselect-all').click();
            $('#dueDateFrom').val("");
            $('#dueDateTo').val("");
            $('#clearFilters').hide();
        });
        function toggleClearFilter() {
            if($('#selectedUsers').val() != '' || $('#dueDateFrom').val() != '' || $('#dueDateTo').val() != '') {
                $('#clearFilters').show();
            } else {
                $('#clearFilters').hide();
            }
        }
    });
</script>
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Advanced Search Tasks                    
                </div>
                <div class="panel-body">
                    <div class="panel-body search-wrapper tasks-search-wrapper">
                        <p>Filters</p>
                        <form method="GET" action="">
                            @if(Auth::user()->status == 'Active')
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="col-md-6">
                                        <label>User</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select onchange="setSelectedValues()" id="user" name="user" multiple data-actions-box="true" class="form-control selectpicker" {{(Auth::user()->group_id=='User')? 'disabled' : ''}}>
                                            @foreach($users as $user)
                                            <option>{{$user->name}}</option>
                                            @endforeach
                                        </select>                                
                                        <input id="selectedUsers" hidden name="selectedUsers" value="{{$selectedUsers}}">
                                    </div>
                                </div>   
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-3">                      
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="glyphicon glyphicon-calendar">From</i></div>
                                        <input type="text" id="dueDateFrom" class="form-control" name="dueDateFrom" autocomplete="off" value="{{$filterFrom}}">
                                    </div>
                                </div>
                                <div class="col-md-3">                      
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="glyphicon glyphicon-calendar" >To</i></div>
                                        <input type="text" id="dueDateTo" class="form-control" name="dueDateTo" autocomplete="off"  value="{{$filterTo}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="filter-button-wrapper text-center">
                                        <button style="display:none;" class="btn btn-sm btn-warning" id="clearFilters">Clear Filters</button>                                    
                                        <button class="btn btn-sm btn-primary" type="submit">Search</button>                                    
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    @if(Auth::user()->group_id == 'Admin')
                    @else
                    @if(Auth::user()->assignedto != NULL AND Auth::user()->id != Auth::user()->assignedto)
                    @else
                    @endif
                    @endif
                    <table id="tasks_list" class="table table-bordered">
                        <thead>
                            <tr id="tableFilters">
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>

                            </tr>     
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Acc Manager</th>
                                <th>Status</th>
                                <th>Due Date</th>
                            </tr>                           
                        </thead>

                        <tbody>
                            @if(isset($customers))
                            @foreach($customers as $customer)                            
                            <tr>
                                <td><a href="{{url('/customer/')}}/{{$customer->id}}">{{$customer->id}}</a></td>
                                <td>{{$customer->type}}</td>
                                <td><a href="{{url('/customer/')}}/{{$customer->id}}"> {{$customer->bname}}</a></td>
                                <td>{{$customer->manager}}</td>
                                <td>{{$customer->statusname or ''}}</td>
                                <td>{{date(DateTimeFormat,strtotime($customer->dueDatetime))}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection