<?PHP
use App\Invoice;
?>
@extends('layouts.app')

@section('content')
<?PHP
$selectedUsers = isset($_GET['selectedUsers']) ? $_GET['selectedUsers'] : '';
$filteruser = isset($_GET['user']) ? $_GET['user'] : Auth::user()->id;
$myob = isset($_GET['myob']) ? $_GET['myob'] : '';
$startDateFrom = isset($_GET['startDateFrom']) ? $_GET['startDateFrom'] : '';
$startDateTo = isset($_GET['startDateTo']) ? $_GET['startDateTo'] : '';
$dueDateFrom = isset($_GET['dueDateFrom']) ? $_GET['dueDateFrom'] : '';
$dueDateTo = isset($_GET['dueDateTo']) ? $_GET['dueDateTo'] : '';
$expiryDateFrom = isset($_GET['expiryDateFrom']) ? $_GET['expiryDateFrom'] : '';
$expiryDateTo = isset($_GET['expiryDateTo']) ? $_GET['expiryDateTo'] : '';
$createdDateFrom = isset($_GET['createdDateFrom']) ? $_GET['createdDateFrom'] : '';
$createdDateTo = isset($_GET['createdDateTo']) ? $_GET['createdDateTo'] : '';
$customername = isset($_GET['customername']) ? $_GET['customername'] : '';
?>
<script>
    $(document).ready(function() {
        toggleClearFilter();
        $('#myob, #user, #createdDateFrom, #createdDateTo').on('change', function (e) {         
            toggleClearFilter();
        });
        $('#clearFilters').on('click', function (e) {        
            $('#myob').val("");        
            $('.bs-deselect-all').click();
            $('#createdDateFrom').val("");
            $('#createdDateTo').val("");
            $('#clearFilters').hide();
        });
        function toggleClearFilter() {
            if($('#myob').val() != '' || $('#selectedUsers').val() != '' || $('#createdDateFrom').val() != '' || $('#createdDateTo').val() != '') {
                $('#clearFilters').show();
            } else {
                $('#clearFilters').hide();
            }
        }
    });
    function hideFilters() {
        $('.invoices-search-wrapper').hide();
        $('#hide-filters').hide();
        $('#show-filters').show();
    }
    function showFilters() {
        $('.invoices-search-wrapper').show();
        $('#hide-filters').show();      
        $('#show-filters').hide();
    }
</script>
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Invoices
                    <div style="display: inline-block; margin: 0" class="pull-right">
                        <a id="hide-filters" class="btn btn-sm btn-primary pull-right" style="display:none" onclick="hideFilters();">Hide Filters</a>
                        <a id="show-filters" class="btn btn-sm btn-primary pull-right" onclick="showFilters();">Show Filters</a>
                        <div id="csvButton" class="btn btn-sm btn-primary pull-right" style="margin: 0 5px;"></div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="invoices-search-wrapper" style="display:none">
                        <p>Filters</p>
                        <form method="GET" action="">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="col-md-6">
                                        <label>MYOB</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="myob" class="form-control" name="myob" autocomplete="off" value="{{$myob or ''}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="col-md-6">
                                        <label>Created By</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select onchange="setSelectedValues()" id="user" name="user" multiple data-actions-box="true" class="form-control selectpicker">
                                            @foreach($users as $user)
                                            <option>{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                        <input id="selectedUsers" hidden name="selectedUsers" value="{{$selectedUsers}}">
                                    </div>
                                </div>                                                               
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>Created date From</div>
                                        <input type="text" id="createdDateFrom" class="form-control datePicker" name="createdDateFrom" autocomplete="off" value="{{$createdDateFrom or ''}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>Created date To</div>
                                        <input type="text" id="createdDateTo" class="form-control datePicker" name="createdDateTo" autocomplete="off" value="{{$createdDateTo or ''}}">
                                    </div>
                                </div>                     
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="filter-button-wrapper text-center">
                                        <button style="display:none;" class="btn btn-sm btn-warning" id="clearFilters">Clear Filters</button>                                    
                                        <button class="btn btn-sm btn-primary" type="submit">Search</button>                                    
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <table class="invoiveListTable" class="table">
                        <thead>
                            <tr>
                                <th>MYOB</th>
                                <th>Created at</th>
                                <th>Customer ID</th>
                                <th>Working Name</th>
                                <th>Created By</th>
                                
                                <th class="amountColumn" style="text-align: right!important;">Total Inc GST</th>
                                <th class="amountColumn" style="text-align: right!important;">Total Paid</th>
                                <th class="amountColumn" style="text-align: right!important;">Payable</th>
                            </tr>                           
                        </thead>
                        <tbody>
                            <?PHP
                            $total = 0;
                            $paid = 0;
                            ?>
                            @foreach($invoices as $invoice)
                            <?PHP 
                            $total += str_replace(',','',number_format($invoice->invoicetotal*1.1,2));
                            $paid += str_replace(',','',number_format($invoice->totalpaid,2)); ?> 
                            <tr>
                                <td>{{$invoice->myob}}</td>
                                                                <td>{{date(DateFormat,strtotime($invoice->created_at))}}</td>
                                <td>{{$invoice->customerid}}</td>
                                <td><a target = "_blank" href="<?= url('/') . "/invoice/$invoice->id" ?>">{{$invoice->bname}}</a></td>
                                <td>{{$invoice->name}}</td>
                                
                                <td class="text-danger amountColumn">${{number_format($invoice->invoicetotal*1.1,2)}}</td>
                                <td class="text-success amountColumn">${{number_format($invoice->totalpaid,2)}}</td>
                                <td class="text-success amountColumn">
                                <?PHP
                                
                                $thisinvoice = Invoice::find($invoice->id);
                                echo $thisinvoice->payable();
                                
                                /*
                                $balance = number_format($invoice->invoicetotal* 1.1 - $invoice->totalpaid , 2);
                                if ($balance > 0) {
                                    echo "<td class='text-success amountColumn'>$$balance</td>";
                                } elseif ($balance < 0) {
                                    echo "<td class='text-danger amountColumn'>$$balance</td>";
                                } else {
                                    echo "<td class='text-success amountColumn'>0.00</td>";
                                }
                                */
                                
                                ?></td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>                               
                                <th></th>
                                <th>Sub Total</th>
                                <th id="total_page_invoiced" class="text-danger amountColumn"></th>
                                <th id="total_page_paid" class="text-success amountColumn"></th>
                                <th id="total_page_payable"  class="text-info amountColumn" ></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>                               
                                <th></th>
                                <th>Grand Total</th>
                                <th id="total_invoiced" class="text-danger amountColumn"></th>
                                <th id="total_paid" class="text-success amountColumn"></th>
                                <th id="total_payable" class="text-info amountColumn"></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection