<?PHP

use App\Customer;
?>
@extends('layouts.app')

@section('content')

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Home - <span id="manager_name">{{Auth::user()->name}}</span> - <?php echo (isset($expired) && !empty($expired)) ? 'Expired' : 'Expiring' ?> Tasks
                    <div class="expired-task-wrapper">
                        <form method="POST" action="{{url('/')}}/<?php echo (isset($expired) && !empty($expired)) ? 'expiringtasks' : 'viewExpiredTasks' ?>">
                            {{ csrf_field() }}
                            <button class="btn btn-sm btn-primary btn-primary" name="view-expired-tasks"><?php echo (isset($expired) && !empty($expired)) ? 'Expiring' : 'Expired' ?></button>
                        </form>
                    </div>
                </div>
                <div class="panel-body">
                    <table id="tasks_list" class="table table-bordered">
                        <thead>
                            <tr id="tableFilters">
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Acc Manager</th>
                                <th>Status</th>
                                <th>Due Date</th>
                                <th>Ad Expiry</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if(isset($customers))
                            @foreach($customers as $customer)
                            <tr>
                                <td><a href="{{url('/customer/')}}/{{$customer->id}}">{{$customer->id}}</a></td>
                                <td>{{$customer->business_type}}</td>
                                <td><a target = "_blank" href="{{url('/customer/')}}/{{$customer->id}}"> {{$customer->bname}}</a></td>
                                <td>{{$customer->manager_name}}</td>
                                <td>{{$customer->status_name or ''}}</td>
                                <td><span hidden>{{date("Ymd",strtotime($customer->dueDatetime))}}</span>{{date("d/m/Y",strtotime($customer->dueDatetime))}}</td>
                                <td>{{Customer::GetAdExpiry($customer->id)}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection