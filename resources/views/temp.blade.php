<?PHP

use App\Customer;

$query = "SELECT types.type, customers.id,customers.bname,cstatuses.dueDatetime,statuses.name FROM types,customers,cstatuses,statuses WHERE customers.btype = types.id AND customers.id = cstatuses.customerid AND cstatuses.latest=1 AND cstatuses.statusId = statuses.id and (statuses.name = 'Active Paid Customer' OR statuses.name='To Be Invoiced')";
$customers = DB::select($query);
?>
<html>
    <head>

        <style>
            .heading{
                padding: 5px;
                background-color: #9e9;
            }
            body{
                font-family: arial;
            }
            div{
                margin: 5px;
            }
            .expInvColumn,.border{
                border: 1px solid #CCC;
            }


            td{
                padding: 5px;
                font-weight: normal !important;

            }
            a{
                text-decoration: none !important;
                color: black;
            }

            td,th{
                color: black !important;
                font-size: 0.8em;
                font-weight: 900;
            }

            hr{
                border: #CCC;
            }
            .expInvColumn{
                min-width: 300px;
            }
        </style>
    </head>
    <body>
        <pre>
            <?php var_dump($customers); ?>
        </pre>
        <div id="editor"></div>
        <div id="paymentsReport" class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading page-panelheading"></div>
                        <h3>This is a reminder from CRM</h3>
                        <h4>List of active paid customers to be invoiced {{date(DateFormat)}}</h4>
                        <div class="panel-body">
                            <table id="paymentsReportTable" class="table table-bordered border" style="width:100%">
                                <thead>
                                    <tr class="border">
                                        <th class="heading" colspan="9">Active Paid Due Customers Report - {{date(DateFormat)}}</th>
                                    </tr>
                                    <tr class="border">
                                        <th style="text-align: center" class="border">Name</th>
                                        <th style="text-align: center" class="border">Type</th>
                                        <th style="text-align: center" class="border">Status</th>
                                        <td colspan="5" class="expInvColumn border">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td class="border" style="min-width: 73px;width: 25%;text-align: center"><strong>Orders</strong></td>
                                                    <td class="border" style="min-width: 73px;width: 25%;text-align: center"><strong>Due Date</strong></td>
                                                    <td class="border" style="min-width: 73px;width: 25%;text-align: center"><strong>Expiry Date</strong></td>
                                                    <td class="border" style="min-width: 73px;text-align: center; width: 25%;"><strong>Amount</strong></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?PHP $total = 0; ?>
                                    @foreach($customers as $customer)
                                    <?PHP
                                    $thisCustomer = Customer::find($customer->id);
                                    $arr = $thisCustomer->getexpiries();
                                    ?>
                                    @if(!empty($arr) || date('Y-m-d',strtotime($customer->dueDatetime))<=date('Y-m-d'))
                                    <tr class="border">
                                        <td style="text-align: center" class="border"><a target="_blank" href="http://crm.adultpress.com.au/public/customer/{{$customer->id}}">{{$customer->bname}}</a></td>
                                        <td style="text-align: center" class="border"><a target="_blank" href="http://crm.adultpress.com.au/public/customer/{{$customer->id}}">{{$customer->type}}</a></td>
                                        <td style="text-align: center" class="border"><a target="_blank" href="http://crm.adultpress.com.au/public/customer/{{$customer->id}}">{{$customer->name}}</a></td>
                                        <td colspan="5" class="border">
                                            <table style="width: 100%;">
                                                @foreach ($thisCustomer->getexpiries() as $exporder)
                                                <tr>
                                                    <td class="border" style="min-width: 73px;width: 25%;text-align: center;"><a href="http://crm.adultpress.com.au/public/invoice/{{$exporder->invoiceid}}">{{$exporder->sitename}} - {{$exporder->page}} - {{$exporder->position}}</a></td>
                                                    <td class="border" style="min-width: 73px;width: 25%;text-align: center">{{date(DateFormat, strtotime($exporder->duedate))}}</td>
                                                    <td class="border" style="min-width: 73px;width: 25%;text-align: center">{{date(DateFormat, strtotime($exporder->expirydate))}}</td>
                                                    <td class="border" style="min-width: 73px;width: 25%; text-align: center !important;">${{number_format($exporder->amount*1.1,2)}}</td>
                                                </tr>
                                                <?PHP $total += $exporder->amount; ?>
                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                <tfoot>
                                    <tr class="border">
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <td colspan="5" class="expInvColumn border">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="width: 0%;text-align: center">&nbsp;</td>
                                                    <td style="width: 0%;text-align: center">&nbsp;</td>
                                                    <td style="width: 0%;text-align: center"></td>
                                                    <td style="text-align: right; width: 100%;">Total ${{number_format($total*1.1,2)}}</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tfoot>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>