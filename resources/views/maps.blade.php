@extends('layouts.app')
@section('content')
<style>
    html{
        overflow: hidden;
    }
    #customerListWrapper a{
        width: 100%;
        float: left;
        padding: 10px 0;
        margin: 5px 0;
        border: 1px solid #ccc;
        border-radius: 2px;
        text-align: center;
        color: black;
        overflow: hidden;
    }
    #customerListContainer{
        overflow: auto;
        height: 100%;
    }
</style>
<script>
    $(document).ready(function() {
        $('select').on('change', function (e) {            
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;    
            if(valueSelected != '-1' || valueSelected != '999999') {
                $('#clearFilters').show();
            } else {
                $('#clearFilters').hide();
            }
        });
        $('#clearFilters').on('click', function (e) {        
            $('#postcode').val("-1");        
            $('#radius').val("999999");
            $('#clearFilters').hide();
        });
    });
</script>
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Find Customers</div>
                <div class="panel-body rounded_wrapper">
                    <div class="col-md-10">
                        <p>Filters</p>
                        <form class="form-inline" method="POST" action="{{url('/')}}/tools/maps">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Suburb</label>                                
                                <select id="postcode" name="postcode">
                                    <option value="-1"></option>
                                    <?php foreach ($postcodes as $postcode => $suburb): ?>
                                        <option value="<?php echo $postcode ?>"><?php echo $suburb; ?> - <?php echo $postcode ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group" style="margin-left: 15px;">
                                <label>Radius</label>
                                <select id="radius" name="radius">
                                    <option value="999999"></option>
                                    <option <?php echo (isset($radius) && $radius == '5') ? 'selected' : '' ?> value="5">5km</option>
                                    <option <?php echo (isset($radius) && $radius == '10') ? 'selected' : '' ?> value="10">10km</option>
                                    <option <?php echo (isset($radius) && $radius == '25') ? 'selected' : '' ?> value="25">25km</option>
                                    <option <?php echo (isset($radius) && $radius == '50') ? 'selected' : '' ?> value="50">50km</option>
                                </select>
                            </div>
                            <div class="filter-button-wrapper text-center">
                                <button style="margin-left: 15px;margin-bottom: 5px;" class="btn btn-primary btn-sm">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel-body rounded_wrapper">
                    <div class="col-md-10">
                        <div id="map" style="height: 50%;"></div>
                    </div>
                    <div class="col-md-2">
                        <div id="customerListWrapper">
                            <div id="customerListContainer">
                                <?php foreach ($customers as $customer): ?>
                                    <a target="_blank" id="{{$customer->id}}" href="{{url('/')}}/customer/{{$customer->id}}">
                                        {{$customer->bname}}
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                @if(isset($customers)&&!empty($customers)&&isset($center)&&!empty($center))
                <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
                </script>
                <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgSHLxC4xeJWrLe0Svg09EfWvIEtp1OfI&callback=initMap">
                </script>
                <script>
                    var map, radius = <?php echo $radius ?>;
                    function initMap() {
                        map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 12,
                            center: {lat: <?php echo $center->lat ?>, lng: <?php echo $center->lon ?>},
                            scrollwheel: false
                        });
                        var centermarker = new google.maps.Marker({
                            map: map,
                            position: new google.maps.LatLng({lat: <?php echo $center->lat ?>, lng: <?php echo $center->lon ?>}),
                            label: <?php echo $postcode ?>
                        });

                        var markers = locations.map(function (location, i) {
                            return new google.maps.Marker({
                                position: location,
                                id: labels[i]
                            });
                        });
                        // Add a marker clusterer to manage the markers.
                        new MarkerClusterer(map, markers,
                                {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

                        var circle = new google.maps.Circle({
                            map: map,
                            radius: radius * 1050,
                            fillColor: '#AA0000',
                            strokeColor: 'white',
                            strokeWeight: .5,
                            fillOpacity: .2
                        });

                        circle.bindTo('center', centermarker, 'position');

                        markers.forEach(function (marker) {
                            google.maps.event.addListener(marker, 'click', function () {
                                var customer_id = marker.get("id");
                                $('#customerListContainer a').each(function () {
                                    if ($(this).attr('id') === customer_id) {
                                        $(this).addClass('btn-danger');
                                        $('#customerListContainer').scrollTop(this.offsetTop);
                                    } else {
                                        $(this).removeClass('btn-danger');
                                    }
                                });
                            });
                        });
                    }

                    var locations = [];
                    var labels = [];
        <?php foreach ($longsAndLats as $postcode => $longsAndLat): ?>
                        var label = <?php echo $postcode ?>;
                        labels.push(label.toString());
                        locations.push({lat: <?php echo $longsAndLat->lat ?>, lng: <?php echo $longsAndLat->lng ?>});
        <?php endforeach; ?>
                </script>
                <script>
                    jQuery(document).ready(function ($) {
                        var mapHeight = $(window).innerHeight() - 200;
                        $('#map').css('height', mapHeight + 'px');
                        $('#customerListWrapper').css('height', mapHeight + 'px');
                    });
                </script>
                @endif
            </div>
        </div>
    </div>
    @endsection