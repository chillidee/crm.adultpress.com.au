<!DOCTYPE html>

<html lang="en">
    <head>
        <style>
            body,html,table{
                font-size: 20px;
                font-family: 'arial';
            }
            table,tr,td,th{
                padding: 10px;
                border: 2px #888 solid;
            }
            div{
                width: 100%;
            }
            a{
                text-decoration: none;
                color: brown;
            }
            .paymentAmount{
                color: #0C0;
                font-weight: bold;
                font-size: 1.2em;
            }
            .minusAmount{
                color: #C00;
                font-weight: bold;
                font-size: 1.2em;
            }
        </style> 
    </head>    
    <body>     
        <div>    
            <h3>This is a reminder from CRM system</h3>
            <h4 <?PHP
            if (floatval($payment->amount) > 0) {
                echo 'class="paymentAmount"';
            } else {
                echo 'class="minusAmount"';
            }
            ?>><?PHP
                    if (floatval($payment->amount) > 0) {
                        echo 'Payment has been made for the invoice';
                    } else {
                        echo 'Credit Note has been made for the invoice - ';
                    }
                    ?>{{date(DateTimeFormat)}}</h4>
            <h5>You can click on customer name to open relevant invoice on CRM system</h5>
            <table>
                <tr>
                    <th>Customer ID: {{$customer->id}}</th>
                    <th><a href="{{url('/')}}/invoice/{{$invoice->id}}">{{$customer->bname}}</a></th>

                    <td>MYOB</td>
                    <td><a href="{{url('/')}}/invoice/{{$invoice->id}}">{{$invoice->myob}}</a></td>
                </tr>
                <tr>
                    <td>Payment</td>
                    <td <?PHP
                    if (floatval($payment->amount) > 0) {
                        echo 'class="paymentAmount"';
                    } else {
                        echo 'class="minusAmount"';
                    }
                    ?>>$ {{$payment->amount}}</td>
                    <td></td>
                    <td></td>
                </tr>   

                @foreach($orders as $order)
                <tr>
                    <td><a href="{{url('/')}}/invoice/{{$order->invoiceid}}">{{$order->sitename}}-{{$order->page}}</a></td>
                    <td><a href="{{url('/')}}/invoice/{{$order->invoiceid}}">{{date(DateFormat,strtotime($order->startdate))}}</a></td>
                    <td><a href="{{url('/')}}/invoice/{{$order->invoiceid}}">{{$order->frequency}}</a></td>
                    <td><a href="{{url('/')}}/invoice/{{$order->invoiceid}}">{{number_format($order->amount*1.1,2)}}</a></td>
                </tr>
                @endforeach
            </table>
        </div>
       
        
    </body>
</html>

