@extends('layouts.app')
@section('content')

@if(isset($customers))

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Deleted Customers
                </div>
                <div class="panel-body">
                    <table id="deleted_customers_table" class="table table-bordered">
                        <thead>
                            <tr id="tableFilters">
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                                <th class="filterhead"></th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Suburb</th>
                                <th>State</th>
                                <th>Acc Manager</th>
                                <th>Deleted By</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $customer)
                            <tr>
                                <td>{{$customer->id}}</td>
                                <td>{{$customer->type}}</td>
                                <td>{{$customer->name}}</td>
                                <td>{{$customer->phone}}</td>
                                <td>{{$customer->email}}</td>
                                <td>{{$customer->suburb}}</td>
                                <td>{{$customer->state}}</td>
                                <td>{{$customer->manager}}</td>
                                <td>{{$customer->deletedBy}}</td>
                                <td>{{$customer->delete_date}}</td>
                                <td>
                                    {{ csrf_field() }}
                                    <button name="deleted_c_restore" value="{{$customer->deleted_id}}" class="btn btn-sm btn-success btn-deleted-customers" type="button"><i class="fa fa-btn fa-refresh"></i></button>
                                    <button name="deleted_c_delete" value="{{$customer->deleted_id}}" class="btn btn-sm btn-danger btn-deleted-customers" type="button"><i class="fa fa-btn fa-trash"></i></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endif
@endsection
