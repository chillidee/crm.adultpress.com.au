<div class="overlay"></div>
<button type="button" class="close-popup btn btn-danger btn-lg"><i class="fa fa-btn fa-close"></i></button>
<table class="table table-bordered" id="myob-accounts-table" style="margin-top:20px;margin-bottom:20px">
    <thead>
        <tr>
            <td>Name</td>
            <td>DisplayID</td>
            <td>Classification</td>
            <td>Type</td>
            <td>Parent Account</td>
        </tr>
    </thead>
    <tbody>
        <?php
        if ((isset($accounts['Items']) && !empty($accounts['Items'])) && empty($accounts['Errors'])):
            foreach ($accounts['Items'] as $accounts):
                $Name = (isset($accounts['Name']) && !empty($accounts['Name'])) ? $accounts['Name'] : '';
                $Classification = (isset($accounts['Classification']) && !empty($accounts['Classification'])) ? $accounts['Classification'] : '';
                $UID = (isset($accounts['UID']) && !empty($accounts['UID'])) ? $accounts['UID'] : '';
                $DisplayID = (isset($accounts['DisplayID']) && !empty($accounts['DisplayID'])) ? $accounts['DisplayID'] : '';
                $Type = (isset($customer['Type']) && !empty($customer['Type'])) ? $customer['Type'] : '';
                $ParentAccount = (isset($customer['ParentAccount']["Name"]) && !empty($customer['ParentAccount']["Name"])) ? $customer['ParentAccount']["Name"] : '';
                ?>
                <tr>
            <input type="hidden" name="uid" value="<?php echo $UID ?>"/>
            <input type="hidden" name="display_id" value="<?php echo $DisplayID ?>"/>
            <td><?php echo $Name ?></td>
            <td><?php echo $DisplayID ?></td>
            <td><?php echo $Classification ?></td>
            <td><?php echo $Type ?></td>
            <td><?php echo $ParentAccount ?></td>
        </tr>
        <?php
    endforeach;
endif;
?>
</tbody>
</table>
<link rel="stylesheet" type="text/css"
      href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>
<script type="text/javascript"
src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
