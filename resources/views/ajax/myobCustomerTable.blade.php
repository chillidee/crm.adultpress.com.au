<div class="overlay"></div>
<button type="button" class="close-popup btn btn-danger btn-lg"><i class="fa fa-btn fa-close"></i></button>
<table class="table table-bordered" id="myob-customers-table" style="margin-top:20px;margin-bottom:20px">
    <thead>
        <tr>
            <td>Company Name</td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>State</td>
            <td>PostCode</td>
            <td>Email</td>
        </tr>
    </thead>
    <tbody>
        <?php
        if ((isset($customers['Items']) && !empty($customers['Items'])) && empty($customers['Errors'])):
            foreach ($customers['Items'] as $customer):
                if ($customer['IsActive']):
                    ?>
                    <tr>
                        <td>
                            <?php echo (isset($customer['CompanyName']) && !empty($customer['CompanyName'])) ? $customer['CompanyName'] : '' ?>
                            <input type="hidden" name="uid" value="<?php echo (isset($customer['UID']) && !empty($customer['UID'])) ? $customer['UID'] : '' ?>"/>
                            <input type="hidden" name="display_id" value="<?php echo (isset($customer['DisplayID']) && !empty($customer['DisplayID'])) ? $customer['DisplayID'] : '' ?>"/>
                        </td>
                        <td><?php echo (isset($customer['FirstName']) && !empty($customer['FirstName'])) ? $customer['FirstName'] : '' ?></td>
                        <td><?php echo (isset($customer['LastName']) && !empty($customer['LastName'])) ? $customer['LastName'] : '' ?></td>
                        <td><?php echo (isset($customer['Addresses'][0]["State"]) && !empty($customer['Addresses'][0]["State"])) ? $customer['Addresses'][0]["State"] : '' ?></td>
                        <td><?php echo (isset($customer['Addresses'][0]["PostCode"]) && !empty($customer['Addresses'][0]["PostCode"])) ? $customer['Addresses'][0]["PostCode"] : '' ?></td>
                        <td><?php echo (isset($customer['Addresses'][0]["Email"]) && !empty($customer['Addresses'][0]["Email"])) ? $customer['Addresses'][0]["Email"] : '' ?></td>
                    </tr>
                    <?php
                endif;
            endforeach;
        endif;
        ?>        
    </tbody>
</table>
<link rel="stylesheet" type="text/css"
      href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>
<script type="text/javascript"
src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
