<div class="overlay"></div>
<button type="button" class="close-popup btn btn-danger btn-lg"><i class="fa fa-btn fa-close"></i></button>
<table class="table table-bordered" id="myob-taxes-table" style="margin-top:20px;margin-bottom:20px">
    <thead>
        <tr>
            <td>Display ID</td>
            <td>Code</td>
            <td>Description</td>
            <td>Type</td>
            <td>Rate</td>
        </tr>
    </thead>
    <tbody>
        <?php
        if ((isset($taxes) && !empty($taxes)) && empty($taxes['Errors'])):
            foreach ($taxes['Items'] as $tax):
                $Code = (isset($tax['Code']) && !empty($tax['Code'])) ? $tax['Code'] : '';
                $Description = (isset($tax['Description']) && !empty($tax['Description'])) ? $tax['Description'] : '';
                $UID = (isset($tax['UID']) && !empty($tax['UID'])) ? $tax['UID'] : '';
                $Type = (isset($tax['Type']) && !empty($tax['Type'])) ? $tax['Type'] : '';
                $Rate = (isset($tax['Rate']) && !empty($tax['Rate'])) ? $tax['Rate'] : '';
                $DisplayID = (isset($tax['DisplayID']) && !empty($tax['DisplayID'])) ? $tax['DisplayID'] : '';
                ?>
                <tr>
            <input type="hidden" name="uid" value="<?php echo $UID ?>"/>
            <input type="hidden" name="display_id" value="<?php echo $DisplayID ?>"/>
            <td><?php echo $Code ?></td>
            <td><?php echo $Description ?></td>
            <td><?php echo $Type ?></td>
            <td><?php echo $Rate ?></td>
            <td><?php echo $DisplayID ?></td>
        </tr>
        <?php
    endforeach;
endif;
?>
</tbody>
</table>
<link rel="stylesheet" type="text/css"
      href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>
<script type="text/javascript"
src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
