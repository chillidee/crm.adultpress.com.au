@extends('layouts.app')

@section('content')
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Customer <a href="{{url('/')}}/customer/{{$invoice->customerid}}">{{$invoice->bname}}</a> - Invoice MYOB: {{$invoice->myob}} Edit</div>
            </div>
        </div>
    </div>
</div>
<div class="fullcontainer">
    <form class="form-horizontal" id="invoiceForm" role="form" method="POST" action=<?= url('/') . "/invoice/$invoice->id" ?> >
        {{ csrf_field() }}
        {{method_field('PATCH')}}
        <input name="invoiceid" value="{{$invoice->id or ''}}" hidden>
        <input name="customerid" value="{{$invoice->customerid or ''}}" hidden>
        <input name="addabn" value="{{$invoice->addabnid or ''}}" hidden>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default rounded_wrapper">
                    <div class="panel-heading page-panelheading">Edit Invoice
                        <div style="display: inline-block; margin: -5px 5px" class="pull-right">
                            <button id="updateAddabnButton" class="btn btn-success btn-sm" type="submit">Save</button>
                            <a href="<?= url('/') . "/invoice/$invoice->id" ?>" id="updateAddabnButton" class="btn btn-danger btn-sm" >Cancel</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <dl class="dl-horizontal">
                                    <div class="borderedDiv">
                                        <div class="borderedTitle">Invoice details </div>
                                        {{-- ******* created at Name   ************ --}}
                                        <div id="createdAtDiv" class="form-group">
                                            <label for="created_at" class="col-md-4 control-label">Invoice Created at</label>
                                            <div class="col-md-6">
                                                <input id="invoiceCreatedAt" placeholder="Created at" class="form-control" name="created_at" value="{{date(DateFormat,strtotime($invoice->created_at))}}">
                                            </div>
                                        </div>
                                        {{-- ******* MYOB Name   ************ --}}
                                        <div id="myobDiv" class="form-group">
                                            <label for="myob" class="col-md-4 control-label">MYOB Invoice No</label>
                                            <div class="col-md-6">
                                                <input class="form-control" value="{{$invoice->myob or ""}}" name="myob">
                                            </div>
                                        </div>

                                        {{-- ********  Method date************** --}}
                                        <div id="paymentmethodDiv"  class="form-group">
                                            <label for="payment_method" class="col-md-4 control-label">Payment Method</label>
                                            <div class="col-md-6">
                                                <select class="form-control" name="payment_method">
                                                    <option disabled selected>Select...</option>
                                                    @foreach($paymentmethod as $paymethod)
                                                    <option value="{{$paymethod->method}}" <?= isset($invoice->payment_method) ? ($invoice->payment_method == $paymethod->method ) ? 'selected' : '' : '' ?> >{{$paymethod->method}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>{{-- ********  Description date************** --}}
                                        <div id="descriptionDiv"  class="form-group">
                                            <label for="description" class="col-md-4 control-label">Description</label>
                                            <div class="col-md-6">
                                                <textarea id="description" class="form-control" name="description"  type="text">{{$invoice->description or ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </dl>
                            </div>
                            <div class="col-sm-7">
                                <dl class="dl-horizontal">
                                    <div class="borderedDiv">
                                        <div class="borderedTitle">Add Order</div>
                                        {{-- ********  Websites ************** --}}
                                        <div class="ordersInputs">
                                            <div id="sitesDiv" class="form-group">
                                                <label for="sites" class="col-md-3 control-label">Advertising Platform</label>
                                                <div class="col-md-3">
                                                    <select id="websites" class="form-control" name="sites">
                                                        <option selected disabled value="">Please Select...</option>
                                                        @foreach($websites as $website)
                                                        <option value="{{$website->id}}">{{$website->website}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="error"></span>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></div>
                                                        <input id="orderamount" class="form-control" name="orderamount" type="text" placeholder="0.00">
                                                        <span class="error"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="sitesDiv" class="form-group">
                                                <label for="orderoption" class="col-md-3 control-label">Invoice Description</label>
                                                <div class="col-md-3">
                                                    <select id="orderoption" class="form-control" name="orderoption">
                                                        <option selected  value="">Please Select...</option>
                                                        @foreach($orderoptions as $orderoption)
                                                        <option value="{{$orderoption->orderoption}}">{{$orderoption->orderoption}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="error"></span>
                                                </div>
                                                <!--                                                <div class="col-md-3">
                                                                                                    <div class="input-group">
                                                                                                        <div class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></div>
                                                                                                        <input id="gstcalcalculator" class="form-control" name="gstcalcalculator" type="text" placeholder="GST Calculator" title="Enter total amount, the subtotal will automatically be calculated">
                                                                                                        <span class="input-group-addon gstcalcalculator-info"><i style="color: #337ab7;" class="fa fa-info"></i></span>
                                                                                                        <span class="error"></span>
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                            <div id="pagepositionDiv" class="form-group">
                                                <label for="page" class="col-md-3 control-label">Page</label>
                                                <div class="col-md-3">
                                                    <select id="orderpage" class="form-control" name="orderpage">
                                                        <option selected  value="">Please Select...</option>
                                                        @foreach($orderpages as $orderpage)
                                                        <option extstr="{{str_replace(' ', '_',$orderpage->page)}}" value="{{$orderpage->page}}">{{$orderpage->page}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="error"></span>
                                                </div>
                                                <label for="position" class="col-md-1 control-label">Position</label>
                                                <div class="col-md-2">
                                                    <input class="form-control" name="position" id="orderposition">
                                                    <span class="error"></span>
                                                </div>
                                                <div class="col-md-1 text-right">
                                                    <span id="addorderbutton" class="btn btn-primary glyphicon glyphicon-plus"></span>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>

                                        <!--  Order dates -->

                                        {{-- ******** order start date************** --}}
                                        <div id="orderstartdateDiv"  class="form-group">
                                            <label for="orderstartdate" class="col-md-3 control-label">Advertising Start Date</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                                    <input placeholder="Advertising Start Date" id="orderstartdate" class="form-control" name="orderstartdate" autocomplete="off" type="text">
                                                </div>
                                                <span class="error" id="orderstartdateerror"></span>

                                            </div>
                                        </div>
                                        {{-- ******** order frequency date************** --}}
                                        <div id="orderfrequencyDiv"  class="form-group">
                                            <label for="orderfrequency" class="col-md-3 control-label">Order Frequency</label>
                                            <div class="col-md-3">
                                                <select class="form-control" name="orderfrequency" id="orderfrequency">
                                                    <option selected value="">Please Select...</option>
                                                    <option value="Weekly">Weekly</option>
                                                    <option value="Fortnightly">Fortnightly</option>
                                                    <option value="Monthly">Monthly</option>
                                                    <option value="Quarterly">Quarterly</option>
                                                    <option value="Bi-annually">Bi-annually</option>
                                                    <option value="Annually">Annually</option>
                                                </select>
                                            </div>
                                            <span class="error" id="orderfrequencyerror"></span>

                                        </div>
                                        {{-- ******** order expiry date************** --}}
                                        <div id="orderexpirydateDiv"  class="form-group">
                                            <label for="orderexpirydate" class="col-md-3 control-label">Order Expiry Date</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                                    <input placeholder="Order Expiry Date" id="orderexpirydate" class="form-control" name="orderexpirydate" autocomplete="off" type="text">
                                                </div>
                                            </div>
                                            <span class="error" id="orderexpirydateerror"></span>

                                        </div>
                                        {{-- ******** order due date************** --}}
                                        <div id="orderduedateDiv"  class="form-group">
                                            <label for="orderduedate" class="col-md-3 control-label">Order Reminder</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                                    <input id="orderduedate" placeholder="Order Reminder Date" class="form-control" name="orderduedate" autocomplete="off" type="text">
                                                </div>
                                                <span class="error" id="orderduedateerror"></span>
                                            </div>
                                        </div>

                                        <div id="accountSelect"  class="form-group">
                                            <label for="orderduedate" class="col-md-3 control-label">MYOB Account</label>
                                            <div class="col-md-3">
                                                <select class="form-control" id="from-myob-account">
                                                    <option value="-1">Select Account</option>
                                                    <option value="mpm">MPM Service Account</option>
                                                    <option value="brothels">Brothels Service Account</option>
                                                </select>
                                                <span class="error" id="orderduedateerror"></span>
                                            </div>
                                        </div>

                                        <!-- End order dates -->
                                        <?PHP
                                        $subtotal = 0;
                                        $jsonorders = [];
                                        ?>
                                    </div>
                                </dl>
                            </div>
                            <div id="ordersTableContainer" class="col-md-10 col-md-offset-1">
                                <table id="ordersTable">
                                    <thead>
                                        <tr class="orderTableTitles">
                                            <th>Platform</th>
                                            <th>Description</th>
                                            <th>Page</th>
                                            <th>Position</th>
                                            <th>Start Date</th>
                                            <th>Frequency</th>
                                            <th>Expiry Date</th>
                                            <th>Reminder</th>
                                            <th></th>
                                            <th class="amountColumn">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orders as $order)
                                        <tr id="orderrow{{$order->siteid}}-{{str_replace(' ', '_',$order->page)}}">
                                            <td>{{$order->sitename}}</td>
                                            <td><?= $order->orderoption != 'null' ? $order->orderoption : '' ?></td>
                                            <td><?= $order->page != 'null' ? $order->page : '' ?></td>
                                            <td><?= $order->position != 'null' ? $order->position : '' ?></td>
                                            <td><?= date(DateFormat, strtotime($order->startdate)) ?></td>
                                            <td><?= $order->frequency != 'null' ? $order->frequency : '' ?></td>
                                            <td><?= date(DateFormat, strtotime($order->expirydate)) ?></td>
                                            <td><?= date(DateFormat, strtotime($order->duedate)) ?></td>
                                            <td class="amountColumn">$</td>
                                            <td class="amountColumn rowordervalue">{{$order->amount or ''}}</td>
                                            <td class="text-right"><span class='btn btn-primary btn-sm glyphicon glyphicon-edit' onclick="editorder(<?= "'" . $order->siteid . '-' . str_replace(' ', '_', $order->page) . "'" ?>)"></span><span class="btn btn-danger btn-sm glyphicon glyphicon-remove" onclick="removeorder(<?= "'" . $order->siteid . '-' . str_replace(' ', '_', $order->page) . "'" ?>)"></span></td>
                                            <td><input type='hidden' value="<?php echo (isset($order->myob_account) && !empty($order->myob_account)) ? $order->myob_account : '-1' ?>" name="myob_account"/><td>
                                        </tr>
                                        <?PHP
                                        $subtotal += $order->amount;
                                        $jsonorders[$order->siteid . '-' . str_replace(' ', '_', $order->page)] = $order->sitename . ',';
                                        $jsonorders[$order->siteid . '-' . str_replace(' ', '_', $order->page)] .= $order->orderoption . ',';
                                        $jsonorders[$order->siteid . '-' . str_replace(' ', '_', $order->page)] .= $order->amount . ',';
                                        $jsonorders[$order->siteid . '-' . str_replace(' ', '_', $order->page)] .= $order->page . ',';
                                        $jsonorders[$order->siteid . '-' . str_replace(' ', '_', $order->page)] .= $order->position . ',';
                                        $jsonorders[$order->siteid . '-' . str_replace(' ', '_', $order->page)] .= $order->startdate . ',';
                                        $jsonorders[$order->siteid . '-' . str_replace(' ', '_', $order->page)] .= $order->frequency . ',';
                                        $jsonorders[$order->siteid . '-' . str_replace(' ', '_', $order->page)] .= $order->expirydate . ',';
                                        $jsonorders[$order->siteid . '-' . str_replace(' ', '_', $order->page)] .= $order->duedate;
                                        ?>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr><td colspan="7" class="amountColumn">Sub Total</td><td></td><td class="text-right">$</td><td id="invoicesubtotal" class="amountColumn">{{number_format($subtotal,2)}}</td><td></td></tr>
                                        <tr><td colspan="7" class="amountColumn">GST</td><td></td><td class="text-right">$</td><td id="invoicegst" class="amountColumn">{{number_format($subtotal/10,2)}}</td><td></td></tr>
                                        <tr><td colspan="7" class="amountColumn">Total Inc GST</td><td></td><td class="text-right">$</td><td id="invoicetotal" class="amountColumn">{{number_format($subtotal*1.1,2)}}</td><td></td></tr>
                                    </tfoot>
                                </table>
                            </div>
                            <input name="orders" id="orders" value="{{json_encode($jsonorders)}}" hidden>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
