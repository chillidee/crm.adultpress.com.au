
@extends('layouts.app')

@section('content')

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Types</div>
                <div class="panel-body">

                    <table id="users_list" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <td>Types</td>
                                <td>Status</td>
                                <td>Number of Customers</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($types as $type)
                            <tr>
                                <td>
                                    <a href="{{url('/admin/types/')}}/{{$type->id}}"> {{$type->type}}</a></td>
                                <td><span class="glyphicon glyphicon-stop" id="{{$type->status}}">
                                </td>
                                <td>{{$type->countit()}}</td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>


                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">
                    <?PHP echo (isset($thistype->id)) ? "Edit Business Type" : "Add New Business Type"; ?>
                    <div class="pull-right">
                    <?PHP
                    if (isset($thistype->id)) {
                        ?>                            
                            @if($thistype->countit()==0)
                            <div style="display: inline-block; margin: -5px 5px">
                                <form id="OptionsDeleteForm" method="POST" action="{{url('/admin/types/' . $thistype->id)}}">
                                    {{method_field('DELETE')}}
                                    {{ csrf_field() }}
                                    <input hidden name="typeid" value="{{$thistype->id}}">
                                    <span class="btn btn-sm btn-danger DeleteButtonOption" type="button" id="DeleteButton" value="Delete">Delete</span>
                                </form>
                            </div>
                            @endif
                            <?PHP
                        }
                        ?>
                    </div>
                </div>                
                <div class="panel-body rounded_wrapper">
                    <form id="typeForm" class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thistype->id)) ? url('/admin/types/edit/' . $thistype->id) : url('/admin/types/store');
                    ?>>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label">Type</label>

                            <div class="col-md-6">
                                <input id="position" type="text" maxlength="30"  class="form-control" name="type" value="{{{ $thistype->type or '' }}}">

                                @if ($errors->has('type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>
                            <div class="col-md-6">
                                <input id="status" type="radio" name="status" value="Active" <?= isset($thistype) ? ($thistype->status === "Active" ? "checked" : "" ) : "checked"; ?>> Active<br>
                                <input id="status" type="radio"  name="status" value="Deactive" <?= (isset($thistype) && $thistype->status === "Deactive" ) ? "checked" : ""; ?>> Deactive<br>
                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">                        
                            <div class="col-md-offset-4 col-md-6">
                                <a type="delete" class="btn btn-sm btn-danger" href="{{url('/admin/types')}}">Cancel</a>
                                <button type="submit" class="btn btn-sm btn-success">                                    
                                    <?PHP
                                    echo (isset($thistype->id)) ? "Update" : "Add";
                                    ?>
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- @if(isset($thistype) && $thistype->countit()!=0)
                    <span class="error">Can't delete this business type. There are {{$thistype->countit()}} customers of this type</span>
                    @endif -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
