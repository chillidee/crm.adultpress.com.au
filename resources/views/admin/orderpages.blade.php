
@extends('layouts.app')

@section('content')

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Pages</div>
                <div class="panel-body">
                    <table id="users_list" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <td>Order Pages</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orderpages as $orderpage)
                            <tr>
                                <td>
                                    </span><a href="{{url('/admin/orderpages/')}}/{{$orderpage->id}}"> {{$orderpage->page}}</a></td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading"><?PHP
                    echo (isset($thisorderpage->id)) ? "Edit Order Page" : "Add New Order Page";
                    ?>
                    <div class="pull-right">
                        <?PHP
                        if (isset($thisorderpage->id)) {
                            ?>                            
                            <div style="display: inline-block; margin: -5px 5px">
                                <form id="OptionsDeleteForm" method="POST" action="{{url('/admin/orderpages/' . $thisorderpage->id)}}">
                                    {{method_field('DELETE')}}
                                    {{ csrf_field() }}
                                    <input hidden name="orderpageid" value="{{$thisorderpage->id}}">
                                    <span class="btn btn-sm btn-danger DeleteButtonOption" type="button" id="DeleteButton">Delete</span>
                                </form>
                            </div>
                            <?PHP
                        }
                        ?>
                    </div>
                </div>
                <div class="panel-body rounded_wrapper">
                    <form id="orderpageForm" class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thisorderpage->id)) ? url('/admin/orderpages/' . $thisorderpage->id) : url('/admin/orderpages');
                    ?>>
                        {{ csrf_field() }}
                        <?PHP if (isset($thisorderpage->id)) { ?>
                            {{method_field('PATCH')}}
                            <?PHP
                        }
                        ?>
                        <div class="form-group{{ $errors->has('orderpage') ? ' has-error' : '' }}">
                            <label for="orderpage" class="col-md-4 control-label">Order Page</label>
                            <div class="col-md-6">
                                <input id="orderpage" type="text" maxlength="30" class="form-control" name="orderpage" value="{{{ $thisorderpage->page or '' }}}">
                                @if ($errors->has('orderpage'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('orderpage') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a type="cancel" class="btn btn-sm btn-danger" href="{{url('/admin/orderpages')}}">Cancel</a>
                                <button type="submit" class="btn btn-sm btn-success">                                    
                                    <?PHP
                                    echo (isset($thisorderpage->id)) ? "Update" : "Add";
                                    ?>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection