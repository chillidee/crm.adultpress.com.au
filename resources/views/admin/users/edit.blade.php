@extends('layouts.app')
@section('content')

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Edit Account Manager

                    <div class="pull-right">
                        <?PHP
                        if (isset($user->id)) {
                            ?>                            
                            @if($user->assignedto != NULL && $user->assignedto != 0 && $user->assignedto != $user->id && $user->id != 253)
                            <div style="display: inline-block; margin: -5px 5px">
                                <form id="OptionsDeleteForm" method="POST" action="{{url('/admin/users/' . $user->id)}}">
                                    {{method_field('DELETE')}}
                                    {{ csrf_field() }}
                                    <input hidden name="userid" value="{{$user->id}}">
                                    <span class="btn btn-sm btn-danger DeleteButtonOption">Delete</span>
                                </form>
                            </div>
                            @endif
                            <?PHP
                        }
                        ?>
                    </div>
                </div>
                <div class="panel-body rounded_wrapper">
                    <form class="form-horizontal" id="userEditForm" role="form" method="POST" action="{{url('/admin/users/'.$user->id.'/update')}}">
                        <div class="col-md-6">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Account Manager</label>

                                <div class="col-md-6">
                                    <input maxlength="30" id="name" type="text" class="form-control" name="name" value="{{ $user->name }}">

                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input maxlength="50"  id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4"><strong>Change password to:</strong></label>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input maxlength="30"  id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input maxlength="30"  id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">Status</label>
                                <div class="col-md-6">
                                    <input id="status" type="radio" name="status" value="Active" <?= ($user->status == "Active" ) ? "checked" : ""; ?>> Active<br>
                                    <input id="status" type="radio" name="status" value="Deactive" <?= ($user->status == "Deactive" ) ? "checked" : ""; ?>> Deactive<br>
                                    @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
                                <label for="group_id" class="col-md-4 control-label">Type</label>
                                <div class="col-md-6">
                                    <input id="status" type="radio" name="group_id" value="Admin" <?= ($user->group_id == "Admin" ) ? "checked" : ""; ?>> Admin<br>
                                    <input id="status" type="radio" name="group_id" value="User" <?= ($user->group_id == "User" ) ? "checked" : ""; ?>> User<br>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('assignedto') ? ' has-error' : '' }}">
                                <label for="assignedto" class="col-md-4 control-label">Tasks assigned to</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="assignedto">
                                        <option value="" disabled selected>select...</option>
                                        @foreach($users as $assignto)
                                        @if($assignto->status!="Deactive")
                                        <option value="{{$assignto->id}}" <?= ($user->assignedto == $assignto->id) ? 'selected' : ''; ?>>{{$assignto->name}}<?= ($user->id == $assignto->id) ? ' - SELF' : ''; ?></option>
                                        @endif
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <a type="cancel" class="btn btn-sm btn-danger" href="{{url('/admin/users')}}">Cancel</a>
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- @if($user->assignedto == NULL || $user->assignedto == 0 || $user->assignedto == $user->id)
                    <span class="error">Can't delete this Account Manager. Please assign tasks to another Account Manager if you wish to delete this.</span>
                    @endif
                    @if($user->id == 253)
                    <span class="error">Can't delete this Account Manager. This is required for the system.</span>
                    @endif -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
