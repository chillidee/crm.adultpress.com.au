
@extends('layouts.app')

@section('content')

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Statuses</div>
                <div class="panel-body">

                    <table id="users_list" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <td>Order</td>
                                <td>Status</td>
                                <td>Days to expire</td>
                                <td>Active</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($statuses as $status)
                            <tr>
                                <td>
                                    {{$status->orderid}}
                                </td>
                                <td>
                                    <a href="{{url('/admin/statuses/')}}/{{$status->id}}"> {{$status->name}} ({{$status->countit()}})</a>
                                </td>
                                <td>
                                    {{$status->expdays}}
                                </td>
                                <td>
                                    <span class="glyphicon glyphicon-stop" id="{{$status->status}}"></span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading"><?PHP
                    echo (isset($thisstatus->id)) ? "Edit Status" : "Add New Status";
                    ?><div class="pull-right">
                    <?PHP
                    if (isset($thisstatus->id)) {
                        ?>                           
                            @if($thisstatus->countit() ==0)
                            <div style="display: inline-block; margin: -5px 5px;">
                                <form id="OptionsDeleteForm" method="POST" action="{{url('/admin/statuses/' . $thisstatus->id)}}">
                                    {{method_field('DELETE')}}
                                    {{ csrf_field() }}
                                    <input hidden name="statusid" value="{{$thisstatus->id}}">
                                    <span class="btn btn-sm btn-danger DeleteButtonOption" type="button" id="DeleteButton">Delete</span>
                                </form>
                            </div>
                            @endif
                            <?PHP
                        }
                        ?>
                    </div>
                </div>                
                <div class="panel-body rounded_wrapper">
                    <form id="statusForm" class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thisstatus->id)) ? url('/admin/statuses/edit/' . $thisstatus->id) : url('/admin/statuses/store');
                    ?>>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('orderid') ? ' has-error' : '' }}">
                            <label for="orderid" class="col-md-4 control-label">Order id</label>
                            <div class="col-md-6">
                                <input id="orderid" type="text" maxlength="10"  class="form-control" name="orderid" value="{{{ $thisstatus->orderid or '' }}}">
                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('orderid') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Status</label>
                            <div class="col-md-6">
                                <input id="position" type="text" maxlength="30"  class="form-control" name="name" value="{{ $thisstatus->name or '' }}">

                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('expdays') ? ' has-error' : '' }}">
                            <label for="expdays" class="col-md-4 control-label">Days to expire</label>
                            <div class="col-md-6">
                                <input id="position" type="text" maxlength="5"  class="form-control" name="expdays" value="{{$thisstatus->expdays or '' }}">

                                @if ($errors->has('expdays'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('expdays') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Active</label>
                            <div class="col-md-6">
                                <input id="status" type="radio" name="status" value="Active" <?= isset($thisstatus) ? ($thisstatus->status === "Active" ? "checked" : "" ) : "checked"; ?>> Active<br>
                                <input id="status" type="radio" name="status" value="Deactive" <?= (isset($thisstatus) && $thisstatus->status === "Deactive" ) ? "checked" : ""; ?>> Deactive<br>


                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a type="Cancle" class="btn btn-sm btn-danger" href="{{url('/admin/statuses')}}">Cancel</a>
                                <button type="submit" class="btn btn-sm btn-success">                                    
                                    <?PHP
                                    echo (isset($thisstatus->id)) ? "Update" : "Add";
                                    ?>
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
