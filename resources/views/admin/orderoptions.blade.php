@extends('layouts.app')

@section('content')

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Invoice Descriptions</div>
                <div class="panel-body">
                    <table id="users_list" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <td>Invoice Descriptions</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orderoptions as $orderoption)
                            <tr>
                                <td>
                                    </span><a href="{{url('/admin/orderoptions/')}}/{{$orderoption->id}}"> {{$orderoption->orderoption}}</a></td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading"><?PHP
                    echo (isset($thisorderoption->id)) ? "Edit Invoice Description" : "Add New Invoice Description";
                    ?>
                    <div class="pull-right">
                        <?PHP
                        if (isset($thisorderoption->id)) {
                            ?>                           
                            <div style="display: inline-block; margin: -5px 5px">

                                <form id="OptionsDeleteForm" method="POST" action="{{url('/admin/orderoptions/' . $thisorderoption->id)}}">
                                    {{method_field('DELETE')}}
                                    {{ csrf_field() }}
                                    <input hidden name="orderoptionid" value="{{$thisorderoption->id}}">
                                    <span class="btn btn-sm btn-danger DeleteButtonOption" type="button" id="DeleteButton">Delete</span>
                                </form>
                            </div>
                            <?PHP
                        }
                        ?>
                    </div>
                </div>
                <div class="panel-body rounded_wrapper">
                    <form id="orderoptionForm" class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thisorderoption->id)) ? url('/admin/orderoptions/' . $thisorderoption->id) : url('/admin/orderoptions');
                    ?>>
                        {{ csrf_field() }}
                        <?PHP if (isset($thisorderoption->id)) { ?>
                            {{method_field('PATCH')}}
                            <?PHP
                        }
                        ?>
                        <div class="form-group{{ $errors->has('orderoption') ? ' has-error' : '' }}">
                            <label for="orderoption" class="col-md-4 control-label">Invoice Description</label>

                            <div class="col-md-6">
                                <input id="orderoption" type="text" maxlength="30" class="form-control" name="orderoption" value="{{{ $thisorderoption->orderoption or '' }}}">

                                @if ($errors->has('orderoption'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('orderoption') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                            <a type="cancel" class="btn btn-sm btn-danger" href="{{url('/admin/orderoptions')}}">Cancel</a>
                                <button type="submit" class="btn btn-sm btn-success">                                    
                                    <?PHP
                                    echo (isset($thisorderoption->id)) ? "Update" : "Add";
                                    ?>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection