@extends('layouts.app')

@section('content')
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Advertising Platforms</div>
                <div class="panel-body">
                    <table id="users_list" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <td>Websites</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($websites as $website)
                            <tr>
                                <td>
                                    </span><a href="{{url('/admin/websites/')}}/{{$website->id}}"> {{$website->website}}</a></td>
                                <td><span class="glyphicon glyphicon-stop" id="{{$website->status}}">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading"><?PHP
                    echo (isset($thiswebsite->id)) ? "Edit Advertising Platform" : "Add New Advertising Platform";
                    ?>
                    <div class="pull-right">
                        <?PHP
                        if (isset($thiswebsite->id)) {
                            ?>                      
                            @if(Auth::user()->name=='Amir Far')
                            <div style="display: inline-block; margin: -5px 5px">
                                <form id="OptionsDeleteForm" method="POST" action="{{url('/admin/websites/' . $thiswebsite->id)}}">
                                    {{method_field('DELETE')}}
                                    {{ csrf_field() }}
                                    <input hidden name="websiteid" value="{{$thiswebsite->id}}">
                                    <span class="btn btn-sm btn-danger DeleteButtonOption" type="button" id="DeleteButton" value="Delete">Delete</span>
                                </form>

                            </div>
                            @endif
                            <?PHP
                        }
                        ?>
                    </div>
                </div>
                <div class="panel-body rounded_wrapper">
                    <form id="websitesForm" class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thiswebsite->id)) ? url('/admin/websites/edit/' . $thiswebsite->id) : url('/admin/websites/store');
                    ?>>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="website" class="col-md-4 control-label">Advertising Platform</label>

                            <div class="col-md-6">
                                <input id="website" type="text" maxlength="30" class="form-control" name="website" value="{{{ $thiswebsite->website or '' }}}">

                                @if ($errors->has('website'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('website') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>
                            <div class="col-md-6">
                                <input id="status" type="radio" name="status" value="Active" <?= isset($thiswebsite) ? ($thiswebsite->status === "Active" ? "checked" : "" ) : "checked"; ?>> Active<br>
                                <input id="status" type="radio" name="status" value="Deactive" <?= (isset($thiswebsite) && $thiswebsite->status === "Deactive" ) ? "checked" : ""; ?>> Deactive<br>
                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <a type="cancel" class="btn btn-sm btn-danger" href="{{url('/admin/websites')}}">Cancel</a>
                                <button type="submit" class="btn btn-sm btn-success">                                    
                                    <?PHP
                                    echo (isset($thiswebsite->id)) ? "Update" : "Add";
                                    ?>
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection