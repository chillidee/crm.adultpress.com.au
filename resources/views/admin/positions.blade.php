
@extends('layouts.app')

@section('content')

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Positions</div>
                <div class="panel-body">
                    <table id="users_list" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <td>Positions</td>
                                <td>Status</td>
                                <td>Contacts on this position</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($positions as $position)
                            <tr>
                                <td>
                                    </span><a href="{{url('/admin/positions/')}}/{{$position->id}}"> {{$position->position}}</a></td>
                                <td><span class="glyphicon glyphicon-stop" id="{{$position->status}}">
                                </td>
                                <td>{{$position->countit()}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading"><?PHP
                    echo (isset($thisposition->id)) ? "Edit Position" : "Add New Position";
                    ?><div class="pull-right">
                    <?PHP
                    if (isset($thisposition->id)) {
                        ?>                            
                            @if($thisposition->countit()==0 && FALSE)
                            <div style="display: inline-block; margin: -5px 5px">

                                <form id="OptionsDeleteForm" method="POST" action="{{url('/admin/positions/' . $thisposition->id)}}">
                                    {{method_field('DELETE')}}
                                    {{ csrf_field() }}
                                    <input hidden name="positionid" value="{{$thisposition->id}}">
                                    <input class="btn btn-sm btn-danger sweetDeleteButtonOptions" type="button" id="DeleteButton" value="Delete">
                                </form>
                            </div>
                            @endif
                            <?PHP
                        }
                        ?>
                    </div>
                </div>
                <div class="panel-body rounded_wrapper">
                    <form id="positionForm" class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thisposition->id)) ? url('/admin/positions/edit/' . $thisposition->id) : url('/admin/positions/store');
                    ?>>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                            <label for="position" class="col-md-4 control-label">Position</label>

                            <div class="col-md-6">
                                <input maxlength="30"  id="position" type="text" class="form-control" name="position" value="{{{ $thisposition->position or '' }}}">
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>
                            <div class="col-md-6">
                                <input id="status" type="radio" name="status" value="Active" <?= isset($thisposition) ? ($thisposition->status === "Active" ? "checked" : "" ) : "checked"; ?>> Active<br>
                                <input id="status" type="radio" name="status" value="Deactive" <?= (isset($thisposition) && $thisposition->status === "Deactive" ) ? "checked" : ""; ?>> Deactive<br>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a class="btn btn-sm btn-danger" href="{{url('/admin/positions')}}">Cancel</a>
                                <button type="submit" class="btn btn-sm btn-success">                                    
                                    <?PHP
                                    echo (isset($thisposition->id)) ? "Update" : "Add";
                                    ?>
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- @if(isset($thisposition) && $thisposition->countit()!=0 && false)
                    <span class="error">Can't delete this position. There are {{$thisposition->countit()}} people on this position</span>
                    @endif -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
