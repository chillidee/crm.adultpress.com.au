@extends('layouts.app')

@section('content')

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Account Managers</div>
                <div class="panel-body">
                    <table id="users_list" class="table table-bordered">
                        <thead>
                            <tr>
                                <td>Account Manager</td>
                                <td>Email</td>
                                <td>Assigned to</td>
                                <td>Type</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>
                                    <span class="glyphicon glyphicon-stop" id="{{$user->status}}"></span>
                                    <a href="{{url('/admin/users/')}}/{{$user->id}}/edit"> {{$user->name}}</a></td>
                                <td>{{$user->email}} </td>
                                <td>@if($user->id != $user->assignedto && $user->assignedto != NULL )
                                    {{$user->assignedto($user->assignedto)}}
                                    @endif
                                </td>
                                <td>{{$user->group_id}}</td>
                                <td>{{$user->status}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Add New Account Manager</div>
                <div class="panel-body rounded_wrapper">
                    <form id="registrationForm" class="form-horizontal" role="form" method="POST" action="{{url('/admin/users/store')}}">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Account Manager</label>

                                <div class="col-md-6">
                                    <input  maxlength="30"  id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input maxlength="50"  id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label maxlength="20"  for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input  maxlength="30"  id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input  maxlength="30"   id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">Status</label>                            
                                <div class="col-md-6">
                                    <input id="status" type="radio" name="status" value="Active" checked> Active<br>
                                    <input id="status" type="radio" name="status" value="Deactive"> Deactive<br>
                                    <div id="statuserror" for="status" class="error"></div>
                                    @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
                                <label for="group_id" class="col-md-4 control-label">Type</label>                            
                                <div class="col-md-6">
                                    <input id="status" type="radio" name="group_id" value="Admin"> Admin<br>
                                    <input id="status" type="radio" name="group_id" value="User" checked> User<br>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('assignedto') ? ' has-error' : '' }}">
                                <label for="assignedto" class="col-md-4 control-label">Tasks assigned to</label>                           
                                <div class="col-md-6">
                                    <select class="form-control" name="assignedto">
                                        <option value="" disabled selected>select...</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success">Add</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Change Customers Account Manager</div>
                <div class="panel-body rounded_wrapper">

                    <form id="changeAccManagers" method="POST" action="{{url('/')}}/admin/changeAccManager">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-3">
                                Change All customers Account managers from
                            </div>
                            <div class="col-md-2">
                                <select name="accFrom" class="form-control">
                                    <option value="" disabled selected>select...</option>
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1">
                                To 
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" name="accTo">
                                    <option value="" disabled selected>select...</option>
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1">
                                <button class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>


</div>

@endsection
