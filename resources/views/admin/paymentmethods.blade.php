
@extends('layouts.app')

@section('content')

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Listing Payment Methods</div>
                <div class="panel-body">
                    <table id="users_list" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <td>Payment Method</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($paymentmethods as $paymentmethod)
                            <tr>
                                <td>
                                    </span><a href="{{url('/admin/paymentmethods/')}}/{{$paymentmethod->id}}"> {{$paymentmethod->method}}</a></td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading"><?PHP
                    echo (isset($thispaymentmethods->id)) ? "Edit Payment Methods" : "Add New Payment Methods";
                    ?>
                    <div class="pull-right">
                        <?PHP
                        if (isset($thispaymentmethod->id)) {
                            ?>                           
                            <div style="display: inline-block; margin: -5px 5px">
                                <form id="OptionsDeleteForm" method="POST" action="{{url('/admin/paymentmethods/' . $thispaymentmethod->id)}}">
                                    {{method_field('DELETE')}}
                                    {{ csrf_field() }}
                                    <input hidden name="paymentmethodid" value="{{$thispaymentmethod->id}}">
                                    <span class="btn btn-sm btn-danger DeleteButtonOption" type="button" id="DeleteButton">Delete</span>
                                </form>
                            </div>
                            <?PHP
                        }
                        ?>
                    </div>
                </div>
                <div class="panel-body rounded_wrapper">
                    <form id="websitesForm" class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thispaymentmethod->id)) ? url('/admin/paymentmethods/' . $thispaymentmethod->id) : url('/admin/paymentmethods');
                    ?>>
                        {{ csrf_field() }}
                        <?PHP if (isset($thispaymentmethod->id)) { ?>
                            {{method_field('PATCH')}}
                            <?PHP
                        }
                        ?>
                        <div class="form-group{{ $errors->has('method') ? ' has-error' : '' }}">
                            <label for="method" class="col-md-4 control-label">Payment Method</label>

                            <div class="col-md-6">
                                <input id="method" type="text" maxlength="30" class="form-control" name="method" value="{{{ $thispaymentmethod->method or '' }}}">

                                @if ($errors->has('method'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('method  ') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a type="cancel" class="btn btn-sm btn-danger" href="{{url('/admin/paymentmethods')}}">Cancel</a>
                                <button type="submit" class="btn btn-sm btn-success">                                    
                                    <?PHP
                                    echo (isset($thispaymentmethod->id)) ? "Update" : "Add";
                                    ?>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection