@extends('layouts.app')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@if(isset($companyFiles) && !empty($companyFiles))
<div class="company-files-table-container popup">
    <h4 style="text-align: center;margin: 20px auto">Companies List</h4>
    <table class="table table-bordered" id="company-files-table">
        <thead>
            <tr>
                <td>Name</td>
                <td>Serial Number</td>
                <td style="display:none;"></td>
            </tr>
        </thead>
        <tbody>
            @foreach($companyFiles as $companyFile)
            <tr>
                <td>{{$companyFile['Name']}}</td>
                <td>{{$companyFile['SerialNumber']}}</td>
                <td style="display:none;">
                    <input type="hidden" name="companyUID" value="{{$companyFile['Id']}}"/>
                    <input type="hidden" name="companyURL" value="{{$companyFile['Uri']}}"/>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <button type="button" class="close-popup btn btn-danger btn-sm">Close</button>
</div>
<link rel="stylesheet" type="text/css"
      href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>
<script type="text/javascript"
src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-12 panel-body rounded_wrapper">
            <div class="hidden myob_search_container popup"></div>
            <div class="col-lg-12 col-md-12">
                <img class="center-block" height="150px" src="{{url('/')}}/img/myob.png"/>                
            </div>
            <div class="col-md-4 filter-button-wrapper" style="    margin-top: 10px;">
                <div class="text-center">
                    <label>Actions</label>
                </div>
                <a href="https://secure.myob.com/oauth2/account/authorize?client_id={{env('MYOB_KEY')}}&redirect_uri={{env('MYOB_REDIRECT')}}&response_type=code&scope=CompanyFile" class="btn btn-sm btn-info center-block btn-myob">Authorise</a>
                <a href="{{url('/') . '/tools/myob/deactivateAllOrders'}}" class="btn btn-sm btn-info center-block btn-myob">Deactivate All Orders</a>
                <a href="{{url('/') . '/tools/myob/test'}}" class="btn btn-sm btn-info center-block btn-myob">Test</a>
                <a href="{{url('/') . '/tools/myob/refreshToken'}}" class="btn btn-sm btn-info center-block btn-myob">Refresh Token</a>
            </div>   
            <div class="col-md-8 myob-wrapper">
                <div class="form-group">                    
                    <div class="col-md-10">
                        <label for="mpm_account_uid">MPM Account UID</label>
                        <input class="form-control" id="mpm_account_uid" name='mpm_account_uid' value="{{(isset($myobDetails)&&!empty($myobDetails))?$myobDetails->accountsMPMUid:''}}"/>
                    </div>
                    <div class="col-md-2">                        
                        <button style="margin-top: 35px" type="button" name="mpm_account_uid_search" class="btn btn-md btn-primary"><i class="fa fa-btn fa-search"></i></button>
                    </div>
                </div>      
                <div class="form-group">
                    <div class="col-md-10">
                        <label>Brothels Account UID</label>
                        <input class="form-control" id="brothels_account_uid" name='brothels_account_uid' value="{{(isset($myobDetails)&&!empty($myobDetails))?$myobDetails->accountBrothelsUid:''}}"/>
                    </div>
                    <div class="col-md-2">
                        <button style="margin-top: 35px" type="button" name="brothels_account_uid_search" class="btn btn-md btn-primary"><i class="fa fa-btn fa-search"></i></button>
                    </div>
                </div>      
                <div class="form-group">
                    <div class="col-md-10">
                        <label>Tax Code UID</label>
                        <input class="form-control" id="myob_tax_code_uid" name='myob_tax_code_uid' value="{{(isset($myobDetails)&&!empty($myobDetails))?$myobDetails->gstCodeUID:''}}"/>
                    </div>
                    <div class="col-md-2">
                        <button style="margin-top: 35px" type="button" name="myob_tax_code_uid_search" class="btn btn-md btn-primary"><i class="fa fa-btn fa-search"></i></button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <label>Admin Password</label>
                        <input type="password" class="form-control" id="myob_admin_password" name='myob_admin_password'/>
                        <!-- <p class="adminPasswordChangeInfo">@php echo (isset($adminPasswordDetails) && !empty($adminPasswordDetails)) ? $adminPasswordDetails : '' @endphp</p> -->
                    </div>
                    <div class="col-md-2">
                        <button style="margin-top: 35px" type="button" name="myob_admin_password_button" class="btn btn-md btn-primary"><i class="fa fa-btn fa-save"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection