<?PHP
$method = isset($_GET['method']) ? $_GET['method'] : $invoice->payment_method;
$amount = isset($_GET['amount']) ? $_GET['amount'] : '';
$created_at = isset($_GET['created_at']) ? $_GET['created_at'] : date(DateFormat);
$paymentid = isset($_GET['paymentid']) ? $_GET['paymentid'] : '';
?>
@extends('layouts.app')
@section('content')
    <div class="fullcontainer">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading page-panelheading">Invoice Details - Customer Working Name: <a
                                href="{{url('/')}}/customer/{{$invoice->customerid}}">{{$invoice->bname}}</a> -
                        ID: {{$invoice->customerid}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fullcontainer">
        <div class="row">
            <div class="col-md-12">
                <div class="panel  panel-info rounded_wrapper">
                    <div class="panel-heading page-panelheading">MYOB: {{$invoice->myob}}

                        <?PHP if (Auth::user()->group_id == 'Admin') { ?>
                        <div style="display: inline-block; margin: -5px 0" class="pull-right">
                            <form id="invoiceDeleteForm" method="POST"
                                  action="<?= url('/') . "/invoice/$invoice->id" ?>">
                                {{method_field('DELETE')}}
                                {{ csrf_field() }}
                                <input hidden name="invoiceid" value="{{$invoice->id}}">
                                <input hidden name="customerid" value="{{$invoice->customerid}}">
                                <span class="btn btn-sm btn-danger sweetDeleteButton" type="button"
                                      onclick="invoiceDeleteButton()">Delete</span>
                            </form>
                        </div>
                        <div style="display: inline-block; margin: -5px 5px" class="pull-right">
                            <a href="{{url('/')}}/invoice/{{$invoice->id}}/edit"><span class="btn btn-success btn-sm"
                                                                                       style="margin: 0;">Edit</span></a>
                        </div>
                        <?PHP } ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="dl-horizontal">
                                        <div class="col-md-6">
                                            <div class="titlesDiv">Invoice To:</div>
                                            <dt>Name:</dt>
                                            <dd>{{$invoice->invname}}</dd>
                                            <hr>
                                            <dt>ABN:</dt>
                                            <dd>{{$invoice->abn}}</dd>
                                            <hr>

                                            <div class="titlesDiv">Send To:</div>
                                            @if($invoice->emailmethod==1)
                                                <dt>Email:</dt>
                                                <dd>
                                                    <a href="mailto:{{$invoice->invemail or ''}}">{{$invoice->invemail or ''}}</a>
                                                </dd>
                                                <hr>
                                            @endif
                                            @if($invoice->addressmethod ==1)
                                                <dt>Address:</dt>
                                                <dd>
                                                    @if($invoice->unit)
                                                        {{$invoice->unit.' / '}}
                                                    @endif
                                                    {{$invoice->number}}&nbsp;{{$invoice->street}}
                                                    &nbsp;{{$invoice->sttype}},
                                                    <br>
                                                    {{$invoice->suburb}},{{$invoice->state}}&nbsp;{{$invoice->postcode}}
                                                </dd>
                                                <hr>
                                            @endif
                                            @if($invoice->poboxmethod==1)
                                                <dt>PO BOX:</dt>
                                                <dd>
                                                    {{$invoice->invpoboxno or ''}}
                                                    &nbsp;-&nbsp;{{$invoice->invpoboxsuburb}}
                                                    ,{{$invoice->invpoboxstate}}&nbsp;{{$invoice->invpoboxpostcode}}

                                                </dd>
                                                <hr>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            <div class="titlesDiv">Description:</div>
                                            <div style="padding:0px 20px;">{{$invoice->description}}</div>
                                            <hr>
                                            <dt>Created at:</dt>
                                            <dd>{{date(DateFormat,strtotime($invoice->created_at))}}</dd>
                                            <br>
                                            <dt>Created by:</dt>
                                            <dd>{{$invoice->username}}</dd>
                                            <br>
                                            <dt>Method:</dt>
                                            <dd>{{$invoice->payment_method or ''}}</dd>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="ordersContainer">
                                    <div class="col-md-12">
                                        <div class="titlesDiv">Orders:</div>
                                        <table class="table table-bordered invoiceordertable">
                                            <thead>
                                            <tr>
                                                <th>Site</th>
                                                <th>Invoice Description</th>
                                                <th>Page</th>
                                                <th>Position</th>
                                                <th>Start Date</th>
                                                <th>Frequency</th>
                                                <th>Expiry Date</th>
                                                <th>Due Date</th>

                                                <th>Amount</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?PHP
                                            $orders['sitenames'] = explode(',', $invoice->sitenames);
                                            $orders['orderoption'] = explode(',', $invoice->orderoptions);
                                            $orders['pages'] = explode(',', $invoice->pages);
                                            $orders['positions'] = explode(',', $invoice->positions);
                                            $orders['startdate'] = explode(',', $invoice->startdates);
                                            $orders['frequency'] = explode(',', $invoice->frequencies);
                                            $orders['expirydate'] = explode(',', $invoice->expirydates);
                                            $orders['duedate'] = explode(',', $invoice->duedates);
                                            $orders['amounts'] = explode(',', $invoice->amounts);
                                            $orders['active'] = explode(',', $invoice->active);
                                            $orders['orderid'] = explode(',', $invoice->orderid);
                                            $subtotal = number_format(array_sum($orders['amounts']), 2);
                                            $GST = number_format(array_sum($orders['amounts']) * 0.1, 2);
                                            $total = number_format(array_sum($orders['amounts']) * 1.1, 2);
                                            $num = count($orders['sitenames']);
                                            ?>
                                            @for($i=0; $i < $num ;$i++)
                                                <tr
                                                <?PHP
                                                    /*
                                                      if (date("Y-m-d") <= $orders['duedate'][$i]) {
                                                      echo " style='background-color: #CFC'";
                                                      }
                                                      if ($orders['duedate'][$i] <= date("Y-m-d") && date("Y-m-d") <= $orders['expirydate'][$i]) {
                                                      echo " style='background-color: #FFC'";
                                                      }
                                                      if ($orders['expirydate'][$i] <= date("Y-m-d")) {
                                                      echo " style='background-color: #FCC'";
                                                      }
                                                     */
                                                    ?>
                                                >
                                                    <td>{{$orders['sitenames'][$i]}}</td>
                                                    <td>{{ $orders['orderoption'][$i] !='null' ? $orders['orderoption'][$i] : ''}}</td>
                                                    <td>{{$orders['pages'][$i]}}</td>
                                                    <td>{{$orders['positions'][$i]}}</td>
                                                    <td>{{date(DateFormat,strtotime($orders['startdate'][$i]))}}</td>
                                                    <td>{{$orders['frequency'][$i]}}</td>
                                                    <td>{{date(DateFormat,strtotime($orders['expirydate'][$i]))}}</td>
                                                    <td>{{date(DateFormat,strtotime($orders['duedate'][$i]))}}</td>
                                                    <td class="amountColumn">${{$orders['amounts'][$i]}}</td>
                                                    <td style="padding: 0px;margin: 0px;text-align: center;">
                                                        <form method="POST"
                                                              action="{{url("invoice/toggleOrderStatus") }}">
                                                            {{ csrf_field() }}
                                                            <input hidden name="orderid"
                                                                   value="{{$orders['orderid'][$i]}}">
                                                            <button style="margin:10px 5px;"
                                                                    class="btn btn-xs <?= $orders['active'][$i] == 1 ? 'fa fa-check btn-success' : 'fa fa-close btn-danger' ?>"
                                                                    title="<?= $orders['active'][$i] == 1 ? 'This Order is Active. to be invoiced next time.' : 'This Order is Deactive. No need to renew.' ?>"><?= $orders['active'][$i] == 1 ? '' : '' ?></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endfor
                                            <tr>
                                                <td colspan="7" rowspan="3">
                                                </td>
                                                <td class="text-right"><b>Subtotal:</b></td>
                                                <td class="amountColumn"><b>${{$subtotal}}</b></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><b>GST:</b></td>
                                                <td class="amountColumn"><b>${{$GST}}</b></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><b>Total:</b></td>
                                                <td class="amountColumn"><b>${{$total}}</b></td>
                                                <td></td>
                                                <input hidden value="{{$total}}" name="invoiceTotalAmount"
                                                       id="invoiceTotalAmount">
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="fullcontainer">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <?PHP
                                            if ($paymentid != '') {
                                            echo 'Edit this payment up to $';
                                            echo number_format($amount + $invoice->subtotal * 1.1 - $paidamount, 2);
                                            } else {
                                            echo 'Make a Payment up to $';
                                            echo number_format($invoice->subtotal * 1.1 - $paidamount, 2);
                                            }
                                            ?>

                                        </div>
                                        <div class="panel-body">
                                            <form id="paymentForm" method="POST"
                                                  action="{{ isset($paymentid) ? url("payment/$paymentid") : url("payment") }}">
                                                <form id="paymentForm" method="POST"
                                                      action="{{ isset($paymentid) ? url("payment/$paymentid") : url("payment") }}">

                                                    <?= (isset($paymentid) && $paymentid != '') ? method_field('PATCH') : ''; ?>
                                                    {{ csrf_field() }}
                                                    <input hidden name="paymentid" value="{{$paymentid or ''}}">
                                                    <input hidden name="invoiceid" value="{{$invoice->id or ''}}">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="method">
                                                                <option disabled selected>Select...</option>
                                                                @foreach($paymentmethod as $paymethod)
                                                                    <option value="{{$paymethod->method}}" <?= ($method == $paymethod->method ) ? 'selected' : '' ?> >{{$paymethod->method}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <div class="input-group-addon"><i
                                                                            class="glyphicon glyphicon-usd"></i></div>
                                                                <input style="height:40px" id='paymentmount' class="form-control"
                                                                       name="amount" value="{{$amount or '' }}"
                                                                       placeholder="0.00">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input style="height:40px" id="paymentCreatedAt" placeholder="Created at"
                                                                   class="form-control" name="created_at"
                                                                   value="{{$created_at}}">
                                                        </div>
                                                    </div>
                                                    <br>
                                                    @if($paymentid!='')
                                                        <div class="col-md-6 col-md-offset-3">
                                                            <button class="btn btn-primary btn-sm col-md-4 col-md-offset-1">
                                                                Submit
                                                            </button>
                                                            <a class="btn btn-success btn-sm btn-sm col-md-4 col-md-offset-2"
                                                               href="<?= url('/') . "/invoice/$invoice->id" ?>">Cancel</a>
                                                        </div>
                                                    @else
                                                        <button class="btn btn-info btn-sm col-md-4">Make a Payment
                                                        </button>
                                                        <div class="col-md-6 col-md-offset-2">
                                                            <input type="checkbox" name="email_notifivation" checked>
                                                            <label>Send Email</label>
                                                        </div>
                                                    @endif
                                                </form>
                                                @if($invoice->payment_method != 'Invoice Cancelled')
                                                    <form id="cancelInvoiceButton"
                                                          action="{{url('/invoice/cancelInvoice')}}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input hidden name="invoiceid" value="{{$invoice->id or ''}}">
                                                        <button class="btn-sm col-md-4 btn btn-danger"
                                                                onclick="invoiceCancelButton()" type="button" style="float:right">Cancel
                                                        </button>
                                                    </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-4">
                                <div class="fullcontainer">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">Payments History
                                        </div>
                                        <div class="panel-body">
                                            <table id="paymentsHistory" class="table">
                                                <thead>
                                                <tr>
                                                    <td>Created at</td>
                                                    <td>Logged by</td>
                                                    <td>Method</td>
                                                    <td>Amount</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?PHP $totalpaid = 0 ?>
                                                @foreach($payments as $payment)
                                                    <tr>
                                                        <td>{{date(DateFormat,strtotime($payment->created_at))}}</td>
                                                        <td>{{$payment->name}}</td>
                                                        <td>{{$payment->method}}</td>
                                                        <td class="amountColumn">${{$payment->amount}}</td>
                                                        <td>
                                                            <div>
                                                                <form method="GET"
                                                                      action="<?= url('/') . "/invoice/$invoice->id" ?>">
                                                                    <button class="glyphicon glyphicon-edit btn btn-sm btn-primary"></button>
                                                                    <input name="method" hidden
                                                                           value="{{$payment->method}}">
                                                                    <input name="amount" hidden
                                                                           value="{{$payment->amount}}">
                                                                    <input name="paymentid" hidden
                                                                           value="{{$payment->id}}">
                                                                    <input name="created_at" hidden
                                                                           value="{{date(DateFormat,strtotime($payment->created_at))}}">
                                                                </form>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div>
                                                                <form id="paymentDeleteForm{{$payment->id}}"
                                                                      method="POST"
                                                                      action="<?= url('/') . "/payment/$payment->id" ?>">
                                                                    {{method_field('DELETE')}}
                                                                    {{ csrf_field() }}
                                                                    <input name="paymentid" hidden=""
                                                                           value="{{$payment->id}}">
                                                                    <span class="glyphicon glyphicon-remove btn btn-sm btn-danger"
                                                                          onclick="DeletePaymentButton({{$payment->id}})"></span>
                                                                </form>
                                                            </div>
                                                        </td>
                                                        <?PHP $totalpaid += $payment->amount; ?>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td></td>

                                                    <td colspan="2" id="amountPaid" class="amountColumn">
                                                        <b>$ {{number_format($totalpaid,2)}}</b></td>
                                                    <td></td>
                                                    <input hidden value="{{number_format($totalpaid,2)}}"
                                                           name="invoicePaidAmount" id="invoicePaidAmount">

                                                    <td></td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                    <form id="customerStatusForm" method="POST" action=<?= url('cstatus/store') ?>>
                                        {{ csrf_field() }}
                                        <input name="customerid" value="{{$customer->id}}" hidden>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">Customer Status</div>
                                            <div class="panel-body center-block">
                                                <div class="form-group">
                                                    <select id="cstatuses" class="form-control" name="cstatus">
                                                        <option disabled selected>Select...</option>

                                                        @foreach($statuses as $status)
                                                            <option
                                                                <?= ($status->status == "Deactive") ? 'disabled' : '' ?> expdays="{{$status->expdays}}"
                                                                value="{{$status->id or ''}}" <?= isset($customer->status()->id) ? ($customer->status()->id == $status->id) ? "selected" : "" : ''; ?> >{{$status->name or ''}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                                <div class="form-inline">
                                                    <label>Due Date</label>
                                                    <br>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i
                                                                    class="glyphicon glyphicon-calendar"></i></div>
                                                        <input type="text" id="dueDate" class="form-control"
                                                               name="dueDate" autocomplete="off"
                                                               value="{{isset($customer->status()->dueDatetime)? date(DateTimeFormat,strtotime($customer->status()->dueDatetime)) : ''}}">
                                                    </div>
                                                    <button style="margin:0px" class="btn btn-sm btn-primary" name="update"
                                                            value="{{$customer->id}}">Update
                                                    </button>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection
