<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CRM Adultpress</title>
        <!-- Scripts -->
        <script src="{{url('/')}}/js/jquery-2.2.4.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/jquery-ui.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/jquery.validate.min.js" ></script>
        <script src="{{url('/')}}/js/additional-methods.min.js" ></script>
        <script src="{{url('/')}}/js/jquery.datetimepicker.full.js" type="text/javascript"></script>

        <script src="{{url('/')}}/js/pace.min.js" type="text/javascript"></script>
        <!-- Fonts -->
        <link href="{{url('/')}}/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="{{url('/')}}/css/adultpress.css">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="{{url('/')}}/dist/sweetalert.css">
        <link rel="stylesheet" href="{{url('/')}}/css/editor.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/chosen/chosen.css">
        <link href="{{url('/')}}/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <?PHP
    if (isset($_SERVER['REQUEST_URI'])) {
        $currenturl = explode('/', $_SERVER['REQUEST_URI']);
        $currentpage = end($currenturl);
        $currentpage = explode('?', $currentpage)[0];
    }
    ?>
    <body id="app-layout" class="<?php echo $currentpage != '' ? $currentpage : 'login'; ?>">
        <div class="se-pre-con"></div>
        <nav class="navbar navbar-default navbar-static-top">            
            <div class="fullcontainer">                               
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Branding Image -->
                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href = "{{ url('/') }}" class="navbar-logo"></a></li>
                    </ul>
                    <!-- Left Side Of Navbar -->
                    {{-- menu available just for users --}}
                    @if (!Auth::guest())
                    <ul class="nav navbar-nav">
                        <li <?= ($currentpage == 'expiringtasks') ? 'class="active"' : '' ?>><a href = "{{ url('/') }}">Home</a></li>
                        <li <?= ($currentpage == 'customers') ? 'class="active"' : '' ?>><a href = "{{ url('customers') }}"><i class="fa fa-odnoklassniki"></i>Customers</a></li>
                        <li <?= ($currentpage == 'invoice') ? 'class="active"' : '' ?>><a href = "{{ url('invoice') }}"><i class="fa fa-money"></i>Invoices</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-wrench"></i>
                                Tools<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li <?= ($currentpage == 'searchtasks') ? 'class="active"' : '' ?>><a href = "{{ url('searchtasks') }}">Advanced Search Tasks</a></li>
                                <li <?= ($currentpage == 'deduplicate') ? 'class="active"' : '' ?>><a href = "{{ url('deduplicate') }}">De-duplicate</a></li>
                                <li <?= ($currentpage == 'deletedcustomers') ? 'class="active"' : '' ?>><a href = "{{ url('tools/deletedcustomers') }}">Deleted Customers</a></li>                                
                                <li <?= ($currentpage == 'import') ? 'class="active"' : '' ?>><a href = "{{ url('import') }}">Import</a></li>                                
                                <li <?= ($currentpage == 'maps') ? 'class="active"' : '' ?>><a href = "{{ url('tools/maps') }}">Maps</a></li>
                                <hr style="margin: 0;">                                
                                <li <?= ($currentpage == 'myob') ? 'class="active"' : '' ?>><a href = "{{ url('tools/myob') }}">MYOB</a></li>                                
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-desktop"></i>
                                Websites<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu website-menu" role="menu">
                                <li><a class="website-item-menu" target="_blank" href="https://www.adultpress.com.au/chilliaccess" title="AdultPress"><div><img src="{{ url('/') }}/img/adultpress_logo.png"/></div><div>Adult Press</div></a></li>
                                <li><a class="website-item-menu" target="_blank" href="https://www.aroundmidnight.com/wp-login.php" title="AroundMidnight"><div><img src="{{ url('/') }}/img/aroundmidnight_logo.png"/></div><div>Around Midnight</div></a></li>
                                <li><a class="website-item-menu" target="_blank" href="https://asianbrothels.com.au/chilliaccess" title="AsianBrothels"><div><img src="{{ url('/') }}/img/asianbrothels_logo.png"/></div><div>Asian Brothels</div></a></li>
                                <li><a class="website-item-menu" target="_blank" href="http://www.brothels.com.au/chilliaccess" title="Brothels"><div><img src="{{ url('/') }}/img/brothels_logo.png"/></div><div>Brothels</div></a></li>
                                <li><a class="website-item-menu" target="_blank" href="https://www.meandmrsjones.com.au/wp-login.php" title="MeAndMrsJones"><div><img src="{{ url('/') }}/img/meandmrsjones_logo.png"/></div><div>Me and Mrs Jones</div></a></li>
                                <li><a class="website-item-menu" target="_blank" href="https://www.mrsjoneslingerie.com.au/wp-login.php" title="MrsJonesLingerie"><div><img src="{{ url('/') }}/img/mrsjoneslingerie_logo.png"/></div><div>Mrs Jones Lingerie</div></a></li>
                                <li><a class="website-item-menu" target="_blank" href="https://myplaymate.com.au/admin/login" title="MyPlayMate"><div><img src="{{ url('/') }}/img/mpm_logo.png"/></div><div>Adult Press</div></a></li>
                            </ul>
                        </li>
                    </ul>
                    @endif
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (!Auth::guest())
                        @if (Auth::user()->group_id=="Admin")
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-cogs"></i>
                                Administration<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li <?= ($currentpage == 'users') ? 'class="active"' : '' ?>><a href = "{{ url('/admin/users') }}">Account Managers</a></li>
                                <hr style="margin: 0;">
                                <li <?= ($currentpage == 'websites') ? 'class="active"' : '' ?>><a href = "{{ url('/admin/websites') }}">Advertising Platforms</a></li>
                                <li <?= ($currentpage == 'orderoptions') ? 'class="active"' : '' ?>><a href = "{{ url('/admin/orderoptions') }}">Invoice Descriptions</a></li>
                                <li <?= ($currentpage == 'paymentmethods') ? 'class="active"' : '' ?>><a href = "{{ url('/admin/paymentmethods') }}">Payment Methods</a></li>
                                <li <?= ($currentpage == 'orderpages') ? 'class="active"' : '' ?>><a href = "{{ url('/admin/orderpages') }}">Order Pages</a></li>
                                <li <?= ($currentpage == 'positions') ? 'class="active"' : '' ?>><a href = "{{ url('/admin/positions') }}">Positions</a></li>
                                <li <?= ($currentpage == 'statuses') ? 'class="active"' : '' ?>><a href = "{{ url('/admin/statuses') }}">Statuses</a></li>
                                <li <?= ($currentpage == 'types') ? 'class="active"' : '' ?>><a href = "{{ url('/admin/types') }}">Types</a></li>
                                <hr style="margin: 0;">
                                <li <?= ($currentpage == 'reports') ? 'class="active"' : '' ?>><a href = "{{ url('/reports/reports') }}">Reports</a></li>
                            </ul>
                        </li>
                        @else
                        @endif
                        @endif
                        @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        {{--
                        <li><a href="{{ url('/register') }}">Register</a></li>
                        --}}
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-user"></i>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/admin/users') }}/{{Auth::user()->id}}/edit"><i class="fa fa-btn fa-user"></i>My Account</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @if(Session::has('success'))
        <div class="systemmsg container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="successcontainer panel-heading">
                            {{Session::get('success')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if(Session::has('error'))
        <div class="systemmsg container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="errorscontainer panel-heading">
                            {{Session::get('error')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if(Session::has('status'))
        <div class="systemmsg container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="statusscontainer panel-heading">
                            {{Session::get('status')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @yield('content')
        <!-- JavaScripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="{{url('/')}}/js//jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/adultpress.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/editor.js" type="text/javascript"></script>
        <script src="{{url('/')}}/plugins/chosen/chosen.jquery.js" type="text/javascript"></script>
        <script src="{{url('/')}}/dist/sweetalert.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js" type="text/javascript"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/plug-ins/1.10.12/api/sum().js" type="text/javascript"></script>
    </body>
</html>