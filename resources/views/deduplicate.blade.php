<?PHP

use App\Customer;
?>
@extends('layouts.app')

@section('content')
<script>
    $(document).ready(function() {
        toggleClearFilter();
        $('#businessTypeSelect, #account_manager, #findBy').on('change', function (e) {         
            toggleClearFilter();
        });
        $('#clearFilters').on('click', function (e) {        
            $('#businessTypeSelect').val("0");
            $('#account_manager').val("0");
            $('#findBy').val("0");
            $('#clearFilters').hide();
        });
        function toggleClearFilter() {
            if($('#businessTypeSelect').val() != '0' || $('#account_manager').val() != '0' || $('#findBy').val() != '0') {
                $('#clearFilters').show();
            } else {
                $('#clearFilters').hide();
            }
        }
    });
</script>
@if(!isset($_GET['ids']))
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">New Deduplicate Customers</div>
                <div class="panel-body rounded_wrapper">
                    <br>
                    <form id="deduplicateForm" method="GET" action="">
                        {{ csrf_field() }}
                        <div  class="form-group col-sm-3">
                            <label for="btype" class="col-md-6 control-label">Business Type</label>
                            <div class="col-md-6"> 
                                <select id="businessTypeSelect" class="form-control" name="btype">
                                    <option selected value="0">All</option>
                                    @foreach($types as $type)
                                    <option value="{{$type->id}}"{{isset($_GET['btype'])?(($_GET['btype']==$type->id)? 'selected' : ''):''}}>{{$type->type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div  class="form-group col-sm-3">
                            <label for="account_manager" class="col-md-6 control-label">Account Manager</label>
                            <div class="col-md-6"> 
                                <select class="form-control" id="account_manager" name="account_manager">
                                    <option selected value="0">All</option>
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}" {{isset($_GET['btype'])?(($_GET['account_manager']==$user->id)? 'selected' : ''):''}}>{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="findBy" class="col-md-6 control-label">Find By</label>
                            <div class="col-md-6"> 
                                <select class="form-control" id="findBy" name="findBy">
                                    <option selected disabled value="0">Select...</option>
                                    <option value="1" {{isset($_GET['findBy'])? (($_GET['findBy']==1)? 'selected' : ''):''}}>Phone</option>
                                    <option value="2" {{isset($_GET['findBy'])? (($_GET['findBy']==2)? 'selected' : ''):''}}>Name & Phone</option>
                                    <option value="3" {{isset($_GET['findBy'])? (($_GET['findBy']==3)? 'selected' : ''):''}}>Name & Email</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="filter-button-wrapper text-center">
                                    <button style="display:none;" class="btn btn-sm btn-warning" id="clearFilters">Clear Filters</button>                                    
                                    <button class="btn btn-sm btn-primary" type="submit">Search</button>                                    
                                </div>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@if(isset($duplist))
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">           
                <div class="panel-body rounded_wrapper">
                    <table id="duplicated_list">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Number</th>
                                <?= isset($_GET['findBy']) ? (($_GET['findBy'] == 3) ? "<th>Email</th>" : '') : '' ?>
                                <th>Type</th>
                                <!--<th>Customers ID</th>-->
                                <th>Account Manager</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($duplist as $item)
                            <tr class='clickable-row' data-target="_blank" data-href= "{{url('/')}}/deduplicate?ids={{$item->customersid}}">
                                <td>{{$item->bname}}</td>
                                <td>{{$item->bphone}}</td>
                                <?= isset($_GET['findBy']) ? (($_GET['findBy'] == 3) ? "<td>$item->bemail</td>" : '') : '' ?>              
                                <td>{{implode(',',array_unique(explode(',',$item->typenames))) }}</td>
                                <!--<td><b>{{$item->counts}}: </b>{{$item->customersid}}</td>-->
                                <td>{{implode(',',array_unique(explode(',',$item->usernames)))}}</td>                             
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@if(isset($customers))

<div class="fullcontainer">
    <div class="row">
        @foreach($customers as $customer)
        <div class="col-md-4" style="min-height: 30em;">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Customer ID {{$customer->id}}
                    <div style="display: inline-block; margin: -5px" class="pull-right">
                        <a href="{{url('/')}}/customer/{{$customer->id}}/edit"><span class="btn btn-primary btn-sm" style="margin: 0 5px;">Edit</span></a> 

                        <div style="display: inline-block; margin: 0 5px" class="pull-right">

                            <form id="customerDeleteForm{{$customer->id}}" method="POST" action="{{url('/customer/' . $customer->id)}}">
                                {{method_field('DELETE')}}
                                {{ csrf_field() }}
                                <input hidden name="customerid" value="{{$customer->id}}">
                                <span class="btn btn-sm btn-danger sweetDeleteButton"  type="button"  value="Delete" onclick = "DeleteButton({{$customer->id}})">Delete</span>
                            </form>
                        </div>

                    </div>
                </div>


                <div class="panel-body rounded_wrapper">
                    <div class="small">
                        <dl class="dl-horizontal">


                            <dt>Working Name</dt>
                            <dd>{{$customer->bname}}</dd>

                            <dt>Working Phone</dt>
                            <dd>{{$customer->bphone}}</dd>

                            <dt>Working Email</dt>
                            <dd>{{$customer->bemail}}</dd>

                            <dt>ABN</dt>
                            <dd>{{$customer->abn}}</dd>

                            <dt>Website</dt>
                            <dd>{{$customer->website}}</dd>

                            <dt>Address</dt>
                            <dd>{{$customer->address}}</dd>

                            <dt>Postcode</dt>
                            <dd>{{$customer->suburb}},{{$customer->state}} {{$customer->postcode}}</dd>

                            <dt>First Name</dt>
                            <dd>{{$customer->firstname}}</dd>

                            <dt>Last Name</dt>
                            <dd>{{$customer->lastname}}</dd>

                            <dt>Personal Phone</dt>
                            <dd>{{$customer->phone}}</dd>

                            <dt>Personal Email</dt>
                            <dd>{{$customer->email}}</dd>

                            <dt>Account Manager</dt>
                            <dd>{{$customer->account_manager_name}}</dd>

                            <dt>Type</dt>
                            <dd>{{$customer->b_type}}</dd>

                            <dt>MPM User Id</dt>
                            <dd>{{$customer->mpm_user_id}}</dd>

                            <dt>MPM {{$customer->b_type}} ID</dt>
                            <dd>{{$customer->mpm_type_id}}</dd>

                            <dt>Brothel ID</dt>
                            <dd>{{$customer->brothel_id}}</dd>
                            <hr>

                            <?PHP
                            $thisCustomer = Customer::find($customer->id);
                            $thisaddabn = $thisCustomer->invaddabn();
                            ?>
                            <dt>Number Of Invoices</dt>
                            <dd>{{ $thisCustomer->numberofinv()}}</dd>


                            <dt>Invoicing ABN</dt>
                            <dd>{{ $thisaddabn->abn or ''}}</dd>

                            <dt>Created at</dt>
                            <dd>{{$thisCustomer->created_at}}</dd>

                        </dl>
                    </div>
                </div>
            </div>
        </div>
        @endforeach


    </div>
</div>
@endif


@endsection

