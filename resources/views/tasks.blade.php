@extends('layouts.app')

@section('content')
<?PHP
$filteruser = isset($_GET['user']) ? $_GET['user'] : Auth::user()->id;
$filterFrom = isset($_GET['dueDateFrom']) ? $_GET['dueDateFrom'] : date("Y-m-d");
$filterTo = isset($_GET['dueDateTo']) ? $_GET['dueDateTo'] : date("Y-m-d");
$selectedUsers = isset($_GET['selectedUsers']) ? $_GET['selectedUsers'] : Auth::user()->name;
?>
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Tasks
                    <div class="pull-right col-lg-8" style="display: inline-block">
                        <form method="GET" action="">

                            @if(Auth::user()->status == 'Active')
                            <div class="col-md-2" style="display: inline-block; margin:  5px">
                                <select onchange="setSelectedValues()" name="user" multiple data-actions-box="true" class="form-control selectpicker" {{(Auth::user()->group_id=='User')? 'disabled' : ''}}>
                                    @foreach($users as $user)
                                    <option>{{$user->name}}</option>
                                    @endforeach
                                </select>
                                <input id="selectedUsers" hidden name="selectedUsers" value="{{$selectedUsers}}">
                            </div>
                            @endif

                            <div class="col-md-3" style="display: inline-block; margin: 5px">                        

                                <div class="input-group">
                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar">From</i></div>
                                    <input type="text" id="dueDateFrom" class="form-control" name="dueDateFrom" autocomplete="off" value="{{$filterFrom}}">
                                </div>
                            </div>
                            <div class="col-md-3" style="display: inline-block; margin: 5px">                        

                                <div class="input-group">
                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar" >To</i></div>
                                    <input type="text" id="dueDateTo" class="form-control" name="dueDateTo" autocomplete="off"  value="{{$filterTo}}">
                                </div>
                            </div> 
                            <div class="col-md-1" style="display: inline-block; margin: 5px">
                                <button class="form-control btn btn-sm btn-primary" type="submit">filter</button>
                            </div>
                        </form>
                    </div>  

                </div>


                <div class="panel-body">
                    @if(Auth::user()->group_id == 'Admin')
                    @else
                    @if(Auth::user()->assignedto != NULL AND Auth::user()->id != Auth::user()->assignedto)
                    @else
                    @endif
                    @endif
                    <table id="tasks_list" class="table table-bordered">
                        <thead>
                            <tr id="tableFilters">
                                <th>#</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Assigned to</th>
                                <th>Acc Manager</th>
                                <th>Status</th>
                                <th>Invoice Reminder</th>
                            </tr>     
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Assigned to</th>
                                <th>Acc Manager</th>
                                <th>Status</th>
                                <th>Invoice Reminder</th>
                            </tr>                           
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Assigned to</th>
                                <th>Acc Manager</th>
                                <th>Status</th>  
                                <th>Invoice Reminder</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @if(isset($customers))
                            @foreach($customers as $customer)
                            @if($customer->account_manager == Auth::user()->id or $customer->assignedtask() == Auth::user()->name or Auth::user()->group_id=='Admin')

                            @if(isset($customer->cstatus()->duedatetime))

                            @if(strpos($selectedUsers, $customer->assignedtask())!==false
                            && $filterFrom <=$customer->cstatus()->duedatetime
                            && $filterTo >= $customer->cstatus()->duedatetime)
                            <tr>
                                <td><a href="{{url('/customer/')}}/{{$customer->id}}">{{$customer->id}}</a></td>
                                <td>{{$customer->type()}}</td>
                                <td><a href="{{url('/customer/')}}/{{$customer->id}}"> {{$customer->bname}}</a></td>
                                <td>{{$customer->assignedtask()}}</td>
                                <td>{{$customer->accmanager()}}</td>
                                <td>{{$customer->status()->name or ''}}</td>
                                <td>{{isset($customer->cstatus()->duedatetime) ? date("Y-m-d",strtotime($customer->cstatus()->duedatetime)):'unknown'}}</td>

                                {{--
                                <td>{{isset($customer->cstatus()->duedatetime) ? ((date("Y-m-d",strtotime($customer->cstatus()->duedatetime)) < date("Y-m-d"))? "YES":"NO") :'unknown'}}</td>
                                --}}

                            </tr>
                            @endif



                            @endif

                            @endif
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection