<?PHP

use App\Customer;
use App\InvoiceStatus;

$invoiceStatus = new InvoiceStatus();
?>
@extends('layouts.app')
@section('content')
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">
                    <div class="row">
                        <div class="col-md-12">{{$customer->type->type}} - {{$customer->bname}} - ID: {{$customer->id}}</div>                     
                        <div class="col-md-12" id="tabBtnGroup">
                            <span class="btn btn-default btn-margin" id="tabBtn1"> View</span>
                            <span class="btn btn-default btn-margin" id="tabBtn8"> Checklist</span>
                            <span class="btn btn-default btn-margin" id="tabBtn2"> Invoicing Details</span>                            
                            <span class="btn btn-default btn-margin" id="tabBtn3"> New Invoice</span>
                            <!--<span class="btn btn-default btn-margin" id="tabBtn4"> Invoice History</span>-->
                            <!--<span class="btn btn-default btn-margin" id="tabBtn6"> Order History</span>-->
                            <span class="btn btn-default btn-margin" id="tabBtn5"> Transactions</span>
                            <!--<span class="btn btn-default btn-margin" id="tabBtn7"> Deleted Invoices</span>-->
                            <span class="btn btn-default btn-margin" id="tabBtn9"> Notes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Tab 1 Customer view --}}
<div class="fullcontainer customerContentContainer" id="customerContainer1">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default rounded_wrapper">
                <div class="panel-heading page-panelheading">Customer Details
                    <div style="display: inline-block; margin: -5px 5px" class="pull-right btn-margin">
                        <form id="customerDeleteForm{{$customer->id}}" method="POST" action="{{url('/customer/' . $customer->id)}}">
                            {{method_field('DELETE')}}
                            {{ csrf_field() }}
                            <input hidden name="customerid" value="{{$customer->id}}">
                            <span class="btn btn-sm btn-danger sweetDeleteButton"  type="button"  onclick = "DeleteButton({{$customer->id}})">Delete</span>
                        </form>
                    </div>
                    <div style="display: inline-block; margin: -5px 5px" class="pull-right btn-margin">
                        <a href="{{url('/')}}/customer/{{$customer->id}}/edit"><span class="btn btn-success btn-sm" style="margin: 0 5px;">Edit</span></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-7">
                            <dl class="dl-horizontal">
                                <dt>Type</dt>
                                <dd>{{$customer->type->type}}</dd>
                                <dt>Working Name</dt>
                                <dd>{{$customer->bname}}</dd>
                                <dt>Working Phone</dt>
                                <dd>{{formatPhone($customer->bphone)}}</dd>
                                <dt>Working Email</dt>
                                <dd><a href="mailto:{{$customer->bemail}}">{{$customer->bemail}}</a>
                                </dd>
                                <dt>ABN</dt>
                                <dd>{{$customer->abn}}
                                </dd>
                                <dt>First Name</dt>
                                <dd>{{$customer->firstname}}</dd>
                                <dt>Last Name</dt>
                                <dd>{{$customer->lastname}}</dd>
                                <dt>Personal Mobile Phone</dt>
                                <dd>{{formatPhone($customer->phone)}}</dd>
                                <dt>Personal Email</dt>
                                <dd><a href="mailto:{{$customer->email}}">{{$customer->email}}</a></dd>
                                <dt>Website</dt>
                                <dd><a target='_blank'  href="{{$customer->website}}">{{$customer->website}}</a></dd>
                                <?PHP
                                $social = json_decode($customer->extend);
                                if (gettype($social) == 'string') {
                                    $social = json_decode($social);
                                }
                                if ($social != null) {
                                    foreach ($social as $key => $value) {
                                        if ($value != '') {
                                            echo '<dt>' . $key . "</dt><dd><a target='_blank' href=" . $value . ">" . $value . '</a></dd>';
                                        }
                                    }
                                }
                                ?>
                                <br>
                                <dt>Address</dt>
                                @if(strlen($customer->address)>2)
                                <dd>{{$customer->address}}</dd>
                                @endif
                                <dd>@if(isset($customer->suburb)&&$customer->suburb!=NULL){{$customer->suburb}}, {{$customer->state}} {{$customer->postcode}}@endif</dd>
                                <br>
                                <dt>Account Manager</dt>
                                <dd>{{$customer->accmanager()}}</dd>
                                @if($customer->position)
                                <dt>Contact Position</dt>
                                <dd>{{$customer->position()}}</dd>
                                @endif

                                @if($customer->mpm_user_id)
                                <dt>MPM User ID</dt>
                                <dd>{{$customer->mpm_user_id}}</dd>
                                @endif

                                @if($customer->mpm_type_id)
                                <dt>MPM {{$customer->type->type}} ID</dt>
                                <dd>{{$customer->mpm_type_id}}</dd>
                                @endif

                                @if($customer->brothel_id)
                                <dt>Brothel ID</dt>
                                <dd>{{$customer->brothel_id}}</dd>
                                @endif


                            </dl>
                        </div>
                        <div class="col-sm-5">
                            <dl class="dl-horizontal">
                                <?php
                                $customerExtend = (array) json_decode($customer->additional_contacts);
                                if (isset($customerExtend["fname"])) {          // If additiopnal contact exist
                                    $count = count($customerExtend["fname"]);   // Count them
                                    echo "Additional Contacts<hr>";
                                    for ($i = 0; $i < $count; $i++) {                       // for each contact
                                        foreach ($customerExtend as $key => $values) {
                                            if (isset($values[$i]) && $values[$i] != '') {
                                                switch ($key) {
                                                    case 'fname':
                                                        echo '<dt>First Name:</dt>';
                                                        break;
                                                    case 'lname':
                                                        echo '<dt>Last Name:</dt>';
                                                        break;
                                                    case 'phone':
                                                        echo '<dt>Phone:</dt>';
                                                        break;
                                                    case 'position':
                                                        echo '<dt>Position:</dt>';
                                                        break;
                                                    case 'email':
                                                        echo '<dt>Email:</dt>';
                                                        break;
                                                }
                                                echo '<dd>' . $values[$i] . '</dd>';
                                            }
                                        }
                                        echo "<hr>";
                                    }
                                }
                                ?>
                            </dl>
                            <form id="customerStatusForm" method="POST" action=<?= url('cstatus/store') ?>>
                                {{ csrf_field() }}
                                <input name="customerid" value="{{$customer->id}}" hidden>
                                <div class="col-md-10">
                                    <div class="panel panel-default">
                                        <label>Status</label>
                                        <div class="panel-body center-block">
                                            <div class="form-group">
                                                <select id="cstatuses" class="form-control" name="cstatus">
                                                    <option disabled selected>Select...</option>
                                                    @foreach($statuses as $status)
                                                    <option id="{{$status->id or ''}}" <?= ($status->status == "Deactive") ? 'disabled' : '' ?> expdays="{{$status->expdays}}"
                                                            value="{{$status->id or ''}}" <?= isset($customer->status) ? ($customer->status->statusId == $status->id) ? "selected" : "" : ''; ?> >{{$status->name or ''}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <?php $statusHiddenArray = array('9999', '1000', '9000', '9200'); ?>
                                            <div class="form-inline due-date-wrapper <?php echo (isset($customer->status()->id) && in_array($customer->status()->id, $statusHiddenArray)) ? 'hidden' : '' ?>">
                                                <label>Due Date</label>
                                                <br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                                    <input type="text" id="dueDate" class="form-control" name="dueDate" autocomplete="off"
                                                           value="{{isset($customer->status()->dueDatetime)? date(DateTimeFormat,strtotime($customer->status()->dueDatetime)) : ''}}">
                                                </div>
                                            </div>
                                            <button class="btn btn-sm btn-primary" name="update" value="{{$customer->id}}">Update</button>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            {{-- Current orders --}}

                            <div class="col-md-10">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Current Advertisment Platforms</div>
                                    <div class="panel-body center-block">
                                        @if(isset($currentOrders)&&!empty($currentOrders))
                                        @foreach($currentOrders as $currentorder)
                                        <a class="btn btn-lg btn-default" href="{{url('/')}}/invoice/{{$currentorder->invoiceid}}">{{$currentorder->sitename}} - {{$currentorder->page}} - {{$currentorder->position}}</a>
                                        @endforeach
                                        @endif
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Tab 7 invoicing details --}}

<div class="fullcontainer customerContentContainer" id="customerContainer8">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default rounded_wrapper">
                <div class="panel-heading page-panelheading">
                    <p>Customer Checklist</p>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="POST" action="{{ url('/') }}/task/store">
                                {{ csrf_field() }}
                                <input name="customerID" value="{{$customer->id}}" type="hidden"/>
                                <div class="form-group customerChecklist">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="mpmchecklist" <?php echo (isset($customerTasks->mpmchecklist) && !empty($customerTasks->mpmchecklist)) ? ' checked ' : '' ?> id="mpmChecklist" value="mpmchecklist">
                                            MPM
                                        </label>
                                    </div>
                                    <div class="mpmchecklistdetails <?php echo (isset($customerTasks->mpmchecklist) && !empty($customerTasks->mpmchecklist)) ? '' : 'hidden' ?>">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->mpmdetailsfilled) && !empty($customerTasks->mpmdetailsfilled)) ? ' checked ' : '' ?> type="checkbox" name="mpmdetailsfilled">
                                                    Details Filled Out
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->mpmconfirmplan) && !empty($customerTasks->mpmconfirmplan)) ? ' checked ' : '' ?>  type="checkbox" name="mpmconfirmplan">
                                                    Confirm Plan
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="col-form-label">
                                                    Base location
                                                </label>
                                                <input class="form-check-input" <?php echo (isset($customerTasks->mpmbaselocation) && !empty($customerTasks->mpmbaselocation)) ? ' value="' . $customerTasks->mpmbaselocation . '" ' : '' ?>  type="text" name="mpmbaselocation">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->mpmpaid) && !empty($customerTasks->mpmpaid)) ? ' checked ' : '' ?> type="checkbox" name="mpmpaid">
                                                    Paid
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->mpmapprovedpics) && !empty($customerTasks->mpmapprovedpics)) ? ' checked ' : '' ?> type="checkbox" name="mpmapprovedpics">
                                                    Approved Pics
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->mpmmainimage) && !empty($customerTasks->mpmmainimage)) ? ' checked ' : '' ?> type="checkbox" name="mpmmainimage">
                                                    Main Image Selected
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->mpmseo) && !empty($customerTasks->mpmseo)) ? ' checked ' : '' ?> type="checkbox" name="mpmseo">
                                                    SEO Written
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->mpmlive) && !empty($customerTasks->mpmlive)) ? ' checked ' : '' ?> type="checkbox" name="mpmlive">
                                                    Live
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="bchecklist" <?php echo (isset($customerTasks->bchecklist) && !empty($customerTasks->bchecklist)) ? ' checked ' : '' ?> id="bChecklist" value="abchecklist">
                                            Brothels
                                        </label>
                                        <div class="customerChecklist bchecklistdetails <?php echo (isset($customerTasks->bchecklist) && !empty($customerTasks->bchecklist)) ? '' : 'hidden' ?>">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->bdelaconcreated) && !empty($customerTasks->bdelaconcreated)) ? ' checked ' : '' ?> type="checkbox" name="bdelaconcreated">
                                                        Delacon Created
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        Delacon ID #
                                                    </label>
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->bdelaconid) && !empty($customerTasks->bdelaconid)) ? ' value="' . $customerTasks->bdelaconid . '" ' : '' ?> type="text" name="bdelaconid">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->bprofilecreated) && !empty($customerTasks->bprofilecreated)) ? ' checked ' : '' ?> type="checkbox" name="bprofilecreated">
                                                        Profile Created
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-check-label">Delacon Phone #
                                                    </label>
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->bdelaconphone) && !empty($customerTasks->bdelaconphone)) ? ' value="' . $customerTasks->bdelaconphone . '" ' : '' ?> type="text" name="bdelaconphone">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->bdescription) && !empty($customerTasks->bdescription)) ? ' checked ' : '' ?> type="checkbox" name="bdescription">
                                                        Description
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->bdapprovedc) && !empty($customerTasks->bdapprovedc)) ? ' checked ' : '' ?> type="checkbox" name="bdapprovedc">
                                                        Description Approved by Client
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input"  <?php echo (isset($customerTasks->b150200c) && !empty($customerTasks->b150200c)) ? ' checked ' : '' ?> type="checkbox" name="b150200c">
                                                        Image (150x200) Created
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->biapprovedc) && !empty($customerTasks->biapprovedc)) ? ' checked ' : '' ?> type="checkbox" name="biapprovedc">
                                                        Image Approved by Client
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->baddtoloc) && !empty($customerTasks->baddtoloc)) ? ' checked ' : '' ?> type="checkbox" name="baddtoloc">
                                                        Added Brothel To Location
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input"  <?php echo (isset($customerTasks->b500500c) && !empty($customerTasks->b500500c)) ? ' checked ' : '' ?> type="checkbox" name="b500500c">
                                                        Social Media Image (500x500) Created
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->bsmiapprovedc) && !empty($customerTasks->bsmiapprovedc)) ? ' checked ' : '' ?> type="checkbox" name="bsmiapprovedc">
                                                        Social Media Image Approved by Client
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->blive) && !empty($customerTasks->blive)) ? ' checked ' : '' ?> type="checkbox" name="blive">
                                                        Live
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" <?php echo (isset($customerTasks->abchecklist) && !empty($customerTasks->abchecklist)) ? ' checked ' : '' ?> type="checkbox" name="abchecklist" id="abchecklist" value="abchecklist">
                                            Asian Brothels
                                        </label>
                                        <div class="customerChecklist abchecklistdetails <?php echo (isset($customerTasks->abchecklist) && !empty($customerTasks->abchecklist)) ? '' : 'hidden' ?>">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->abdelaconcreated) && !empty($customerTasks->abdelaconcreated)) ? ' checked ' : '' ?> type="checkbox" name="abdelaconcreated">
                                                        Delacon Created
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        Delacon ID #
                                                    </label>
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->abdelaconid) && !empty($customerTasks->abdelaconid)) ? ' value="' . $customerTasks->abdelaconid . '" ' : '' ?> type="text" name="abdelaconid">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->abprofilecreated) && !empty($customerTasks->abprofilecreated)) ? ' checked ' : '' ?> type="checkbox" name="abprofilecreated">
                                                        Profile Created Via Post
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        Delacon Phone #
                                                    </label>
                                                    <input class="form-check-input" <?php echo (isset($customerTasks->abdelaconphone) && !empty($customerTasks->abdelaconphone)) ? ' value="' . $customerTasks->abdelaconphone . '" ' : '' ?> type="text" name="abdelaconphone">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->abseo) && !empty($customerTasks->abseo)) ? ' checked ' : '' ?> type="checkbox" name="abseo">
                                                        SEO Info
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->ab500292created) && !empty($customerTasks->ab500292created)) ? ' checked ' : '' ?> type="checkbox" name="ab500292created">
                                                        Image (500x292) Created
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->abi500292ca) && !empty($customerTasks->abi500292ca)) ? ' checked ' : '' ?> type="checkbox" name="abi500292ca">
                                                        Image (500x292) Approved By Client
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->absmicreated) && !empty($customerTasks->absmicreated)) ? ' checked ' : '' ?> type="checkbox" name="absmicreated">
                                                        Social Media Image Created
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->absmiabc) && !empty($customerTasks->absmiabc)) ? ' checked ' : '' ?> type="checkbox" name="absmiabc">
                                                        Social Media Image Approved by Client
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" <?php echo (isset($customerTasks->ablive) && !empty($customerTasks->ablive)) ? ' checked ' : '' ?> type="checkbox" name="ablive">
                                                        Live
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button style="margin-left:15px" class="btn btn-success btn-sm">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Tab 2 invoicing details --}}
<div class="fullcontainer customerContentContainer" id="customerContainer2">
    <form class="form-horizontal" id="addabnForm" role="form" method="POST" action=<?= url('/') . "/addabn/store" ?> >
        {{ csrf_field() }}
        <input  maxlength="10" name="customerid" value="{{$customer->id}}" hidden>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default rounded_wrapper">
                    <div class="panel-heading page-panelheading">Update Invoicing Details
                        <div style="display: inline-block; margin: -5px 5px" class="pull-right">
                            <button id="updateAddabnButton" class="btn btn-success btn-sm" type="submit"
                            <?PHP
                            if (isset($addabn->emailmethod) && isset($addabn->addressmethod) && isset($addabn->poboxmethod)) {
                                if ($addabn->emailmethod != 1 && $addabn->addressmethod != 1 && $addabn->poboxmethod != 1) {
                                    echo "disabled";
                                }
                            } else {
                                echo "disabled";
                            }
                            ?>>Update</button>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <div  class="currentInvDetailsContainer">
                                    @if((!isset($addabn->invname)))
                                    <div class="currentInvDetails">
                                        <dl class="dl-horizontal">
                                            @if(!isset($addabn->invname))
                                            @if($customer->bname)
                                            <dt>Working Name: </dt>
                                            <dd>{{$customer->bname or ""}}</dd>
                                            <input maxlength="50" id="workingName" value="{{$customer->bname}}" hidden>
                                            <br>
                                            Invoicing Name is same as Working name?<span id="sameInvName" class="btn btn-primary col-md-12 btn-margin">YES</span>
                                            <hr>
                                            @endif
                                            @endif
                                            @if($customer->abn)
                                            <dt>Working ABN: </dt>
                                            <dd>{{$customer->abn or ""}}</dd>
                                            <input maxlength="15"  id="workingABN" value="{{$customer->abn}}" hidden>
                                            <br>
                                            Invoicing ABN is same as Working ABN?<span id="sameInvABN" class="btn btn-primary col-md-12 btn-margin">YES</span>
                                            @endif
                                            <hr>
                                        </dl>
                                    </div>
                                    @endif
                                    <div class="currentInvDetails">
                                        <dl class="dl-horizontal">
                                            <dt>Invoicing Name: </dt>
                                            <dd>{{$addabn->invname or ""}}</dd>
                                            <dt>Invoicing ABN: </dt>
                                            <dd>{{$addabn->abn or ""}}</dd>
                                        </dl>
                                    </div>
                                    <dl class="dl-horizontal">
                                        <input id="btype" name="btype" value="{{$customer->type->type}}" hidden>
                                        {{-- ******* Invoice Name   ************ --}}
                                        <div id="invnameDiv" class="form-group">
                                            <label for="invname" class="col-md-4 control-label">Invoicing Name</label>
                                            <div class="col-md-6">
                                                <input maxlength="50" placeholder="Invoicing Name" id="invNameInput" class="form-control" value="{{$addabn->invname or ""}}" name="invname">
                                            </div>
                                        </div>
                                        {{-- ********  Invoice Abn  ************** --}}
                                        <div id="invabnDiv"  class="form-group">
                                            <label for="invabn" class="col-md-4 control-label">Invoicing ABN</label>
                                            <div class="col-md-6">
                                                <input maxlength="15"  id="invABNInput"  class="form-control abnInput" value="{{$addabn->abn or ''}}" name="invabn" placeholder="00 000 000 000">
                                            </div>
                                        </div>
                                        {{-- ********  Invoice MYOB  ************** --}}
                                        <div id="invmyobDiv"  class="form-group">
                                            <label for="invmyobisInput" class="col-md-4 control-label">MYOB ID Number</label>
                                            <div class="col-md-5">
                                                <input id="invmyobisInput"  class="form-control abnInput" value="{{$addabn->myobIdNumber or ''}}" name="invmyobisInput">
                                            </div>
                                            <div class="col-md-3">
                                                <button type="button" class="myob-company-search btn btn-primary btn-sm"><i class="fa fa-btn fa-search"></i></button>
                                                <div class="col-lg-10 col-lg-offset-1 myob-company-search-table-container popup hidden">

                                                </div>
                                            </div>
                                        </div>
                                    </dl>
                                </div>
                            </div>
                            <div class="col-sm-7">

                                <div  class="currentInvDetailsContainer">
                                    <div class="currentInvDetails">
                                        <div class="dl-horizontal">Mathod of Invoice - At least one method is required</div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="col-md-3">
                                                    <input  id='invemailCk' name="emailopt" type="checkbox" value="1" class="form-control" <?= isset($addabn->emailmethod) ? ($addabn->emailmethod == 1) ? 'checked' : '' : '' ?>>
                                                </div>
                                                <div class="col-md-6">
                                                    Email
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-3">
                                                    <input id='invaddressCk' name="addressopt" type="checkbox" value="1" class="form-control" <?= isset($addabn->addressmethod) ? ($addabn->addressmethod == 1) ? 'checked' : '' : '' ?>>
                                                </div>
                                                <div class="col-md-6">
                                                    Address
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-3">
                                                    <input id='invpoboxCk' name="poboxopt" type="checkbox" value="1" class="form-control"  <?= isset($addabn->poboxmethod) ? ($addabn->poboxmethod == 1) ? 'checked' : '' : '' ?>>
                                                </div>
                                                <div class="col-md-6">
                                                    PO BOX
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="invemailDiv" class="currentInvDetailsContainer" <?= isset($addabn->emailmethod) ? $addabn->emailmethod == 0 ? 'style="display:none"' : 'style="display:block"' : 'style="display:none"' ?>>

                                    @if(!isset($addabn->invemail))
                                    @if($customer->bemail !='')
                                    <div class="currentInvDetails">

                                        <dl class="dl-horizontal">
                                            <dt>Working Email:</dt>
                                            <dd><a href="mailto:{{$customer->bemail}}">{{$customer->bemail or ''}}</a></dd>
                                            @if(isset($customer->bemail))
                                            <br>
                                            <input id="workingEmail" value="{{$customer->bemail}}" hidden>
                                            Invoicing Email is same as Working Email?<span id="sameInvEmail" class="btn btn-primary col-md-12 btn-margin">YES</span>
                                            @endif
                                            <hr>
                                        </dl>
                                    </div>
                                    @endif
                                    @endif

                                    <div class="currentInvDetails">
                                        <dl class="dl-horizontal">
                                            <dt>Invoicing Email: </dt>
                                            <dd><a href="mailto:{{$addabn->invemail or ''}}">{{$addabn->invemail or ''}}</a></dd>
                                            <hr>
                                        </dl>
                                    </div>
                                    <dl class="dl-horizontal">
                                        {{-- *******  Invoicing Email ************** --}}
                                        <div class="form-group">
                                            <label for="invemail" class="col-md-3 control-label">Email</label>
                                            <div class="col-md-6">
                                                <input placeholder="Invoicing Email" maxlength="50"  id="invemail"  class="form-control" value="{{$addabn->invemail or ''}}" name="invemail">
                                            </div>
                                        </div>

                                    </dl>


                                </div>


                                <div id="invaddressDiv"  <?= isset($addabn->addressmethod) ? $addabn->addressmethod == 0 ? 'style="display:none"' : 'style="display:block"' : 'style="display:none"' ?>>
                                    <div  class="currentInvDetailsContainer">
                                        @if(!isset($addabn->postcode) && isset($customer->postcode) && $customer->postcode!='')
                                        <div class="currentInvDetails">
                                            <dl class="dl-horizontal">
                                                <dt>Working Address: </dt>
                                                <dd>{{$customer->address}} @if(isset($customer->suburb)&&$customer->suburb!=NULL){{$customer->suburb}}, {{$customer->state}} {{$customer->postcode}}@endif</dd>
                                                @if((isset($customer->address) || isset($customer->suburb)))
                                                <br>
                                                Invoicing Address is same as Working Address?<span id="sameInvAdd"  class="btn btn-primary col-md-12 btn-margin">YES</span>
                                                <?PHP
                                                //** To Convert address from old format of one string to separated fields
                                                $customer->address = rtrim($customer->address, ' ');
                                                if (strpos($customer->address, '/') !== false) {
                                                    $customer->findunit = explode('/', $customer->address);
                                                    $customer->unit = $customer->findunit[0];
                                                    $customer->address = $customer->findunit[1];
                                                }
                                                $customer->exp = explode(' ', $customer->address);
                                                $customer->counts = count($customer->exp);
                                                if ($customer->counts == 3) {
                                                    $customer->number = $customer->exp[0];
                                                    $customer->street = $customer->exp[1];
                                                    $customer->sttype = $customer->exp[2];
                                                }
                                                if ($customer->counts == 4) {
                                                    $customer->number = $customer->exp[0];
                                                    $customer->street = $customer->exp[1] . ' ' . $customer->exp[2];
                                                    $customer->sttype = $customer->exp[3];
                                                }
                                                if ($customer->counts == 5) {
                                                    $customer->unit = $customer->exp[0] . ' ' . $customer->exp[1];
                                                    $customer->unit = rtrim($customer->unit, ",");
                                                    $customer->number = $customer->exp[2];
                                                    $customer->street = $customer->exp[3];
                                                    $customer->sttype = $customer->exp[4];
                                                }
                                                ?>
                                                <input id="workingUnit" value="{{$customer->unit or ''}}" hidden>
                                                <input id="workingNumber" value="{{$customer->number or ''}}" hidden >

                                                <input id="workingStreet" value="{{$customer->street or ''}}"  hidden>
                                                <input id="workingSttype" value="{{$customer->sttype or ''}}"  hidden>

                                                <input id="workingSuburb" value="{{$customer->suburb or ''}}" hidden >
                                                <input id="workingState" value="{{$customer->state or ''}}"  hidden>
                                                <input id="workingPostcode" value="{{$customer->postcode or ''}}"  hidden>

                                                @endif
                                                <hr>
                                            </dl>

                                        </div>

                                        @endif

                                        <div class="currentInvDetails">
                                            <dl class="dl-horizontal">
                                                <dt>Invoicing Address: </dt>
                                                <dd>{{$addabn->unit or ''}}{{isset($addabn->unit) && $addabn->unit !='' ? '/' : ''}}{{$addabn->number or ''}} {{$addabn->street or ''}} {{$addabn->sttype or ''}}, {{$addabn->suburb or ''}}, {{$addabn->state or ''}} {{$addabn->postcode or ''}}</dd>
                                                <hr>
                                            </dl>

                                        </div>
                                        <dl class="dl-horizontal">
                                            {{-- *******  Address ************** --}}
                                            <div class="form-group">
                                                <label for="unit" class="col-md-3 control-label">Unit</label>
                                                <div class="col-md-3">
                                                    <input placeholder="Unit" maxlength="10"  id="invUnitInput"  class="form-control" value="{{$addabn->unit or ''}}" name="unit">
                                                </div>


                                                <label for="number" class="col-md-3 control-label">Number</label>
                                                <div class="col-md-3">
                                                    <input placeholder="Number" maxlength="20" id="invNumberInput"  class="form-control" value="{{$addabn->number or ''}}" name="number">
                                                </div>
                                            </div>
                                            <div class="form-group">

                                                <label for="street" class="col-md-3 control-label">Street</label>
                                                <div class="col-md-3">
                                                    <input maxlength="30" placeholder="Street" id="invStreetInput"  class="form-control" value="{{$addabn->street or ''}}" name="street">
                                                </div>

                                                <label for="searchSttype" class="col-md-3 control-label">Street Type</label>
                                                <div class="col-md-3">
                                                    <input maxlength="15" placeholder="Type" name="searchSttype" type="text" id="searchSttype" class="form-control"  autocomplete="off" value="{{$addabn->sttype or ''}}" >
                                                </div>
                                            </div>
                                            <div class="form-group">

                                                <label for="postcode" class="col-md-3 control-label">Postcode Lookup</label>
                                                <div class="col-md-3">
                                                    <input name="searchPostcode" type="text" placeholder="Search Suburb or Postcode" id="SearchPostcodes" class="form-control" value="{{(isset($addabn->postcode)&& $addabn->postcode!='') ? $addabn->postcode.', '.$addabn->suburb.', '.$addabn->state : ''}}"  autocomplete="off">
                                                    <input name="LockSearch" readonly type="text" placeholder="Search Suburb or Postcode" id="LockSearch" class="form-control" value="{{$addabn->postcode or ''}}, {{$addabn->suburb or ''}}, {{$addabn->state or ''}}" autocomplete="off">

                                                    <input style="display: none" name="postcode" type="text" id="postcode" class="form-control" value="{{$addabn->postcode or ''}}">
                                                    <input style="display: none" name="suburb" type="text" id="suburb" class="form-control" value="{{$addabn->suburb or ''}}">
                                                    <input style="display: none" name="state" type="text" id="state" class="form-control" value="{{$addabn->state or ''}}">
                                                </div>
                                            </div>
                                        </dl>
                                    </div>
                                </div>
                                <div id="invpoboxDiv"  <?= isset($addabn->poboxmethod) ? $addabn->poboxmethod == 0 ? 'style="display:none"' : 'style="display:block"' : 'style="display:none"' ?>>
                                    <div  class="currentInvDetailsContainer">
                                        <div class="currentInvDetails">
                                            <dl class="dl-horizontal">
                                                <dt>Invoicing PO BOX:</dt>
                                                <dd>{{$addabn->invpoboxno or ''}} {{$addabn->invpoboxsuburb or ''}}, {{$addabn->invpoboxstate or ''}} {{$addabn->invpoboxpostcode or ''}}</dd>
                                                <hr>
                                            </dl>
                                        </div>
                                        <dl class="dl-horizontal">
                                            {{-- *******  PO BOX ************** --}}
                                            <div class="form-group">
                                                <label for="invpoboxno" class="col-md-3 control-label">PO BOX</label>
                                                <div class="col-md-3">
                                                    <input maxlength="10" placeholder="PO BOX" id="invpoboxnoInput"  class="form-control" value="{{$addabn->invpoboxno or ''}}" name="invpoboxno">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="invpoboxpostcodesearch" class="col-md-3 control-label">Postcode Lookup</label>
                                                <div class="col-md-3">
                                                    <input name="invpoboxpostcodesearch" type="text" placeholder="Search Suburb or Postcode" id="invpoboxpostcodesearch" class="form-control" value="{{(isset($addabn->invpoboxpostcode)&& $addabn->invpoboxpostcode!='') ? $addabn->invpoboxpostcode.', '.$addabn->invpoboxsuburb.', '.$addabn->invpoboxstate : ''}}"  autocomplete="off">
                                                    <input name="invpoboxLockSearch" readonly type="text" placeholder="Search Suburb or Postcode" id="invpoboxpostcodeSearch" class="form-control" value="{{$addabn->invpoboxpostcode or ''}}, {{$addabn->invpoboxsuburb or ''}}, {{$addabn->invpoboxstate or ''}}" autocomplete="off">

                                                    <input style="display: none"  name="invpoboxpostcode" type="text" id="invpoboxpostcode" class="form-control" value="{{$addabn->invpoboxpostcode or ''}}">
                                                    <input style="display: none"  name="invpoboxsuburb" type="text" id="invpoboxsuburb" class="form-control" value="{{$addabn->invpoboxsuburb or ''}}">
                                                    <input style="display: none"  name="invpoboxstate" type="text" id="invpoboxstate" class="form-control" value="{{$addabn->invpoboxstate or ''}}">
                                                </div>
                                            </div>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>

{{-- Tab 3 New Invoice --}}
<div class="fullcontainer customerContentContainer" id="customerContainer3">
    <form class="form-horizontal" id="invoiceForm" role="form" method="POST" action=<?= url('/') . "/invoice" ?> >
        {{ csrf_field() }}
        <input name="customerid" value="{{$customer->id or ''}}" hidden>
        <input name="addabn" value="{{$addabn->id or ''}}" hidden>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default rounded_wrapper">
                    <div class="panel-heading page-panelheading">Create New Invoice
                        @if(!isset($addabn->id) or $addabn->id == NULL)
                        <span class="error"> - Unable to create invoice. Set Invoicing Details</span>
                        @else
                        <div style="display: inline-block; margin: -5px 5px" class="pull-right">
                            <button id="updateAddabnButton" class="btn btn-success btn-sm" type="submit">Add</button>
                        </div>
                        @endif
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @if(isset($addabn->id) AND $addabn->id != NULL)
                            <div class="col-sm-5">
                                <dl class="dl-horizontal">
                                    <div class="borderedDiv">
                                        <div class="borderedTitle">Invoice details </div>
                                        {{-- ******* created at Name   ************ --}}
                                        <div id="createdAtDiv" class="form-group">
                                            <label for="created_at" class="col-md-4 control-label">Invoice Created at</label>
                                            <div class="col-md-6">
                                                <input id="invoiceCreatedAt" placeholder="Created at" class="form-control" name="created_at" value="{{date(DateFormat)}}">
                                            </div>
                                        </div>
                                        {{-- ******* MYOB Name   ************ --}}
                                        <div id="myobDiv" class="form-group">
                                            <label for="myob" class="col-md-4 control-label">MYOB Invoice No</label>
                                            <div class="col-md-6">
                                                <input maxlength="15" placeholder="MYOB Invoice No" class="form-control" value="{{$invoice->myob or ""}}" name="myob">
                                            </div>
                                        </div>
                                        {{-- ********  Method ************** --}}
                                        <div id="paymentmethodDiv"  class="form-group">
                                            <label for="payment_method" class="col-md-4 control-label">Payment Method</label>
                                            <div class="col-md-6">
                                                <select class="form-control" name="payment_method">
                                                    <option disabled selected>Select...</option>

                                                    @foreach($paymentmethod as $method)
                                                    <option value="{{$method->method}}" <?= isset($invoice->payment_method) ? (($invoice->payment_method == $method->method) ? 'selected' : '') : '' ?>>{{$method->method}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>{{-- ********  Description date************** --}}
                                        <div id="descriptionDiv"  class="form-group">
                                            <label for="description" class="col-md-4 control-label">Description</label>
                                            <div class="col-md-6">
                                                <textarea id="description" class="form-control" value="{{$invoice->description or ''}}" name="description"  type="text" style="resize: none; height: 7em;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </dl>
                            </div>
                            <div class="col-sm-7">
                                <dl class="dl-horizontal">
                                    <div class="borderedDiv">
                                        <div class="borderedTitle">Add Order</div>
                                        {{-- ********  Websites - Advertising platform ************** --}}
                                        <div class="ordersInputs">
                                            <div id="sitesDiv"  class="form-group">
                                                <label for="sites" class="col-md-3 control-label">Advertising Platform</label>
                                                <div class="col-md-3">
                                                    <select id="websites" class="form-control" name="sites">
                                                        <option selected value="">Please Select...</option>
                                                        @foreach($websites as $website)
                                                        <option value="{{$website->id}}">{{$website->website}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="error"></span>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></div>
                                                        <input id="orderamount" class="form-control" name="orderamount" type="text" placeholder="0.00">
                                                        <span class="error"></span>
                                                    </div>
                                                </div>
                                                <!--                                                <div class="col-md-3">
                                                                                                    <div class="input-group">
                                                                                                        <div class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></div>
                                                                                                        <input id="gstcalcalculator" class="form-control" name="gstcalcalculator" type="text" placeholder="GST Calculator" title="Enter total amount, the subtotal will automatically be calculated">
                                                                                                        <span class="input-group-addon gstcalcalculator-info"><i style="color: #337ab7;" class="fa fa-info"></i></span>
                                                                                                        <span class="error"></span>
                                                                                                    </div>
                                                                                                </div>-->
                                            </div>
                                            <div id="sitesDiv"  class="form-group">
                                                <label for="orderoption" class="col-md-3 control-label">Invoice Description</label>
                                                <div class="col-md-3">
                                                    <select id="orderoption" class="form-control" name="orderoption">
                                                        <option selected value="">Please Select...</option>
                                                        @foreach($orderoptions as $orderoption)
                                                        <option value="{{$orderoption->orderoption}}">{{$orderoption->orderoption}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="error"></span>
                                                </div>
                                            </div>
                                            <div id="pagepositionDiv"  class="form-group">
                                                <label for="page" class="col-md-3 control-label">Page</label>
                                                <div class="col-md-3">
                                                    <select id="orderpage" class="form-control" name="orderpage">
                                                        <option selected value="">Please Select...</option>
                                                        @foreach($orderpages as $orderpage)
                                                        <option extstr="{{str_replace(' ', '_',$orderpage->page)}}" value="{{$orderpage->page}}">{{$orderpage->page}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="error"></span>
                                                </div>
                                                <label for="page" class="col-md-1 control-label">Position</label>
                                                <div class="col-md-2">
                                                    <input maxlength="10" placeholder="" class="form-control" name="position" id="orderposition">
                                                    <span class="error"></span>
                                                </div>
                                                <div class="col-md-1">
                                                    <span id="addorderbutton" class="btn btn-primary glyphicon glyphicon-plus"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <!--  Order dates -->

                                        {{-- ******** order start date************** --}}
                                        <div id="orderstartdateDiv"  class="form-group">
                                            <label for="orderstartdate" class="col-md-3 control-label">Advertising Start Date</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                                    <input placeholder="Start Date" id="orderstartdate" class="form-control" name="orderstartdate" autocomplete="off" type="text">

                                                </div>
                                                <span class="error" id="orderstartdateerror"></span>
                                            </div>
                                        </div>
                                        {{-- ******** order frequency date************** --}}
                                        <div id="orderfrequencyDiv"  class="form-group">
                                            <label for="orderfrequency" class="col-md-3 control-label">Frequency</label>
                                            <div class="col-md-3">
                                                <select class="form-control" name="orderfrequency" id="orderfrequency">
                                                    <option selected value="">Please Select...</option>
                                                    <option value="Weekly">Weekly</option>
                                                    <option value="Fortnightly">Fortnightly</option>
                                                    <option value="Monthly">Monthly</option>
                                                    <option value="Quarterly">Quarterly</option>
                                                    <option value="Bi-annually">Bi-annually</option>
                                                    <option value="Annually">Annually</option>
                                                </select>
                                            </div>
                                            <span class="error" id="orderfrequencyerror"></span>

                                        </div>
                                        {{-- ******** order expiry date************** --}}
                                        <div id="orderexpirydateDiv"  class="form-group">
                                            <label for="orderexpirydate" class="col-md-3 control-label">Advertising Expiry Date</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                                    <input placeholder="Expiry Date" id="orderexpirydate" class="form-control" name="orderexpirydate" autocomplete="off" type="text">
                                                </div>
                                                <span class="error" id="orderexpirydateerror"></span>
                                            </div>
                                        </div>
                                        {{-- ******** order due date************** --}}
                                        <div id="orderduedateDiv"  class="form-group">
                                            <label for="orderduedate" class="col-md-3 control-label">Invoice Reminder</label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                                    <input id="orderduedate" placeholder="Reminder Date" class="form-control" name="orderduedate" autocomplete="off" type="text">
                                                </div>
                                                <span class="error" id="orderduedateerror"></span>
                                            </div>
                                        </div>
                                        <!-- End order dates -->

                                        <div id="accountSelect"  class="form-group">
                                            <label for="orderduedate" class="col-md-3 control-label">MYOB Account</label>
                                            <div class="col-md-3">
                                                <select class="form-control" id="from-myob-account">
                                                    <option value="-1">Select Account</option>
                                                    <option value="mpm">MPM Service Account</option>
                                                    <option value="brothels">Brothels Service Account</option>
                                                </select>
                                                <span class="error" id="orderduedateerror"></span>
                                            </div>
                                        </div>
                                        <input name="orders" id="orders" value="" hidden>
                                    </div>
                                </dl>
                            </div>
                            <hr>
                            <div class="row">
                                <div id="ordersTableContainer" class="col-md-11">
                                    <table id="ordersTable">
                                        <thead>
                                            <tr class="orderTableTitles">
                                                <th>Platform</th>
                                                <th>Description</th>
                                                <th>Page</th>
                                                <th>Position</th>
                                                <th>Start</th>
                                                <th>Frequency</th>
                                                <th>Expiry</th>
                                                <th>Reminder</th>
                                                <th></th>
                                                <th class="amountColumn">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr><td colspan="7" class="amountColumn">Sub Total</td><td></td><td  class="amountColumn" >$</td><td class="amountColumn" id="invoicesubtotal">0.00</td><td></td></tr>
                                            <tr><td colspan="7" class="amountColumn">GST</td><td></td><td  class="amountColumn" >$</td><td class="amountColumn" id="invoicegst">0.00</td><td></td></tr>
                                            <tr><td colspan="7" class="amountColumn">Total Inc GST</td><td></td><td  class="amountColumn" >$</td><td class="amountColumn" id="invoicetotal">0.00</td><td></td></tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

{{-- Tab 4 Invoice History --}}
<div class="fullcontainer customerContentContainer" id="customerContainer4">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">                
                <div class="panel-body rounded_wrapper"> 
                    <div class="panel-heading page-panelheading">Listing Invoices</div>                     
                    <table class="invoiveListTable">
                        <thead>
                            <tr>
                                <th>MYOB</th>
                                <th>Created at</th>
                                <th>No of Platforms</th>
                                <th hidden>Start Date</th>
                                <th hidden>Expiry Date</th>
                                <th hidden>Frequency</th>
                                <th>Created by</th>
                                <th>Payment Method</th>
                                <th class="amountColumn">Amount</th>
                                <th class="amountColumn">Paid</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($customer->invoices() as $invoice)

                            <tr class="clickable-row" data-target="_blank" data-href="<?= url('/') . "/invoice/$invoice->id" ?>">
                                <td>{{$invoice->myob}}</td>
                                <td><span hidden>{{date("Ymd",strtotime($invoice->created_at))}}</span>{{date(DateFormat,strtotime($invoice->created_at))}}</td>
                                <td>{{count(explode(',',$invoice->sitenames))}}</td>
                                <td hidden><span hidden>{{date("Ymd",strtotime($invoice->startdate))}}</span>{{date(DateFormat,strtotime($invoice->startdate))}}</td>
                                <td hidden><span hidden>{{date("Ymd",strtotime($invoice->expirydate))}}</span>{{date(DateFormat,strtotime($invoice->expirydate))}}</td>
                                <td hidden>{{$invoice->frequency}}</td>
                                <td>{{$invoice->name}}</td>
                                <td>{{$invoice->payment_method}}</td>
                                <td class="amountColumn">$ {{number_format((array_sum(explode(',',$invoice->amounts)))*1.1,2)}}</td>
                                <td class="amountColumn">$ {{ number_format($customer->amountPaidforInvoice($invoice->id),2)}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Tab 6 Orders History --}}
<div class="fullcontainer customerContentContainer" id="customerContainer6">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">                
                <div class="panel-body rounded_wrapper">                      
                    <div class="panel-heading page-panelheading">Listing Orders</div> 
                    <table class="invoiveListTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Platform</th>
                                <th>Page</th>
                                <th>Position</th>
                                <th>Start Date</th>
                                <th>Frequency</th>
                                <th>Expiry Date</th>
                                <th>Reminder Date</th>
                                <th class="amountColumn">Amount</th>
                                <th>Active</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?PHP $thiscustomer = Customer::find($customer->id); ?>
                            @foreach($thiscustomer->orders() as $order)
                            <tr>
                                <td><a target="_blank" href="<?= url('/') . "/invoice/$order->invoiceid" ?>">{{$order->invoiceid}}</a></td>
                                <td><a target="_blank" href="<?= url('/') . "/invoice/$order->invoiceid" ?>">{{$order->sitename}}</a></td>
                                <td>{{$order->page}}</td>
                                <td>{{$order->position}}</td>
                                <td><span hidden>{{date("Ymd",strtotime($order->startdate))}}</span>{{date(DateFormat,strtotime($order->startdate))}}</td>
                                <td>{{$order->frequency}}</td>
                                <td><span hidden>{{date("Ymd",strtotime($order->expirydate))}}</span>{{date(DateFormat,strtotime($order->expirydate))}}</td>
                                <td><span hidden>{{date("Ymd",strtotime($order->duedate))}}</span>{{date(DateFormat,strtotime($order->duedate))}}</td>
                                <td class="amountColumn">$ {{ number_format($order->amount,2)}}</td>
                                <td class="<?= $order->active == 1 ? 'text-success' : 'text-danger' ?>"><?= $order->active == 1 ? 'Active' : 'Deactive' ?></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



{{-- Tab 5 Transactions History --}}

<div class="fullcontainer customerContentContainer" id="customerContainer5">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">                
                <div class="panel-body rounded_wrapper">                      
                    <div class="panel-heading page-panelheading">Listing Transactions</div> 
                    <?PHP
                    $transactions = array_merge($customer->invoices(), $payments);

                    function cmp($a, $b) {
                        return strcmp($a->created_at, $b->created_at);
                    }

                    //var_dump($transactions);
                    usort($transactions, "cmp");
                    ?>
                    <table id="transactionsListTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Created at</th>
                                <th hidden>Inv ID</th>
                                <th>MYOB</th>
                                <th hidden>Start Date</th>
                                <th hidden>Frequency</th>
                                <th>Invoice Status</th>
                                <th class="sum amountColumn" style="text-align: right!important;">Invoice Amount</th>
                                <th class="sum amountColumn" style="text-align: right!important;">Payment Received</th>
                                <th class="amountColumn" style="text-align: right!important;">Balance</th>
                                <th class="amountColumn" style="text-align: right!important;">Paid For Invoice</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?PHP $remain = 0; ?>
                            <?PHP $totalInvoiced = 0; ?>
                            @foreach($transactions as $transaction)
                            <tr class="clickable-row" data-target="_blank" data-href="<?= url('/') . "/invoice/$transaction->invoiceid" ?>">
                                <td>{{$transaction->invoiceid}}</td>
                                <td>{{date(DateFormat,strtotime($transaction->created_at))}}</td>
                                <td hidden>
                                    <?PHP
                                    if (isset($transaction->invoiceid)) {
                                        echo $transaction->invoiceid;
                                    } else {
                                        echo $transaction->id;
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?PHP
                                    if (isset($transaction->myob)) {
                                        echo $transaction->myob;
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>

                                <td hidden>
                                    <?PHP
                                    if (isset($transaction->startdate)) {
                                        echo date(DateFormat, strtotime($transaction->startdate));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>
                                <td hidden>
                                    <?PHP
                                    if (isset($transaction->frequency)) {
                                        echo $transaction->frequency;
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php echo (isset($transaction->amount) && !empty($transaction->amount)) ? 'Payment' : InvoiceStatus::getInvoiceStatus($transaction->invoiceid) ?>
                                </td>
                                <td class="amountColumn">
                                    <?PHP
                                    if (isset($transaction->amounts)) {
                                        echo "$ " . number_format((array_sum(explode(',', $transaction->amounts))) * 1.1, 2);
                                        $totalInvoiced += str_replace(',', '', number_format((array_sum(explode(',', $transaction->amounts))) * 1.1, 2));
                                        $remain += str_replace(',', '', number_format((array_sum(explode(',', $transaction->amounts))) * 1.1, 2));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>
                                <td class="amountColumn text-success sum">
                                    <?PHP
                                    if (isset($transaction->amount)) {
                                        echo "$ " . number_format($transaction->amount, 2);
                                        $remain -= str_replace(',', '', number_format($transaction->amount, 2));
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>
                                <td class="amountColumn">
                                    $ {{number_format($remain,2)}}
                                </td>
                                <td class="amountColumn">
                                    <?PHP
                                    if (isset($transaction->amounts)) {
                                        echo "$ " . number_format($customer->amountPaidforInvoice($transaction->invoiceid), 2);
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>
                            </tr>
                            @endforeach
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th hidden></th>
                                <th hidden></th>
                                <th hidden></th>
                                <th></th>
                                <th></th>
                                <th class="amountColumn">$ {{number_format($totalInvoiced,2)}}</th>
                                <th class="amountColumn text-success">$ {{number_format($customer->amountPaid(),2)}}</th>
                                <th class="amountColumn"></th>

                                <th></th>
                            </tr>
                        </tfoot>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Tab 6 Deleted Invoices --}}
<div class="fullcontainer customerContentContainer" id="customerContainer7">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">                
                <div class="panel-body rounded_wrapper">
                    <div class="panel-heading page-panelheading">Listing Deleted Invoices</div>                       
                    @if(isset($deleted) && !empty($deleted))
                    <table id="transactionsListTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Invoice ID</th>
                                <th>Deleted on</th>
                                <th>Deleted by</th>
                                <th>MYOB</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($deleted as $deletedInvoice)
                            <tr>
                                <th>{{$deletedInvoice->invoice_id}}</th>
                                <th>{{$deletedInvoice->created_at}}</th>
                                <th>{{$deletedInvoice->user_id}}</th>
                                <th>{{$deletedInvoice->myob_number}}</th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="panel-body text-center">
                        <label>No Deleted Invoices</label>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Tab 9 Notes --}}
<div class="fullcontainer customerContentContainer" id="customerContainer9">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">                
                <div class="panel-body rounded_wrapper">
                    <form id="updateNote" method="POST" action=<?= url("/note/store") ?>>
                        {{ csrf_field() }}
                        <input hidden name="customerid" value="{{$customer->id}}">
                        <div class="fullcontainer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading page-panelheading">Notes
                                            <div style="display: inline-block; margin: -5px" class="pull-right">

                                                <button id="saveNote" type="submit" name="submit" class="btn btn-sm btn-primary pull-right">Save</button>
                                            </div>
                                        </div>
                                        <div class="panel-body center-block">
                                            <textarea id="txtEditor" name="noteBody"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $("#saveNote").click(function(){
                            $("#txtEditor").val($("#txtEditor").Editor("getText"));
                            })
                        </script>
                    </form>

                    @if($customer->notes() !=NULL )
                    @foreach($customer->notes() as $note)
                    <form id="updateNote" method="POST" action=<?= url("/note/update/" . $note->id) ?>>
                        {{method_field('PATCH')}}
                        {{ csrf_field() }}
                        <input name="id" value="{{$note->id}}" hidden>
                        <div class="fullcontainer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading page-panelheading">{{date(DateTimeFormat,strtotime($note->datetime))}} - {{$note->name}}
                                            <div style="display: inline-block; margin: -5px" class="pull-right">
                                                <span id="editNote{{$note->id}}" class="glyphicon glyphicon-pencil btn btn-primary btn-sm pull-right btn-margin"  style="margin: 0 5px;" onclick="editNote({{$note->id}})"></span>

                                                <button style="display: none" id="updateNote{{$note->id}}" class="btn btn-success btn-sm pull-right btn-margin"  style="margin: 0 5px;">Update</button>
                                                <a href="{{url('/')}}/customer/{{$customer->id}}" style="display: none" id="cancelUpdate{{$note->id}}" class="btn btn-danger btn-sm pull-right btn-margin"  style="margin: 0 5px;">Cancel</a>
                                            </div>
                                        </div>
                                        <div class="panel-body center-block">
                                            <div id="note{{$note->id}}"><?= $note->note ?></div>
                                            <div><textarea id="txtEditor{{$note->id}}" style="display: none" name="noteBody"></textarea></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $("#updateNote" + <?= $note->id ?>).click(function(){
                            $("#txtEditor" + <?= $note->id ?>).val($("#txtEditor" + <?= $note->id ?>).Editor("getText"));
                            });

                        </script>

                    </form>
                    <hr>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection