@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Import Customers
                    <div style="display: inline-block; margin: -5px" class="pull-right">
                    </div>
                </div>
                <div class="panel-body rounded_wrapper">
                    <form id="importForm" class="form-horizontal" action="import/save" method="POST" files= 'true' enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3 col-md-offset-2">
                                    <label>Account manager</label>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control" name="account_manager">
                                        <option selected disabled>Select...</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3 col-md-offset-2">
                                    <label>Business Type</label>
                                </div>
                                <div class="col-md-3">
                                    <select id="businessTypeSelect" class="form-control" name="btype">
                                        <option selected disabled>Select...</option>
                                        @foreach($types as $type)
                                        <option value="{{$type->id}}">{{$type->type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3 col-md-offset-2">
                                    <label>Select CSV File</label>
                                </div>
                                <div class="col-md-3 fileinput">
                                    <input id="fileupload" name="customersFile" class="form-control btn btn-default" type="file">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3 col-md-offset-2"></div>
                                <div class="col-md-3">
                                    <button class="btn btn-success">Import</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
