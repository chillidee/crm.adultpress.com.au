@extends('layouts.app')

@section('content')
<div id="paymentsReport" class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">
                    {{$graphtitle}}
                </div>
                <div class="panel-body">
                    <div id="graphReport" class="rounded_wrapper"><?= $graph ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">        
            <a class="btn btn-sm btn-primary pull-right" href = "{{ url('/reports/reports') }}"><span class=" fa  fa-btn glyphicon glyphicon-arrow-left"></span>Back</a>
        </div>
    </div>
</div>

@endsection
