<?php

use App\Customer;
use App\Invoice;
?>
<html>
    <head>

        <style>
            .heading{
                padding: 5px;
                background-color: #9e9;
            }
            body{
                font-family: arial;
            }
            div{
                margin: 5px;
            }
            .expInvColumn,.border{
                border: 1px solid #CCC;
            }


            td{
                padding: 5px;
                font-weight: normal !important;

            }
            a{
                text-decoration: none !important;
                color: black;
            }

            td,th{
                color: black !important;
                font-size: 0.8em;
                font-weight: 900;
            }

            hr{
                border: #CCC;
            }
            .expInvColumn{
                min-width: 300px;
            }
        </style>
    </head>
    <body>
        <div id="editor"></div>
        <div id="paymentsReport" class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading page-panelheading"></div>
                        <h3>This is a reminder from CRM</h3>
                        <h4>List of new invoices created {{date(DateFormat)}}</h4>
                        <div class="panel-body">
                            <table id="paymentsReportTable" class="table table-bordered border" style="width: 100%">
                                <thead>
                                    <tr class="border">
                                        <th class="heading" colspan="9">New Invoices Created - {{date(DateFormat)}}</th>
                                    </tr>
                                    <tr class="border">
                                        <td style="text-align: center;font-weight: bold;width: 50px;" class="border">MYOB ID</td>
                                        <td style="text-align: center;font-weight: bold;width: 100px;" class="border">Working Name</td>
                                        <td style="text-align: center;font-weight: bold;width: 100px;" class="border">Site</td>
                                        <td style="text-align: center;font-weight: bold;width: 100px;" class="border">Page</td>
                                        <td style="text-align: center;font-weight: bold;width: 50px;" class="border">Position</td>
                                        <td style="text-align: center;font-weight: bold;width: 100px;" class="border">Amount</td>
                                        <td style="text-align: center;font-weight: bold;width: 100px;" class="border">Start Date</td>
                                        <td style="text-align: center;font-weight: bold;width: 100px;" class="border">Expiry Date</td>
                                        <td style="text-align: center;font-weight: bold;width: 100px;" class="border">Due Date</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    foreach ($emailArray as $invoiceID => $ordersArray):
                                    $invoice = Invoice::find($invoiceID);
                                    $customer = Customer::find($invoice->customerid);
                                    @endphp
                                    <tr class="border">
                                        <td class="border" style="text-align: center;width: 50px;"><a href="{{ url('/') }}/invoice/{{$invoiceID}}">{{$invoice->myob}}</a></td>
                                        <td class="border" style="text-align: center;width: 100px;"><a href="{{ url('/') }}/invoice/{{$invoiceID}}">{{$customer->bname}}</a></td>
                                        <td class="border" colspan="7">
                                            <table style="width:100%;">
                                                @foreach($ordersArray as $order)
                                                <tr>
                                                    <td class="border" style="text-align: center;width: 100px;border: none;"><a href="{{ url('/') }}/invoice/{{$invoiceID}}">{{$order->sitename}}</a></td>
                                                    <td class="border" style="text-align: center;width: 100px;border: none;"><a href="{{ url('/') }}/invoice/{{$invoiceID}}">{{$order->page}}</a></td>
                                                    <td class="border" style="text-align: center;width: 50px;border: none;"><a href="{{ url('/') }}/invoice/{{$invoiceID}}">{{$order->position}}</a></td>
                                                    <td class="border" style="text-align: center;width: 100px;border: none;"><a href="{{ url('/') }}/invoice/{{$invoiceID}}">${{$order->amount * 1.1}}</a></td>
                                                    <td class="border" style="text-align: center;width: 100px;border: none;"><a href="{{ url('/') }}/invoice/{{$invoiceID}}">{{date('d-m-Y',strtotime($order->startdate))}}</a></td>
                                                    <td class="border" style="text-align: center;width: 100px;border: none;"><a href="{{ url('/') }}/invoice/{{$invoiceID}}">{{date('d-m-Y',strtotime($order->expirydate))}}</a></td>
                                                    <td class="border" style="text-align: center;width: 100px;border: none;"><a href="{{ url('/') }}/invoice/{{$invoiceID}}">{{date('d-m-Y',strtotime($order->duedate))}}</a></td>
                                                </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>