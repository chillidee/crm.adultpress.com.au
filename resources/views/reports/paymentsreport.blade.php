<html>
    <head>

        <style>
            .heading{
                padding: 10px;
                background-color: #9e9;
            }
            body{
                font-family: arial;
            }
            div{
                margin: 10px;
            }
            table,th{
                border: 1px solid #CCC;
            }    


            td{
                padding: 10px;
                font-weight: normal !important;
            }
            a{
                text-decoration: none !important;
                color: black;
            }

            td,th{
                color: black !important;
                font-size: 0.8em;
                font-weight: 900;
            }
            td{
                background-color: #fff;

            }
            th,tr,td,table{
                padding: 0px;
                margin: 0px;
            }
            td{
                padding: 2px;
                border-bottom: 1px solid #CCC;
            }
        </style>

    </head>
    <body>
        <div id="editor"></div>

        <div id="paymentsReport" class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading page-panelheading">
                        </div>
                        <div class="panel-body">
                            <table id="paymentsReportTable" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="heading" colspan="7">Payments Report FROM : {{date(DateFormat, strtotime(str_replace('/', '-', $from)))}} - TO : {{ date(DateFormat, strtotime(str_replace('/', '-', $to)))}} ordered by payments received date</th>

                                    </tr>
                                    <tr>
                                        <th>MYOB</th>
                                        <th>Customer</th>
                                        <th>Created By</th>                                        
                                        <th>Invoice date</th>
                                        <th>Payment Received</th>
                                        <th>Payment Amount</th>
                                        <th>Method</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?PHP $totalPaid = 0; ?>
                                    @foreach($reports as $report)
                                    <tr>
                                        <td><a target="_blank" href="{{url('/')}}/invoice/{{$report->invoiceid}}">{{$report->myob}}</a></td>
                                        <td><a target="_blank" href="{{url('/')}}/customer/{{$report->customerid}}">{{$report->bname}}</a></td>
                                        <td><a target="_blank" href="{{url('/')}}/invoice/{{$report->invoiceid}}">{{$report->name}}</a></td>
                                        <td style="text-align: center;" ><a target="_blank" href="{{url('/')}}/invoice/{{$report->invoiceid}}">{{date(DateFormat,strtotime($report->invDate))}}</a></td>
                                        <td style="text-align: center;" ><a target="_blank" href="{{url('/')}}/invoice/{{$report->invoiceid}}">{{date(DateFormat,strtotime($report->created_at))}}</a></td>
                                        <td style="text-align: right;"><a target="_blank" href="{{url('/')}}/invoice/{{$report->invoiceid}}">$ {{number_format($report->amount,2)}}</a></td>
                                        <td><a target="_blank" href="{{url('/')}}/invoice/{{$report->invoiceid}}">{{$report->method}}</a></td>  

                                        <?PHP $totalPaid+= str_replace(',', '', number_format($report->amount, 2)); ?>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th style="text-align: right;">$ {{number_format($totalPaid,2)}}</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>