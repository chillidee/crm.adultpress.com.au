@extends('layouts.app')

@section('content')


<div class="fullcontainer">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Generate Report<span class="pull-right">{{date(DateTimeFormat)}}</span></div>
                <div class="panel-body rounded_wrapper"> 
                    <form id="paymentReportForm" method="GET" action="{{url('/')}}/reports/paymentsreport">
                        <div class="row">
                            <div class="form-group">
                                <span class="col-md-5">Payments</span>
                                <div class="col-md-1">
                                    <label for="reportfrom">From</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" id="reportfrom" class="form-control" name="reportfrom">
                                </div>
                                <div class="col-md-1">
                                    <label for="reportto">to</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" id="reportto" class="form-control" name="reportto">
                                </div>
                                <div class="col-md-1">
                                    <button class="fa fa-file-pdf-o btn btn-pdf" type="submit"></button>
                                </div>
                            </div>                           
                        </div>
                    </form>
                    <hr>
                    <form id="paymentReportForm" method="GET" action="{{url('/')}}/reports/owingcustomers">
                        <div class="row">
                            <div class="form-group">
                                <span class="col-md-11">Customers Owing</span>
                                <div class="col-md-1">
                                    <button class="fa fa-file-pdf-o btn btn-pdf" type="submit"></button>
                                </div>
                            </div>                           
                        </div>
                    </form>
                    <hr>
                    <form id="paymentReportForm" method="GET" action="{{url('/')}}/reports/overdueactivepaid">
                        <div class="row">
                            <div class="form-group">
                                <span class="col-md-11">Due Active Paid Customers</span>
                                <div class="col-md-1">
                                    <button class="fa fa-file-pdf-o btn btn-pdf" type="submit"></button>
                                </div>
                            </div>                           
                        </div>
                    </form>
                    <hr>
                    <div class="row text-center">
                        <div class="report-button-wrapper">
                            <form id="paymentReportForm" method="GET" action="{{url('/')}}/reports/graphreport">
                                <input hidden name="graph" value="monthly">
                                <div class="form-group">
                                    <button class="fa  fa-bar-chart btn btn-danger btn-block report-button" type="submit"><br>Monthly</button>
                                </div>
                            </form>
                        </div>
                        <div class="report-button-wrapper">
                            <form id="soldPerPlatformReportForm" method="GET" action="{{url('/')}}/reports/graphreport">
                                <input hidden name="graph" value="platform">
                                <div class="form-group">
                                    <button class="fa  fa-bar-chart btn btn-danger btn-block report-button" type="submit"><br>Platform</button>
                                </div>
                            </form>
                        </div>
                        <div class="report-button-wrapper">
                            <form id="soldPerInvoiceDescriptionReportForm" method="GET" action="{{url('/')}}/reports/graphreport">
                                <input hidden name="graph" value="description">
                                <div class="form-group">
                                    <button class="fa  fa-bar-chart btn btn-danger btn-block report-button" type="submit"><br>Description</button>
                                </div>
                            </form>
                        </div>
                        <div class="report-button-wrapper">
                            <form id="soldPerInvoiceDescriptionReportForm" method="GET" action="{{url('/')}}/reports/graphreport">
                                <input hidden name="graph" value="page">
                                <div class="form-group">
                                    <button class="btn btn-danger btn-block report-button" type="submit"><span class="fa  fa-rotate-90 fa-bar-chart"></span><br>Page</button>
                                </div>
                            </form>
                        </div> 
                        <div class="report-button-wrapper">
                            <form id="soldPerBusinessTypeReportForm" method="GET" action="{{url('/')}}/reports/graphreport">
                                <input hidden name="graph" value="btype">
                                <div class="form-group">
                                    <button class="fa  fa-bar-chart btn btn-danger btn-block report-button"  type="submit"><br>Business Type</button>
                                </div>
                            </form>
                        </div>                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection







