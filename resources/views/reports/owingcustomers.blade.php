<html>
    <head>
        <style>
            .heading{
                padding: 10px;
                background-color: #9e9;
            }
            body{
                font-family: arial;
            }
            div{
                margin: 10px;
            }
            table,td,th{
                border: 2px solid #CCC;
            }

            td{
                padding: 10px;
                font-weight: normal !important;
            }
            a{
                text-decoration: none !important;
                color: black;
            }

            td,th{
                color: black !important;
                font-size: 0.8em;
                font-weight: 900;
            }
            td{
                background-color: #fff;
            }
        </style>
    </head>
    <body>
        <div id="paymentsReport" class="">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">
                </div>
                <div class="panel-body">
                    <table id="paymentsReportTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="heading" colspan="9">Owing Customers Report - {{date(DateTimeFormat)}}</th>
                            </tr>
                            <tr>
                                <th>MYOB</th>
                                <th>Working Name</th>
                                <th>Advertisement Start Date</th>
                                {{--
                                <th>Invoice Reminder</th>
                                <th>Expiry Date</th>
                                        --}}
                                <th>Total Ordered</th>
                                <th>Total Paid</th>
                                <th>0 - 30</th>
                                <th>31 - 60</th>
                                <th>61 - 90</th>
                                <th>90 +</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total = 0;
                            $paid = 0;
                            $totalA = 0;
                            $totalB = 0;
                            $totalC = 0;
                            $totalD = 0;
                            ?>
                            @foreach($invoices as $invoice)
                            @if(isset($invoice) && !empty($invoice) && $invoice->daysFromThisDue() >= 0 && number_format($invoice->totalAmount() +$invoice->credit()  - $invoice->totalPaid(),2) > 0 && strtolower($invoice->payment_method) != 'invoice cancelled')
                            <tr>
                                <td><a target="_blank" href="{{url('/')}}/invoice/{{$invoice->id}}">{{$invoice->myob}}</a></td>
                                <td><a target="_blank" href="{{url('/')}}/customer/{{$invoice->customerid}}">{{$invoice->customerName()}}</a></td>
                                <td
                                <?php
                                if ($invoice->firststartdate() <= date("Y-m-d")):
                                    echo 'style="background-color: #FCC"';
                                endif;
                                ?>
                                    >{{date(DateFormat,strtotime($invoice->firststartdate()))}}</td>
                                {{--
                                <td>{{date(DateFormat,strtotime($invoice->firstDueDate()))}}</td>
                                <td>{{date(DateFormat,strtotime($invoice->firstExpiryDate()))}}</td>
                                --}}
                                <td style="text-align: right;">$ {{number_format($invoice->totalAmount(),2)}}</td>
                                <td style="text-align: right;">$ {{number_format($invoice->totalPaid(),2)}}</td>
                                <td style="text-align: right;"><?php echo ($invoice->daysFromThisDue() <= 30) ? "<b>$ " . number_format($invoice->totalAmount() - $invoice->totalPaid(), 2) . "</b>" : '' ?></td>
                                <td style="text-align: right;"><?php echo ($invoice->daysFromThisDue() >= 31 && $invoice->daysFromThisDue() <= 60) ? "<b>$ " . number_format($invoice->totalAmount() - $invoice->totalPaid(), 2) . "</b>" : '' ?></td>
                                <td style="text-align: right;"><?php echo ($invoice->daysFromThisDue() >= 61 && $invoice->daysFromThisDue() <= 90) ? "<b>$ " . number_format($invoice->totalAmount() - $invoice->totalPaid(), 2) . "</b>" : '' ?></td>
                                <td style="text-align: right;"><?php echo ($invoice->daysFromThisDue() >= 91) ? "<b>$ " . number_format($invoice->totalAmount() - $invoice->totalPaid(), 2) . "</b>" : '' ?></td>
                                <?php
                                $total += $invoice->totalAmount();
                                $paid += $invoice->totalPaid();

                                if ($invoice->daysFromThisDue() <= 30) {
                                    $totalA += ($invoice->totalAmount() - $invoice->totalPaid());
                                }
                                if ($invoice->daysFromThisDue() >= 31 && $invoice->daysFromThisDue() <= 60) {
                                    $totalB += ($invoice->totalAmount() - $invoice->totalPaid());
                                }
                                if ($invoice->daysFromThisDue() >= 61 && $invoice->daysFromThisDue() <= 90) {
                                    $totalC += ($invoice->totalAmount() - $invoice->totalPaid());
                                }
                                if ($invoice->daysFromThisDue() >= 91) {
                                    $totalD += ($invoice->totalAmount() - $invoice->totalPaid());
                                }
                                ?>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="text-align: right;">$ {{number_format($total,2)}}</th>
                                <th style="text-align: right;">$ {{number_format($paid,2)}}</th>
                                <th style="text-align: right;">$ {{number_format($totalA,2)}}</th>
                                <th style="text-align: right;">$ {{number_format($totalB,2)}}</th>
                                <th style="text-align: right;">$ {{number_format($totalC,2)}}</th>
                                <th style="text-align: right;">$ {{number_format($totalD,2)}}</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>