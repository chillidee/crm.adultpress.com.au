<?PHP

use App\Customer;
?>
<html>
    <head>

        <style>
            .heading{
                padding: 5px;
                background-color: #9e9;
            }
            body{
                font-family: arial;
            }
            div{
                margin: 5px;
            }
            .expInvColumn,.border{
                border: 1px solid #CCC;
            }


            td{
                padding: 5px;
                font-weight: normal !important;

            }
            a{
                text-decoration: none !important;
                color: black;
            }

            td,th{
                color: black !important;
                font-size: 0.8em;
                font-weight: 900;
            }

            hr{
                border: #CCC;
            }
            .expInvColumn{
                min-width: 300px;
            }


        </style>
    </head>
    <body>
        <div id="editor"></div>
        <div id="paymentsReport" class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading page-panelheading"></div>
                        <h3>This is a reminder from CRM</h3>
                        <h4>List of overdue invoices {{date(DateFormat)}}</h4>
                        <div class="panel-body">
                            <table id="paymentsReportTable" class="table table-bordered border" style="width: 100%">
                                <thead>
                                    <tr class="border">
                                        <th class="heading" colspan="3">Overdue Invoices - {{date(DateFormat)}}</th>
                                    </tr>
                                    <tr class="border">
                                        <th class="border">MYOB Invoice Number</th>
                                        <th class="border">Working Name</th>
                                        <th class="border">Invoice Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($orderArray as $customer_id => $invoiceArray):
                                        foreach ($invoiceArray as $invoice_id):
                                            ?>
                                            <tr class="border">
                                                <td class="border" style="text-align: center;"><a target="_blank" href="{{ url('/') }}/invoice/{{$invoice_id['invoice_id']}}">{{$invoice_id['myob']}}</a></td>
                                                <td class="border" style="text-align: center;"><a target="_blank" href="{{ url('/') }}/customer/{{$customer_id}}">{{$invoice_id['bname']}}</a></td>
                                                <td class="border" style="text-align: center;"><a target="_blank" href="{{ url('/') }}/invoice/{{$invoice_id['invoice_id']}}">${{$invoice_id['amount']}}</a></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>