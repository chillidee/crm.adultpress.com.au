
@extends('layouts.app')

@section('content')


<div id="paymentsReport" class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">
                    <h3>CRM Monthly Report</h3>
                </div>
                <div class="panel-body">
                    <div id="monthlyReport"><?= $graph ?></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
