<?PHP

use App\Customer;
?>
@extends('layouts.app')

@section('content')

<?PHP
$filteruser = isset($_GET['user']) ? $_GET['user'] : Auth::user()->id;
$filterFrom = isset($_GET['dueDateFrom']) ? $_GET['dueDateFrom'] : date("Y-m-d");
$filterTo = isset($_GET['dueDateTo']) ? $_GET['dueDateTo'] : date("Y-m-d");
$selectedUsers = isset($_GET['selectedUsers']) ? $_GET['selectedUsers'] : Auth::user()->name;
?>
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">Home -
                    @if(Auth::user()->group_id=='Admin')
                    Expiring Tasks For:
                    <div class="pull-right col-md-8" style="display: block; margin: -5px 0">
                        <form method="GET" action="">
                            @if(Auth::user()->status == 'Active')
                            <div class="col-md-2 multipleSelectContainer btn-margin" style="display: inline-block; margin: 0 5px">
                                <select onchange="setSelectedValues()" name="user" multiple data-actions-box="true" class="form-control selectpicker" {{(Auth::user()->group_id=='User')? 'disabled' : ''}}>
                                    @foreach($users as $user)
                                    <option>{{$user->name}}</option>
                                    @endforeach
                                </select>
                                <input id="selectedUsers" hidden name="selectedUsers" value="{{$selectedUsers}}">
                            </div>
                            @endif
                            <div class="col-md-2" style="display: inline-block; margin: 0 5px">
                                <button class="form-control btn btn-sm btn-primary btn-margin" type="submit">Refine</button>
                            </div>
                        </form>
                    </div>
                    @else
                    Expiring Tasks
                    @endif
                </div>
                <div class="panel-body">
                    @if(Auth::user()->group_id == 'Admin')
                    @else
                    @if(Auth::user()->assignedto != NULL AND Auth::user()->id != Auth::user()->assignedto)
                    @else
                    @endif
                    @endif
                    <table id="tasks_list" class="table table-bordered">
                        <thead>
                            <tr id="tableFilters">
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                                <th class="taskfilterhead"></th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Acc Manager</th>
                                <th>Status</th>
                                <th>Due Date</th>
                                <th>Exp Items</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if(isset($customers))
                            @foreach($customers as $customer)
                            <tr>
                                <td><a href="{{url('/customer/')}}/{{$customer->id}}">{{$customer->id}}</a></td>
                                <td>{{$customer->type}}</td>
                                <td><a href="{{url('/customer/')}}/{{$customer->id}}"> {{$customer->bname}}</a></td>
                                <td>{{$customer->manager}}</td>
                                <td>{{$customer->statusname or ''}}</td>
                                <td><span hidden>{{date("Ymd",strtotime($customer->dueDatetime))}}</span>{{date(DateTimeFormat,strtotime($customer->dueDatetime))}}</td>
                                <td class="expInvColumn">
                                    <?PHP
                                    if ($customer->statusname == 'Active Paid Customer') {
                                        $thisCustomer = Customer::find($customer->id);
                                        //var_dump($thisCustomer->getexpiries());
                                        $i = 0;
                                        foreach ($thisCustomer->getexpiries() as $exporder) {
                                            $i++;
                                            echo '<a href="' . url('/') . '/invoice/' . $exporder->invoiceid . '">';
                                            echo "($i) $exporder->sitename - $exporder->page <br></a>";
                                        }
                                        /**
                                          if (isset($thisCustomer->lastinvoice()->expirydate)) {
                                          if ($thisCustomer->lastinvoice()->expirydate <= date("Y-m-d"))
                                          echo date(DateFormat, strtotime($thisCustomer->lastinvoice()->expirydate));
                                          }
                                         */
                                    }
                                    ?>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection