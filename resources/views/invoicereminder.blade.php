<!DOCTYPE html>
<?PHP
use App\Customer;
?>
<html lang="en">
    <head>
        <style>
            body,html,table{
                font-size: 20px;
                font-family: 'arial';
            }
            table,tr,td,th{
                padding: 10px;
                border: 2px #888 solid;
            }
            div{
                width: 100%;
            }
            a{
                text-decoration: none;
                color: brown;
            }
        </style>
    </head>    
    <body>     
        <div>    
            <h3>This is a reminder from CRM system</h3>
            <h4>This Customer status has recently changed</h4>
            <h5>You can click on customer name to open the profile on CRM system</h5>
            <table>
                <tr>
                    <th>Customer ID: {{$customer->id}}</th>
                    <th><a href="http://crm.adultpress.com.au/public/customer/{{$customer->id}}">{{$customer->bname}}</a></th>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>{{$status->name}}</td>
                </tr>    

                <tr>
                    <td>Expiring Items</td>

                    <td class="expInvColumn">
                        <?PHP
                        $thisCustomer = Customer::find($customer->id);
                        //var_dump($thisCustomer->getexpiries());
                        $i = 0;
                        foreach ($thisCustomer->getexpiries() as $exporder) {
                            $i++;
                            echo '<a href="' . url('/') . '/invoice/' . $exporder->invoiceid . '">';
                            echo "($i) $exporder->sitename - $exporder->page <br></a>";
                        }
                        /**
                          if (isset($thisCustomer->lastinvoice()->expirydate)) {
                          if ($thisCustomer->lastinvoice()->expirydate <= date("Y-m-d"))
                          echo date(DateFormat, strtotime($thisCustomer->lastinvoice()->expirydate));
                          }
                         */
                        ?>
                    </td>

                </tr>








            </table>
        </div>
    </body>
</html>

