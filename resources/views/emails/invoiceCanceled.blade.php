<html>
    <head>
        <style>
            .heading{
                padding: 5px;
                background-color: #9e9;
            }
            body{
                font-family: arial;
            }
            div{
                margin: 5px;
            }
            .expInvColumn,.border{
                border: 1px solid #CCC;
            }


            td{
                padding: 5px;
                font-weight: normal !important;

            }
            a{
                text-decoration: none !important;
                color: black;
            }

            td,th{
                color: black !important;
                font-size: 0.8em;
                font-weight: 900;
            }

            hr{
                border: #CCC;
            }
            .expInvColumn{
                min-width: 300px;
            }
        </style>
    </head>
    <body>
        <div id="editor"></div>
        <div id="paymentsReport" class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading page-panelheading"></div>
                        <h3>This is a reminder from CRM system</h3>
                        <h4>A invoice has been cancelled for {{$customerName}} on {{date('d-m-Y H:i:s')}}</h4>
                        <p>You can click on customer name to open the customer or on the invoice number to see the new invoice on CRM system</p>
                        <div class="panel-body">
                            <table id="paymentsReportTable" class="table table-bordered border" style="width: 100%">
                                <thead>
                                    <tr class="border">
                                        <th class="heading" colspan="4">Invoice Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="border">
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/customer/{{$oldInvoice->customerid}}">Customer ID: {{$oldInvoice->customerid}}</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/customer/{{$oldInvoice->customerid}}">{{$customerName}}</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$newInvoice->id}}">MYOB</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$newInvoice->id}}">{{$newInvoice->myob}}</a></td>
                                    </tr>
                                    <tr class="border">
                                        <td class="border" style="text-align: center;">Subtotal</td>
                                        <td class="border" style="text-align: center;">${{number_format($totalAmount,2)}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr class="border">
                                        <td class="border" style="text-align: center;">Total Amount</td>
                                        <td class="border" style="text-align: center;">${{number_format($totalAmount * 1.1,2)}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @foreach($orders as $order)
                                    <tr>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$newInvoice->id}}">{{$order->sitename}}-{{$order->page}}</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$newInvoice->id}}">{{$order->startdate}}</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$newInvoice->id}}">{{$order->frequency}}</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$newInvoice->id}}">${{number_format(-abs((float) $order->amount), 2)}}</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>