<html>
    <head>
        <style>
            .heading{
                padding: 5px;
                background-color: #9e9;
            }
            body{
                font-family: arial;
            }
            div{
                margin: 5px;
            }
            .expInvColumn,.border{
                border: 1px solid #CCC;
            }


            td{
                padding: 5px;
                font-weight: normal !important;

            }
            a{
                text-decoration: none !important;
                color: black;
            }

            td,th{
                color: black !important;
                font-size: 0.8em;
                font-weight: 900;
            }

            hr{
                border: #CCC;
            }
            .expInvColumn{
                min-width: 300px;
            }
        </style>
    </head>
    <body>
        <div id="editor"></div>
        <div id="paymentsReport" class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading page-panelheading"></div>
                        <h3>This is a reminder from CRM system</h3>
                        <h4>A new invoice has created for {{$emailArray['customer_name']}} on {{date('d-m-Y H:i:s')}} by {{$emailArray['mangerName']}}</h4>
                        <p>You can click on customer name to open the customer or on the invoice number to see the new invoice on CRM system</p>
                        <div class="panel-body">
                            <table id="paymentsReportTable" class="table table-bordered border" style="width: 100%">
                                <thead>
                                    <tr class="border">
                                        <th class="heading" colspan="4">Invoice Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="border">
                                        <td class="border" style="text-align: center;"><a style="font-weight: bold" target="_blank" href="{{url('/')}}/customer/{{$emailArray['customer_id']}}">Customer Name:</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/customer/{{$emailArray['customer_id']}}">{{$emailArray['customer_name']}}</a></td>
                                        <td class="border" style="text-align: center;"><a style="font-weight: bold" target="_blank" href="{{url('/')}}/invoice/{{$emailArray['invoice_id']}}">MYOB</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$emailArray['invoice_id']}}">{{$emailArray['myob_number']}}</a></td>
                                    </tr>
                                    <tr>
                                        <td class="border" style="text-align: center;"><a style="font-weight: bold" target="_blank" href="{{url('/')}}/invoice/{{$emailArray['customer_id']}}">Details</a></td>
                                        <td class="border" style="text-align: center;"><a style="font-weight: bold" target="_blank" href="{{url('/')}}/invoice/{{$emailArray['customer_id']}}">Start date</a></td>
                                        <td class="border" style="text-align: center;"><a style="font-weight: bold" target="_blank" href="{{url('/')}}/invoice/{{$emailArray['customer_id']}}">Frequency</a></td>
                                        <td class="border" style="text-align: center;"><a style="font-weight: bold" target="_blank" href="{{url('/')}}/invoice/{{$emailArray['customer_id']}}">Amount</a></td>
                                    </tr>
                                    @foreach($emailArray['orders'] as $order)
                                    <tr>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$emailArray['customer_id']}}">{{$order->sitename}}-{{$order->page}}</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$emailArray['customer_id']}}">{{$order->startdate}}</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$emailArray['customer_id']}}">{{$order->frequency}}</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="{{url('/')}}/invoice/{{$emailArray['customer_id']}}">${{number_format((float)$order->amount * 1.1, 2)}}</a></td>
                                    </tr>
                                    @endforeach
                                    <tr class="border">
                                        <td class="border" style="text-align: center;font-weight: bold">Subtotal</td>
                                        <td class="border" style="text-align: center;">${{number_format((float)$emailArray['invoice_amount'],2)}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr class="border">
                                        <td class="border" style="text-align: center;font-weight: bold">Total Amount</td>
                                        <td class="border" style="text-align: center;">${{number_format((float)$emailArray['invoice_amount'] * 1.1,2)}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>