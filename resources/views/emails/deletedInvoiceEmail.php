<html>
    <head>
        <style>
            .heading{
                padding: 5px;
                background-color: #9e9;
            }
            body{
                font-family: arial;
            }
            div{
                margin: 5px;
            }
            .expInvColumn,.border{
                border: 1px solid #CCC;
            }


            td{
                padding: 5px;
                font-weight: normal !important;

            }
            a{
                text-decoration: none !important;
                color: black;
            }

            td,th{
                color: black !important;
                font-size: 0.8em;
                font-weight: 900;
            }

            hr{
                border: #CCC;
            }
            .expInvColumn{
                min-width: 300px;
            }
        </style>
    </head>
    <body>
        <div id="editor"></div>
        <div id="paymentsReport" class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading page-panelheading"></div>
                        <h3>This is a reminder from CRM system</h3>
                        <h4>Invoice number <?php echo $emailData['invoice_id'] ?> has been deleted by <?php echo $emailData['username'] ?> on <?php echo date('d-m-Y H:i:s') ?> for <?php echo $emailData['customer']->bname ?></h4>
                        <div class="panel-body">
                            <table id="paymentsReportTable" class="table table-bordered border" style="width: 100%">
                                <thead>
                                    <tr class="border">
                                        <th class="heading" colspan="4">Invoice Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="border">
                                        <td class="border" style="text-align: center;"><a target="_blank" href="<?php echo url('/') ?>/customer/<?php echo $emailData['invoice']->customerid ?>">Customer ID: <?php echo $emailData['invoice']->customerid ?></a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="<?php echo url('/') ?>/customer/<?php echo $emailData['invoice']->customerid ?>"><?php echo $emailData['customer']->bname ?></a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="<?php echo url('/') ?>/customer/<?php echo $emailData['invoice']->customerid ?>">MYOB</a></td>
                                        <td class="border" style="text-align: center;"><a target="_blank" href="<?php echo url('/') ?>/customer/<?php echo $emailData['invoice']->customerid ?>"><?php echo $emailData['myobnumber'] ?></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>