@extends('layouts.app')

@section('content')
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading page-panelheading">
                    <div class="btn-group page-panelheading" role="group">Edit Customer
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form class="form-horizontal" id="customer-form" role="form" method="POST" action=<?= (isset($customer->id)) ? url('/') . "/customer/$customer->id" : url('customer/store'); ?>>
    {{ csrf_field() }}
    @if(isset($customer->id))
    {{method_field('PATCH')}}
    @endif
    <div class="fullcontainer">
        <div class="row">
            <div class="col-md-10  col-md-offset-1 rounded_wrapper">
                <div class="panel panel-default">
                    <div class="panel-heading page-panelheading">Customer Details From
                        <div style="display: inline-block; margin: -5px" class="pull-right">
                            <button id="customer-form-submit" value="submit" type="submit" name="btnSubmit" class="btn btn-sm btn-success btn-margin">                                
                                <?= (isset($customer->id)) ? "Update" : "Add" ?>
                            </button>
                            <a type="back" class="btn btn-sm btn-danger btn-margin" href="<?= isset($customer->id) ? url('/customer/' . $customer->id) : url('/customers') ?>">
                                Cancel</a>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <dl class="dl-horizontal">
                                    {{-- ******* Working Name   ************ --}}
                                    <div id="bnameDiv" class="form-group {{ $errors->has('bname') ? ' has-error' : '' }}  required">
                                        <label for="bname" class="col-md-4 control-label">Working Name</label>
                                        <div class="col-md-6">
                                            <input id="bname" maxlength="30"  placeholder="Working Name" class="form-control" value="{{$customer->bname or ""}}" name="bname" autocomplete="off">
                                        </div>
                                    </div>
                                    {{-- **********  Working Phone ************** --}}
                                    <div id="bphoneDiv" class="form-group  {{ $errors->has('bphone') ? ' has-error' : '' }} ">
                                        <label for="bphone" class="col-md-4 control-label">Working Phone</label>
                                        <div class="col-md-6">
                                            <input id="bphone"  placeholder="Working Phone" maxlength="20"  class="contact-group form-control bphoneInput" value="{{$customer->bphone or ''}}" name="bphone" autocomplete="off">
                                        </div>
                                    </div>
                                    {{-- ********  Working Email  ************** --}}
                                    <div id="bemailDiv" class="form-group {{ $errors->has('bemail') ? ' has-error' : '' }}">
                                        <label for="bemail" class="col-md-4 control-label">Working Email</label>
                                        <div class="col-md-6">
                                            <input id="bemail"  placeholder="Working Email" type="email" maxlength="50" class="contact-group form-control" value="{{$customer->bemail or ''}}" name="bemail"  autocomplete="off">
                                        </div>
                                    </div>

                                    {{-- ********  Working Abn  ************** --}}
                                    <div id="abnDiv" class="form-group{{ $errors->has('abn') ? ' has-error' : '' }}">
                                        <label for="abn" class="col-md-4 control-label">ABN Number</label>
                                        <div class="col-md-6">
                                            <input maxlength="15" placeholder="Working ABN" class="form-control abnInput" value="{{$customer->abn or ''}}" id="abn" name="abn">
                                        </div>
                                    </div>
                                    {{-- *******  First Name  ************** --}}
                                    <div id="firstnameDiv" class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}  required">
                                        <label for="firstname" class="col-md-4 control-label">First Name</label>
                                        <div class="col-md-6">
                                            <input maxlength="20" placeholder="First Name" class="form-control" value="{{$customer->firstname or ''}}" name="firstname">

                                        </div>
                                    </div>
                                    {{-- *******  Last Name  ************** --}}
                                    <div id="lastnameDiv" class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                        <label for="lastname" class="col-md-4 control-label">Last Name</label>
                                        <div class="col-md-6">
                                            <input  maxlength="20" placeholder="Last Name" class="form-control" value="{{$customer->lastname or ''}}" name="lastname">
                                        </div>
                                    </div>
                                    {{-- *******  Personal Mobile Phone ************** --}}
                                    <div id="phoneDiv" class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone" class="col-md-4 control-label">Contact Phone</label>
                                        <div class="col-md-6">
                                            <input maxlength="20" placeholder="Personal Mobile Phone" class="pcontact-group form-control bphoneInput" value="{{$customer->phone or ''}}" name="phone">
                                        </div>
                                    </div>

                                    {{-- *******  Personal Email ************** --}}
                                    <div id="emailDiv" class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">Personal Email</label>
                                        <div class="col-md-6">
                                            <input maxlength="50" placeholder="Personal Email" class="pcontact-group form-control" value="{{$customer->email or ''}}" name="email">
                                        </div>
                                    </div>
                                    {{-- *******  Contact Position ************** --}}
                                    <div id="positionDiv"  class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                                        <label for="position" class="col-md-4 control-label">Contact Position</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="position">
                                                <option disabled selected>Contact Position</option>
                                                @foreach($positions as $position)
                                                @if($position->status=='Active')
                                                <option value="{{$position->id}}"
                                                        <?= (isset($customer) && $position->id == $customer->position) ? 'selected' : ''; ?>>{{$position->position}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- *******  Website ************** --}}
                                    <div id="websiteDiv" class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                                        <label for="website" class="col-md-4 control-label">Website</label>
                                        <div class="col-md-6">
                                            <input placeholder="http://URL.COM" class="form-control" value="{{$customer->website or ''}}" name="website">
                                        </div>
                                    </div>
                                    <?PHP
                                    if (isset($customer->extend)) {
                                        $social = (array) json_decode($customer->extend);
                                    }
                                    ?>
                                    {{-- *******  Social  ************** --}}
                                    <div  id="extendDiv">
                                        <div class="form-group">
                                            <label for="facebook" class="col-md-4 control-label">Facebook</label>
                                            <div class="col-md-6">
                                                <input name="extend[facebook]" type="text"  maxlength="80"  placeholder="facebook" id="facebook" class="form-control" value="{{$social ['facebook'] or ''}}">
                                                <div id="facebookerr" class="error"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="twitter" class="col-md-4 control-label">Twitter</label>
                                            <div class="col-md-6">
                                                <input name="extend[twitter]" type="text" maxlength="80"  placeholder="twitter" id="twitter" class="form-control" value="{{$social ['twitter'] or ''}}">
                                                <div id="twittererr" class="error"></div>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="instagram" class="col-md-4 control-label">Instagram</label>
                                            <div class="col-md-6">
                                                <input name="extend[instagram]" type="text" maxlength="80"  placeholder="instagram" id="instagram" class="form-control" value="{{$social ['instagram'] or ''}}">
                                                <div id="instagramerr" class="error"></div>

                                            </div>
                                        </div>
                                    </div>
                                    {{-- *******  Address ************** --}}
                                    <div id="addressDiv" class="{{ $errors->has('address') ? ' has-error' : '' }}" >

                                        Current Address is: {{$customer->address or ''}}
                                        <div class="form-group">
                                            <label for="unit" class="col-md-4 control-label">Unit</label>
                                            <div class="col-md-3">
                                                <input maxlength="10" placeholder="Unit" class="form-control" value="{{$customer->address_unit or ''}}" name="unit">
                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <label for="number" class="col-md-4 control-label">Number</label>
                                            <div class="col-md-3">
                                                <input maxlength="15" placeholder="Number" class="form-control" value="{{$customer->address_no or ''}}" name="number">
                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <label for="street" class="col-md-4 control-label">Street</label>
                                            <div class="col-md-3">
                                                <input maxlength="30" placeholder="Street" class="form-control" value="{{$customer->address_street or ''}}" name="street">
                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <label for="searchSttype" class="col-md-4 control-label">Street Type</label>
                                            <div class="col-md-3">
                                                <input maxlength="20" placeholder="Street Type" name="searchSttype" value="{{$customer->searchSttype or ''}}" type="text" maxlength="30"  id="searchSttype" class="form-control"  autocomplete="off">
                                            </div>
                                        </div>
                                    </div>

                                    <div id="addressDiv" hidden=""  class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label for="address" class="col-md-4 control-label">Address</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->address or ''}}" name="address">
                                        </div>
                                    </div>

                                    {{-- *******  search postcode  ************** --}}
                                    <div id="postcodeDiv"  class="form-group  required">
                                        <label for="lastname" class="col-md-4 control-label">Postcode Lookup</label>
                                        <div class="col-md-6">
                                            <input name="searchPostcode" type="text"  maxlength="30" placeholder="Search Suburb or Postcode" id="SearchPostcodes" class="form-control" value="{{(isset($customer->postcode)&&$customer->postcode!='') ? $customer->postcode.', '.$customer->suburb.', '.$customer->state : ''}}"  autocomplete="off">
                                            <input name="LockSearch" readonly type="text" maxlength="30"  placeholder="Search Suburb or Postcode" id="LockSearch" class="form-control" value="{{$customer->postcode or ''}}, {{$customer->suburb or ''}}, {{$customer->state or ''}}" autocomplete="off">

                                            <input style="display: none" name="postcode" type="text" id="postcode" class="form-control" value="{{$customer->postcode or ''}}">
                                            <input style="display: none" name="suburb" type="text" id="suburb" class="form-control" value="{{$customer->suburb or ''}}">
                                            <input style="display: none" name="state" type="text" id="state" class="form-control" value="{{$customer->state or ''}}">
                                        </div>
                                    </div>
                                </dl>
                            </div>
                            <div class="col-sm-6">
                                <dl class="dl-horizontal">
                                    {{-- *******  Business Type  ********* --}}
                                    <div class="form-group required">
                                        <label for="btype" class="col-md-4 control-label">Business Type</label>
                                        <div class="col-md-6">
                                            <select id="businessTypeSelect" class="form-control" name="btype">
                                                <option selected disabled>Select...</option>
                                                @foreach($types as $type)
                                                <option value="{{$type->id}}" <?= (isset($customer->btype)) ? (($type->id == $customer->btype) ? 'selected' : '') : '' ?>>{{$type->type}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('btype'))
                                            <span class="help-block"><strong>{{ $errors->first('btype') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- *******  Acount Manager  ********* --}}
                                    <div id="accountmanagerDiv"  class="form-group required">
                                        <label for="account_manager" class="col-md-4 control-label">Account Manager</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="account_manager">
                                                <option selected disabled>Select...</option>
                                                @foreach($users as $user)
                                                <option value="{{$user->id}}"<?= isset($customer->account_manager) ? (($user->id == $customer->account_manager) ? 'selected' : '') : ''; ?>>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <hr>
                                    <div  id="mpmDiv" >
                                        <div class="form-group">
                                            <label for="mpm_user_id" class="col-md-4 control-label">MPM User ID</label>
                                            <div class="col-md-3">
                                                <input name="mpm_user_id" type="text" maxlength="30"  placeholder="MPM user id" id="instagram" class="form-control" value="{{$customer->mpm_user_id or ''}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="mpm_type_id" class="col-md-4 control-label">MPM <span id="SelectedBusinessType"></span> ID</label>
                                            <div class="col-md-3">
                                                <input name="mpm_type_id" type="text" maxlength="30"  placeholder="MPM type id" id="instagram" class="form-control" value="{{$customer->mpm_type_id or ''}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div id="brothelDiv"  class="form-group">
                                        <label for="brothel_id" class="col-md-4 control-label">Brothel ID</label>
                                        <div class="col-md-3">
                                            <input name="brothel_id" type="text" maxlength="30"  placeholder="Brothel ID" id="instagram" class="form-control" value="{{$customer->brothel_id or ''}}">
                                        </div>
                                    </div>
                                    <hr>
                                    {{-- ********* Additional Contact *******--}}
                                    <div  id="additionalContactsDiv">
                                        <div>Additional Contacts
                                            <span id="addContactButton" class="btn btn-success glyphicon glyphicon-plus pull-right"></span>
                                        </div>
                                        <br>
                                        {{-- convert PHP array tp javascript array --}}
                                        <script type="text/javascript">var extContact = <?= isset($customer->additional_contacts) ? $customer->additional_contacts : "''"; ?>;</script>

                                        <div id="additionalContactsWrapper">
                                            <ol>
                                            </ol>
                                        </div>
                                    </div>
                                    {{-- ******************************--}}
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection
<div class="addContactTemplate" style="display:none;" id="addContactTemplate[]">
    <li>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="removeButton btn btn-xs btn-danger glyphicon glyphicon-minus pull-right"></span>
                </div>
                <div class="panel-body center-block">
                    <div class="form-group">
                        <div class="col-md-4"><input id="fname" name="additional_contact[fname][]" class="form-control" placeholder="first name"></div>
                        <div class="col-md-4"><input id="lname" name="additional_contact[lname][]" class="form-control" placeholder="last name"></div>
                        <div class="col-md-4"><input id="phone" name="additional_contact[phone][]" class="bphoneInput form-control" placeholder="phone"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <select class="form-control" id="position" name="additional_contact[position][]">
                                <option></option>
                                @foreach($positions as $position)
                                <option value="{{$position->id}}">{{$position->position}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-8"><input id="email" name="additional_contact[email][]" class="form-control" placeholder="email"></div>
                    </div>
                </div>
            </div>
        </div>
    </li>
</div>
