<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class CustomerTasks extends Model {

    private $tableName = 'customer_tasks';
    protected $fillable = ['customerid',
        'mpmchecklist', 'mpmdetailsfilled', 'mpmconfirmplan', 'mpmbaselocation', 'mpmpaid', 'mpmapprovedpics', 'mpmmainimage', 'mpmseo', 'mpmlive',
        'bchecklist', 'bdelaconcreated', 'bdelaconid', 'bprofilecreated', 'bdelaconphone', 'bdescription', 'bdapprovedc', 'b150200c', 'biapprovedc', 'baddtoloc', 'b500500c', 'bsmiapprovedc', 'blive',
        'abchecklist', 'abdelaconcreated', 'abdelaconid', 'abprofilecreated', 'abdelaconphone', 'abseo', 'ab500292created', 'abi500292ca', 'absmicreated', 'absmiabc', 'ablive',
        'updated_at', 'created_at'];

    public function __construct() {
        $this->CheckTableExists();
    }

    private function CheckTableExists() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`customerid` INT NOT NULL , "
                    . "`mpmchecklist` BOOLEAN NULL , "
                    . "`mpmdetailsfilled` BOOLEAN NULL , "
                    . "`mpmconfirmplan` BOOLEAN NULL , "
                    . "`mpmbaselocation` text NULL , "
                    . "`mpmpaid` BOOLEAN NULL , "
                    . "`mpmapprovedpics` BOOLEAN NULL , "
                    . "`mpmmainimage` BOOLEAN NULL , "
                    . "`mpmseo` BOOLEAN NULL , "
                    . "`mpmlive` BOOLEAN NULL , "
                    . "`bchecklist` BOOLEAN NULL , "
                    . "`bdelaconcreated` BOOLEAN NULL , "
                    . "`bdelaconid` text NULL , "
                    . "`bprofilecreated` BOOLEAN NULL , "
                    . "`bdelaconphone` text NULL , "
                    . "`bdescription` BOOLEAN NULL , "
                    . "`bdapprovedc` BOOLEAN NULL , "
                    . "`b150200c` BOOLEAN NULL , "
                    . "`biapprovedc` BOOLEAN NULL , "
                    . "`baddtoloc` BOOLEAN NULL , "
                    . "`b500500c` BOOLEAN NULL , "
                    . "`bsmiapprovedc` BOOLEAN NULL , "
                    . "`blive` BOOLEAN NULL , "
                    . "`abchecklist` BOOLEAN NULL , "
                    . "`abdelaconcreated` BOOLEAN NULL , "
                    . "`abdelaconid` text NULL , "
                    . "`abprofilecreated` BOOLEAN NULL , "
                    . "`abdelaconphone` text NULL , "
                    . "`abseo` BOOLEAN NULL , "
                    . "`ab500292created` BOOLEAN NULL , "
                    . "`abi500292ca` BOOLEAN NULL , "
                    . "`absmicreated` BOOLEAN NULL , "
                    . "`absmiabc` BOOLEAN NULL , "
                    . "`ablive` BOOLEAN NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
    }

    public static function getCustomerTasks($customerID) {
        new CustomerTasks();
        $result = DB::select("SELECT * FROM customer_tasks WHERE customerid = $customerID");
        return (isset($result) && !empty($result)) ? $result[0] : false;
    }

}
