<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Myob;

class RefreshMyobToken extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'myob:refreshToken';
    protected $name = 'myob:refreshToken';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh the myob api token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $myob = new Myob();
        $myob->renewToken();
    }

}
