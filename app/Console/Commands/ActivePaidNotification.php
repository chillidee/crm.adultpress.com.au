<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use PDF;

class ActivePaidNotification extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:dueactivepaid';
    protected $name = 'email:dueactivepaid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $dateToday = date("Y-m-d H:i:s", mktime(8, 0, 0, date("m"), date("d"), date("Y")));
        $dateNextWeek = date("Y-m-d H:i:s", mktime(8, 0, 0, date("m"), date("d") + 7, date("Y")));

        $query = "SELECT types.type, customers.id,customers.bname,cstatuses.dueDatetime,statuses.name "
                . "FROM types,customers,cstatuses,statuses,invoices,orders "
                . "WHERE customers.btype = types.id "
                . "AND customers.id = cstatuses.customerid "
                . "AND cstatuses.latest=1 "
                . "AND customers.id = invoices.customerid "
                . "AND orders.invoiceid = invoices.id "
                . "AND orders.active = 1 "
                . "AND cstatuses.statusId = statuses.id "
                . "AND (statuses.name = 'Active Paid Customer' "
                . "AND orders.expirydate <= '$dateNextWeek' "
                . "AND orders.expirydate >= '$dateToday'"
                . "OR statuses.name='To Be Invoiced')";


        $customers = DB::select($query);

        //**** SEND EMAIL NOTIFICATION TO ACCOUNTS  ****
        if (isset($customers) && !empty($customers)):
            $data = array('customers' => $customers);

            Mail::send("reports/overdueactivepaid", $data, function($message) {
                $message->from('crm.adultpress@gmail.com')
                        ->to('info@adultpress.com.au')
                        ->subject('Overdue Active Paid Customers');
            });
        endif;
    }

}
