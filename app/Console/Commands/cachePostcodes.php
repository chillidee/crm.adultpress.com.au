<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Maps;

class cachePostcodes extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maps:cachePostcodes';
    protected $name = 'maps:cachePostcodes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates cached results for postcode search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $maps = new Maps();
        $maps->cacheFindCustomersResults();
    }

}
