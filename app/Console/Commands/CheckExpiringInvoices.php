<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AppAutomation;

class CheckExpiringInvoices extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:CheckExpiringInvoices';
    protected $name = 'invoices:CheckExpiringInvoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks for active invoices expiring whithin 7 days then duplicates them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $automation = new AppAutomation();
        $automation->checkActiveCustomers();
        $automation->checkExpriedOrders();
        $automation->sendInvoicesToBeSentEmails();
    }

}
