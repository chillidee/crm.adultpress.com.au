<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\ActivePaidNotification',
        'App\Console\Commands\CheckExpiringInvoices',
        'App\Console\Commands\RefreshMyobToken',
        'App\Console\Commands\cachePostcodes'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->command('email:dueactivepaid >> storage\schedule_logs.log')
                ->dailyAt('09:00');
        $schedule->command('invoices:CheckExpiringInvoices >> storage\schedule_logs.log')
                ->daily();
        $schedule->command('myob:refreshToken >> storage\schedule_logs.log')
                ->everyTenMinutes();
        //$schedule->command('maps:cachePostcodes >> storage\schedule_logs.log')
        //->dailyAt('02:00');
//        $schedule->command('email:dueactivepaid >> storage\schedule_logs.log')
//                ->everyMinute();
//        $schedule->command('invoices:CheckExpiringInvoices >> storage\schedule_logs.log')
//                ->everyFiveMinutes();
//        $schedule->command('myob:refreshToken >> storage\schedule_logs.log')
//                ->everyMinute();
    }

}
