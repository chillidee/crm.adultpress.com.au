<?php

namespace App;

use DB;
use Mail;
use App\Invoice;
use App\Order;
use Illuminate\Database\Eloquent\Model;
use App\Myob;
use App\InvoiceStatus;

class AppAutomation extends Model {

    public function checkActiveCustomers() {
        $invoiceArray = array();
        $newInvoicesArray = array();
        $emailArray = array();
        $orders = DB::select($this->createQueryString());
        if (isset($orders) && !empty($orders)):
            foreach ($orders as $order):
                if (!in_array($order->invoiceid, $invoiceArray, true)):
                    array_push($invoiceArray, $order->invoiceid);
                endif;
            endforeach;
            foreach ($invoiceArray as $oldInvoiceID):
                array_push($newInvoicesArray, $this->dupicateExistingInvoiceNew($oldInvoiceID));
            endforeach;
            foreach ($newInvoicesArray as $id):
                $emailArray[$id] = DB::select("SELECT * FROM orders WHERE invoiceid = $id");
            endforeach;
            if (isset($emailArray) && !empty($emailArray)):
                $this->sendEmail('reports/duplicatedInvoicesCreated', array('emailArray' => $emailArray), 'info@adultpress.com.au', 'CRM: New Invoices Created');
            endif;
        endif;
    }

    private function getInvoicesToBeSentData() {
        $query = "SELECT invoices.id "
                . "FROM orders, invoices, cstatuses "
                . "WHERE orders.invoiceid = invoices.id "
                . "AND orders.active = 1 "
                . "AND invoices.customerid = cstatuses.customerid "
                . "AND cstatuses.latest = 1 "
                . "AND cstatuses.statusId = 10014";
        return DB::select($query);
    }

    public function sendInvoicesToBeSentEmails() {
        foreach ($this->getInvoicesToBeSentData() as $Invoice):
            Invoice::sendNewInvoiceEmail($Invoice->id);
        endforeach;
    }

    private function getOverdueInvoiceData() {
        $customers = array();
        $query = "SELECT invoices.id, invoices.customerid "
                . "FROM orders, invoices, customers, cstatuses "
                . "WHERE orders.invoiceid = invoices.id "
                . "AND invoices.customerid = customers.id "
                . "AND orders.expirydate <= '" . date("Y-m-d") . "' "
                . "AND orders.active = 1 "
                . "AND customers.id = cstatuses.customerid "
                . "AND cstatuses.latest = 1 "
                . "AND cstatuses.statusId = 10001";
        $orders = array();
        foreach (DB::select($query) as $order):
            if (isset($order) && !empty($order) && !in_array($order->customerid, $customers)):
                $_amount_paid = (float) DB::select("SELECT sum(amount) total FROM `payments` WHERE `invoiceid` = $order->id")[0]->total;
                $invoice_amount = (float) DB::select("SELECT sum(amount*1.1) total FROM orders WHERE orders.invoiceid= $order->id")[0]->total;
                if ($invoice_amount > $_amount_paid && $invoice_amount > 0.00):
                    if (!isset($orders[$order->customerid])):
                        $orders[$order->customerid] = array();
                    endif;
                    $orders[$order->customerid][$order->id] = array(
                        'invoice_id' => (string) $order->id,
                        'bname' => DB::select("SELECT bname FROM `customers` WHERE `id` = $order->customerid")[0]->bname,
                        'amount' => number_format($invoice_amount, 2),
                        'myob' => DB::select("SELECT myob FROM `invoices` WHERE `id` = $order->id")[0]->myob
                    );
                endif;
            endif;
        endforeach;
        return $orders;
    }

    private function changeExpriedInvoiceStaus() {
        $changedStatus = array();
        $query = "SELECT invoices.*, orders.* "
                . "FROM orders, invoices, customers, cstatuses "
                . "WHERE orders.invoiceid = invoices.id "
                . "AND invoices.customerid = customers.id "
                . "AND orders.expirydate <= '" . date("Y-m-d", mktime(8, 0, 0, date("m"), date("d") - 1, date("Y"))) . "' "
                . "AND orders.active = 1 "
                . "AND customers.id = cstatuses.customerid "
                . "AND cstatuses.latest = 1 "
                . "AND cstatuses.statusId = 10004";
        $orders = DB::select($query);
        foreach ($orders as $order):
            if (isset($order->customerid) && !empty($order->customerid) && !in_array($order->customerid, $changedStatus)):
                $this->updateStatus($order->customerid, '10001', date("Y-m-d H:i:s", mktime(8, 0, 0, date("m"), date("d") + 1, date("Y"))));
                array_push($changedStatus, $order->customerid);
            endif;
        endforeach;
    }

    public function checkExpriedOrders() {
        $this->changeExpriedInvoiceStaus();
        $this->sendOverdueInvoiceEmails($this->getOverdueInvoiceData());
    }

    private function sendOverdueInvoiceEmails($orderArray) {
        $this->sendEmail('reports/overdueInvoices', array('orderArray' => $orderArray), 'info@adultpress.com.au', 'CRM: Overdue Invoices');
    }

    public function sendEmail($template, $data, $to, $subject) {
        Mail::send($template, $data, function($message) use ($to, $subject) {
            $message->from('crm.adultpress@gmail.com')
                    ->to($to)
                    ->subject($subject);
        });
    }

    public static function sendMail($template, $data, $to, $subject) {
        Mail::send($template, $data, function($message) use ($to, $subject) {
            $message->from('crm.adultpress@gmail.com')
                    ->to($to)
                    ->subject($subject);
        });
    }

    private function dupicateExistingOrders($oldInoviceID, $newInvoiceID) {
        $returnArray = array();
        $returnArray['Total'] = 0;
        $_invoice = Invoice::find($newInvoiceID);
        if ((isset($oldInoviceID) && !empty($oldInoviceID)) && (isset($newInvoiceID) && !empty($newInvoiceID))):
            $oldOrders = DB::select("SELECT * FROM orders WHERE invoiceid = $oldInoviceID");
            if (isset($oldOrders) && !empty($oldOrders)):
                foreach ($oldOrders as $oldOrder):
                    $startdate = date("Y-m-d H:i:s", strtotime($oldOrder->expirydate . ' +1 day'));
                    $expirydate = $this->getInvoiceExpiryDate($startdate, $oldOrder->frequency);
                    $due_date = date("Y-m-d H:i:s", strtotime($expirydate . ' -7 days'));
                    $order = new Order;
                    $order->invoiceid = $newInvoiceID;
                    $order->sitename = $oldOrder->sitename;
                    $order->orderoption = $oldOrder->orderoption;
                    $order->amount = $oldOrder->amount;
                    $order->page = $oldOrder->page;
                    $order->myob_account = $oldOrder->myob_account;
                    $order->position = $oldOrder->position;
                    $order->startdate = $startdate;
                    $order->frequency = $oldOrder->frequency;
                    $order->expirydate = $expirydate;
                    $order->duedate = $due_date;
                    $order->active = 1;
                    $order->save();
                    $returnArray['Total'] = $returnArray['Total'] + $oldOrder->amount;
                    $returnArray['expirydate'] = $expirydate;
                    $returnArray[$order->id]['Description'] = Myob::createInvoiceDescription($_invoice, $order);
                    $returnArray[$order->id]['Total'] = $oldOrder->amount;
                    $returnArray[$order->id]['Account'] = Myob::getAccountUID($oldOrder->myob_account, $_invoice->customerid);
                    $this->deactivatePrevoiusOrder($oldOrder->id);
                endforeach;
                $returnArray['invoiceNumber'] = str_pad($newInvoiceID, 8, "0", STR_PAD_LEFT);
            endif;
        endif;
        return (isset($returnArray) && !empty($returnArray)) ? $returnArray : false;
    }

    private function dupicateExistingInvoiceNew($invoice_id) {
        if (isset($invoice_id) && !empty($invoice_id)):
            $_invoice = new Invoice;
            $oldInvoice = DB::select("SELECT * FROM invoices WHERE id = $invoice_id")[0];
            if (isset($oldInvoice) && !empty($oldInvoice)):
                $_invoice->addabnid = $oldInvoice->addabnid;
                $_invoice->customerid = $oldInvoice->customerid;
                $_invoice->userid = $oldInvoice->userid;
                $_invoice->myob = '';
                $_invoice->payment_method = $oldInvoice->payment_method;
                $_invoice->description = $oldInvoice->description;
                $_invoice->frequency = $oldInvoice->frequency;
                $_invoice->save();
                $orderDetails = $this->dupicateExistingOrders($oldInvoice->id, $_invoice->id);
                $this->updateStatus($_invoice->customerid, '10014', $orderDetails['expirydate']);
                $myobNumber = $this->createNewMyobInvoice($_invoice->customerid, $orderDetails);
                DB::update("UPDATE `invoices` SET `myob` = '$myobNumber' WHERE `invoices`.`id` = $_invoice->id");
                InvoiceStatus::updateInvoiceStatus($_invoice->id, '1');
                $id = $_invoice->id;
            endif;
        endif;
        return (isset($id) && !empty($id)) ? $id : false;
    }

    private function createNewMyobInvoice($customerid, $orderDetails) {
        $tempArray = array();
        foreach ($orderDetails as $index => $value):
            if (is_int($index)):
                array_push($tempArray, $value);
            endif;
        endforeach;
        $myob = new Myob();
        $data = array(
            'invoiceNumber' => $orderDetails['invoiceNumber'],
            'myobCustomerID' => $myob->getCustomerUIDFromApi($customerid),
            'invoiceTotal' => $orderDetails['Total'],
            'Orders' => $tempArray
        );
        return $myob->createNewInvoice($data);
    }

    private function dupicateExistingInvoice($invoice) {
        if (isset($invoice) && !empty($invoice)):
            $_invoice = new Invoice;
            $_invoice->addabnid = DB::select("SELECT id FROM addabns WHERE customerid=$invoice->customerid order by created_at DESC limit 1")[0]->id;
            $_invoice->customerid = $invoice->customerid;
            $_invoice->userid = $invoice->userid;
            $_invoice->myob = $invoice->myob;
            $_invoice->startdate = $invoice->startdate;
            $_invoice->expirydate = $invoice->expirydate;
            $_invoice->duedate = $invoice->duedate;
            $_invoice->payment_method = $invoice->payment_method;
            $_invoice->description = $invoice->description;
            $_invoice->frequency = $invoice->frequency;
            $_invoice->created_at = $invoice->created_at;
            $_invoice->save();

            //**** Save orders in orders table

            $order = new Order;
            $order->invoiceid = $_invoice->id;
            $order->sitename = $invoice->sitename;
            $order->orderoption = $invoice->sitename;
            $order->amount = $invoice->amount;
            $order->page = $invoice->page;
            $order->position = $invoice->position;

            $order->startdate = $invoice->startdate;
            $order->frequency = $invoice->frequency;
            $order->expirydate = $invoice->expirydate;
            $order->duedate = $invoice->duedate;
            $order->created_at = $_invoice->created_at;
            $order->save();

        endif;

        return $order;
    }

    public function updateStatus($customer_id, $newStatusCode, $due_date) {
        if (!isset($due_date) || empty($due_date)):
            $due_date = date("Y-m-d H:i:s", mktime(8, 0, 0, date("m") + 1, date("d"), date("Y")));
        endif;
        $status_id = DB::select("SELECT cstatuses.id FROM `cstatuses` WHERE `customerid` = $customer_id AND `latest` = 1")[0]->id;
        if (isset($status_id) && !empty($status_id)):
            DB::update("UPDATE `cstatuses` SET `statusId` = '$newStatusCode', `dueDatetime` = '$due_date' WHERE `cstatuses`.`id` = $status_id");
        endif;
    }

    private function deactivatePrevoiusOrder($order_id) {
        DB::update("UPDATE `orders` SET `active` = '0' WHERE `orders`.`id` = $order_id");
    }

    private function createInvoiceObject($customer) {
        $startdate = date("Y-m-d H:i:s", strtotime($customer->expirydate . ' +1 day'));

        $expirydate = $this->getInvoiceExpiryDate($startDate, $customer->frequency);

        $due_date = date("Y-m-d H:i:s", strtotime($expirydate . ' -7 days'));

        $new_invoice = array(
            'customerid' => $customer->customerid,
            'startdate' => $startdate,
            'duedate' => $due_date,
            'expirydate' => $expirydate,
            'myob' => '',
            'payment_method' => $customer->payment_method,
            'description' => $customer->description,
            'frequency' => $customer->frequency,
            'created_at' => date("Y-m-d H:i:s"),
            'sitename' => $customer->sitename,
            'orderoption' => $customer->orderoption,
            'amount' => $customer->amount,
            'page' => $customer->page,
            'position' => $customer->position,
            'userid' => $customer->userid
        );

        return (object) $new_invoice;
    }

    private function createQueryString() {
        $query = "SELECT orders.invoiceid "
                . "FROM orders, invoices, customers, cstatuses "
                . "WHERE cstatuses.customerid = invoices.customerid "
                . "AND orders.invoiceid = invoices.id "
                . "AND invoices.customerid = customers.id "
                . "AND orders.expirydate BETWEEN '" . date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d"), date("Y"))) . "' AND '" . date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") + 7, date("Y"))) . "' "
                . "AND orders.active = 1 "
                . "AND cstatuses.statusId = '600'"
                . "AND cstatuses.latest = '1'";
        return $query;
    }

    private function getInvoiceExpiryDate($startdate, $frequency) {
        if (isset($startdate) && !empty($startdate) && isset($frequency) && !empty($frequency)):
            switch ($frequency):
                case 'Weekly':
                    $expirydate = date("Y-m-d H:i:s", strtotime($startdate . '-1 day  +1 week'));
                    break;
                case 'Fortnightly':
                    $expirydate = date("Y-m-d H:i:s", strtotime($startdate . '-1 day  +2 weeks'));
                    break;
                case 'Monthly':
                    $expirydate = date("Y-m-d H:i:s", strtotime($startdate . '-1 day +1 month'));
                    break;
                case 'Quarterly':
                    $expirydate = date("Y-m-d H:i:s", strtotime($startdate . '-1 day  +3 months'));
                    break;
                case 'Bi-annually':
                    $expirydate = date("Y-m-d H:i:s", strtotime($startdate . '-1 day  +6 months'));
                    break;
                case 'Annually':
                    $expirydate = date("Y-m-d H:i:s", strtotime($startdate . '-1 day  +1 year'));
                    break;
                default :
                    $expirydate = date("Y-m-d H:i:s", strtotime($startdate . '-1 day  +1 month'));
                    break;
            endswitch;
        endif;
        return ($expirydate) ? $expirydate : false;
    }

}
