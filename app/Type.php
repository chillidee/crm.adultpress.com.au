<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Type extends Model {

    protected $fillable = [
        'type', 'status'
    ];

    public function customers() {
        return $this->hasMany('App\Customer');
    }

    public function select() {
        $type = Type::All();
        return $type;
    }

    public function countit() {
        $count = DB::select("SELECT count(*) as count FROM `customers` WHERE btype=$this->id")[0];
        return $count->count;
    }

}
