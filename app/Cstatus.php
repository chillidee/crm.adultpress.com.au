<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cstatus extends Model {

    protected $fillable = [
        'statusid', 'datetime', 'dueDatetime','userid','customerid'
    ];

    public function Customer()
    {
        return $this->belongsTo('App\Customer', 'customerid' );
    }

    public function name() {
        return $this->belongsTo('App\Status', 'statusId');
    }

}
