<?php

namespace App;

use DB;
use App\Note;
use App\users;
use App\status;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    public $amountowing = 0;
    public $totalordered = 0;
    public $totalpaid = 0;
    protected $fillable = [
        'bname', 'bphone', 'bemail', 'abn', 'website', 'address'
        , 'suburb', 'state', 'postcode', 'firstname', 'lastname'
        , 'position', 'phone', 'email', 'additional_contacts'
        , 'account_manager', 'btype', 'mpm_user_id', 'mpm_type_id',
        'brothel_id', 'extend', 'myob_id', 'address_no', 'address_unit', 'address_street', 'searchSttype'
    ];

    public static function getList()
    {       
        $customers = '';
        $query = "SELECT customers.id, types.type AS btype, customers.bname, customers.bphone, customers.bemail, customers.suburb, customers.state, ";
        $query .= "users.name AS manager, statuses.name AS statusname, DATE_FORMAT(cstatuses.dueDatetime, '%d/%m/%Y') AS dueDatetime "; //, ";
        //$query .= "'' AS email, customers.firstname, customers.lastname, customers.phone, customers.abn, customers.website, customers.address, customers.postcode ";
        $query .= "FROM customers LEFT JOIN ";
        $query .= "types ON types.id = customers.btype LEFT JOIN ";
        $query .= "users ON users.id = customers.account_manager LEFT JOIN ";
        $query .= "cstatuses ON cstatuses.customerid = customers.id AND cstatuses.latest = 1 LEFT JOIN ";
        $query .= "statuses ON statuses.id = cstatuses.statusId ";
        $customers = DB::select($query);
        return $customers;
    }

    public function status()
    {
        return $this->hasOne('App\Cstatus', 'customerid')->where('latest', 1);
    }

    public function statusName()
    {
        $status = $this->status()->get();
        return $status->name;

    }

    public function type()
    {
        return $this->belongsTo('App\Type', 'btype', 'id');
    }

    public function currentorders()
    {
        $availableorders = '';
        $query = "SELECT orders.sitename,invoices.id,invoices.startdate,invoices.expirydate FROM invoices,orders WHERE invoices.customerid= $this->id AND invoices.id = orders.invoiceid AND invoices.startdate <= date(now()) AND date(now()) <= invoices.expirydate GROUP BY orders.sitename";
        $availableorders = DB::select($query);
        return $availableorders;
    }

    public function invoices()
    {
        $invoices = '';
        if (isset($this->id)) {
            $invoices = DB::select("SELECT orderstable.*,invoices.*,users.name FROM (SELECT invoiceid,GROUP_CONCAT(orders.sitename)sitenames,GROUP_CONCAT(orders.amount)amounts from orders GROUP BY invoiceid) orderstable , invoices,users WHERE orderstable.invoiceid = invoices.id and customerid = $this->id AND users.id = invoices.userid  ORDER BY invoices.created_at ASC");

            if ($invoices != NULL)
                return $invoices;
        }
        return $invoices;
    }

    public function notes()
    {
        if (isset($this->id)) {
            $notes = DB::select("SELECT notes.*,users.name FROM notes,users WHERE users.id = notes.userId AND notes.customerId= $this->id  ORDER BY datetime DESC");
            if ($notes != NULL)
                return $notes;
        }
        return '';
    }

    public function positions()
    {
        $positions = position::All();
        return $positions;
    }

    public function accmanager()
    {
        $manager = '';
        if (isset($this->account_manager) && $this->account_manager > 0) {
            $manager = User::find($this->account_manager);
            if (isset($manager))
                $manager = $manager->name;
        }

        return $manager;
    }

    public function position()
    {
        if (isset($this->position) && $this->position > 0) {
            $position = position::find($this->position);
            return $position->position;
        }
        return '';

//if($position)
    }

    // last status of the customer
    /*
      public function cstatus() {
      $id = $this->id;
      $cstatus = DB::select("SELECT * FROM cstatuses WHERE customerid= $id  order by datetime desc limit 1");
      //$cstatus = DB::select("SELECT name FROM statuses WHERE orderid= $statusid limit 1");
      if (isset($cstatus[0]))
      return $cstatus[0];
      return '';
      }
     */
    // status name for customer asdfasdf

    public function assignedtask()
    {
        if (isset($this->account_manager) && $this->account_manager > 0) {
            $user = User::find($this->account_manager);
            if (isset($user->assignedto)) {
                if ($user->assignedto == NULL OR $user->assignedto == $user->id) {
                    return $user->name;
                } else {
                    $user = User::find($user->assignedto);
                    return $user->name;
                }
            }
        } else {
            return 'unknown!';
        }
        return Auth::user()->name;
    }

    public function amountPaidforInvoice($invoiceid)
    {
        $query = "SELECT SUM(payments.amount)paid FROM payments WHERE payments.invoiceid = $invoiceid";

        $result = DB::select($query);
        if (isset($result[0]->paid))
            return $result[0]->paid;
        return 0;
    }

    public function amountPaid()
    {
        $query = "SELECT sum(amount)paid FROM payments,customers,invoices WHERE payments.invoiceid = invoices.id AND invoices.customerid = customers.id AND customers.id = $this->id ";

        $result = DB::select($query);
        if (isset($result[0]->paid))
            return $result[0]->paid;
        return 0;
    }

    public function amountInvoiced()
    {
        $query = "SELECT sum(orders.amount)paid
FROM orders,invoices
WHERE orders.invoiceid = invoices.id AND invoices.customerid = $this->id";
        $result = DB::select($query);
        if (isset($result[0]->paid))
            return $result[0]->paid;
        return 0;
    }

    public function invaddabn()
    {
        $result = '';
        $query = "SELECT * FROM `addabns` WHERE customerid = $this->id ORDER BY created_at DESC LIMIT 1";
        $result = DB::select($query);
        if (isset($result[0]))
            return $result[0];
        return '';
    }

    public function numberofinv()
    {

        $result = '';
        $query = "SELECT count(id)counts FROM `invoices` WHERE customerid = $this->id";
        $result = DB::select($query)[0]->counts;

        if (isset($result)) {
            return $result;
        }
        return '';
    }

    public function amountowing()
    {
        $query = "SELECT sum(orders.amount)as amount FROM orders,invoices,customers WHERE orders.invoiceid = invoices.id AND invoices.customerid = customers.id
AND customers.id=$this->id";
        $ordered = DB::select($query)[0]->amount;
        if ($ordered != NULL && $ordered > 0) {
            $ordered = $ordered * 1.1;
            $this->totalordered = $ordered;
        } else {
            $ordered = 0;
            $this->totalordered = $ordered;
        }
        $query = "SELECT sum(payments.amount)as amount FROM payments,invoices,customers WHERE payments.invoiceid = invoices.id AND invoices.customerid = customers.id
AND customers.id= $this->id";
        $paid = DB::select($query)[0]->amount;
        if ($paid != NULL && $paid > 0) {
            $paid = $paid;
            $this->totalpaid = $paid;
        } else {
            $paid = 0;
            $this->totalpaid = $paid;
        }
        $this->amountowing = number_format($ordered - $paid, 2);
        $this->totalordered = number_format($ordered, 2);
        $this->totalpaid = number_format($paid, 2);
        return $this->amountowing;
    }

    public function getexpiries()
    {
        $expiries = [];
        $query = "SELECT concat(orders.sitename,'--',orders.page)identifier FROM orders,invoices WHERE orders.invoiceid = invoices.id AND invoices.customerid = $this->id
GROUP BY identifier";
        $orderCategories = DB::select($query);
        foreach ($orderCategories as $category) {
            $query = "SELECT orders.* FROM orders,invoices WHERE orders.invoiceid = invoices.id AND invoices.customerid = $this->id  AND concat(orders.sitename,'--',orders.page) = '" . $category->identifier . "'ORDER BY orders.duedate DESC limit 1";
            $lastorder = DB::select($query);
            if (isset($lastorder[0])) {
                if ($lastorder[0]->duedate <= date("Y-m-d")) {
                    //if ($lastorder[0]->duedate <= date("Y-m-d") && $lastorder[0]->active == 1) {
                    array_push($expiries, $lastorder[0]);
                }
            }
        }
        return $expiries;
    }

    public function orders()
    {
        $query = "SELECT orders.* FROM invoices,orders WHERE orders.invoiceid=invoices.id AND invoices.customerid= $this->id";
        $orders = DB::select($query);
        return $orders;
    }

    public static function GetAdExpiry($customer_id)
    {
        $query = "SELECT orders.expirydate as ad_expiry "
            . "FROM orders, invoices "
            . "WHERE invoices.id = orders.invoiceid "
            . "AND orders.active = 1 "
            . "AND invoices.customerid = $customer_id "
            . "ORDER BY `ad_expiry` "
            . "DESC LIMIT 1";
        $ad = DB::select($query);
        return (isset($ad) && !empty($ad) && isset($ad[0]->ad_expiry) && !empty($ad[0]->ad_expiry)) ? date("d/m/Y", strtotime($ad[0]->ad_expiry)) : '';
    }

}
