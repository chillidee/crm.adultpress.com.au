<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Customer;
use App\DeletedOrder;
use App\DeletedCustomerInvoices;
use App\DeletedNotes;
use App\DeletedAddAbns;
use App\Invoice;
use App\Order;
use App\Addabn;
use App\Note;
use App\Cstatus;

class DeletedCustomer extends Model {

    private $tableName = 'deleted_customers';
    protected $fillable = ['customer_id', 'bname', 'bphone', 'bemail', 'abn', 'website', 'address', 'suburb', 'state', 'postcode', 'firstname', 'lastname', 'position',
        'phone', 'email', 'additional_contacts', 'account_manager', 'btype', 'mpm_user_id', 'mpm_type_id', 'brothel_id', 'extend', 'address_no', 'address_unit', 'address_street',
        'searchSttype', 'deletedBy'];

    public function __construct() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`customer_id` int NOT NULL , "
                    . "`bname` text NULL , "
                    . "`bphone` text NULL , "
                    . "`bemail` text NULL , "
                    . "`abn` text NULL , "
                    . "`website` text NULL , "
                    . "`address` text NULL , "
                    . "`suburb` text NULL , "
                    . "`state` text NULL , "
                    . "`postcode` text NULL , "
                    . "`firstname` text NULL , "
                    . "`lastname` text NULL , "
                    . "`position` text NULL , "
                    . "`phone` text NULL , "
                    . "`email` text NULL , "
                    . "`additional_contacts` text NULL , "
                    . "`account_manager` int NULL , "
                    . "`btype` smallint NULL , "
                    . "`mpm_user_id` int NULL , "
                    . "`mpm_type_id` int NULL , "
                    . "`brothel_id` text NULL , "
                    . "`extend` text NULL , "
                    . "`address_no` text NULL , "
                    . "`address_unit` text NULL , "
                    . "`address_street` text NULL , "
                    . "`searchSttype` text NULL , "
                    . "`deletedBy` text NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
    }

    public function getDeletedCustomersTableData() {
        $query = "SELECT deleted_customers.customer_id as id,"
                . " deleted_customers.ID as deleted_id,"
                . " types.type as type,"
                . " deleted_customers.bname as name,"
                . " deleted_customers.bphone as phone,"
                . " deleted_customers.bemail as email,"
                . " deleted_customers.suburb, deleted_customers.state,"
                . " users.name as manager,"
                . " deleted_customers.deletedBy,"
                . " deleted_customers.created_at as delete_date "
                . "FROM "
                . "deleted_customers,"
                . "types,"
                . "users "
                . "WHERE "
                . "types.id = deleted_customers.btype "
                . "AND "
                . "deleted_customers.account_manager = users.id";
        $d_customers = DB::select($query);
        foreach ($d_customers as $d_customer):
            $d_customer->deletedBy = DB::select("SELECT name FROM users WHERE id = $d_customer->deletedBy")[0]->name;
        endforeach;
        return $d_customers;
    }

    public function insertDeletedCustomer($customer_id, $deleted_by) {
        $customer = Customer::find($customer_id);
        $this->insertDeletedOrders($customer_id);
        $this->insertDeletedCustomerDetails($customer_id);
        $deletedCustomer = new DeletedCustomer();
        $deletedCustomer->customer_id = $customer_id;
        $deletedCustomer->bname = $customer->bname;
        $deletedCustomer->bphone = $customer->bphone;
        $deletedCustomer->bemail = $customer->bemail;
        $deletedCustomer->abn = $customer->abn;
        $deletedCustomer->website = $customer->website;
        $deletedCustomer->address = $customer->address;
        $deletedCustomer->suburb = $customer->suburb;
        $deletedCustomer->state = $customer->state;
        $deletedCustomer->postcode = $customer->postcode;
        $deletedCustomer->firstname = $customer->firstname;
        $deletedCustomer->lastname = $customer->lastname;
        $deletedCustomer->position = $customer->position;
        $deletedCustomer->phone = $customer->phone;
        $deletedCustomer->email = $customer->email;
        $deletedCustomer->additional_contacts = $customer->additional_contacts;
        $deletedCustomer->account_manager = $customer->account_manager;
        $deletedCustomer->btype = $customer->btype;
        $deletedCustomer->mpm_user_id = $customer->mpm_user_id;
        $deletedCustomer->mpm_type_id = $customer->mpm_type_id;
        $deletedCustomer->brothel_id = $customer->brothel_id;
        $deletedCustomer->extend = $customer->extend;
        $deletedCustomer->address_no = $customer->address_no;
        $deletedCustomer->address_unit = $customer->address_unit;
        $deletedCustomer->address_street = $customer->address_street;
        $deletedCustomer->searchSttype = $customer->searchSttype;
        $deletedCustomer->deletedBy = $deleted_by;
        $deletedCustomer->save();
    }

    private function insertDeletedOrders($customer_id) {
        $deletedOrders = new DeletedOrder();
        $deletedInvoice = new DeletedCustomerInvoices();
        $invoices = DB::select("SELECT * FROM invoices WHERE customerid = $customer_id");
        foreach ($invoices as $invoice):
            $deletedInvoice->insertInvoice($invoice->id);
            $deletedOrders->insertOrders($invoice->id);
        endforeach;
    }

    private function insertDeletedCustomerDetails($customer_id) {
        $deletedNotes = new DeletedNotes();
        $deltedAddAbns = new DeletedAddAbns();
        $deletedNotes->insertNotes($customer_id);
        $deltedAddAbns->insertAddAbns($customer_id);
    }

    public static function deleteDeletedCustomer($database_id) {
        $customer = DeletedCustomer::find($database_id);
        $old_customer_id = $customer->customer_id;
        $invoices = DB::select("SELECT * FROM deleted_customer_invoices WHERE customerid = $old_customer_id");
        foreach ($invoices as $invoice):
            $orders = DB::select("SELECT * FROM `deleted_orders` WHERE `invoiceid` = $invoice->invoiceid");
            foreach ($orders as $order):
                DB::delete("DELETE FROM `deleted_orders` WHERE `deleted_orders`.`ID` = $order->ID");
            endforeach;
            DB::delete("DELETE FROM `deleted_customer_invoices` WHERE `deleted_customer_invoices`.`ID` = $invoice->ID");
        endforeach;
        $notes = DB::select("SELECT * FROM `deleted_notes` WHERE `customerId` = $old_customer_id");
        foreach ($notes as $note):
            DB::delete("DELETE FROM `deleted_notes` WHERE `deleted_notes`.`ID` = $note->ID");
        endforeach;
        $addabns = DB::select("SELECT * FROM `deleted_add_abns` WHERE `customerid` = $old_customer_id");
        foreach ($addabns as $addabn):
            DB::delete("DELETE FROM `deleted_add_abns` WHERE `deleted_add_abns`.`ID` = $addabn->ID");
        endforeach;
        return DB::delete("DELETE FROM `deleted_customers` WHERE `deleted_customers`.`ID` = $database_id");
    }

    public static function restoreDeletedCustomer($database_id) {
        $d_customer = DeletedCustomer::find($database_id);
        $old_customer_id = $d_customer->customer_id;
        $dueDate = "";
        $customer = new Customer();
        $customer->bname = $d_customer->bname;
        $customer->bphone = $d_customer->bphone;
        $customer->bemail = $d_customer->bemail;
        $customer->abn = $d_customer->abn;
        $customer->website = $d_customer->website;
        $customer->address = $d_customer->address;
        $customer->suburb = $d_customer->suburb;
        $customer->state = $d_customer->state;
        $customer->postcode = $d_customer->postcode;
        $customer->firstname = $d_customer->firstname;
        $customer->lastname = $d_customer->lastname;
        $customer->position = $d_customer->position;
        $customer->phone = $d_customer->phone;
        $customer->email = $d_customer->email;
        $customer->additional_contacts = $d_customer->additional_contacts;
        $customer->account_manager = $d_customer->account_manager;
        $customer->btype = $d_customer->btype;
        $customer->mpm_user_id = $d_customer->mpm_user_id;
        $customer->mpm_type_id = $d_customer->mpm_type_id;
        $customer->brothel_id = $d_customer->brothel_id;
        $customer->extend = $d_customer->extend;
        $customer->address_no = $d_customer->address_no;
        $customer->address_unit = $d_customer->address_unit;
        $customer->address_street = $d_customer->address_street;
        $customer->searchSttype = $d_customer->searchSttype;
        $customer->save();
        $d_addabns = DB::select("SELECT * FROM `deleted_add_abns` WHERE `customerid` = $old_customer_id");
        foreach ($d_addabns as $d_addabn):
            $addabns = new Addabn();
            $addabns->customerid = $customer->id;
            $addabns->unit = ($d_addabn->unit) ? $d_addabn->unit : '';
            $addabns->number = ($d_addabn->number) ? $d_addabn->number : '';
            $addabns->street = ($d_addabn->street) ? $d_addabn->street : '';
            $addabns->sttype = ($d_addabn->sttype) ? $d_addabn->sttype : '';
            $addabns->suburb = ($d_addabn->suburb) ? $d_addabn->suburb : '';
            $addabns->state = ($d_addabn->state) ? $d_addabn->state : '';
            $addabns->postcode = ($d_addabn->postcode) ? $d_addabn->postcode : '';
            $addabns->invname = ($d_addabn->invname) ? $d_addabn->invname : '';
            $addabns->abn = ($d_addabn->abn) ? $d_addabn->abn : '';
            $addabns->invemail = ($d_addabn->invemail) ? $d_addabn->invemail : '';
            $addabns->invpoboxno = ($d_addabn->invpoboxno) ? $d_addabn->invpoboxno : '';
            $addabns->invpoboxsuburb = ($d_addabn->invpoboxsuburb) ? $d_addabn->invpoboxsuburb : '';
            $addabns->invpoboxstate = ($d_addabn->invpoboxstate) ? $d_addabn->invpoboxstate : '';
            $addabns->invpoboxpostcode = ($d_addabn->invpoboxpostcode) ? $d_addabn->invpoboxpostcode : '';
            $addabns->emailmethod = ($d_addabn->emailmethod) ? $d_addabn->emailmethod : 0;
            $addabns->addressmethod = ($d_addabn->addressmethod) ? $d_addabn->addressmethod : 0;
            $addabns->poboxmethod = ($d_addabn->poboxmethod) ? $d_addabn->poboxmethod : 0;
            $addabns->latest = ($d_addabn->latest) ? $d_addabn->latest : 1;
            $addabns->myobIdNumber = ($d_addabn->myobIdNumber) ? $d_addabn->myobIdNumber : '';
            $addabns->save();
        endforeach;
        $d_invoices = DB::select("SELECT * FROM deleted_customer_invoices WHERE customerid = $old_customer_id");
        foreach ($d_invoices as $d_invoice):
            $invoice = new Invoice();
            $invoice->customerid = $customer->id;
            $invoice->addabnid = $addabns->id;
            $invoice->userid = $d_invoice->userid;
            $invoice->myob = $d_invoice->myob;
            $invoice->startdate = $d_invoice->startdate;
            $invoice->expirydate = $d_invoice->expirydate;
            $invoice->duedate = $d_invoice->duedate;
            $invoice->frequency = $d_invoice->frequency;
            $invoice->description = $d_invoice->description;
            $invoice->payment_method = $d_invoice->payment_method;
            $invoice->save();
            $d_orders = DB::select("SELECT * FROM `deleted_orders` WHERE `invoiceid` = $d_invoice->invoiceid");
            foreach ($d_orders as $d_order):
                $order = new Order();
                $order->invoiceid = $invoice->id;
                $order->sitename = $d_order->sitename;
                $order->orderoption = $d_order->orderoption;
                $order->page = $d_order->page;
                $order->position = $d_order->position;
                $order->amount = $d_order->amount;
                $order->startdate = $d_order->startdate;
                $order->expirydate = $d_order->expirydate;
                $order->duedate = $d_order->duedate;
                $order->frequency = $d_order->frequency;
                $order->active = $d_order->active;
                $order->myob_account = $d_order->myob_account;
                $order->save();
                $dueDate = $d_order->duedate;
            endforeach;
        endforeach;
        $d_notes = DB::select("SELECT * FROM `deleted_notes` WHERE `customerId` = $old_customer_id");
        foreach ($d_notes as $d_note):
            $note = new Note();
            $note->datetime = $d_note->datetime;
            $note->userId = $d_note->userId;
            $note->note = $d_note->note;
            $note->customerId = $customer->id;
            $note->save();
        endforeach;
        $cstatus = new Cstatus;
        $cstatus->dueDatetime = $dueDate;
        $cstatus->userid = $customer->account_manager;
        $cstatus->customerid = $customer->id;
        $cstatus->statusid = 100;
        $cstatus->datetime = date("Y-m-d H:i");
        $cstatus->latest = 1;
        $cstatus->save();
        return self::deleteDeletedCustomer($database_id);
    }

}
