<?php

namespace App;

use App\Order;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\AppAutomation;

class Invoice extends Model {

    protected $fillable = [
        'addabnid', 'customerid', 'userid', 'myob', 'startdate', 'expirydate', 'duedate', 'frequency', 'description', 'created_at', 'updated_at'
    ];

    public function totalAmount() {
        $query = "SELECT sum(amount*1.1) total FROM orders WHERE orders.invoiceid= $this->id";
        $amount = DB::select($query);

        if (isset($amount[0])) {
            if (is_numeric($amount[0]->total)) {
                return $amount[0]->total;
            }
        }
        return 0;
    }

    public function totalPaid() {

        $query = "SELECT sum(amount)total FROM payments WHERE payments.invoiceid= $this->id";
        $amount = DB::select($query);

        if (isset($amount[0])) {
            if (is_numeric($amount[0]->total)) {
                return $amount[0]->total;
            }
        }

        return 0;
    }

    private static function getNewInvoiceEmailData($invoice_id) {
        $invoice = DB::select("SELECT * FROM invoices WHERE id = $invoice_id");
        if (isset($invoice) && !empty($invoice)):
            $invoice = $invoice[0];
            $customer_name = DB::select("SELECT bname FROM `customers` WHERE id = $invoice->customerid");
            $mangerName = DB::select("SELECT name FROM users WHERE id = $invoice->userid");
            $customer_name = (isset($customer_name) && !empty($customer_name)) ? $customer_name = $customer_name[0]->bname : false;
            $invoice_amount = DB::select("SELECT sum(amount) total FROM orders WHERE invoiceid = $invoice->id");
            $invoice_amount = (isset($invoice_amount) && !empty($invoice_amount)) ? $invoice_amount[0]->total : false;
            $mangerName = (isset($mangerName) && !empty($mangerName)) ? $mangerName[0]->name : false;
            $myob_number = (isset($invoice->myob) && !empty($invoice->myob)) ? $invoice->myob : false;
            $orders = DB::select("SELECT * FROM orders WHERE invoiceid = $invoice->id");
            $orders = (isset($orders) && !empty($orders)) ? $orders : false;
        endif;
        return array(
            'customer_name' => ($customer_name) ? $customer_name : '',
            'mangerName' => ($mangerName) ? $mangerName : '',
            'myob_number' => ($myob_number) ? $myob_number : '',
            'invoice_amount' => ($invoice_amount) ? $invoice_amount : '',
            'invoice_id' => ($invoice->id) ? $invoice->id : false,
            'orders' => $orders,
            'customer_id' => ($invoice->customerid) ? $invoice->customerid : ''
        );
    }

    public static function sendNewInvoiceEmail($invoice_id) {
        AppAutomation::sendMail('emails/newInvoiceCreated', array('emailArray' => self::getNewInvoiceEmailData($invoice_id)), 'info@adultpress.com.au', 'CRM: New Invoice Created');
    }

    public static function isBalanceRemaining($invoiceID) {
        $orderTotal = DB::select("SELECT sum(amount*1.1) total FROM orders WHERE orders.invoiceid= $invoiceID");
        $totalPaid = DB::select("SELECT sum(amount)total FROM payments WHERE payments.invoiceid= $invoiceID");
        if ((isset($orderTotal[0]) && !empty($orderTotal[0])) && (isset($totalPaid[0]) && !empty($totalPaid[0]))):
            if ((int) $totalPaid[0]->total == 0):
                return false;
            else:
                return abs(((float) $orderTotal[0]->total - (float) $totalPaid[0]->total) / (float) $totalPaid[0]->total) < 0.00001;
            endif;
        else:
            return false;
        endif;
    }

    public function orders() {
        $orders = Order::where('invoiceid', '=', $this->id)->get();
        return $orders;
    }

    public function customerName() {
        $query = "SELECT customers.bname FROM customers,invoices WHERE invoices.customerid=customers.id AND invoices.id= $this->id";
        $customer = DB::select($query)[0];
        return $customer->bname;
    }

    public function firstDueDate() {
        $query = "SELECT orders.duedate FROM orders WHERE orders.invoiceid = $this->id ORDER BY orders.duedate ASC LIMIT 1";
        $duedate = DB::select($query);
        if (isset($duedate[0]->duedate)) {
            return $duedate[0]->duedate;
        }
        return '';
    }

    public function firstExpiryDate() {
        $query = "SELECT orders.expirydate FROM orders WHERE orders.invoiceid = $this->id ORDER BY orders.expirydate ASC LIMIT 1";
        $expiry = DB::select($query);
        if (isset($expiry[0]->expirydate)) {
            return $expiry[0]->expirydate;
        }
        return '';
    }

    public function firstStartDate() {
        $query = "SELECT orders.startdate FROM orders WHERE orders.invoiceid = $this->id ORDER BY orders.startdate ASC LIMIT 1";
        $startdate = DB::select($query);
        if (isset($startdate[0]->startdate)) {
            return $startdate[0]->startdate;
        }
        return '';
    }

    public function credit() {

        $query = "SELECT invoices.myob FROM invoices WHERE invoices.id = $this->id";

        $myob = DB::select($query);
        if (!isset($myob[0]->myob)) {
            return number_format(0, 2);
        }
        $myob = $myob[0]->myob;

        $creditid = substr($myob, 2);
        $creditid = 'CN' . $creditid;
        //dd($creditid);


        $query = "SELECT sum(orders.amount*1.1)total FROM orders,invoices WHERE invoices.myob= '" . $creditid . "' AND invoices.id!= $this->id AND orders.invoiceid=invoices.id";
        $amount = DB::select($query);

        if (isset($amount[0])) {
            if (is_numeric($amount[0]->total)) {
                if ($amount[0]->total < 0) {
                    return number_format($amount[0]->total, 2);
                }
            }
        }
        return number_format(0, 2);
    }

    public function payable() {
        if (substr($this->myob, 0, 2) !== "CN") {
            $payable = $this->totalAmount() - $this->totalPaid() + $this->credit();
            return $payable >= 0 ? number_format($payable, 2) : number_format(0, 2);
        }
        return number_format(0, 2);
    }

    public function daysFromExpiry() {
        $query = "SELECT orders.expirydate FROM orders WHERE orders.invoiceid = $this->id ORDER BY orders.expirydate ASC LIMIT 1";
        $expiry = DB::select($query);
        if (isset($expiry[0]->expirydate)) {
            $expiry = $expiry[0]->expirydate;

            $now = time(); // or your date as well
            $expiry = strtotime($expiry);
            $datediff = $now - $expiry;
            return floor($datediff / (60 * 60 * 24));
        }
        return '';
    }

    public function daysFromStart() {
        $query = "SELECT orders.startdate FROM orders WHERE orders.invoiceid = $this->id ORDER BY orders.startdate ASC LIMIT 1";
        $startdate = DB::select($query);
        if (isset($startdate[0]->startdate)) {
            $startdate = $startdate[0]->startdate;

            $now = time(); // or your date as well
            $startdate = strtotime($startdate);
            $datediff = $now - $startdate;
            return floor($datediff / (60 * 60 * 24));
        }
        return '';
    }

    public function daysFromThisDue() {
        $query = "SELECT orders.startdate FROM orders WHERE orders.invoiceid = $this->id ORDER BY orders.startdate ASC LIMIT 1";
        $startdate = DB::select($query);
        if (isset($startdate[0]->startdate)) {
            $startdate = $startdate[0]->startdate;

            $now = time(); // or your date as well
            $startdate = strtotime($startdate);
            $datediff = $now - $startdate - 7;
            return floor($datediff / (60 * 60 * 24));
        }
        return '';
    }

    public function createdBy() {
        $user = User::where($this->userid)->get();
    }

    public static function checkIfMyobInvoice($invoiceId) {
        $invoice = Invoice::find($invoiceId);
        return (isset($invoice->myob) && !empty($invoice->myob)) ? true : false;
    }

}
