<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\AppAutomation;
use App\InvoiceStatus;
use App\Order;
use App\Addabn;

class Myob extends Model {

    private $tableName = 'myobs';
    protected $fillable = ['access_token', 'access_token_expires', 'refresh_token', 'expires_in', 'user', 'username', 'updated_at', 'created_at', 'uri', 'customer_id'];

    public function __construct() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`access_token` text NOT NULL , "
                    . "`access_token_expires` text NOT NULL , "
                    . "`refresh_token` text NOT NULL , "
                    . "`expires_in` text NOT NULL , "
                    . "`user` text NOT NULL , "
                    . "`username` text NOT NULL , "
                    . "`customer_id` text NOT NULL , "
                    . "`uri` text NOT NULL , "
                    . "`gstCodeUID` text NOT NULL , "
                    . "`accountsMPMUid` text NOT NULL , "
                    . "`accountBrothelsUid` text NOT NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
    }

    public function makeApiCall($url, $headers = null, $fields = null) {
        $session = curl_init($url);
        curl_setopt($session, CURLOPT_HTTPHEADER, (isset($headers) && !empty($headers)) ? $headers : $this->getHeaders());
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        if (isset($fields) && !empty($fields)):
            curl_setopt($session, CURLOPT_POST, true);
            curl_setopt($session, CURLOPT_POSTFIELDS, $fields);
        endif;
        $response = curl_exec($session);
        curl_close($session);
        if (isset($response['Errors']) && !empty($response['Errors'])):
            $appAuto->sendEmail('emails.test', array('json' => json_encode($response)), 'matt@chillidee.com.au', 'Errors Json');
        endif;
        return json_decode($response, true);
    }

    private function getHeaders() {
        $cftoken = (isset($_SESSION['refresh_token']) && !empty($_SESSION['refresh_token'])) ? $_SESSION['refresh_token'] : DB::select("SELECT `refresh_token` FROM `$this->tableName`")[0]->refresh_token;
        return $headers = array(
            'Authorization: Bearer ' . $this->getAccessToken(),
            'x-myobapi-key: ' . env('MYOB_KEY'),
        );
    }

    public function saveCompanyFiles() {
        $companyFiles = $this->makeApiCall(api_url);
        $_SESSION['customer_id'] = $companyFiles[0]['Id'];
        $_SESSION['Uri'] = $companyFiles[0]['Uri'];
    }

    public function saveCompanyFile($uid, $url) {
        $id = DB::select("SELECT ID FROM myobs");
        if (isset($id[0]) && !empty($id[0])):
            $id = $id[0]->ID;
            DB::update("UPDATE `myobs` SET `customer_id`='$uid',`uri`='$url' WHERE ID = $id");
        endif;
    }

    public function getCompanyFiles() {
        return $this->makeApiCall(api_url);
    }

    public function clearTable() {
        DB::statement("TRUNCATE $this->tableName");
    }

    public function test() {
        //$auto = new AppAutomation();
        //$appAuto = new AppAutomation();
        //$appAuto->checkExpriedOrders();
        //var_dump($this->deleteInvoice('00345703'));
        //var_dump($this->getAllInvoices());
        //var_dump($this->findInvoiceByDate('2017-02-02T14:40:09'));
        //var_dump($this->getInvoiceNumber(0));
        //var_dump($this->createBankAccount());
        //$this->createNewCustomer(3928);
        //var_dump($this->getAllCustomers());
        //var_dump($this->makeUrlRequest('https://ar1.api.myob.com/accountright/bf375f5d-8dd4-485c-bfa0-d20e9780620c/Contact/Customer?$top=400&$skip=400'));
        //var_dump($this->deleteCustomer('https://ar1.api.myob.com/accountright/bf375f5d-8dd4-485c-bfa0-d20e9780620c/Contact/Customer/e89f8008-e168-4284-b3ac-fc4e137caa6d'));
        //var_dump($auto->sendInvoicesToBeSentEmails());
        $this->deactivateAllOrders();
    }

    public function test1() {
        //$appAuto = new AppAutomation();
        echo '<pre>';
        var_dump($this->getAllInvoices());
        //var_dump(self::createReadableDate('2017-02-22 11:25:20'));
        // var_dump($appAuto->getInvoiceExpiryDate('2017-02-22 11:25:20', 'Annually'));
        echo '</pre>';
    }

    private function deactivateAllOrders() {
        foreach (DB::select("SELECT * FROM orders WHERE active = 1") as $order):
            DB::update("UPDATE `orders` SET `active` = '0' WHERE `orders`.`id` = $order->id");
        endforeach;
    }

    public function searchContactList($type, $query) {
        $url = (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
        $filter = $this->createSearchQuery($query);
        $url .= "/Contact/$type/?$$filter";
        $this->makeApiCall($url, $this->getApiHeaders());
    }

    private function getAccessToken() {
        return (isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) ? $_SESSION['access_token'] : DB::select("SELECT `access_token` FROM `$this->tableName`")[0]->access_token;
    }

    private function getRefreshToken() {
        return (isset($_SESSION['refresh_token']) && !empty($_SESSION['refresh_token'])) ? $_SESSION['refresh_token'] : DB::select("SELECT `refresh_token` FROM `$this->tableName`")[0]->refresh_token;
    }

    private function getApiHeaders() {
        $username = env('MYOB_USERNAME');
        $password = env('MYOB_PASWWORD');
        $base64 = base64_encode($username . ':' . $password);
        return array(
            'Authorization: Bearer ' . $this->getAccessToken(),
            'x-myobapi-key: ' . env('MYOB_KEY'),
            'x-myobapi-cftoken: ' . $base64,
            'x-myobapi-version: v2'
        );
    }

    private function createSearchQuery($queryArray) {
        $returnString = 'filter = ';
        $indexTotal = count($queryArray);
        $index = 1;
        foreach ($queryArray as $key => $value):
            $returnString .= "substringof('$value',%20$key)";
            $returnString .= ($index < $indexTotal) ? '%20or%20' : '';
            $index++;
        endforeach;
        $returnString .= '%20eq%20true';
        return $returnString;
    }

    public function renewToken() {
        require_once base_path('vendor/myob/includes/class.myob_oauth.php');
        $myob_api_oauth = new \myob_api_oauth();
        $tokens = $myob_api_oauth->refreshAccessToken(env('MYOB_KEY'), env('MYOB_SECRET'), $this->getRefreshToken());
        if (isset($tokens) && !empty($tokens)):
            if (!isset($tokens->error) && empty($tokens->error)):
                $bd_id = DB::select("SELECT * FROM `myobs`")[0]->ID;
                $query = "UPDATE `myobs` SET `access_token` = '$tokens->access_token', `refresh_token` = '$tokens->refresh_token' WHERE `myobs`.`ID` = $bd_id";
                DB::update($query);
            else:
                $appAuto = new AppAutomation();
                $appAuto->sendEmail('', '', 'matt@chillidee.com.au', 'Error refreshing token');
            endif;
        endif;
    }

    public function getAllCustomers() {
        return $this->makeApiCall($this->getApiUrl() . "/Contact/Customer", $this->getApiHeaders());
    }

    public static function getAccountUID($account_Name, $customer_id) {
        if (!isset($account_Name) || empty($account_Name)):
            $btype = DB::select("SELECT btype FROM customers WHERE id = $customer_id");
            $btype = (isset($btype) && !empty($btype)) ? (int) $btype[0]->btype : 4;
            $account_Name = ($btype == 1) ? 'mpm' : 'brothels';
        endif;
        return ($account_Name == 'mpm') ? self::getMPMAccountUID() : self::getBrothelAccountUID();
    }

    public static function getBrothelAccountUID() {
        $accountUID = DB::select("SELECT accountBrothelsUid FROM myobs");
        return (isset($accountUID) && !empty($accountUID)) ? $accountUID[0]->accountBrothelsUid : false;
    }

    public static function getMPMAccountUID() {
        $accountUID = DB::select("SELECT accountsMPMUid FROM myobs");
        return (isset($accountUID) && !empty($accountUID)) ? $accountUID[0]->accountsMPMUid : false;
    }

    public function createNewInvoice($data) {
        $appAuto = new AppAutomation();
        $url = (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['Uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
        $url .= "/Sale/Invoice/Service";
        $date = date('Y-m-d\TH:i:s', time());
        if (strpos($data['invoiceNumber'], 'CN') !== false):
            $_invoiceNumber = $data['invoiceNumber'];
        endif;
        $parms = array(
            'Date' => $date,
            "Number" => (isset($_invoiceNumber) && !empty($_invoiceNumber)) ? $_invoiceNumber : '',
            "CustomerPurchaseOrderNumber" => (isset($_invoiceNumber) && !empty($_invoiceNumber)) ? $_invoiceNumber : '',
            'Customer' => array(
                'UID' => $data['myobCustomerID'],
            ),
            "BalanceDueAmount" => $data['invoiceTotal'] * 1.1,
            "Status" => "Open",
            "IsTaxInclusive" => false,
            'Lines' => array(
                array(
                    "Type" => "Header",
                    "Description" => "Advertising"
                ),
            )
        );
        foreach ($data['Orders'] as $order):
            array_push($parms['Lines'], array(
                "Type" => "Transaction",
                'Description' => $order['Description'],
                'Total' => $order['Total'],
                'Account' => array(
                    'UID' => $order['Account']
                ),
                'TaxCode' => array(
                    'UID' => $this->getGstUid()
                )
            ));
        endforeach;
        $result = $this->makeApiCall($url, $this->getApiHeaders(), json_encode($parms));  
        $appAuto->sendEmail('emails.test', array('json' => json_encode($result)), 'boni@chillidee.com.au', 'Errors Json');      
        if (strpos($data['invoiceNumber'], 'CN') === false):
            $invoice = $this->findInvoiceByDate($date);
            if (isset($invoice) && !empty($invoice)):
                $invoice = $invoice["Number"];
            endif;
        else:
            if (isset($data['invoiceNumber']) && !empty($data['invoiceNumber'])):
                $invoice = $data['invoiceNumber'];
            endif;
        endif;
        if (isset($result['Errors']) && !empty($result['Errors'])):
            $appAuto->sendEmail('emails.test', array('json' => json_encode($result)), 'boni@chillidee.com.au', 'Errors Json');
        endif;
        return (!isset($result['Errors']) && empty($result['Errors']) && isset($invoice) && !empty($invoice)) ? $invoice : false;
    }

    public function saveGSTCodeUID($newUID = null) {
        $id = DB::select("SELECT ID FROM `$this->tableName`")[0]->ID;
        if (!isset($newUID) && empty($newUID)):
            $taxUID = $this->getTaxCodeFromAPI();
            if (isset($taxUID['Errors']) && !empty($taxUID['Errors'])):
                echo $taxUID;
            endif;
        else:
            $taxUID = $newUID;
        endif;
        DB::update("UPDATE `$this->tableName` SET `gstCodeUID` = '$taxUID' WHERE `$this->tableName`.`ID` = $id");
    }

    private function getTaxCodeFromAPI() {
        $url = (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['Uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
        $url .= "/GeneralLedger/TaxCode";
        $result = json_decode(json_encode($this->makeApiCall($url, $this->getApiHeaders())), true);
        if (isset($result) && !empty($result['Errors'])):
            foreach ($result['Items'] as $tax):
                if ($tax['Code'] == 'GST'):
                    return $tax['UID'];
                endif;
            endforeach;
        else:
            if (isset($result) && !empty($result['Errors'])):
                return $result;
            endif;
        endif;
    }

    private function getGstUid() {
        return DB::select("SELECT `gstCodeUID` FROM `$this->tableName`")[0]->gstCodeUID;
    }

    public function setAccountsUid($newUID = null) {
        if (!isset($newUID) && empty($newUID)):
            $url = (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['Uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
            $url .= "/GeneralLedger/Account";
            $result = $this->makeApiCall($url, $this->getApiHeaders());
            if (isset($result['Items']) && !empty($result['Items']) && empty($result['Errors'])):
                $result = $result["Items"][1]["UID"];
            else:
                if (isset($result['Errors']) && !empty($result['Errors'])):
                    exit;
                endif;
            endif;
        else:
            $result = $newUID;
        endif;
        $id = DB::select("SELECT ID FROM `$this->tableName`")[0]->ID;
        DB::update("UPDATE `$this->tableName` SET `accountsUid` = '$result' WHERE `$this->tableName`.`ID` = $id");
    }

    public function updateMPMAccountUid($newUID = null) {
        if (!isset($newUID) && empty($newUID)):
            $url = (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['Uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
            $url .= "/GeneralLedger/Account";
            $result = $this->makeApiCall($url, $this->getApiHeaders());
            if (isset($result['Items']) && !empty($result['Items']) && empty($result['Errors'])):
                $result = $result["Items"][1]["UID"];
            else:
                if (isset($result['Errors']) && !empty($result['Errors'])):
                    exit;
                endif;
            endif;
        else:
            $result = $newUID;
        endif;
        $id = DB::select("SELECT ID FROM `$this->tableName`")[0]->ID;
        DB::update("UPDATE `$this->tableName` SET `accountsMPMUid` = '$result' WHERE `$this->tableName`.`ID` = $id");
    }

    public function updateBrothelsAccountUid($newUID = null) {
        if (!isset($newUID) && empty($newUID)):
            $url = (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['Uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
            $url .= "/GeneralLedger/Account";
            $result = $this->makeApiCall($url, $this->getApiHeaders());
            if (isset($result['Items']) && !empty($result['Items']) && empty($result['Errors'])):
                $result = $result["Items"][1]["UID"];
            else:
                if (isset($result['Errors']) && !empty($result['Errors'])):
                    exit;
                endif;
            endif;
        else:
            $result = $newUID;
        endif;
        $id = DB::select("SELECT ID FROM `$this->tableName`")[0]->ID;
        DB::update("UPDATE `$this->tableName` SET `accountBrothelsUid` = '$result' WHERE `$this->tableName`.`ID` = $id");
    }

    public function getAccountsUid() {
        return DB::select("SELECT `accountsUid` FROM `$this->tableName`")[0]->accountsUid;
    }

    public function getSortedDBFields() {
        $fields = DB::select("SELECT * FROM `$this->tableName`");
        return (isset($fields) && !empty($fields)) ? $fields[0] : false;
    }

    public function getAllAccounts() {
        $url = (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['Uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
        $url .= "/GeneralLedger/Account";
        return $this->makeApiCall($url, $this->getApiHeaders());
    }

    public function getAllTaxes() {
        $url = (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['Uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
        $url .= "/GeneralLedger/TaxCode";
        return $this->makeApiCall($url, $this->getApiHeaders());
    }

    public function getAllInvoices() {
        $url = (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['Uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
        $url .= "/Sale/Invoice";
        return $this->makeApiCall($url, $this->getApiHeaders());
    }

    private function getInvoiceUID($invoiceID) {
        $invoice = $this->getExsitingInvoice($invoiceID);
        return ($invoice) ? $invoice['UID'] : $invoice;
    }

    private function getExsitingInvoice($invoiceID) {
        $cleanInvoiceArray = array();
        $_invoice = false;
        $invoiceID = DB::select("SELECT myob FROM `invoices` WHERE id = $invoiceID")[0]->myob;
        if (isset($invoiceID) && !empty($invoiceID)):
            $invoices = $this->getAllInvoices();
            foreach ($invoices["Items"] as $invoice):
                array_push($cleanInvoiceArray, $invoice);
            endforeach;
            if (isset($invoices["NextPageLink"]) && !empty($invoices["NextPageLink"])):
                while ($invoices["NextPageLink"]):
                    $invoices = $this->makeUrlRequest($invoices["NextPageLink"]);
                    foreach ($invoices["Items"] as $invoice):
                        array_push($cleanInvoiceArray, $invoice);
                    endforeach;
                endwhile;
            endif;
            if (isset($invoices['Items']) && !empty($invoices['Items']) && empty($invoices['Errors'])):
                foreach ($cleanInvoiceArray as $invoice):
                    if ((string) $invoice["Number"] == (string) $invoiceID):
                        $_invoice = $invoice;
                        break;
                    endif;
                endforeach;
            endif;
        endif;
        return $_invoice;
    }

    public function creditInvoice($invoiceID) {
        $invoiceUID = $this->getInvoiceUID($invoiceID);
        $customer_id = DB::select("SELECT `customerid` FROM `invoices` WHERE `id` = $invoiceID")[0]->customerid;
        $amount = $this->getInvoiceTotal($invoiceID);
        if ($invoiceUID):
            $result = $this->createNewCreditInvoice(array('invoiceUID' => $invoiceUID, 'customerid' => $customer_id, 'Amount' => $amount));
        endif;
        return $result;
    }

    private function createNewCreditInvoice($data) {
        $url = (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['Uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
        $url .= "/Sale/CreditRefund";
        $parms = array(
            'Account' => array(
                'UID' => self::getAccountUID(null, $data['customerid'])
            ),
            'Invoice' => array(
                'UID' => $data['invoiceUID']
            ),
            'Customer' => array(
                'UID' => $this->getCustomerUID($data['customerid'])
            ),
            'Date' => date("Y-m-d H:i:s"),
            'Amount' => $data['Amount'],
        );
        $result = $this->makeApiCall($url, $this->getApiHeaders(), json_encode($parms));
        return (!isset($result['Errors']) && empty($result['Errors'])) ? true : false;
    }

    private function getCustomerUID($customerid) {
        $UID = DB::select("SELECT myobIdNumber FROM `addabns` WHERE `customerid` = $customerid AND `latest` = 1")[0];
        return (isset($UID) && !empty($UID) && isset($UID->myobIdNumber) && !empty($UID->myobIdNumber)) ? $UID->myobIdNumber : false;
    }

    private function setCustomerUID($customerid, $myobId) {
        $db_id = DB::select("SELECT id FROM `addabns` WHERE `customerid` = $customerid AND `latest` = 1")[0]->id;
        DB::update("UPDATE `addabns` SET `myobIdNumber` = '$myobId' WHERE `addabns`.`id` = $db_id");
    }

    private function getInvoiceTotal($invoiceID) {
        $total = 0;
        $orders = DB::select("SELECT amount FROM `orders` WHERE `invoiceid` = $invoiceID");
        foreach ($orders as $order):
            $total = $total + $order->amount;
        endforeach;
        return $total;
    }

    public function createNewCustomer($customerID) {
        $url = $this->getApiUrl() . '/Contact/Customer';
        $customerData = DB::select("SELECT * FROM customers WHERE id = $customerID")[0];
        $invoiceData = DB::select("SELECT * FROM addabns WHERE customerid = $customerID")[0];
        $parms = array(
            'CompanyName' => (isset($invoiceData->invname) && !empty($invoiceData->invname)) ? $invoiceData->invname : ' ',
            'LastName' => (isset($customerData->lastname) && !empty($customerData->lastname)) ? $customerData->lastname : ' ',
            'FirstName' => (isset($customerData->firstname) && !empty($customerData->firstname)) ? $customerData->firstname : ' ',
            'IsIndividual' => (isset($customerData->btype) && !empty($customerData->btype) && ((int) $customerData->btype == 1)) ? true : false,
            'Addresses' => array(
                array(
                    'Street' => (isset($customerData->address) && !empty($customerData->address)) ? $customerData->address : ' ',
                    'City' => (isset($customerData->suburb) && !empty($customerData->suburb)) ? $customerData->suburb : ' ',
                    'State' => (isset($customerData->state) && !empty($customerData->state)) ? $customerData->state : ' ',
                    'PostCode' => (isset($customerData->postcode) && !empty($customerData->postcode)) ? $customerData->postcode : ' ',
                    'Phone1' => (isset($customerData->bphone) && !empty($customerData->bphone)) ? $customerData->bphone : ' ',
                    'Email' => (isset($invoiceData->invemail) && !empty($invoiceData->invemail)) ? $invoiceData->invemail : ' ',
                    'Website' => (isset($customerData->website) && !empty($customerData->website)) ? $customerData->website : ' ',
                ),
            ),
            "SellingDetails" => array(
                'SaleLayout' => 'Service',
                "InvoiceDelivery" => "Email",
                'ABN' => (isset($invoiceData->abn) && !empty($invoiceData->abn)) ? $invoiceData->abn : '',
                'TaxCode' => array(
                    'UID' => $this->getGstUid()
                ),
                'FreightTaxCode' => array(
                    'UID' => $this->getGstUid()
                ),
            ),
        );
        $result = $this->makeApiCall($url, $this->getApiHeaders(), json_encode($parms));
        return (isset($result['Errors']) && !empty($result['Errors'])) ? false : true;
    }

    private function getApiUrl() {
        return (isset($_SESSION['Uri']) && !empty($_SESSION['Uri'])) ? $_SESSION['Uri'] : DB::select("SELECT `uri` FROM `$this->tableName`")[0]->uri;
    }

    public function getCustomerUIDFromApi($customer_id) {
        $hasUIDNumber = $this->getCustomerUID($customer_id);
        if (!$hasUIDNumber):
            $customer_data = DB::select("SELECT * FROM `customers` WHERE id = $customer_id")[0];
            $cleanCustomerArray = array();
            if (isset($customer_data) && !empty($customer_data)):
                $customers = $this->getAllCustomers();
                if (isset($customers) && !empty($customers) && empty($customers['Errors'])):
                    foreach ($customers["Items"] as $customer):
                        array_push($cleanCustomerArray, $customer);
                    endforeach;
                    if (isset($customers["NextPageLink"]) && !empty($customers["NextPageLink"])):
                        while ($customers["NextPageLink"]):
                            $customers = $this->makeUrlRequest($customers["NextPageLink"]);
                            foreach ($customers["Items"] as $customer):
                                array_push($cleanCustomerArray, $customer);
                            endforeach;
                        endwhile;
                    endif;
                    foreach ($cleanCustomerArray as $customer):
                        if (isset($customer['CompanyName']) && !empty($customer['CompanyName']) && isset($customer_data->bname) && !empty($customer_data->bname)):
                            if ($customer['CompanyName'] == $customer_data->bname):
                                $_customer = $customer;
                                break;
                            endif;
                        endif;
                        if (isset($customer['Addresses'][0]['Email']) && !empty($customer['Addresses'][0]['Email'])):
                            if (isset($customer_data->bemail) && !empty($customer_data->bemail) && $customer['Addresses'][0]['Email'] == $customer_data->bemail):
                                $_customer = $customer;
                                break;
                            endif;
                            if (isset($customer_data->email) && !empty($customer_data->email) && $customer['Addresses'][0]['Email'] == $customer_data->email):
                                $_customer = $customer;
                                break;
                            endif;
                        endif;
                        if (isset($customer['Addresses'][0]['Phone1']) && !empty($customer['Addresses'][0]['Phone1'])):
                            if (isset($customer_data->bphone) && !empty($customer_data->bphone) && $customer['Addresses'][0]['Phone1'] == $customer_data->bphone):
                                $_customer = $customer;
                                break;
                            endif;
                            if (isset($customer_data->bphone) && !empty($customer_data->bphone) && $customer['Addresses'][0]['Phone1'] == $customer_data->phone):
                                $_customer = $customer;
                                break;
                            endif;
                        endif;
                    endforeach;
                endif;
                if (isset($_customer["UID"]) && !empty($_customer["UID"])):
                    $hasUIDNumber = $_customer["UID"];
                    $this->setCustomerUID($customer_id, $hasUIDNumber);
                endif;
            endif;
        endif;
        return (isset($hasUIDNumber) && !empty($hasUIDNumber)) ? $hasUIDNumber : false;
    }

    private function getInvoiceFromApi($invoice) {
        $url = $this->getApiUrl() . '/Sale/Invoice';
        $customer = Addabn::find($invoice->addabnid);
        $invoices = $this->makeApiCall($url, $this->getApiHeaders());
        if (isset($invoices) && !empty($invoices) && empty($invoices['Errors']) && isset($customer) && !empty($customer)):
            foreach ($invoices['Items'] as $_invoice):
                if (((string) $_invoice['Number'] === (string) $invoice->myob) && ($customer->myobIdNumber === $_invoice["Customer"]["UID"])):
                    return $_invoice;
                endif;
            endforeach;
        endif;
        return false;
    }

    public function updateMYOBInvoice($invoice) {
        $data = array();
        if (isset($invoice) && !empty($invoice) && !empty($invoice->myob)):
            $apiInvoice = $this->getInvoiceFromApi($invoice);
            if (isset($apiInvoice) && !empty($apiInvoice)):
                $orders = Order::getOrdersForInvoice($invoice->id);
                if (isset($orders) && !empty($orders)):
                    $data['Orders'] = array();
                    $data['invoiceTotal'] = 0;
                    foreach ($orders as $order):
                        $data['invoiceTotal'] = $data['invoiceTotal'] + $order->amount;
                        array_push($data['Orders'], array(
                            'Description' => self::createInvoiceDescription($invoice, $order),
                            'Total' => $order->amount,
                            'Account' => self::getAccountUID($order->myob_account, $invoice->customerid)
                        ));
                    endforeach;
                endif;
                $parms = array(
                    'UID' => $apiInvoice['UID'],
                    'Date' => date("Y-m-d H:i:s"),
                    "Number" => $invoice->myob,
                    "CustomerPurchaseOrderNumber" => $invoice->myob,
                    'Customer' => array(
                        'UID' => $this->getCustomerUIDFromApi($invoice->customerid),
                    ),
                    "BalanceDueAmount" => $data['invoiceTotal'],
                    "Status" => "Open",
                    "IsTaxInclusive" => false,
                    'Lines' => array(
                        array(
                            "Type" => "Header",
                            "Description" => "Advertising"
                        ),
                    ),
                    'RowVersion' => $apiInvoice['RowVersion']
                );
                foreach ($data['Orders'] as $order):
                    array_push($parms['Lines'], array(
                        "Type" => "Transaction",
                        'Description' => $order['Description'],
                        'Total' => $order['Total'],
                        'Account' => array(
                            'UID' => $order['Account']
                        ),
                        'TaxCode' => array(
                            'UID' => $this->getGstUid()
                        )
                    ));
                endforeach;
                return $this->makeApiCall($apiInvoice['URI'], $this->getApiHeaders(), json_encode($parms));
            endif;
        endif;
    }

    private function findInvoiceByDate($date) {
        $cleanInvoiceArray = array();
        if (isset($date) && !empty($date)):
            $invoices = $this->getAllInvoices();
            if (isset($invoices['Items']) && !empty($invoices['Items']) && empty($invoices['Errors'])):
                foreach ($invoices["Items"] as $invoice):
                    array_push($cleanInvoiceArray, $invoice);
                endforeach;
                if (isset($invoices["NextPageLink"]) && !empty($invoices["NextPageLink"])):
                    while ($invoices["NextPageLink"]):
                        $invoices = $this->makeUrlRequest($invoices["NextPageLink"]);
                        foreach ($invoices["Items"] as $invoice):
                            array_push($cleanInvoiceArray, $invoice);
                        endforeach;
                    endwhile;
                endif;
                foreach ($cleanInvoiceArray as $invoice):
                    array_push($cleanInvoiceArray, $invoice);
                endforeach;
                if (isset($invoices["NextPageLink"]) && !empty($invoices["NextPageLink"])):
                    while ($invoices["NextPageLink"]):
                        $invoices = $this->makeUrlRequest($invoices["NextPageLink"]);
                        foreach ($invoices["Items"] as $invoice):
                            array_push($cleanInvoiceArray, $invoice);
                        endforeach;
                    endwhile;
                endif;
                if (isset($invoices['Items']) && !empty($invoices['Items']) && empty($invoices['Errors'])):
                    foreach ($cleanInvoiceArray as $invoice):
                        if ($invoice["Date"] == $date):
                            $_invoice = $invoice;
                            break;
                        endif;
                    endforeach;
                endif;
            endif;
        endif;
        return (isset($_invoice) && !empty($_invoice)) ? $_invoice : false;
    }

    public function deleteInvoice($invoice_number) {
        $invoice = $this->getInvoiceByNumber($invoice_number);
        if (isset($invoice) && !empty($invoice)):
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $invoice['URI']);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getApiHeaders());
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            $result = curl_exec($ch);
            curl_close($ch);
        endif;
        return (!isset($result['Errors']) && empty($result['Errors'])) ? true : false;
    }

    private function getInvoiceByUID($invoice_uid) {
        if (isset($invoice_uid) && !empty($invoice_uid)):
            $invoices = $this->getAllInvoices();
            if (isset($invoices) && !empty($invoices['Items'])):
                foreach ($invoices['Items'] as $invoice):
                    if ($invoice['UID'] === $invoice_uid):
                        $_invoice = $invoice;
                        break;
                    endif;
                endforeach;
            endif;
        endif;
        return (isset($_invoice) && !empty($_invoice)) ? $_invoice : false;
    }

    private function getInvoiceByNumber($invoice_number) {
        $cleanInvoiceArray = array();
        if (isset($invoice_number) && !empty($invoice_number)):
            $invoices = $this->getAllInvoices();
            foreach ($invoices["Items"] as $invoice):
                array_push($cleanInvoiceArray, $invoice);
            endforeach;
            if (isset($invoices["NextPageLink"]) && !empty($invoices["NextPageLink"])):
                while ($invoices["NextPageLink"]):
                    $invoices = $this->makeUrlRequest($invoices["NextPageLink"]);
                    foreach ($invoices["Items"] as $invoice):
                        array_push($cleanInvoiceArray, $invoice);
                    endforeach;
                endwhile;
            endif;
            if (isset($invoices['Items']) && !empty($invoices['Items']) && empty($invoices['Errors'])):
                foreach ($cleanInvoiceArray as $invoice):
                    if ($invoice['Number'] === $invoice_number):
                        $_invoice = $invoice;
                        break;
                    endif;
                endforeach;
            endif;
        endif;
        return (isset($_invoice) && !empty($_invoice)) ? $_invoice : false;
    }

    private function makeUrlRequest($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getApiHeaders());
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return (isset($result) && !empty($result)) ? json_decode($result, true) : false;
    }

    public function deleteCustomer($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getApiHeaders());
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        $result = curl_exec($ch);
        curl_close($ch);
        return (!isset($result['Errors']) && empty($result['Errors'])) ? true : false;
    }

    public static function createInvoiceDescription($invoice, $order) {
        $customer = DB::select("SELECT * FROM customers WHERE id = $invoice->customerid")[0];
        $startDate = self::createReadableDate($order->startdate);
        $expiryDate = self::createReadableDate($order->expirydate);
        switch ($order->sitename):
            case 'My Playmate':
                if (strtolower($order->page) == 'discount'):
                    $description = "Discount applied";
                else:
                    $description = "$customer->bname - $order->frequency subscription on www.myplaymate.com.au - $order->orderoption - $order->page - $startDate to $expiryDate";
                endif;
                break;
            case 'Brothels':
                if (strtolower($order->page) == 'discount'):
                    $description = "Discount applied";
                else:
                    $description = "$customer->bname – $order->frequency subscription on www.brothels.com.au - $order->orderoption - $order->page - position - $order->position - $startDate to $expiryDate";
                endif;
                break;
            case 'Asian Brothels':
                if (strtolower($order->page) == 'discount'):
                    $description = "Discount applied";
                else:
                    $description = "$customer->bname – $order->frequency subscription on www.asianbrothels.com.au - $order->orderoption - $order->page - position - $order->position - $startDate to $expiryDate";
                endif;
                break;
        endswitch;
        return (isset($description) && !empty($description)) ? $description : '';
    }

    public static function createReadableDate($dateString) {
        return date("d-m-Y", strtotime($dateString));
    }

}
