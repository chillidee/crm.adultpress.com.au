<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'userid','invoiceid', 'amount', 'created_at', 'updated_at'
    ];
}
