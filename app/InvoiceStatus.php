<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class InvoiceStatus extends Model {

    private $invoiceStatusTableName = 'invoiceStatus';
    private $invoiceStatusCodesTableName = 'invoiceStatusCode';

    public function __construct() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->invoiceStatusTableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->invoiceStatusTableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`invoiceID` text NOT NULL , "
                    . "`statusCode` text NOT NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->invoiceStatusCodesTableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->invoiceStatusCodesTableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`codeID` text NOT NULL , "
                    . "`status` text NOT NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
            DB::insert("INSERT INTO `invoiceStatusCode` (`ID`, `codeID`, `status`, `updated_at`, `created_at`) VALUES (NULL, '1', 'Owing', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
            DB::insert("INSERT INTO `invoiceStatusCode` (`ID`, `codeID`, `status`, `updated_at`, `created_at`) VALUES (NULL, '2', 'Paid', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
            DB::insert("INSERT INTO `invoiceStatusCode` (`ID`, `codeID`, `status`, `updated_at`, `created_at`) VALUES (NULL, '3', 'Cancelled', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
            DB::insert("INSERT INTO `invoiceStatusCode` (`ID`, `codeID`, `status`, `updated_at`, `created_at`) VALUES (NULL, '4', 'Payment', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
        endif;
    }

    public static function getInvoiceStatus($invoice_id) {
        $status = DB::select("SELECT invoiceStatusCode.status FROM invoiceStatus, invoiceStatusCode WHERE invoiceStatus.statusCode = invoiceStatusCode.codeID AND invoiceStatus.invoiceID = $invoice_id");
        return (isset($status[0]) && !empty($status[0])) ? $status[0]->status : '';
    }

    public static function updateInvoiceStatus($invoice_id, $newStatus) {
        $exists = DB::select("SELECT ID FROM `invoiceStatus` WHERE invoiceID = $invoice_id");
        if (isset($exists[0]) && !empty($exists[0])):
            $id = $exists[0]->ID;
            DB::update("UPDATE `invoiceStatus` SET `statusCode` = '$newStatus', `updated_at` = 'CURRENT_TIMESTAMP' WHERE `invoiceStatus`.`ID` = $id");
        else:
            DB::insert("INSERT INTO `invoiceStatus` (`ID`, `invoiceID`, `statusCode`, `updated_at`, `created_at`) VALUES (NULL, '$invoice_id', '$newStatus', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
        endif;
    }

}
