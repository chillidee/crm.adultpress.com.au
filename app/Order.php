<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Order extends Model {

    protected $fillable = [
        'invoiceid', 'sitename', 'orderoptionid', 'page', 'position', 'amount', 'active', 'created_at', 'updated_at', 'myob_account'
    ];

    public static function getOrdersForInvoice($invoice_id) {
        return DB::select("SELECT * FROM `orders` WHERE invoiceid = $invoice_id");
    }

}
