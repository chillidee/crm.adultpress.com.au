<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderOption extends Model {

    protected $fillable = [
        'orderoption', 'created_at', 'updated_at'
    ];

}
