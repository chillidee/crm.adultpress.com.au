<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderPage extends Model {

    protected $fillable = [
        'page', 'created_at', 'updated_at'
    ];

}
