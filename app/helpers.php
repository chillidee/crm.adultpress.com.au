<?PHP

function clean($string) {
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^0-9 ]/', '', $string); // Removes special chars.
}
//**  To format phone numbers in view
function formatPhone($str) {
    $str = clean($str);
    $phone = '';
    $strlen = strlen($str);
    if ($strlen == 0)
        return $phone;
    //dd($strlen);
    if ($str[0] == 0 && $str[1] == 4 && $strlen == 10) {
        //**  mobile number
        $phone = substr($str, 0, 4) . ' ' . substr($str, 4, 3) . ' ' . substr($str, 7, 3);
        return $phone;
    } elseif ($str[0] == 0 && $str[1] != 4 && $strlen == 10) {
        //** Landline
        $phone = substr($str, 0, 2) . ' ' . substr($str, 2, 4) . ' ' . substr($str, 6, 4);
        return $phone;
    } elseif ($strlen == 8) {
        //**** landline with no aria code
        $phone = substr($str, 0, 4) . ' ' . substr($str, 4, 4);
        return $phone;
    } elseif ($str[0] == 1 && $strlen == 10) {
        //*** 1300 or 1800
        $phone = substr($str, 0, 4) . ' ' . substr($str, 4, 3) . ' ' . substr($str, 7, 3);
        return $phone;
    } else {
        $phone = substr($str, 0, 2) . ' ' . substr($str, 2, 4) . ' ' . substr($str, 6, $strlen - 6);
        return $phone;
    }
    return $str;
}

function dateToMysql($str){
    $arr = explode('/', $str);
    if(count($arr) == 1)
        return $str;
    return $arr[2].'-'.$arr[1].'-'.$arr[0];
}