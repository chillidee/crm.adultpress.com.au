<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model {
  protected $fillable = [
        'datetime', 'note', 'userid', 'customerid','updated_at','created_at'
      ];
}
