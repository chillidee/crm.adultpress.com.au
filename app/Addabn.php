<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addabn extends Model {

    protected $fillable = [
        'invname', 'invabn', 'unit', 'number', 'street', 'sttype', 'suburb', 'state', 'postcode', 'created_at', 'updated_at',
        'invemail', 'invpoboxno', 'invpoboxsuburb', 'invpoboxstate', 'invpoboxpostcode', 'myobIdNumber'
    ];

}
