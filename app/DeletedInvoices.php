<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\User;

class DeletedInvoices extends Model {

    private $tableName = 'deleted_invoices';
    protected $fillable = ['invoice_id', 'user_id', 'myob_number', 'customer_id'];

    public function __construct() {
        $this->setupDatabase();
    }

    private function getTableName() {
        return $this->tableName;
    }

    private function setupDatabase() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`invoice_id` text NOT NULL , "
                    . "`user_id` text NOT NULL , "
                    . "`myob_number` text NOT NULL , "
                    . "`customer_id` text NOT NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
    }

    public function getDeletedOrdersByCustomerId($customer_id) {
        $invoices = DB::select("SELECT * FROM $this->tableName WHERE customer_id LIKE $customer_id");
        foreach ($invoices as $invoice):
            $invoice->user_id = User::find($invoice->user_id);
            $invoice->user_id = $invoice->user_id->name;
        endforeach;
        return (isset($invoices) && !empty($invoices)) ? $invoices : false;
    }

}
