<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class CustomerLocation extends Model {

    private $tableName = 'customer_locations';
    protected $fillable = ['customer_id', 'lng', 'lat'];

    public function __construct() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`customer_id` int NOT NULL , "
                    . "`lng` decimal(10,6) NULL , "
                    . "`lat` decimal(10,6) NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
    }

    public function populateCustomersLocations() {
//        $apiAddress = "https://maps.googleapis.com/maps/api/geocode/json?address=";
//        $apiKey = "&key=AIzaSyBgSHLxC4xeJWrLe0Svg09EfWvIEtp1OfI";
//        foreach (DB::select("SELECT * FROM customers") as $customer):
//            if (isset($customer->address) && !empty($customer->address) && empty(DB::select("SELECT * FROM $this->tableName WHERE customer_id = $customer->id"))):
//                $address = str_replace(' ', '+', $customer->address);
//                $address .= ",$customer->state,$customer->postcode";
//                $url = $apiAddress . $address . $apiKey;
//                $json = file_get_contents($url);
//                $obj = json_decode($json, true);
//                if (isset($obj["results"][0]) && !empty($obj["results"][0])):
//                    $lat = $obj["results"][0]["geometry"]["location"]["lat"];
//                    $lng = $obj["results"][0]["geometry"]["location"]["lng"];
//                    if (!empty($lat) && !empty($lng)):
//                        $customerLocation = new CustomerLocation();
//                        $customerLocation->customer_id = $customer->id;
//                        $customerLocation->lng = $lng;
//                        $customerLocation->lat = $lat;
//                        $customerLocation->save();
//                    endif;
//                endif;
//            endif;
//        endforeach;
    }

}
