<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class DeletedOrder extends Model {

    private $tableName = 'deleted_orders';
    protected $fillable = ['invoiceid', 'sitename', 'orderoption', 'page', 'position', 'amount', 'startdate', 'expirydate', 'duedate', 'frequency', 'active', 'myob_account'];

    public function __construct() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`invoiceid` int NOT NULL , "
                    . "`sitename` text NULL , "
                    . "`orderoption` text NULL , "
                    . "`page` text NULL , "
                    . "`position` int NULL , "
                    . "`amount` decimal NULL , "
                    . "`startdate` date NULL , "
                    . "`expirydate` date NULL , "
                    . "`duedate` date NULL , "
                    . "`frequency` text NULL , "
                    . "`active` tinyint NULL , "
                    . "`myob_account` text NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
    }

    public function insertOrders($invoice_id) {
        $orders = DB::select("SELECT * FROM `orders` WHERE invoiceid = $invoice_id");
        foreach ($orders as $order):
            $deletedOrder = new DeletedOrder();
            $deletedOrder->invoiceid = $invoice_id;
            $deletedOrder->sitename = $order->sitename;
            $deletedOrder->orderoption = $order->orderoption;
            $deletedOrder->page = $order->page;
            $deletedOrder->position = $order->position;
            $deletedOrder->amount = $order->amount;
            $deletedOrder->startdate = $order->startdate;
            $deletedOrder->expirydate = $order->expirydate;
            $deletedOrder->duedate = $order->duedate;
            $deletedOrder->frequency = $order->frequency;
            $deletedOrder->active = $order->active;
            $deletedOrder->myob_account = $order->myob_account;
            $deletedOrder->save();
        endforeach;
    }

}
