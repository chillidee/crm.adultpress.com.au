<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Maps extends Model {

    public function __construct() {
        $doesFunctionExsit = DB::select("SELECT ROUTINE_NAME FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='calc_dist' AND ROUTINE_SCHEMA='" . env("DB_DATABASE") . "'");
        if (empty($doesFunctionExsit)):
            $query = "CREATE FUNCTION calc_dist(geo1_latitude decimal(10,6), geo1_longitude decimal(10,6), geo2_latitude decimal(10,6), geo2_longitude decimal(10,6))
                returns decimal(10,3) DETERMINISTIC
                BEGIN
                    return ((ACOS(SIN(geo1_latitude * PI() / 180) * SIN(geo2_latitude * PI() / 180) + COS(geo1_latitude * PI() / 180) * COS(geo2_latitude * PI() / 180) * COS((geo1_longitude - geo2_longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.852);
                END";
            DB::unprepared($query);
        endif;
    }

    public function calc_dist($latitude1, $longitude1, $latitude2, $longitude2) {
        $thet = $longitude1 - $longitude2;
        $dist = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($thet)));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $kmperlat = 111.325;
        $dist = $dist * $kmperlat;
        return (round($dist));
    }

    public function postcode_dist($postcode1, $postcode2, $suburb1 = '', $suburb2 = '') {
        $tableName = 'ap_postcodes';
        $extra = "";
        if ($suburb1 != '') {
            $extra = " and suburb = '$suburb1'";
        }
        $res = DB::select("SELECT * FROM $tableName WHERE lat <> 0 and lon <> 0 and postcode = '$postcode1'$extra");
        $num = count($res);

        $extra = "";
        if ($suburb2 != '') {
            $extra = " and suburb = '$suburb2'";
        }
        $res1 = DB::select("SELECT * FROM $tableName WHERE lat <> 0 and lon <> 0 and postcode = '$postcode2'$extra");
        $num1 = count($res1);

        if ($num != 0 && $num1 != 0) {
            $lat1 = $res[0]->lat;
            $lon1 = $res[0]->lon;
            $lat2 = $res1[0]->lat;
            $lon2 = $res1[0]->lon;
            $dist = $this->calc_dist($lat1, $lon1, $lat2, $lon2);
            if (is_numeric($dist)) {
                return (int) $dist;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    public function findBusinessWithinRadius($startPostcode, $radius) {
        $fileName = "$startPostcode-$radius.json";
        if (!$this->isCached($fileName)):
            $returnArray = array();
            foreach (DB::select("SELECT * FROM customers") as $customer):
                $distance = $this->postcode_dist($startPostcode, $customer->postcode);
                if ($distance <= $radius && $distance != -1):
                    array_push($returnArray, $customer);
                endif;
            endforeach;
            $this->saveCachedResults($returnArray, $startPostcode . "-" . $radius . ".json");
            return $returnArray;
        else:
            return $this->readCachedResults($fileName);
        endif;
    }

    public function findBusinessWithinRadiusV2($startPostcode, $radius) {
        $lonLat = $this->getPostCodeLongLat($startPostcode);
        if (isset($lonLat) && !empty($lonLat)):
            $customers = DB::select("SELECT customers.* FROM customers, customer_locations WHERE customers.id = customer_locations.ID AND calc_dist($lonLat->lat, $lonLat->lon, customer_locations.lat, customer_locations.lng) < $radius");
        endif;
        return (isset($customers) && !empty($customers)) ? $customers : array();
    }

        public function findBusinessWithinRadiusV3($startPostcode, $radius) {
            $lonLat = $this->getPostCodeLongLat($startPostcode);
            if (isset($lonLat) && !empty($lonLat)):
                $customers = DB::select("SELECT customers.* FROM customers left join customer_locations on customers.id = customer_locations.customer_id Where calc_dist($lonLat->lat, $lonLat->lon, customer_locations.lat, customer_locations.lng) < $radius");
            endif;
            return (isset($customers) && !empty($customers)) ? $customers : array();
        }

    public function getPostCodeLongLat($postCode) {
        $tableName = 'ap_postcodes';
        $res = DB::select("SELECT * FROM $tableName WHERE lat <> 0 and lon <> 0 and postcode = '$postCode'");
        return (isset($res) && !empty($res)) ? $res[0] : false;
    }

    public function getCustomersLongsAndLats($customersArray) {
        $tableName = 'ap_postcodes';
        $postcodeArray = array();
        $longLatsArray = array();
        foreach ($customersArray as $customer):
            if (!in_array($customer->postcode, $postcodeArray)):
                $res = DB::select("SELECT * FROM $tableName WHERE lat <> 0 and lon <> 0 and postcode = '$customer->postcode'");
                array_push($postcodeArray, $customer->postcode);
                $longLatsArray[$customer->postcode] = $res[0];
            endif;
        endforeach;
        return $longLatsArray;
    }

    private function getCustomerPostcodes() {
        $postcodeArray = array();
        $customers = DB::select("SELECT postcode FROM customers");
        foreach ($customers as $customer):
            if (!in_array($customer->postcode, $postcodeArray) && isset($customer->postcode) && !empty($customer->postcode)):
                array_push($postcodeArray, $customer->postcode);
            endif;
        endforeach;
        return $postcodeArray;
    }

    public function getGoogleMapsData($customersArray) {
        $tableName = 'ap_postcodes';
        $longLatsArray = array();
        foreach ($customersArray as $customer):
            $res = DB::select("SELECT * FROM customer_locations WHERE customer_id = '$customer->id'");
            if (isset($res[0]) && !empty($res[0])):
                $longLatsArray[$customer->id] = $res[0];
            endif;
        endforeach;
        return $longLatsArray;
    }

    public function getCustomerPostcodesSuburbs() {
        $postSub = array();
        foreach ($this->getCustomerPostcodes() as $postcode):
            $suburb = DB::select("SELECT suburb FROM customers WHERE postcode = '$postcode'");
            if (isset($suburb) && !empty($suburb)):
                $postSub[$postcode] = $suburb[0]->suburb;
            endif;
        endforeach;
        asort($postSub);
        return $postSub;
    }

    public function cacheFindCustomersResults() {
        $radiusArray = array(5, 10, 25, 50);
        foreach ($this->getCustomerPostcodes() as $postcode):
            foreach ($radiusArray as $radius):
                $this->saveCachedResults($this->findBusinessWithinRadius($postcode, $radius), $postcode . "-" . $radius . ".json");
            endforeach;
        endforeach;
    }

    private function saveCachedResults($results, $filename) {
        $destFolder = __DIR__ . "/cachedPostcodes";
        if (!file_exists($destFolder)) {
            mkdir($destFolder, 0775, true);
        }
        $destFolder = "$destFolder/$filename";
        file_put_contents($destFolder, json_encode($results));
    }

    private function readCachedResults($filename) {
        $destFolder = __DIR__ . "/cachedPostcodes";
        $_filename = $destFolder . '/' . $filename;
        if ($this->isCached($filename)):
            return json_decode(file_get_contents($_filename));
        else:
            return false;
        endif;
    }

    private function isCached($fileName) {
        $destFolder = __DIR__ . "/cachedPostcodes";
        return file_exists($destFolder . "/" . $fileName);
    }

}
