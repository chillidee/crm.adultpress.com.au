<?php

define('DateFormat', "d/m/Y");
define('DateTimeFormat', "d/m/Y H:i");
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    //return view('welcome');
    if (Auth::guest()) {
        return view('welcome');
    } else {
        return Redirect::to('expiringtasks');
    }
});

Route::get('postcodes', 'PostcodesController@autocomplete');
Route::get('streettypes', 'StreettypesController@autocomplete');
Route::get('customersname', 'CustomerNamesController@autocomplete');
Route::get('customersphone', 'CustomerPhonesController@autocomplete');
Route::get('customersemail', 'CustomerEmailsController@autocomplete');

Route::group(['middlewareGroups' => ['web']], function() {
    Route::auth();

    Route::get('customerslist', 'CustomersController@customerslist');
    Route::post('customerslistrefined', 'CustomersController@customerslistrefined');

    Route::post('/admin/users/store', 'AdminController@store');
    Route::resource('/admin/users', 'AdminController');
    Route::post('/admin/users/{id}/update', 'AdminController@update');
    Route::post('/admin/changeAccManager', 'AdminController@changeacc');


    Route::resource('/admin/positions', 'PositionsController');
    Route::post('/admin/positions/store', 'PositionsController@store');
    Route::post('/admin/positions/edit/{id}', 'PositionsController@edit');

    Route::resource('/admin/websites', 'WebsitesController');
    Route::post('/admin/websites/store', 'WebsitesController@store');
    Route::post('/admin/websites/edit/{id}', 'WebsitesController@edit');

    Route::resource('/admin/websites', 'WebsitesController');
    Route::post('/admin/websites/store', 'WebsitesController@store');
    Route::post('/admin/websites/edit/{id}', 'WebsitesController@edit');

    Route::resource('/admin/statuses', 'StatusesController');
    Route::post('/admin/statuses/store', 'StatusesController@store');
    Route::post('/admin/statuses/edit/{id}', 'StatusesController@edit');

    Route::resource('/admin/orderoptions', 'OrderOptionsController');
    Route::post('/admin/orderoptions/store', 'OrderOptionsController@store');
    Route::post('/admin/orderoptions/edit/{id}', 'OrderOptionsController@edit');

    Route::resource('/admin/orderpages', 'OrderPagesController');
    Route::post('/admin/orderpages/store', 'OrderPagesController@store');
    Route::post('/admin/orderpages/edit/{id}', 'OrderPagesController@edit');

    Route::resource('/admin/types', 'TypesController');
    Route::post('/admin/types/store', 'TypesController@store');
    Route::post('/admin/types/edit/{id}', 'TypesController@edit');

    Route::get('/customers/deduplicate', 'CustomersController@deduplicate');
    Route::get('/deduplicate', 'DeduplicateController@index');
    Route::post('/customer/store', 'CustomersController@store');
    Route::get('/customers', 'CustomersController@index');
    Route::resource('/customer', 'CustomersController');

    Route::post('/cstatus/store', 'CstatusController@store');
    Route::post('/note/store', 'NotesController@store');
    Route::patch('/note/update/{id}', 'NotesController@update');
    Route::get('/searchtasks', 'TasksController@search');
    Route::get('/expiringtasks', 'TasksController@expiringTasks');
    Route::post('/expiringtasks', 'TasksController@expiringTasks');
    Route::post('viewExpiredTasks', 'TasksController@expiredTasks');
    Route::get('/import', 'ImportController@index');
    Route::post('/import/save', 'ImportController@save');

    Route::post('/addabn/store', 'AddabnController@store');
    Route::resource('/invoice', 'InvoiceController');
    Route::resource('/payment', 'PaymentsController');
    Route::resource('/admin/paymentmethods', 'PaymentmethodsController');
    Route::get('/invoicereminder/{id}', 'CustomersController@invoicereminder');
    Route::get('/paymentreminder/{id}', 'PaymentsController@paymentreminder');
    Route::get('/reports/paymentsreport', 'PaymentsController@paymentsreport');
    Route::get('/reports/reports', 'ReportsController@index');
    Route::get('/reports/owingcustomers', 'ReportsController@owingcustomers');
    Route::get('/reports/overdueactivepaid', 'ReportsController@overdueactivepaid');
    Route::get('/reports/montlyreport', 'ReportsController@montlyreport');

    Route::get('reports/graphreport', 'ReportsController@graphreport');
    Route::post('invoice/toggleOrderStatus', 'InvoiceController@toggleOrderStatus');
    Route::post('invoice/cancelInvoice', 'InvoiceController@creditInvoice');
    Route::post('invoice/checkMyobInvoice', 'InvoiceController@checkForMyob');

    Route::get('tools/myob', 'MyobController@index');
    Route::get('tools/myob/authorise', 'MyobController@authorise');
    Route::get('tools/myob/deactivateAllOrders', 'MyobController@test');
    Route::get('tools/myob/test', 'MyobController@test1');
    Route::get('tools/myob/refreshToken', 'MyobController@refreshToken');
    Route::get('tools/myob/getAllCustomers', 'MyobController@getAllCustomers');
    Route::get('tools/myob/getAllAccounts', 'MyobController@getAllAccounts');
    Route::get('tools/myob/getAllTaxes', 'MyobController@getAllTaxes');
    Route::post('tools/myob/updateBrothelsAccountUid', 'MyobController@updateBrothelsAccountUid');
    Route::post('tools/myob/updateMPMAccountUid', 'MyobController@updateMPMAccountUid');
    Route::post('tools/myob/updateTaxUid', 'MyobController@updateTaxUid');
    Route::post('tools/myob/updateCompanyFile', 'MyobController@updateCompanyFile');

    Route::post('tools/myob/updatePassword', 'MyobController@updatePassword');
    Route::post('tools/myob/checkPassword', 'MyobController@checkPassword');

    Route::get('tools/maps', 'MapController@index');
    Route::post('tools/maps', 'MapController@search');
    Route::post('/task/store', 'CustomerTasksController@store');

    Route::get('tools/deletedcustomers', 'DeletedCustomerController@index');


    Route::post('/customer/restoreDeletedCustomer', 'DeletedCustomerController@restore');
    Route::post('/customer/deleteDeletedCustomer', 'DeletedCustomerController@delete');

    /* overdueactivepaid
      Route::get('/admin/backup', 'BackupController@index');
      Route::get('/admin/create_backup', 'BackupController@create_backup');
     */

    Route::get('/temp', 'TempController@search');
});


Route::any('{slug}', function($slug) {
    return view('404');
    //do whatever you want with the slug
})->where('slug', '(.*)?');
