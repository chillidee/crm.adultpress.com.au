<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Response;
use DB;

class MonthlyInvoicedController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function data() {
        $query = "SELECT DATE_FORMAT(created_at,'%b %Y')as month, sum(amount)as amount FROM payments GROUP BY DATE_FORMAT(created_at,'%b')";
        $results = array();
        $results = DB::select($query);

        return Response::json($results);
    }

}
