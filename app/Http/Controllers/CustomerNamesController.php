<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Response;

class CustomerNamesController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function autocomplete() {
        $term = $_GET['term'];
        $results = array();
        $queries = DB::table('customers')
                ->where('bname', 'LIKE', '%' . $term . '%')
                ->get();

        foreach ($queries as $query) {
            $results[] = ['id' => $query->id, 'value' => $query->bname];
        }

        return Response::json($results);
    }

}
