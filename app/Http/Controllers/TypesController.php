<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Type;

class TypesController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $types = Type::all();
        return view('admin.types', compact('types'));
    }
    public function store(Request $request) {
        //var_dump($_REQUEST);
        $type = new Type;
        $type->type = $request->type;
        $type->status = $request->status;
        $type->save();
        Session::flash('success', 'Business type created successfully');
        return back();
    }

    public function show($id) {
        $types = Type::all();

        $thistype = Type::findOrFail($id);
        return view('admin.types', compact('types'), compact('thistype'));
    }

    public function edit($id) {

        $type = Type::findOrFail($id);
        $type->update($_REQUEST);
        Session::flash('status', 'Business type updated successfully');

        return back();
    }

    public function destroy() {

        Type::destroy($_REQUEST['typeid']);
        Session::flash('error', 'Business type deleted successfully');

        return redirect('admin/types');
    }

}
