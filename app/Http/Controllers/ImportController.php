<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Customer;
use App\Cstatus;
use App\Status;
use App\User;
use App\Type;
use Excel;
use File;
use Auth;
use DB;

class ImportController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function index() {
        $types = Type::all();
        $users = User::where('status', '!=', 'deleted')->orderby('name')->get();
        return view('import', compact('users', 'types'));
    }

    public function save(Request $request) {

        /*
          dd($request->customersFile);
          $fileext = explode('/', $request->customersFile);

         */




        $file = Input::file($request->customersFile);


        // process the form
        try {
            \Excel::load(Input::file('customersFile'), function ($reader) {
            $imported = false;

                foreach ($reader->toArray() as $row) {
                    $imported = true;

                    if (!isset($_POST['btype']) || $_POST['btype'] == '') {
                        Session::flash('error', 'Wrong File Format.');
                        return redirect('import');
                    } else {
                        $customer = new Customer($row);
                        
                        $customer->btype = $_POST['btype'];
                        $customer->account_manager = $_POST['account_manager'];
                        $customer->save();
                        $cstatus = new Cstatus();
                        $cstatus->dueDatetime = date("Y-m-d H:i");
                        $cstatus->userid = Auth::USER()->id;
                        $cstatus->customerid = $customer->id;
                        $cstatus->statusid = Status::first()->id;
                        $cstatus->datetime = date("Y-m-d H:i");
                        $cstatus->save();
                    }
                    //echo "$customer->id<hr>";
                }
                if ($imported == true)
                    Session::flash('success', 'CustomerFile uploaded successfully.');
                else
                    Session::flash('error', 'Nothing to import.');
            });
            return redirect('import');
        } catch (\Exception $e) {
            echo "Error<br>";
            echo $e->getMessage() . "<br>";

            Session::flash('error', $e->getMessage());
            return redirect('import');
        }
    }

}
