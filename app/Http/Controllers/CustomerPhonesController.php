<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Response;
use DB;

class CustomerPhonesController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function autocomplete() {
        $term = str_replace(' ', '', $_GET['term']);

        $results = array();
        $queries = DB::select("SELECT * FROM customers WHERE bphone like '%$term%' OR REPLACE(bphone,' ','') LIKE '%$term%' OR REPLACE(phone,' ','') LIKE '%$term%'");

        foreach ($queries as $query) {
            $results[] = ['id' => $query->id, 'value' => formatPhone($query->bphone)];
        }
        return Response::json($results);
    }

}
