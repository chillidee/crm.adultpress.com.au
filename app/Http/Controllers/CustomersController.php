<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Paymentmethod;
//use App\Http\Requests;
use App\OrderOption;
use App\OrderPage;
use App\position;
use App\Customer;
use App\Cstatus;
use Response;
//use App\Payment;
use App\Website;
use App\Status;
use App\Addabn;
use App\User;
use App\Type;
use Auth;
use App\DeletedInvoices;
use App\Maps;
use App\CustomerTasks;
use App\DeletedCustomer;

class CustomersController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function index() {
        $maps = new Maps();
        $customers = Customer::all();
        $postcodes = $maps->getCustomerPostcodesSuburbs();
        $radiuses = array(5, 10, 25, 50);
        return view('customers', compact('customers', 'postcodes', 'radiuses'));
    }

    public function customerslist() {        
        $customers = Customer::getList();
        /*$customers = Customer::all();
        foreach( $customers as $customer ) {
            $customer->manager = $customer->accmanager();
            $status = $customer->status->name;
            $customer->btype = $customer->type->type;
            $customer->dueDatetime = date ( 'd-m-Y', strtotime($customer->status->dueDatetime));
            $customer->statusname = ( $status ? $status->name : '' );
        }*/        
        $response = (object) [];
        $response->aaData = $customers;      
        echo json_encode($response);
    }

    public function customerslistrefined(Request $request) {
        $maps = new Maps();
        $results = $maps->findBusinessWithinRadiusV3((int) $request['postcode'], (int) $request['radius']);
        foreach ($results as $customer):
            $type = DB::select("SELECT type FROM types WHERE id = '$customer->btype'");
            $customer->btype = (isset($type) && !empty($type)) ? $type[0]->type : '';
            $statusname = DB::select("SELECT statuses.name FROM cstatuses, statuses WHERE cstatuses.customerid = $customer->id AND cstatuses.latest = 1 AND statuses.id = cstatuses.statusId");
            $customer->statusname = (isset($statusname) && !empty($statusname)) ? $statusname[0]->name : '';
            $manager = DB::select("SELECT name FROM users WHERE id = '$customer->account_manager'");
            $customer->manager = (isset($manager) && !empty($manager)) ? $manager[0]->name : '';
            $dueDatetime = DB::select("SELECT DATE_FORMAT(cstatuses.dueDatetime, '%m/%d/%Y') as dueDatetime FROM cstatuses WHERE customerid = '$customer->id'");
            $customer->dueDatetime = (isset($dueDatetime) && !empty($dueDatetime)) ? $dueDatetime[0]->dueDatetime : '';
        endforeach;
        echo json_encode($results);
    }

    public function create() {
        $types = Type::all();
        $users = User::where('status', '!=', 'deleted')->orderby('name')->get();
        $positions = position::all();
        return view('customer-form', compact('positions', 'types', 'users'));
    }

    public function edit($id) {
        $customer = Customer::find($id);
        $res = DB::select("SELECT * FROM customer_locations WHERE customer_id = '$customer->id'");
        if (isset($res[0]) && !empty($res[0])):
            DB::delete("DELETE FROM `customer_locations` WHERE `customer_locations`.`customer_id` = $customer->id");
        endif;
        $types = Type::all();
        $positions = position::all();
        $users = User::where('status', '!=', 'deleted')->orderby('name')->get();
        //$customer->address = $address;
        if ($customer) {
            return view('customer-form', compact('customer', 'types', 'users', 'positions'));
        }
        return back();
    }

    public function show($id) {
        if (is_numeric($id) == false) {
            return back();
        }
        $customer = Customer::find($id);
        if ($customer == null) {
            return back();
            //dd($customer);
        }

        $payments = DB::select("SELECT payments.*,users.name FROM payments,customers,invoices,users WHERE payments.invoiceid = invoices.id AND invoices.customerid = customers.id AND customers.id =$id AND users.id = payments.userid ORDER BY payments.created_at ASC
");

        $orderoptions = OrderOption::orderby('orderoption')->get();
        $statuses = Status::orderby('orderid')->get();
        $paymentmethod = Paymentmethod::all();
        $orderpages = OrderPage::orderby('page')->get();
        $addabn = Addabn::where('customerid', '=', $id)->where('latest', '=', 1)->first();
        $websites = Website::where('status', '=', 'Active')->get();
        $deletedInvoices = new DeletedInvoices();
        $deleted = $deletedInvoices->getDeletedOrdersByCustomerId($id);
        $customerTasks = CustomerTasks::getCustomerTasks($id);
        $currentOrders = DB::select("SELECT orders.* FROM invoices, orders, cstatuses WHERE invoices.customerid = $id AND orders.active = 1 AND invoices.id = orders.invoiceid AND cstatuses.customerid = $id AND cstatuses.latest = 1 AND cstatuses.statusId = 600");

        if ($customer) {
            return view('customer', compact('customer', 'addabn', 'websites', 'payments', 'statuses', 'paymentmethod', 'orderoptions', 'orderpages', 'deleted', 'customerTasks', 'currentOrders'));
        }
        return back();
    }

    public function update(Request $request, Customer $customer) {
        $request['extend'] = json_encode($request['extend']);
        $request['additional_contacts'] = json_encode($request['additional_contact']);
        // ****** Set the address format
        $request['address'] = "";
        if (isset($request['unit']) && $request['unit'] != NULL)
            $request['address'] = $request['unit'] . "/";
        $request['address'] .= $request['number'] . ' ' . $request['street'] . ' ' . $request['searchSttype'];
        $request->bphone = formatPhone($request->bphone);
        $request->phone = formatPhone($request->phone);
        $customer->address_unit = $request->unit;
        $customer->address_no = $request->number;
        $customer->address_street = $request->street;
        //echo $request->bphone;
        $customer->update($request->all());
        //dd($customer->bphone);

        Session::flash('status', 'Customer file updated successfully');
        return redirect("customer/$customer->id");
    }

    public function store(Request $request) {

        $request['extend'] = json_encode($request['extend']);
        $request['additional_contacts'] = json_encode($request['additional_contact']);

        // ****** Set the address format
        $request['address'] = "";
        if (isset($request['unit']) && $request['unit'] != NULL)
            $request['address'] = $request['unit'] . "/";
        $request['address'] .= $request['number'] . ' ' . $request['street'] . ' ' . $request['searchSttype'];
        //$customer = new Customer;
        if (!isset($request['account_manager']) or $request['account_manager'] == '') {
            $request['account_manager'] = User::where('name', '=', 'unassigned')->first()->id;
        }
        $request->address_unit = $request['unit'];
        $request->address_no = $request['number'];
        $request->address_street = $request['street'];
        $request->bphone = formatPhone($request->bphone);
        $request->phone = formatPhone($request->phone);
        $customer = Customer::create($request->all());

        $query = "UPDATE cstatuses SET latest = 0 WHERE customerid = $customer->id";
        DB::update($query);

        $cstatus = new Cstatus;
        $cstatus->dueDatetime = date("Y-m-d H:i");
        if (Auth::user() != NULL) {
            $cstatus->userId = Auth::user()->id;
        } else {
            $cstatus->userId = 11;
        }

        //dd(Auth::user());

        $cstatus->customerid = $customer->id;
        $cstatus->latest = 1;
        $cstatus->statusId = Status::first()->id;
        $cstatus->datetime = date("Y-m-d H:i");
        $cstatus->save();

        Session::flash('success', 'New customer file created successfully');

        return redirect("customer/$customer->id");
    }

    public function destroy() {
        $customerid = $_REQUEST['customerid'];
        $deletedCustomer = new DeletedCustomer();

        $deletedCustomer->insertDeletedCustomer($customerid, Auth::user()->id);

        Customer::destroy($_REQUEST['customerid']);

        DB::table('cstatuses')->where('customerid', '=', $customerid)->delete();
        DB::table('notes')->where('customerid', '=', $customerid)->delete();
        DB::table('addabns')->where('customerid', '=', $customerid)->delete();

        $query = "delete FROM payments WHERE payments.invoiceid in (SELECT invoices.id FROM invoices WHERE invoices.customerid =  $customerid)";
        $result = DB::delete($query);



        $query = "delete FROM orders WHERE orders.invoiceid in (SELECT invoices.id FROM invoices WHERE invoices.customerid =  $customerid)";
        $result = DB::delete($query);

        // remove invoices orders payments

        DB::table('invoices')->where('customerid', '=', $customerid)->delete();
        //var_dump($result);
        Session::flash('error', 'Customer file deleted successfully, including all relevant data and history!');
        return redirect("customers");
    }

    public function invoicereminder($id) {
        $customer = Customer::find($id);
        $status = DB::select("SELECT * FROM cstatuses,statuses WHERE cstatuses.statusId = statuses.id and cstatuses.customerid = $id and latest = 1")[0];
        return view('invoicereminder', compact('customer', 'status'));
    }

}
