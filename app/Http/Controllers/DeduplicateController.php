<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\position;
use App\Payment;
use App\Customer;
use App\Cstatus;
use App\Status;
use App\Type;
use App\Website;
use App\Paymentmethod;
use App\User;
use App\Addabn;
use Auth;

class DeduplicateController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function index() {


        $get = $_GET;
      

        $users = User::where('status', '!=', 'deleted')->orderby('name')->get();
        $types = Type::all();

        if (isset($get['findBy'])) {

            //**********************************
            //find group of same customers, return list of one from each group
            switch ($get['findBy']) {
                case 1:
                    // by phone
                    $query = "select * from 
                    (SELECT count(*)as counts,GROUP_CONCAT(customers.id) as customersid, 
                    GROUP_CONCAT(types.type) as typenames,GROUP_CONCAT(users.name) as usernames, 
                    GROUP_CONCAT(account_manager) as managers, GROUP_CONCAT(btype) as types , 
                    bname,bemail,bphone 
    FROM customers,types,users 
    where customers.btype=types.id AND customers.account_manager=users.id AND bphone !='' GROUP BY 
    RIGHT(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(bphone,' ',''),'/',''),'-',''),'.',''),'_',''),8)
    )as tmp_table where counts >1";
                    break;
                case 2:
                    //by phone and name
                    $query = "select * from 
                    (SELECT count(*)as counts,GROUP_CONCAT(customers.id) as customersid, 
                    GROUP_CONCAT(types.type) as typenames,GROUP_CONCAT(users.name) as usernames, 
                    GROUP_CONCAT(account_manager) as managers, GROUP_CONCAT(btype) as types , 
                    bname,bemail,bphone 
    FROM customers,types,users 
    where customers.btype=types.id AND customers.account_manager=users.id AND bname !='' AND bphone !='' GROUP BY 
    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(bname,' ',''),'/',''),'-',''),'.',''),'_',''),
    RIGHT(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(bphone,' ',''),'/',''),'-',''),'.',''),'_',''),8)
    )as tmp_table where counts > 1";
                    break;
                case 3:
                    //by email and name
                    $query = "select * from 
                    (SELECT count(*)as counts,GROUP_CONCAT(customers.id) as customersid, 
                    GROUP_CONCAT(types.type) as typenames,GROUP_CONCAT(users.name) as usernames, 
                    GROUP_CONCAT(account_manager) as managers, GROUP_CONCAT(btype) as types , 
                    bname,bemail,bphone 
    FROM customers,types,users         
    where customers.btype=types.id AND customers.account_manager=users.id AND  bname !='' AND bemail !='' GROUP BY 
    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(bname,' ',''),'/',''),'-',''),'.',''),'_',''),
    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(bemail,' ',''),'/',''),'-',''),'.',''),'_','')
    )as tmp_table where counts > 1";
                    break;
            }

            if ($get['account_manager'] > 0)
                $query .= " and (managers LIKE '%," . $get['account_manager'] . "' or managers LIKE '" . $get['account_manager'] . ",%' or managers LIKE '%," . $get['account_manager'] . ",%')";
            if ($get['btype'] > 0)
                $query .= " and (types LIKE '%," . $get['btype'] . "' or types LIKE '" . $get['btype'] . ",%' or types LIKE '%," . $get['btype'] . ",%')";

            $duplist = DB::select($query);

            //************************************
        }
        if (isset($get['ids'])) {
            $ids = $get['ids'];
            $customers = DB::select("SELECT customers.*,users.name as account_manager_name,types.type as b_type FROM customers,types,users WHERE customers.id in ($ids) AND customers.account_manager=users.id AND types.id=customers.btype");
        }

        return view("/deduplicate", compact('duplist', 'users', 'types', 'customers'));
    }

}
