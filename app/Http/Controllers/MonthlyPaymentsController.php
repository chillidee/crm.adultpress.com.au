<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Response;
use DB;

class MonthlyPaymentsController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function data() {
        $query = "SELECT DATE_FORMAT(created_at,'%b %Y')as month, sum(amount)as amount FROM orders GROUP BY DATE_FORMAT(created_at,'%b')";
        //$results = array();
        $results = DB::select($query);
        
        return $results;
        //return Response::json($results);
    }

}
