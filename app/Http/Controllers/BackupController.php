<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class BackupController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $databases = DB::select("SHOW DATABASES like 'laravelcrm%'");
        return view('admin.backup', compact('databases'));
    }

    public function create_backup() {

        $query = "CREATE DATABASE laravelcrm_" . date("Y_m_d_H_i_s");
        $result = DB::query($query);
        //dd($query);

        $databases = DB::select("SHOW DATABASES like 'laravelcrm%'");
        return view('admin.backup', compact('databases'));
    }

    function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }

}
