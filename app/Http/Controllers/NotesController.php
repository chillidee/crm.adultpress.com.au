<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Note;
use App\User;
use Auth;
class NotesController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function update(Request $request, Note $note) {

        $note = Note::find($request->id);
        $note->note = $request->noteBody;
        $note->update();
        Session::flash('status', 'Note updated successfully');
        return redirect("customer/$note->customerId");
    }

    public function store(Request $request) {
        if (strlen($request->noteBody) > 2) {
            $note = new Note;
            $note->note = $request->noteBody;
            $note->datetime = date("Y-m-d H:i");
            $note->customerId = $request->customerid;
            $note->userId = Auth::user()->id;
            Session::flash('success', 'New note created successfully');
            $note->save();
        }
        return redirect("customer/$request->customerid");
    }

}
