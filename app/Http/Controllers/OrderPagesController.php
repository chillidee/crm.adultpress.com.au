<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\OrderPage;

class OrderPagesController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $orderpages = OrderPage::all();
        //dd($orderpages);
        return view('admin.orderpages', compact('orderpages'));
    }

    public function store(Request $request) {
        try {
            $orderpage = new OrderPage;
            $orderpage->page = $request->orderpage;
            //dd("$request");
            $orderpage->save();
            Session::flash('success', 'New Order Page created successfully');
        } catch (\Exception $e) {
            Session::flash('error', 'Unable to create this  Order Page!');
        }

        return back();
    }

    public function show($id) {
        $orderpages = OrderPage::all();
        $thisorderpage = OrderPage::find($id);
        return view('admin.orderpages', compact('orderpages'), compact('thisorderpage'));
    }

    public function update($id, Request $request) {
        $orderpage = OrderPage::find($id);
        $orderpage->page = $request->orderpage;
        $orderpage->update();
        //dd($orderpage);
        Session::flash('status', 'Order Page updated successfully');
        return back();
    }

    public function destroy($id) {
        OrderPage::destroy($id);
        Session::flash('error', 'Order Page deleted successfully');
        return redirect('admin/orderpages');
    }

}
