<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Response;

class CustomerEmailsController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function autocomplete() {
        $term = $_GET['term'];
        $results = array();
        $queries = DB::table('customers')
                ->where('bemail', 'LIKE', '%' . $term . '%')
                ->get();

        foreach ($queries as $query) {
            $results[] = ['id' => $query->id, 'value' => $query->bemail];
        }

        return Response::json($results);
    }

}
