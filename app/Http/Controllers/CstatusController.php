<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cstatus;
use App\Customer;
use App\User;
use Auth;
use DB;
use Mail;

class CstatusController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function store(Request $request) {
        $request->dueDate = (isset($request->dueDate) && !empty($request->dueDate)) ? date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $request->dueDate))) : null;
        $cstatus = new Cstatus;
        $cstatus->dueDatetime = $request->dueDate;
        $cstatus->userid = Auth::USER()->id;
        $cstatus->customerid = $request['customerid'];
        $cstatus->statusid = $request['cstatus'];
        $cstatus->datetime = date("Y-m-d H:i");
        $query = "UPDATE cstatuses SET latest = 0 WHERE  customerid= $cstatus->customerid";
        DB::update($query);
        $cstatus->latest = 1;
        $cstatus->save();
        Session::flash('success', 'Customer status updated successfully');
        //**** SEND EMAIL NOTIFICATION TO ACCOUNTS  ****
        $status = DB::select("SELECT * FROM cstatuses,statuses WHERE cstatuses.statusId = statuses.id and cstatuses.customerid = $cstatus->customerid and latest = 1")[0];
        if ($status->orderid == 610) {
            $customer = Customer::find($cstatus->customerid);
            $data = array('customer' => $customer, 'status' => $status);
//            Mail::send("invoicereminder", $data, function($message) {
//                $message->from('crm.adultpress@gmail.com')
//                        ->to('accounts@adultpress.com.au')
//                        ->subject('Invoice Notification');
//            });
            Session::flash('status', 'Reminder sent to accounts!');
        }


        //************************************************




        return redirect("customer/$cstatus->customerid");
    }

}
