<?php

namespace App\Http\Controllers;

use App\Myob;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\AdminPassword;
use Auth;

class MyobController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $myob = new Myob();
        $myobDetails = $myob->getSortedDBFields();
        $adminPassword = new AdminPassword();
        $adminPasswordDetails = $adminPassword->getPasswordDetails();
        return view('admin.myob', compact('myobDetails', 'adminPasswordDetails'));
    }

    public function authorise() {

        require_once base_path('vendor/myob/index.php');

        if (isset($_SESSION) && !empty($_SESSION)):
            $myob = new Myob();
            $myob->clearTable();
            $companyFiles = $myob->getCompanyFiles();
            $myob->access_token = $_SESSION['access_token'];
            $myob->access_token_expires = $_SESSION['access_token_expires'];
            $myob->refresh_token = $_SESSION['refresh_token'];
            $myob->expires_in = $_SESSION['expires_in'];
            $myob->user = $_SESSION['user'];
            $myob->username = $_SESSION['username'];
            $myob->gstCodeUID = '';
            $myob->accountsMPMUid = '';
            $myob->accountBrothelsUid = '';
            $myob->save();
        endif;

        $myobDetails = (isset($myob) && !empty($myob)) ? $myob->getSortedDBFields() : array();

        $companyFiles = (isset($companyFiles) && !empty($companyFiles) && empty($companyFiles['Errors'])) ? $companyFiles : array();

        return view('admin.myob', compact('myobDetails', 'companyFiles'));
    }

    public function test() {
        $myob = new Myob();
        $myob->test();
        $myobDetails = $myob->getSortedDBFields();
        return view('admin.myob', compact('myobDetails'));
    }

    public function test1() {
        $myob = new Myob();
        $myob->test1();
        $myobDetails = $myob->getSortedDBFields();
        return view('admin.myob', compact('myobDetails'));
    }

    public function refreshToken() {
        $myob = new Myob();
        $myob->renewToken();
        $myobDetails = $myob->getSortedDBFields();
        return view('admin.myob', compact('myobDetails'));
    }

    public function getAllCustomers() {
        $myob = new Myob();
        $customers = $myob->getAllCustomers();
        return view('ajax.myobCustomerTable', compact('customers'));
    }

    public function getAllAccounts() {
        $myob = new Myob();
        $accounts = $myob->getAllAccounts();
        return view('ajax.myobAccountsTable', compact('accounts'));
    }

    public function getAllTaxes() {
        $myob = new Myob();
        $taxes = $myob->getAllTaxes();
        return view('ajax.myobTaxTable', compact('taxes'));
    }

    public function updateAccountUid(Request $request) {
        $myob = new Myob();
        $myob->setAccountsUid($request['value']);
    }

    public function updateBrothelsAccountUid(Request $request) {
        $myob = new Myob();
        $myob->updateBrothelsAccountUid($request['value']);
    }

    public function updateMPMAccountUid(Request $request) {
        $myob = new Myob();
        $myob->updateMPMAccountUid($request['value']);
    }

    public function updateTaxUid(Request $request) {
        $myob = new Myob();
        $result = $myob->saveGSTCodeUID($request['value']);
        if (isset($result['Errors']) && !empty($result['Errors'])):
            Session::flash('error', $result['Errors']['Message']);
        else:
            Session::flash('status', 'Tax UID was updated successfully');
        endif;
    }

    public function updateCompanyFile(Request $request) {
        $myob = new Myob();
        $myob->saveCompanyFile($request['uid'], $request['url']);
    }

    public function updatePassword(Request $request) {
        $adminPassword = new AdminPassword();
        return $adminPassword->updatePassword($request->newPassword, Auth::user()->id);
    }

    public function checkPassword(Request $request) {
        $adminPassword = new AdminPassword();
        echo $adminPassword->checkPassword($request->password);
    }

}
