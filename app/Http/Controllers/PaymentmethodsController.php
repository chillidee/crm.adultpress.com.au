<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Paymentmethod;
use App\Http\Requests;

class PaymentmethodsController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $paymentmethods = Paymentmethod::all();
        return view('admin.paymentmethods', compact('paymentmethods'));
    }

    public function store(Request $request) {
        try {
            $paymentmethods = new Paymentmethod;
            $paymentmethods->method = $request->method;
            $paymentmethods->save();
            Session::flash('success', 'New payment method created successfully');
        } catch (\Exception $e) {
            Session::flash('error', 'Unable to create this Payment method!');
        }

        return back();
    }

    public function show($id) {
        $paymentmethods = Paymentmethod::all();
        $thispaymentmethod = Paymentmethod::find($id);
        return view('admin.paymentmethods', compact('paymentmethods'), compact('thispaymentmethod'));
    }

    public function update($id) {
        $paymentmethods = Paymentmethod::find($id);
        $paymentmethods->update($_REQUEST);
        Session::flash('status', 'Payment method updated successfully');
        return back();
    }

    public function destroy($id) {
        Paymentmethod::destroy($id);
        Session::flash('error', 'Payment methods deleted successfully');
        return redirect('admin/paymentmethods');
    }

}
