<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\CustomerTasks;

class CustomerTasksController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function store(Request $request) {
        $hasRecord = CustomerTasks::getCustomerTasks($request->customerID);
        if ($hasRecord):
            CustomerTasks::where('id', '=', $hasRecord->ID)->update($this->prepareUpdateTasksObject($request));
        else:
            $ctasks = $this->prepareSaveTasksObject($request);
            $ctasks->save();
        endif;

        Session::flash('status', 'Customer checklist updated successfully');
        return redirect("customer/$request->customerID");
    }

    private function prepareUpdateTasksObject($request) {
        $returnArray = array();
        $returnArray['customerid'] = $request->customerID;
        $returnArray['mpmchecklist'] = (isset($request->mpmchecklist) && !empty($request->mpmchecklist)) ? true : false;
        $returnArray['mpmdetailsfilled'] = (isset($request->mpmdetailsfilled) && !empty($request->mpmdetailsfilled)) ? true : false;
        $returnArray['mpmconfirmplan'] = (isset($request->mpmconfirmplan) && !empty($request->mpmconfirmplan)) ? true : false;
        $returnArray['mpmbaselocation'] = (isset($request->mpmbaselocation) && !empty($request->mpmbaselocation)) ? $request->mpmbaselocation : '';
        $returnArray['mpmpaid'] = (isset($request->mpmpaid) && !empty($request->mpmpaid)) ? true : false;
        $returnArray['mpmapprovedpics'] = (isset($request->mpmapprovedpics) && !empty($request->mpmapprovedpics)) ? true : false;
        $returnArray['mpmmainimage'] = (isset($request->mpmmainimage) && !empty($request->mpmmainimage)) ? true : false;
        $returnArray['mpmseo'] = (isset($request->mpmseo) && !empty($request->mpmseo)) ? true : false;
        $returnArray['mpmlive'] = (isset($request->mpmlive) && !empty($request->mpmlive)) ? true : false;
        $returnArray['bchecklist'] = (isset($request->abchecklist) && !empty($request->abchecklist)) ? true : false;
        $returnArray['bdelaconcreated'] = (isset($request->bdelaconcreated) && !empty($request->bdelaconcreated)) ? true : false;
        $returnArray['bdelaconid'] = (isset($request->bdelaconid) && !empty($request->bdelaconid)) ? $request->bdelaconid : '';
        $returnArray['bprofilecreated'] = (isset($request->bprofilecreated) && !empty($request->bprofilecreated)) ? true : false;
        $returnArray['bdelaconphone'] = (isset($request->bdelaconphone) && !empty($request->bdelaconphone)) ? $request->bdelaconphone : '';
        $returnArray['bdescription'] = (isset($request->bdescription) && !empty($request->bdescription)) ? true : false;
        $returnArray['bdapprovedc'] = (isset($request->bdapprovedc) && !empty($request->bdapprovedc)) ? true : false;
        $returnArray['b150200c'] = (isset($request->b150200c) && !empty($request->b150200c)) ? true : false;
        $returnArray['biapprovedc'] = (isset($request->biapprovedc) && !empty($request->biapprovedc)) ? true : false;
        $returnArray['baddtoloc'] = (isset($request->baddtoloc) && !empty($request->baddtoloc)) ? true : false;
        $returnArray['b500500c'] = (isset($request->b500500c) && !empty($request->b500500c)) ? true : false;
        $returnArray['bsmiapprovedc'] = (isset($request->bsmiapprovedc) && !empty($request->bsmiapprovedc)) ? true : false;
        $returnArray['blive'] = (isset($request->blive) && !empty($request->blive)) ? true : false;
        $returnArray['abchecklist'] = (isset($request->abchecklist) && !empty($request->abchecklist)) ? true : false;
        $returnArray['abdelaconcreated'] = (isset($request->abdelaconcreated) && !empty($request->abdelaconcreated)) ? true : false;
        $returnArray['abdelaconid'] = (isset($request->abdelaconid) && !empty($request->abdelaconid)) ? $request->abdelaconid : '';
        $returnArray['abprofilecreated'] = (isset($request->abprofilecreated) && !empty($request->abprofilecreated)) ? true : false;
        $returnArray['abdelaconphone'] = (isset($request->abdelaconphone) && !empty($request->abdelaconphone)) ? $request->abdelaconphone : '';
        $returnArray['abseo'] = (isset($request->abseo) && !empty($request->abseo)) ? true : false;
        $returnArray['ab500292created'] = (isset($request->ab500292created) && !empty($request->ab500292created)) ? true : false;
        $returnArray['abi500292ca'] = (isset($request->abi500292ca) && !empty($request->abi500292ca)) ? true : false;
        $returnArray['absmicreated'] = (isset($request->absmicreated) && !empty($request->absmicreated)) ? true : false;
        $returnArray['absmiabc'] = (isset($request->absmiabc) && !empty($request->absmiabc)) ? true : false;
        $returnArray['ablive'] = (isset($request->ablive) && !empty($request->ablive)) ? true : false;
        return $returnArray;
    }

    private function prepareSaveTasksObject($request) {
        $ctasks = new CustomerTasks();
        $ctasks->customerid = $request->customerID;
        $ctasks->mpmchecklist = (isset($request->mpmchecklist) && !empty($request->mpmchecklist)) ? true : false;
        $ctasks->mpmdetailsfilled = (isset($request->mpmdetailsfilled) && !empty($request->mpmdetailsfilled)) ? true : false;
        $ctasks->mpmconfirmplan = (isset($request->mpmconfirmplan) && !empty($request->mpmconfirmplan)) ? true : false;
        $ctasks->mpmbaselocation = (isset($request->mpmbaselocation) && !empty($request->mpmbaselocation)) ? $request->mpmbaselocation : '';
        $ctasks->mpmpaid = (isset($request->mpmpaid) && !empty($request->mpmpaid)) ? true : false;
        $ctasks->mpmapprovedpics = (isset($request->mpmapprovedpics) && !empty($request->mpmapprovedpics)) ? true : false;
        $ctasks->mpmmainimage = (isset($request->mpmmainimage) && !empty($request->mpmmainimage)) ? true : false;
        $ctasks->mpmseo = (isset($request->mpmseo) && !empty($request->mpmseo)) ? true : false;
        $ctasks->mpmlive = (isset($request->mpmlive) && !empty($request->mpmlive)) ? true : false;
        $ctasks->bchecklist = (isset($request->bchecklist) && !empty($request->bchecklist)) ? true : false;
        $ctasks->bdelaconcreated = (isset($request->bdelaconcreated) && !empty($request->bdelaconcreated)) ? true : false;
        $ctasks->bdelaconid = (isset($request->bdelaconid) && !empty($request->bdelaconid)) ? $request->bdelaconid : '';
        $ctasks->bprofilecreated = (isset($request->bprofilecreated) && !empty($request->bprofilecreated)) ? true : false;
        $ctasks->bdelaconphone = (isset($request->bdelaconphone) && !empty($request->bdelaconphone)) ? $request->bdelaconphone : '';
        $ctasks->bdescription = (isset($request->bdescription) && !empty($request->bdescription)) ? true : false;
        $ctasks->bdapprovedc = (isset($request->bdapprovedc) && !empty($request->bdapprovedc)) ? true : false;
        $ctasks->b150200c = (isset($request->b150200c) && !empty($request->b150200c)) ? true : false;
        $ctasks->biapprovedc = (isset($request->biapprovedc) && !empty($request->biapprovedc)) ? true : false;
        $ctasks->baddtoloc = (isset($request->baddtoloc) && !empty($request->baddtoloc)) ? true : false;
        $ctasks->b500500c = (isset($request->b500500c) && !empty($request->b500500c)) ? true : false;
        $ctasks->bsmiapprovedc = (isset($request->bsmiapprovedc) && !empty($request->bsmiapprovedc)) ? true : false;
        $ctasks->blive = (isset($request->blive) && !empty($request->blive)) ? true : false;
        $ctasks->abchecklist = (isset($request->abchecklist) && !empty($request->abchecklist)) ? true : false;
        $ctasks->abdelaconcreated = (isset($request->abdelaconcreated) && !empty($request->abdelaconcreated)) ? true : false;
        $ctasks->abdelaconid = (isset($request->abdelaconid) && !empty($request->abdelaconid)) ? $request->abdelaconid : '';
        $ctasks->abprofilecreated = (isset($request->abprofilecreated) && !empty($request->abprofilecreated)) ? true : false;
        $ctasks->abdelaconphone = (isset($request->abdelaconphone) && !empty($request->abdelaconphone)) ? $request->abdelaconphone : '';
        $ctasks->abseo = (isset($request->abseo) && !empty($request->abseo)) ? true : false;
        $ctasks->ab500292created = (isset($request->ab500292created) && !empty($request->ab500292created)) ? true : false;
        $ctasks->abi500292ca = (isset($request->abi500292ca) && !empty($request->abi500292ca)) ? true : false;
        $ctasks->absmicreated = (isset($request->absmicreated) && !empty($request->absmicreated)) ? true : false;
        $ctasks->absmiabc = (isset($request->absmiabc) && !empty($request->absmiabc)) ? true : false;
        $ctasks->ablive = (isset($request->ablive) && !empty($request->ablive)) ? true : false;
        return $ctasks;
    }

}
