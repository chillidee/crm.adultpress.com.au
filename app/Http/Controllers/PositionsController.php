<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\position;

class PositionsController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $positions = Position::all();
        return view('admin.positions', compact('positions'));
    }

    public function store(Request $request) {
        //var_dump($_REQUEST);
        $position = new position;
        $position->status = $request->status;
        $position->position = $request->position;
        $position->save();
        Session::flash('success', 'New position saved successfully');
        return back();
    }

    public function show($id) {
        $positions = Position::all();

        $thisposition = Position::findOrFail($id);
        return view('admin.positions', compact('positions'), compact('thisposition'));
    }

    public function edit($id) {

        $position = Position::findOrFail($id);
        $position->update($_REQUEST);
        return back();
    }

    public function destroy() {
        Position::destroy($_REQUEST['positionid']);
        Session::flash('error', 'position deleted successfully');

        return redirect('admin/positions');
    }

}
