<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maps;
use App\CustomerLocation;

class MapController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function index(Request $get) {
        $maps = new Maps();
        $customerLocation = new CustomerLocation();
        $customerLocation->populateCustomersLocations();
        $customers = array();
        $postcodes = $maps->getCustomerPostcodesSuburbs();
        return view('maps', compact('customers', 'postcodes'));
    }

    public function search(Request $request) {
        $maps = new Maps();
        $radius = (int) $request->radius;
        $customers = $maps->findBusinessWithinRadiusV2($request->postcode, $radius);
        $longsAndLats = $maps->getGoogleMapsData($customers);
        $center = $maps->getPostCodeLongLat($request->postcode);
        $postcodes = $maps->getCustomerPostcodesSuburbs();
        return view('maps', compact('customers', 'center', 'postcodes', 'longsAndLats', 'radius'));
    }

}
