<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DeletedCustomer;

class DeletedCustomerController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $deletedCustomers = new DeletedCustomer();
        $customers = $deletedCustomers->getDeletedCustomersTableData();
        return view('deletedCustomers', compact('customers'));
    }

    public function restore(Request $request) {
        echo DeletedCustomer::restoreDeletedCustomer($request->id);
    }

    public function delete(Request $request) {
        echo DeletedCustomer::deleteDeletedCustomer($request->id);
    }

}
