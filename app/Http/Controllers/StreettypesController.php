<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Response;

class StreettypesController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function autocomplete() {
        $term = $_GET['term'];
        $results = array();
        $queries = DB::table('streettypes')
                ->where('type', 'LIKE', $term . '%')
                ->orWhere('abb', 'LIKE', $term . '%')
                ->get();

        foreach ($queries as $query) {
            $results[] = ['id' => $query->id, 'value' => ucfirst(strtolower($query->abb))];
        }
        foreach ($queries as $query) {
            $results[] = ['id' => $query->id, 'value' => ucfirst(strtolower($query->type))];
        }
        return Response::json($results);
    }

}
