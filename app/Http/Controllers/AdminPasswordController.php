<?php

namespace App\Http\Controllers;

use App\AdminPassword;
use Illuminate\Http\Request;
use App\AdminPassword;

class AdmimPasswordController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function checkPassword(Request $request) {
        $adminPassword = new AdminPassword();
        echo $adminPassword->checkPassword($request->password);
    }

    public function updatePassword(Request $request) {
        $adminPassword = new AdminPassword();
        echo $adminPassword->updatePassword($request->newPassword, Auth::user()->id);
    }

    public function getLastChangedDetails() {
        $adminPassword = new AdminPassword();
        echo $adminPassword->getPasswordDetails();
    }

}
