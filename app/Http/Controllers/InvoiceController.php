<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Mail;
use App\Invoice;
use App\Customer;
use App\Status;
use App\Payment;
use App\Paymentmethod;
use App\User;
use App\Addabn;
use App\Website;
use App\Order;
use App\OrderOption;
use App\OrderPage;
use App\AppAutomation;
use App\Myob;
use App\InvoiceStatus;
use App\DeletedInvoices;

class InvoiceController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function index(Request $get) {
        $users = User::where('status', '!=', 'deleted')->orderby('name')->get();

        // $get->createdDateFrom= dateToMysql($get->createdDateFrom);
        //dd($get->createdDateFrom);
        //dd(date("Y-m-d",strtotime($get->createdDateFrom)));
        $query = "SELECT * FROM
(
SELECT invoices.*,users.name,customers.bname,sum(orders.amount)invoicetotal FROM
 invoices, users, customers, orders
WHERE ";
        if ($get->myob != '') {
            $query .= "myob LIKE '%" . $get->myob . "%' and ";
        }

        if ($get->startDateFrom != '') {
            $query .= "startdate >= '" . dateToMysql($get->startDateFrom) . "'  and ";
        }


        if ($get->startDateTo != '') {
            $query .= "startdate <= '" . dateToMysql($get->startDateTo) . "'  and ";
        }


        if ($get->dueDateFrom != '') {
            $query .= "duedate >= '" . dateToMysql($get->dueDateFrom) . "'  and ";
        }


        if ($get->dueDateTo != '') {
            $query .= "duedate <= '" . dateToMysql($get->dueDateTo) . "'  and ";
        }


        if ($get->expiryDateFrom != '') {
            $query .= "expirydate >= '" . dateToMysql($get->expiryDateFrom) . "'  and ";
        }


        if ($get->expiryDateTo != '') {
            $query .= "expirydate <= '" . dateToMysql($get->expiryDateTo) . "'  and ";
        }


        if ($get->createdDateFrom != '') {
            $query .= " invoices.created_at >= '" . dateToMysql($get->createdDateFrom) . "'  and ";
        }
        if ($get->createdDateTo != '') {
            $query .= " invoices.created_at <= '" . dateToMysql($get->createdDateTo) . "'  and ";
        }

        if ($get->selectedUsers != '') {
            $selectedusers = explode(',', $get->selectedUsers);
            $usersList = '';
            foreach ($selectedusers as $selecteduser) {
                $usersList .= '"' . $selecteduser . '",';
            }
            $usersList .= '""';
            $query .= "users.name in ( $usersList ) and ";
        }
        $query .= ' invoices.userid = users.id AND invoices.customerid = customers.id AND orders.invoiceid=invoices.id   GROUP BY invoices.id
    )as left_table
    LEFT JOIN (SELECT invoiceid,sum(payments.amount)totalpaid FROM payments GROUP BY invoiceid)as right_table
    on left_table.id = right_table.invoiceid';

        $invoices = DB::select($query);

        return view('searchinv', compact('users', 'invoices'));
    }

    public function store(Request $request) {
        $myob = new Myob();
        $appAuto = new AppAutomation();
        $request->orders = json_decode($request->orders);
        $request->startdate = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $request->startdate)));
        $request->expirydate = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $request->expirydate)));
        $request->duedate = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $request->duedate)));
        if ($request->orders == NULL) {
            Session::flash('error', 'Unable to create invoice! No order was set!');
            return back();
        }
        $invoice = new Invoice;
        //***** Get current invoicing adress
        $query = "SELECT id FROM addabns WHERE customerid=$request->customerid order by created_at DESC limit 1";

        $invoice->addabnid = DB::select($query)[0]->id;
        //***** Get current invoicing adress
        $invoice->customerid = $request->customerid;
        $customerID = (int) $invoice->customerid;
        //*************
        $invoice->userid = Auth::user()->id;
        $invoice->myob = $request->myob;
        $invoice->startdate = $request->startdate;
        $invoice->expirydate = $request->expirydate;
        $invoice->duedate = $request->duedate;
        $invoice->payment_method = $request->payment_method;
        $invoice->description = $request->description;
        $invoice->frequency = $request->frequency;
        $invoice->created_at = dateToMysql($request->created_at);
        $invoice->save();
        InvoiceStatus::updateInvoiceStatus($invoice->id, '1');

        //**** Save orders in orders table

        $invoiceTotal = 0;

        $orders = array();


        foreach ($request->orders as $orderinput) {
            $order = new Order;
            $order->invoiceid = $invoice->id;
            $thisOrder = explode(',', $orderinput);

            if (!isset($thisOrder[2])) {
                continue;
            }

            $order->sitename = $thisOrder[0];
            $order->orderoption = $thisOrder[1];
            $order->amount = $thisOrder[2];
            $order->page = $thisOrder[3];
            $order->position = $thisOrder[4];
            $order->myob_account = (isset($thisOrder[9]) && !empty($thisOrder[9])) ? $thisOrder[9] : '';
            $order->startdate = ($thisOrder[5] == '0000-00-00' || $thisOrder[5] == '' || $thisOrder[5] == null) ? date("Y-m-d", strtotime($invoice->startdate)) : dateToMysql($thisOrder[5]);
            $order->frequency = ($thisOrder[6] == '') ? $invoice->frequency : $thisOrder[6];
            $order->expirydate = ($thisOrder[7] == '0000-00-00' || $thisOrder[7] == '' || $thisOrder[7] == null) ? date("Y-m-d", strtotime($invoice->expirydate)) : dateToMysql($thisOrder[7]);
            $order->duedate = ($thisOrder[8] == '0000-00-00' || $thisOrder[8] == '' || $thisOrder[8] == null) ? date("Y-m-d", strtotime($invoice->duedate)) : dateToMysql($thisOrder[8]);
            $order->created_at = $invoice->created_at;

            $order->save();

            array_push($orders, array(
                'Description' => Myob::createInvoiceDescription($invoice, $order),
                'Total' => $order->amount,
                'Account' => Myob::getAccountUID($order->myob_account, $request->customerid)
            ));

            $invoiceTotal = $invoiceTotal + $order->amount;
        }

        $myobId = DB::select("SELECT myobIdNumber FROM `addabns` WHERE `customerid` = $invoice->customerid AND `latest` = 1")[0]->myobIdNumber;

        if (empty($myobId)):
            $myobId = $myob->getCustomerUIDFromApi($customerID);
            if (empty($myobId)):
                if ($myob->createNewCustomer($invoice->customerid)):
                    $myobId = $myob->getCustomerUIDFromApi($customerID);
                    if (isset($myobId) && !empty($myobId)):
                        $db_id = DB::select("SELECT id FROM `addabns` WHERE `customerid` = $customerID AND `latest` = 1")[0]->id;
                        DB::update("UPDATE `addabns` SET `myobIdNumber` = '$myobId' WHERE `addabns`.`id` = $db_id");
                    endif;
                endif;
            else:
                $db_id = DB::select("SELECT id FROM `addabns` WHERE `customerid` = $customerID AND `latest` = 1")[0]->id;
                DB::update("UPDATE `addabns` SET `myobIdNumber` = '$myobId' WHERE `addabns`.`id` = $db_id");
            endif;
        endif;

        if (isset($myobId) && !empty($myobId)):
            $invoiceNumber = str_pad($invoice->id, 8, "0", STR_PAD_LEFT);
            $data = array(
                'invoiceNumber' => $invoiceNumber,
                'myobCustomerID' => $myobId,
                'invoiceTotal' => $invoiceTotal,
                'Orders' => $orders
            );

            $appAuto->sendEmail('emails.test', array('json' => json_encode('test')), 'boni@chillidee.com.au', 'Errors Json');
            $myobNumber = $myob->createNewInvoice($data);
            if (isset($myobNumber) && !empty($myobNumber)):
                DB::update("UPDATE `invoices` SET `myob` = '$myobNumber' WHERE `invoices`.`id` = $invoice->id");
            endif;

            Invoice::sendNewInvoiceEmail($invoice->id);
            ($myobNumber) ? Session::flash('success', 'MYOB Invoice Created Successfully') : Session::flash('error', 'Error Creating MYOB Invoice');

        endif;



        $appAuto->updateStatus($customerID, '10014', $order->startdate);
        Session::flash('success', 'Invoice Created Successfully');
        Session::set('customerTab', 4);
        return back();
    }

    public function show($id) {
        if (is_numeric($id) == false) {
            return back();
        }
        $query = "select types.type, invoices.*,
addabns.unit,addabns.number,addabns.street,addabns.sttype,addabns.suburb,addabns.state,addabns.postcode,addabns.invname,addabns.abn,
addabns.invemail,addabns.invpoboxno,addabns.invpoboxsuburb,addabns.invpoboxstate,addabns.invpoboxpostcode,addabns.emailmethod,addabns.addressmethod,addabns.poboxmethod,
(users.name)username,
customers.bname,customers.bphone,customers.bemail,customers.btype,customers.mpm_user_id,customers.mpm_type_id,customers.brothel_id,
GROUP_CONCAT(orders.sitename)sitenames,GROUP_CONCAT(orders.page)pages,GROUP_CONCAT(orders.orderoption)orderoptions,GROUP_CONCAT(orders.position)positions,GROUP_CONCAT(orders.id)orderid,
GROUP_CONCAT(orders.amount)amounts,GROUP_CONCAT(orders.startdate)startdates,GROUP_CONCAT(orders.frequency)frequencies,GROUP_CONCAT(orders.expirydate)expirydates,GROUP_CONCAT(orders.duedate)duedates,GROUP_CONCAT(orders.active)active,sum(orders.amount)subtotal
FROM invoices,customers,addabns,users,orders,types
WHERE invoices.customerid=customers.id AND invoices.userid=users.id AND invoices.addabnid=addabns.id AND orders.invoiceid = invoices.id AND invoices.id=$id AND types.id = customers.btype";
        $invoice = DB::select($query)[0];
        if (!isset($invoice->id)) {
            return back();
        }
        $query = "SELECT users.name,payments.* FROM users,payments WHERE users.id=payments.userid AND payments.invoiceid = $id";
        $payments = DB::select($query);
        $query = "SELECT sum(payments.amount)totalpaid FROM payments WHERE payments.invoiceid = $id";
        $paidamount = DB::select($query)[0]->totalpaid;
        $paymentmethod = Paymentmethod::all();
        $statuses = Status::orderby('orderid')->get();
        $customer = Customer::find($invoice->customerid);
        return view('invoice', compact('invoice', 'payments', 'paidamount', 'paymentmethod', 'customer', 'statuses'));
    }

    public function destroy(Request $request) {
        $invoice = Invoice::find($request->invoiceid);
        $myobNumber = $invoice->myob;
        if (isset($myobNumber) && !empty($myobNumber)):
            $myob = new Myob();
            $myob->deleteInvoice($myobNumber);
        else:
            $myobNumber = '';
        endif;
        $this->recordDeletedInvoice(array('invoice_id' => $request->invoiceid, 'myobnumber' => $myobNumber, 'user_id' => Auth::id(), 'invoice' => $invoice, 'customer' => Customer::find($invoice->customerid)));
        Invoice::destroy($request->invoiceid);
        DB::table('payments')->where('invoiceid', '=', $request->invoiceid)->delete();
        DB::table('orders')->where('invoiceid', '=', $request->invoiceid)->delete();
        Session::flash('success', 'Invoice was deleted successfully');
        return redirect("customer/$request->customerid");
    }

    public function edit($id) {
        $query = "select
types.type,
invoices.*,
addabns.unit,addabns.number,addabns.street,addabns.sttype,addabns.suburb,addabns.state,addabns.postcode,addabns.invname,addabns.abn,
(users.name)username,
customers.bname,customers.bphone,customers.bemail,customers.btype,customers.mpm_user_id,customers.mpm_type_id,customers.brothel_id,
GROUP_CONCAT(orders.sitename)sitenames,GROUP_CONCAT(orders.page)pages,GROUP_CONCAT(orders.orderoption)orderoption,GROUP_CONCAT(orders.position)positions,GROUP_CONCAT(orders.amount)amounts
FROM invoices,customers,addabns,users,orders,types
WHERE invoices.customerid=customers.id AND invoices.userid=users.id AND invoices.addabnid=addabns.id AND orders.invoiceid = invoices.id AND invoices.id=$id AND types.id = customers.btype";
        $invoice = DB::select($query)[0];
        if (!isset($invoice->id)) {
            return back();
        }
        $query = "SELECT users.name,payments.* FROM users,payments WHERE users.id=payments.userid AND payments.invoiceid = $id";
        $payments = DB::select($query);
        $orderpages = OrderPage::orderby('page')->get();
        $websites = Website::where('status', '=', 'active')->get();
        $orderoptions = OrderOption::orderby('orderoption')->get();
        $query = "SELECT orders.*,(websites.id)siteid FROM orders,websites WHERE websites.website = orders.sitename and orders.invoiceid =$id";
        $orders = DB::select($query);
        $paymentmethod = Paymentmethod::all();
        return view('invoice-form', compact('invoice', 'payments', 'websites', 'orders', 'paymentmethod', 'orderoptions', 'orderpages'));
    }

    public function update(Request $request) {
        $request->orders = json_decode($request->orders);
        $request->startdate = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $request->startdate)));
        $request->expirydate = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $request->expirydate)));
        $request->duedate = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $request->duedate)));
        if (is_string($request->orders)) {
            $request->orders = json_decode($request->orders);
        }

        //dd($request->orders);
        if ($request->orders == NULL) {

            Invoice::destroy($request->invoiceid);
            Session::flash('error', 'No order was set. Invoice deleted!');
            return redirect(url('/'));
        }
        $invoice = Invoice::find($request->invoiceid);
        //***** Get current invoicing adress
        $query = "SELECT id FROM addabns WHERE customerid=$request->customerid order by id DESC limit 1";
        $invoice->addabnid = DB::select($query)[0]->id;
        //***** Get current invoicing adress
        $invoice->customerid = $request->customerid;
        //*************
        $invoice->userid = Auth::user()->id;
        $invoice->myob = $request->myob;
        $invoice->startdate = $request->startdate;
        $invoice->expirydate = $request->expirydate;
        $invoice->duedate = $request->duedate;
        $invoice->payment_method = $request->payment_method;
        $invoice->description = $request->description;
        $invoice->frequency = $request->frequency;
        $invoice->created_at = dateToMysql($request->created_at);
        $invoice->update();

        //**** Save orders in orders table
        Order::where('invoiceid', '=', $invoice->id)->delete();
        //dd($request->orders);
        $count = 0;
        foreach ($request->orders as $orderinput) {
            $order = new Order;
            $order->invoiceid = $invoice->id;
            $thisOrder = explode(',', $orderinput);
            if (!isset($thisOrder[2])) {
                continue;
            }
            $count++;
            $order->sitename = $thisOrder[0];
            $order->orderoption = $thisOrder[1];
            $order->amount = $thisOrder[2];
            $order->page = $thisOrder[3];
            $order->position = $thisOrder[4];

            $order->startdate = ($thisOrder[5] == '0000-00-00' || $thisOrder[5] == '' || $thisOrder[5] == null) ? date("Y-m-d", strtotime($invoice->startdate)) : dateToMysql($thisOrder[5]);
            $order->frequency = ($thisOrder[6] == '') ? $invoice->frequency : $thisOrder[6];
            $order->expirydate = ($thisOrder[7] == '0000-00-00' || $thisOrder[7] == '' || $thisOrder[7] == null) ? date("Y-m-d", strtotime($invoice->expirydate)) : dateToMysql($thisOrder[7]);
            $order->duedate = ($thisOrder[8] == '0000-00-00' || $thisOrder[8] == '' || $thisOrder[8] == null) ? date("Y-m-d", strtotime($invoice->duedate)) : dateToMysql($thisOrder[8]);
            $order->created_at = $invoice->created_at;
            $order->myob_account = (isset($thisOrder[9]) && !empty($thisOrder[9])) ? $thisOrder[9] : '';
            //dd($order);
            $order->save();
        }

//        dd($count);
        if ($count == 0) {
            DB::delete("delete from orders where invoiceid = $request->invoiceid");
            Invoice::destroy($request->invoiceid);
            Session::flash('error', 'No order was set. Invoice deleted!');
            return redirect(url('/'));
        }
        // to clean up old addresses which are not in use!
        $query = "DELETE FROM addabns WHERE id not in (SELECT invoices.addabnid FROM invoices) AND latest = 0";
        DB::delete($query);

        $myob = new Myob();
        $myob->updateMYOBInvoice($invoice);

        (Invoice::isBalanceRemaining($request->invoiceid)) ? InvoiceStatus::updateInvoiceStatus($request->invoiceid, '2') : InvoiceStatus::updateInvoiceStatus($request->invoiceid, '1');
        Session::flash('status', 'Invoice was updated successfully');
        return redirect(url('/') . "/invoice/$invoice->id");
    }

    public function toggleOrderStatus(Request $request) {
        $order = Order::find($request->orderid);
        if ($order->active == 1) {
            $order->active = 0;
        } else {
            $order->active = 1;
        }
        $order->update();
        return back();
    }

    public function creditInvoice(Request $request) {
        $invoice_id = $request->invoiceid;
        if (!isset($invoice_id) || empty($invoice_id)):
            Session::flash('error', 'Invoice id not set');
            return back();
        endif;
        $oldInvoice = Invoice::find($invoice_id);
        $oldOrders = DB::select("SELECT * FROM `orders` WHERE `invoiceid` = $invoice_id");
        $this->updateOldOrdersInvoiceForCreditNote($oldInvoice, $oldOrders);
        $this->updateCustomerStatusForCreditNote($oldInvoice->customerid);
        $newInvoice = $this->duplicateInvoiceForCreditNote($oldInvoice);
        $orders = $this->duplicateOrdersForCreditNote($newInvoice, $oldOrders);
        $appauto = new AppAutomation();
        $appauto->sendEmail('emails/invoiceCanceled', array('totalAmount' => $orders['invoiceTotal'], 'oldInvoice' => $oldInvoice, 'newInvoice' => $newInvoice, 'orders' => $oldOrders, 'customerName' => DB::select("SELECT bname FROM `customers` WHERE `id` = $oldInvoice->customerid")[0]->bname), 'info@adultpress.com.au', 'CRM: Canceled Invoice');

        $myobNumber = DB::select("SELECT myobIdNumber FROM `addabns` WHERE `customerid` = $oldInvoice->customerid AND `latest` = 1")[0]->myobIdNumber;

        if (isset($myobNumber) && !empty($myobNumber)):

            $myob = new Myob();

            $Orders = array();

            foreach ($orders as $index => $_order):
                if ($index !== 'invoiceTotal'):
                    array_push($Orders, array(
                        'Description' => $_order['Description'],
                        'Total' => $_order['Total'],
                        'Account' => Myob::getAccountUID($_order['Account'], $oldInvoice->customerid)
                    ));
                endif;
            endforeach;

            $data = array(
                'invoiceNumber' => $newInvoice->myob,
                'myobCustomerID' => $myobNumber,
                'invoiceTotal' => $orders['invoiceTotal'],
                'Orders' => $Orders
            );

            $result = $myob->createNewInvoice($data);

        endif;

        Session::flash('success', 'Invoice was credited successfully');
        return redirect(url('/') . "/customer/$oldInvoice->customerid");
    }

    private function updateCustomerStatusForCreditNote($customer_id) {
        $status_id = DB::select("SELECT id  FROM `cstatuses` WHERE `customerid` = $customer_id AND `latest` = 1")[0]->id;
        DB::update("UPDATE `cstatuses` SET `statusId` = '9100', `dueDatetime` = '" . date("Y-m-d H:i:s", mktime(8, 0, 0, date("m") + 2, date("d"), date("Y"))) . "' WHERE `cstatuses`.`id` = $status_id");
    }

    private function updateOldOrdersInvoiceForCreditNote($oldInvoice, $oldOrders) {
        $oldInvoice->payment_method = 'Invoice Cancelled';
        $oldInvoice->update();
        foreach ($oldOrders as $order):
            $order = Order::find($order->id);
            $order->active = 0;
            $order->update();
        endforeach;
    }

    private function duplicateInvoiceForCreditNote($oldInvoiceObject) {
        $invoice = new Invoice;
        $invoice->addabnid = $oldInvoiceObject->addabnid;
        $invoice->customerid = $oldInvoiceObject->customerid;
        $invoice->userid = $oldInvoiceObject->userid;
        $invoice->myob = $this->createCreditMYOBNumber($oldInvoiceObject->myob);
        $invoice->startdate = $oldInvoiceObject->startdate;
        $invoice->expirydate = $oldInvoiceObject->expirydate;
        $invoice->duedate = $oldInvoiceObject->duedate;
        $invoice->payment_method = $oldInvoiceObject->payment_method;
        $invoice->description = $oldInvoiceObject->description;
        $invoice->frequency = $oldInvoiceObject->frequency;
        //$invoice->created_at = $oldInvoiceObject->created_at;
        $invoice->save();
        InvoiceStatus::updateInvoiceStatus($invoice->id, '3');
        InvoiceStatus::updateInvoiceStatus($oldInvoiceObject->id, '3');
        return $invoice;
    }

    private function createCreditMYOBNumber($myobNumber = '') {
        $returnMyob = $myobNumber;
        if (strpos(substr($myobNumber, 0, 2), '00') === false):
            $returnMyob = 'CN' . $myobNumber;
        else:
            if (strpos(substr($myobNumber, 0, 1), '0') === false):
                $returnMyob = substr_replace($myobNumber, 'CN', 0, 1);
            else:
                $returnMyob = substr_replace($myobNumber, 'CN', 0, 2);
            endif;
        endif;
        return $returnMyob;
    }

    private function duplicateOrdersForCreditNote($newInvoice, $oldOrders) {
        $appauto = new AppAutomation();

        $returnArray = array();
        $returnArray['invoiceTotal'] = 0.00;
        foreach ($oldOrders as $_order) {
            $order = new Order;
            $order->invoiceid = $newInvoice->id;
            $order->sitename = $_order->sitename;
            $order->orderoption = $_order->orderoption;
            $order->amount = -abs((float) $_order->amount);
            $order->page = $_order->page;
            $order->position = $_order->position;
            $order->startdate = $_order->startdate;
            $order->frequency = $_order->frequency;
            $order->expirydate = $_order->expirydate;
            $order->myob_account = $_order->myob_account;
            $order->duedate = $_order->duedate;
            $order->created_at = $newInvoice->created_at;
            $order->active = 0;
            $order->save();

            array_push($returnArray, array(
                'Total' => -abs((float) $_order->amount),
                'Description' => 'Credit Note for Non Payment: ' . Myob::createInvoiceDescription($newInvoice, $order),
                'Account' => $_order->myob_account
            ));

            $returnArray['invoiceTotal'] = $returnArray['invoiceTotal'] + -abs((float) $_order->amount);
        }

        return $returnArray;
    }

    public function checkForMyob(Request $request) {
        echo Invoice::checkIfMyobInvoice($request->invoice_id);
    }

    private function recordDeletedInvoice($data) {
        $deletedInvoices = new DeletedInvoices();
        $deletedInvoices->invoice_id = $data['invoice_id'];
        $deletedInvoices->myob_number = $data['myobnumber'];
        $deletedInvoices->user_id = $data['user_id'];
        $deletedInvoices->customer_id = $data['invoice']->customerid;
        $deletedInvoices->save();
        $emailData = array(
            'username' => Auth::user()->name,
            'invoice_id' => $data['invoice_id'],
            'myobnumber' => $data['myobnumber'],
            'deleted_date' => date("Y-m-d H:i:s"),
            'customer' => $data['customer'],
            'invoice' => $data['invoice']
        );
        AppAutomation::sendMail('emails/deletedInvoiceEmail', array('emailData' => $emailData), 'info@adultpress.com.au', "CRM Deleted Invoice");
    }

}
