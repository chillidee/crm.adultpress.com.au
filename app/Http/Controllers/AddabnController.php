<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Addabn;
use DB;

class AddabnController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function store(Request $request) {
        $addabn = new Addabn;

        $addabn->invname = $request->invname;
        $addabn->abn = $request->invabn;
        $addabn->unit = $request->unit;
        $addabn->number = $request->number;
        $addabn->street = $request->street;
        $addabn->sttype = $request->searchSttype;
        $addabn->suburb = $request->suburb;
        $addabn->state = $request->state;
        $addabn->postcode = $request->postcode;
        $addabn->customerid = $request->customerid;

        $addabn->emailmethod = ($request->emailopt == '1') ? 1 : 0;
        $addabn->addressmethod = ($request->addressopt == '1') ? 1 : 0;
        $addabn->poboxmethod = ($request->poboxopt == '1') ? 1 : 0;

        $addabn->invemail = $request->invemail;
        $addabn->invpoboxno = $request->invpoboxno;
        $addabn->invpoboxsuburb = $request->invpoboxsuburb;
        $addabn->invpoboxpostcode = $request->invpoboxpostcode;
        $addabn->invpoboxstate = $request->invpoboxstate;
        $addabn->myobIdNumber = $request->invmyobisInput;
        $addabn->latest = 1;

        //** Check old addresses, if no invoice for this address -> delete address
        $oldaddresses = DB::select("SELECT * FROM addabns WHERE customerid = $addabn->customerid");

        foreach ($oldaddresses as $oldaddabn) {
            $invoice = DB::select("SELECT * FROM invoices WHERE addabnid = $oldaddabn->id");
            if ($invoice == []) {
                Addabn::destroy($oldaddabn->id);
            }
        }
        DB::update("UPDATE addabns SET latest = 0 WHERE customerid = $addabn->customerid");

        $addabn->save();
        Session::flash('success', 'Invoicing details updated successfully');

        return back();
    }

}
