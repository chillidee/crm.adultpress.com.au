<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\AppAutomation;
use App\Payment;
use App\Customer;
use App\Invoice;
use App\Order;
use App\User;
use Mail;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use DB;
use App\InvoiceStatus;

class PaymentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('activeuser');
    }

    public function store(Request $request)
    {
        $payment = new Payment;
        $payment->invoiceid = $request->invoiceid;
        $payment->amount = $request->amount;
        $payment->method = $request->method;
        $payment->created_at = dateToMysql($request->created_at);
        $payment->userid = Auth::user()->id;
        $totalordered = DB::select("SELECT sum(amount)ordered FROM orders WHERE invoiceid=$payment->invoiceid");
        $totalpaid = DB::select("SELECT sum(amount)paid FROM payments WHERE invoiceid=$payment->invoiceid");


        if (isset($totalordered[0]->ordered) && $totalordered[0]->ordered != NULL) {
            $totalordered = $totalordered[0]->ordered * 1.1;
        } else {
            $totalordered = 0;
        }

        if (isset($totalpaid[0]->paid) && $totalpaid[0]->paid != NULL) {
            $totalpaid = $totalpaid[0]->paid;
        } else {
            $totalpaid = 0;
        }
        $payable = $totalordered - $totalpaid;

        $a = (float)$payment->amount;
        $b = (float)$payable;
        $paidInFull = (abs(($a - $b) / $b) < 0.00001) ? true : false;
        ($paidInFull ? InvoiceStatus::updateInvoiceStatus($request->invoiceid, '2') : InvoiceStatus::updateInvoiceStatus($request->invoiceid, '1'));
        $payment->save();
        $invoice = Invoice::find($payment->invoiceid);
        $orders = Order::where('invoiceid', '=', $invoice->id)->get();

        if ($paidInFull) {
            $automation = new AppAutomation();
            $automation->updateStatus($invoice->customerid, '600', $orders[0]->duedate);
            return back();
        }
    }

    public function update(Request $request)
    {
        //var_dump($_REQUEST);
        $payment = Payment::find($request->paymentid);
        $payment->amount = $request->amount;
        $payment->method = $request->method;
        $payment->created_at = dateToMysql($request->created_at);
        //$payment->userid = Auth::user()->id;
        //var_dump($payment);
        $payment->save();


        //******* SEND PAYMENT REMINDER

        $payment = Payment::find($payment->id);
        $invoice = Invoice::find($payment->invoiceid);
        $customer = Customer::find($invoice->customerid);
        $orders = Order::where('invoiceid', '=', $invoice->id)->get();

        $data = array('payment' => $payment, 'invoice' => $invoice, 'customer' => $customer, 'orders' => $orders);
        //return view('paymentreminder0',compact('payment','invoice','customer'));
        /*
          Mail::send("paymentreminder", $data, function($message) {
          $message->from('crm.adultpress@gmail.com')
          ->to('info@adultpress.com.au')
          ->subject('Payment Update Notification!');
          });
         */

//        Session::flash('status', 'Payment notification sent to info@adultpress.com.au!');

        //********* END SENDING PAYMENT REMINDER
        Session::flash('status', 'Payment updated successfully');
        return redirect(url('/') . "/invoice/$request->invoiceid");
    }

    public function destroy(Request $request)
    {
        $invoiceId = DB::select("SELECT invoiceid FROM payments WHERE id = $request->paymentid");
        Payment::destroy($request->paymentid);
        if (isset($invoiceId[0]) && !empty($invoiceId[0]->invoiceid)):
            (Invoice::isBalanceRemaining($invoiceId[0]->invoiceid)) ? InvoiceStatus::updateInvoiceStatus($invoiceId[0]->invoiceid, '2') : InvoiceStatus::updateInvoiceStatus($invoiceId[0]->invoiceid, '1');
        endif;
        Session::flash('error', 'Payment was deleted successfully');
        return back();
    }

    public function paymentreminder($id)
    {
        $payment = Payment::find($id);
        $invoice = Invoice::find($payment->invoiceid);
        $customer = Customer::find($invoice->customerid);

        return view("paymentreminder", compact('payment', 'invoice', 'customer'));
    }

    public function paymentsreport()
    {
        $from = filter_input(INPUT_GET, 'reportfrom');
        $to = filter_input(INPUT_GET, 'reportto');
        $from = date("Y-m-d", strtotime(str_replace('/', '-', $from)));
        $to = date("Y-m-d", strtotime(str_replace('/', '-', $to)));
        $query = "SELECT invoices.created_at as invDate,payments.amount,payments.invoiceid,payments.method,payments.created_at, customers.bname,
invoices.myob,users.name,invoices.customerid
FROM payments,invoices ,users,customers
where DATE_FORMAT(payments.created_at,'%Y-%m-%d') <= '" . $to . "' and DATE_FORMAT(payments.created_at,'%Y-%m-%d') >= '" . $from . "'  and payments.invoiceid = invoices.id AND payments.userid = users.id AND customers.id = invoices.customerid ORDER BY created_at";
        $reports = DB::select($query);
        //dd($query);
        //return view('reports/paymentsreport', compact('reports','from','to'));
        $data = array('reports' => $reports, 'from' => $from, 'to' => $to);
        $pdf = PDF::loadView('reports/paymentsreport', $data)->setPaper('a4', 'landscape');
        return $pdf->download('crm_payments_report_FROM(' . $from . ')_TO(' . $to . ').pdf');
    }

}
