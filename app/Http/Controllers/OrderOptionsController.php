<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\OrderOption;
use App\Http\Requests;

class OrderOptionsController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $orderoptions = OrderOption::all();
        return view('admin.orderoptions', compact('orderoptions'));
    }

    public function store(Request $request) {
        try {
            $orderoption = new OrderOption;
            $orderoption->orderoption = $request->orderoption;
            // dd($orderoption);
            $orderoption->save();
            Session::flash('success', 'New Invoice Description created successfully');
        } catch (\Exception $e) {
            Session::flash('error', 'Unable to create this  Invoice Description!');
        }

        return back();
    }

    public function show($id) {
        $orderoptions = OrderOption::all();
        $thisorderoption = OrderOption::find($id);
        return view('admin.orderoptions', compact('orderoptions'), compact('thisorderoption'));
    }

    public function update($id) {
        $orderoptions = OrderOption::find($id);
        $orderoptions->update($_REQUEST);
        Session::flash('status', 'Invoice Description updated successfully');
        return back();
    }

    public function destroy($id) {
        OrderOption::destroy($id);
        Session::flash('error', 'Invoice Description deleted successfully');
        return redirect('admin/orderoptions');
    }

}
