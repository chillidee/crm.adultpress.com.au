<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Response;

class PostcodesController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    //SearchController.php
    public function autocomplete() {
        $term = $_GET['term'];
        $results = array();
        $queries = DB::table('postcodes')
                        ->where('postcode', 'LIKE', '%' . $term . '%')
                        ->orWhere('suburb', 'LIKE', '%' . $term . '%')
                        /*->take(20)*/
                        ->get();
        foreach ($queries as $query) {
            $results[] = [ 'id' => $query->id, 'value' => $query->postcode . ', ' . $query->suburb. ', ' . $query->state];
        }
        return Response::json($results);
    }

}
