<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Status;

class StatusesController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $statuses = Status::all();
        return view('admin.statuses', compact('statuses'));
    }

    public function store(Request $request) {
        //var_dump($_REQUEST);
        $status = new Status;
        $status->name = $request->name;
        $status->status = $request->status;
        $status->orderid = $request->orderid;
        $status->expdays = (isset($request->expdays) && !empty($request->expdays)) ? $request->expdays : null;
        $status->save();
        Session::flash('success', 'New status created successfully');
        return back();
    }

    public function show($id) {
        $statuses = Status::all();

        $thisstatus = Status::findOrFail($id);
        return view('admin.statuses', compact('statuses'), compact('thisstatus'));
    }

    public function edit($id) {
        $status = Status::find($id);
        $status->status = $_REQUEST['status'];
        $status->orderid = $_REQUEST['orderid'];
        $status->name = $_REQUEST['name'];
        $status->expdays = (isset($_REQUEST['expdays']) && !empty($_REQUEST['expdays'])) ? $_REQUEST['expdays'] : null;
        $status->update();
        Session::flash('status', 'Status updated successfully');
        return back();
    }

    public function destroy() {
        Status::destroy($_REQUEST['statusid']);
        Session::flash('error', 'Status deleted successfully');
        return redirect('admin/statuses');
    }

}
