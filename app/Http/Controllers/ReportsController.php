<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use PDF;
use DB;
use Mail;
use App\Invoice;
use Lava;

class ReportsController extends Controller {

    public $attachement;

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        return view('reports.reports');
    }

    public function owingcustomers() {
        $invoices = Invoice::orderby('startdate')->get();

        //return view('reports/owingcustomers',compact('invoices'));

        $data = array('invoices' => $invoices);
        $pdf = PDF::loadView('reports/owingcustomers', $data)->setPaper('a4', 'landscape');
        return $pdf->download('crm_owing_customers_report_' . date("d-m-Y-H-i") . '.pdf');
    }

    public function overdueactivepaid() {
        /*
        $query = "SELECT types.type, customers.id,customers.bname,cstatuses.dueDatetime,statuses.name FROM types,customers,cstatuses,statuses WHERE customers.btype = types.id AND customers.id = cstatuses.customerid AND cstatuses.latest=1 AND cstatuses.statusId = statuses.id and (statuses.name = 'Active Paid Customer' OR statuses.name='To Be Invoiced')
AND DATE_FORMAT(cstatuses.dueDatetime,'%Y-%m-%d')  <= '" . date("Y-m-d") . "'";
        */
        //** To ingnore the status due date
        
        $query = "SELECT types.type, customers.id,customers.bname,cstatuses.dueDatetime,statuses.name FROM types,customers,cstatuses,statuses WHERE customers.btype = types.id AND customers.id = cstatuses.customerid AND cstatuses.latest=1 AND cstatuses.statusId = statuses.id and (statuses.name = 'Active Paid Customer' OR statuses.name='To Be Invoiced')";
        
        $customers = DB::select($query);
        //return view('reports/overdueactivepaid', compact('customers'));
        //**** SEND EMAIL NOTIFICATION TO ACCOUNTS  ****
        $data = array('customers' => $customers);
        $pdf = PDF::loadView('reports/overdueactivepaid', $data)->setPaper('a4', 'landscape');
        return $pdf->download('crm_active_paid_customers_due_' . date("d-m-Y-H-i") . '.pdf');
        //dd();
        /*
          Mail::send("reports/overdueactivepaid", $data, function($message) {
          $message->from('crm.adultpress@gmail.com')
          ->to('info@adultpress.com.au')
          ->cc('amir@chillidee.com.au')
          ->subject('Overdue Active Paid Customers');
          });
         */
        //************************************************ 
        //return back();
    }

    public function montlyreport(Request $request) {
        $query = "SELECT count(orders.id)as counts, sum(amount)as total FROM orders,invoices WHERE orders.invoiceid=invoices.id and DATE_FORMAT(invoices.created_at,'%b') = '" . $request->montlyReporMonth . "' AND  DATE_FORMAT(invoices.created_at,'%Y') = '" . $request->montlyReporYear . "'";
        $orders = DB::select($query)[0];
        $query = "SELECT count(id)as counts, sum(amount)as total FROM payments WHERE DATE_FORMAT(created_at,'%b') = '" . $request->montlyReporMonth . "' AND  DATE_FORMAT(created_at,'%Y') = '" . $request->montlyReporYear . "'";
        $payments = DB::select($query)[0];
        $month = $request->montlyReporMonth . ' ' . $request->montlyReporYear;
        $data = array('orders' => $orders, 'payments' => $payments, 'month' => $month);
        $pdf = PDF::loadView('reports/monthlyreports', $data);
        return $pdf->download('crm_monthly_report_' . $request->montlyReporMonth . '_' . $request->montlyReporYear . '.pdf');
        //return view('reports.monthlyreports',compact('orders','payments','month'));
    }

    public function graphreport() {
        $graphreport = $_GET['graph'];

        if ($graphreport == 'monthly') {
            
            /*
              $query = "SELECT A.created_at ,A.month, A.paid as paid , B.invoiced as invoiced
              FROM
              (SELECT DATE_FORMAT(invoices.created_at,'%Y-%m')as created_at, DATE_FORMAT(invoices.created_at,'%b %Y')as month, sum(amount)as paid FROM payments,invoices WHERE payments.invoiceid=invoices.id GROUP BY DATE_FORMAT(invoices.created_at,'%b %Y'))as A
              INNER JOIN
              (SELECT DATE_FORMAT(invoices.created_at,'%b %Y')as month, sum(replace(format(amount*1.1,2),',',''))as invoiced FROM orders,invoices WHERE orders.invoiceid=invoices.id GROUP BY DATE_FORMAT(invoices.created_at,'%b %Y'))as B
              WHERE A.month = B.month
              order by created_at ASC";
             */
            
            $query = "SELECT A.created_at ,A.month, A.paid as paid , B.invoiced as invoiced
              FROM
              (SELECT DATE_FORMAT(created_at,'%Y-%m')as created_at, DATE_FORMAT(created_at,'%b %Y')as month, sum(amount)as paid FROM payments WHERE payments.created_at>='2015-09-01' GROUP BY DATE_FORMAT(created_at,'%b %Y'))as A
              INNER JOIN
              (SELECT DATE_FORMAT(invoices.created_at,'%b %Y')as month, sum(replace(format(amount*1.1,2),',',''))as invoiced FROM orders,invoices WHERE orders.created_at>='2015-09-01' AND orders.invoiceid=invoices.id GROUP BY DATE_FORMAT(invoices.created_at,'%b %Y'))as B
              WHERE A.month = B.month
              order by created_at ASC";

            $report = DB::select($query);
            $finances = Lava::DataTable();

            $finances->addStringColumn('Month')
                    ->addNumberColumn('Paid')
                    ->addNumberColumn('Invoiced');
            foreach ($report as $row) {
                $finances->addRow([$row->month, str_replace(',', '', number_format($row->paid, 2)), str_replace(',', '', number_format($row->invoiced, 2))]);
            }
            Lava::ColumnChart('Finances', $finances, [
                'title' => 'CRM Performance',
                'titleTextStyle' => ['color' => '#eb6b2c', 'fontSize' => 14],
                'colors' => ['#006064', '#B71C1C'],
                'height' => 500,
                'legend' => ['position' => 'top'],
                'vAxis' => [
                    'minValue' => 0
                ],
                    ]
            );

            $graphtitle = 'Monthly Report';

            $graph = Lava::render('ColumnChart', 'Finances', 'graphReport');
/*
            $data = array('graph' => $graph, 'graphtitle' => $graphtitle);
            $pdf = PDF::loadView('reports/graphreport', $data);
            return $pdf->download('crm_monthly_graph.pdf');
*/
            return view('reports.graphreport', compact('graph', 'graphtitle'));
        }
        if ($graphreport == 'platform') {
            $query = "SELECT ROUND(sum(amount * 1.1),2)total,sitename FROM orders GROUP BY sitename  ORDER BY total DESC";
            $report = DB::select($query);
            $finances = Lava::DataTable();
            $finances->addStringColumn('Platform')
                    ->addNumberColumn('Total');
            foreach ($report as $row) {
                $finances->addRow([$row->sitename, $row->total]);
            }
            Lava::ColumnChart('Finances', $finances, [
                'title' => 'Sold per platform',
                'titleTextStyle' => ['color' => '#eb6b2c', 'fontSize' => 14],
                'colors' => ['#00BCD4'],
                'height' => 500,
                'legend' => ['position' => 'top'],
            ]);
            $graph = Lava::render('ColumnChart', 'Finances', 'graphReport');
            $graphtitle = 'Sold Per Platform Report';

            return view('reports.graphreport', compact('graph', 'graphtitle'));
        }
        if ($graphreport == 'description') {
            $query = "SELECT ROUND(sum(amount * 1.1),2)total,orderoption FROM orders where orderoption !='' GROUP BY orderoption  ORDER BY total DESC";
            $report = DB::select($query);
            $finances = Lava::DataTable();
            $finances->addStringColumn('Invoice Description')
                    ->addNumberColumn('Total');
            foreach ($report as $row) {
                $finances->addRow([$row->orderoption, $row->total]);
            }
            Lava::ColumnChart('Finances', $finances, [
                'title' => 'Sold per Invoice Description',
                'titleTextStyle' => ['color' => '#eb6b2c', 'fontSize' => 14],
                'colors' => ['#BF360C'],
                'height' => 500,
                'legend' => ['position' => 'top'],
            ]);
            $graph = Lava::render('ColumnChart', 'Finances', 'graphReport');
            $graphtitle = 'Sold Per Invoice Description Report';
            return view('reports.graphreport', compact('graph', 'graphtitle'));
        }
        if ($graphreport == 'page') {

            $query = "SELECT ROUND(sum(amount * 1.1),2)total,page FROM orders where page !='' GROUP BY page ORDER BY total DESC";
            $report = DB::select($query);
            $finances = Lava::DataTable();
            $finances->addStringColumn('Order Page')
                    ->addNumberColumn('Total');
            foreach ($report as $row) {
                $finances->addRow([$row->page, $row->total]);
            }
            Lava::BarChart('Finances', $finances, [
                'title' => 'Sold per Page',
                'titleTextStyle' => ['color' => '#eb6b2c', 'fontSize' => 14],
                'colors' => ['#0D47A1'],
                'height' => 500,
                'legend' => ['position' => 'top'],
            ]);
            $graph = Lava::render('BarChart', 'Finances', 'graphReport');
            $graphtitle = 'Sold Per Page Report';
            return view('reports.graphreport', compact('graph', 'graphtitle'));
        }
        if ($graphreport == 'btype') {

            $query = "SELECT ROUND(sum(orders.amount * 1.1),2)total,types.type FROM orders,invoices,customers,types 
WHERE orders.invoiceid = invoices.id AND invoices.customerid = customers.id AND customers.btype = types.id
GROUP BY type ORDER BY total DESC";
            $report = DB::select($query);
            $finances = Lava::DataTable();
            $finances->addStringColumn('Business Type')
                    ->addNumberColumn('Total');
            foreach ($report as $row) {
                $finances->addRow([$row->type, $row->total]);
            }
            Lava::ColumnChart('Finances', $finances, [
                'title' => 'Sold Per Business Type',
                'titleTextStyle' => ['color' => '#eb6b2c', 'fontSize' => 14],
                'colors' => ['#827717'],
                'height' => 500,
                'legend' => ['position' => 'top'],
            ]);
            $graph = Lava::render('ColumnChart', 'Finances', 'graphReport');
            $graphtitle = 'Sold Per Business Type';
            return view('reports.graphreport', compact('graph', 'graphtitle'));
        }
    }

}
