<?php

namespace App\Http\Controllers;

use App\AppAutomation;
use DB;

class TempController extends Controller {

    public function search() {
        $query = "SELECT types.type, customers.id,customers.bname,cstatuses.dueDatetime,statuses.name FROM types,customers,cstatuses,statuses WHERE customers.btype = types.id AND customers.id = cstatuses.customerid AND cstatuses.latest=1 AND cstatuses.statusId = statuses.id and (statuses.name = 'Active Paid Customer' OR statuses.name='To Be Invoiced')";
        $customers = DB::select($query);
        $data = array('customers' => $customers);
        $appAuto = new AppAutomation();
        $appAuto->sendEmail('reports/overdueactivepaid', $data, 'info@adultpress.com.au', 'CRM: email');
        return view('temp', compact('customers', 'users'));
    }

}
