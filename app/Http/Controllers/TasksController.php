<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\position;
use App\Customer;
use App\Cstatus;
use App\Status;
use App\Type;
use App\User;
use App\Task;
use Auth;
use DateTime;

class TasksController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function search(Request $request) {
        $selectedUsers = $request->selectedUsers;
        if (isset($selectedUsers) && $selectedUsers != '' && $selectedUsers != NULL) {
            $selectedUsers = explode(',', $request->selectedUsers);
            if (count($selectedUsers) > 0):
                $selectedUsers[0] = '"' . $selectedUsers[0];
                $selectedUsers[count($selectedUsers) - 1] = $selectedUsers[count($selectedUsers) - 1] . '"';
            endif;
            $managers = implode('","', $selectedUsers);

            $query = "SELECT cstatuses.*,statuses.name as statusname,customers.*,types.type,users.name as manager
FROM customers,users,cstatuses,statuses,types
WHERE customers.account_manager = users.id
AND users.name in ($managers)
AND cstatuses.latest=1 AND cstatuses.statusid = statuses.id
AND customers.id = cstatuses.customerid AND customers.btype = types.id
AND cstatuses.dueDatetime BETWEEN '" . date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $request->dueDateFrom))) . "' AND '" . date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $request->dueDateTo) . '+23hours 59 minutes + 59 seconds')) . "'";

            $customers = DB::select($query);
        }

        $users = User::where('status', '!=', 'deleted')->orderby('name')->get();
        return view('searchtasks', compact('customers', 'users'));
    }

    public function expiring(Request $request) {
        $selectedUsers = $request->selectedUsers;
        $allcustomers = Customer::all();
        $tomorrow = new DateTime('tomorrow');
        if (Auth::user()->group_id == 'Admin' && isset($selectedUsers) && $selectedUsers != '' && $selectedUsers != NULL) {
            $selectedUsers = explode(',', $request->selectedUsers);
            $managers = implode("','", $selectedUsers);





            $query = "SELECT cstatuses.*,statuses.name as statusname,customers.*,types.type,users.name as manager
FROM customers,users,cstatuses,statuses,types
WHERE customers.account_manager = users.id AND users.id in ( SELECT users.id from users where users.assignedto in (SELECT users.id from users where users.name in ('$managers')) )
and cstatuses.latest=1 AND cstatuses.statusid = statuses.id
AND customers.id = cstatuses.customerid AND customers.btype = types.id
and cstatuses.dueDatetime <= '" . $tomorrow->format('Y-m-d') . "' ";
            $customers = DB::select($query);

            //dd($query);
            //die();
            //$customers = Customer::whereIn('account_manager', $users)->get();
            //$managers = DB::select("select users.id,users.name,customers.* from customers,users where customers.account_manager = users.id AND users.name in $users");
        }
        //$customers = Customer::all();        //All customers



        if (Auth::user()->group_id == 'User') {


            $userid = Auth::user()->id;

            $customers = DB::select("SELECT cstatuses.*,statuses.name as statusname,customers.*,types.type,users.name as manager
FROM customers,users,cstatuses,statuses,types
WHERE customers.account_manager = users.id AND users.assignedto = $userid
and cstatuses.latest=1 AND cstatuses.statusid = statuses.id
AND customers.id = cstatuses.customerid AND customers.btype = types.id
and cstatuses.dueDatetime <='" . $tomorrow->format('Y-m-d') . "' ");

            $invoices = DB::select("SELECT invoices.*,customers.*
FROM customers,users,types ,invoices
WHERE customers.account_manager = users.id AND users.assignedto=$userid
AND invoices.customerid = customers.id");

            //dd($invoices);

            /*
              $idList = NULL;
              $i = 0;
              foreach ($managers as $id) {
              $idList[$i] = $id->id;
              $i++;
              }
              $customers = Customer::whereIn('account_manager', $idList)->get();
              } else {
              $customers = NULL;
              }
             *
             */
        }

        $users = User::where('status', '!=', 'deleted')->orderby('name')->get();
        return view('exptasks', compact('customers', 'users', 'invoices', 'allcustomers'));
    }

    public function expiringTasks(Request $request) {
        $dateToday = date("Y-m-d H:i:s", mktime(8, 0, 0, date("m"), date("d"), date("Y")));
        $dateNextWeek = date("Y-m-d H:i:s", mktime(8, 0, 0, date("m"), date("d") + 7, date("Y")));
        $query = "SELECT cstatuses.*, customers.*, types.type as business_type, users.name as manager_name, statuses.name as status_name "
                . "FROM `cstatuses`,`customers`,`types`,`users`,`statuses` "
                . "WHERE `dueDatetime` BETWEEN '$dateToday' AND '$dateNextWeek' "
                . "AND `latest` = 1 "
                . "AND customers.id = cstatuses.customerid "
                . "AND customers.btype = types.id "
                . "AND users.id = customers.account_manager "
                . "AND cstatuses.statusId = statuses.id "
                . "ORDER BY `cstatuses`.`userId` ASC";
        $customers = DB::select($query);
        $users = User::all();

        return view('expiringtasks', compact('customers', 'users'));
    }

    public function expiredTasks(Request $request) {
        $dateToday = date("Y-m-d H:i:s", mktime(8, 0, 0, date("m"), date("d"), date("Y")));
        $query = "SELECT cstatuses.*, customers.*, types.type as business_type, users.name as manager_name, statuses.name as status_name "
                . "FROM `cstatuses`,`customers`,`types`,`users`,`statuses` "
                . "WHERE `dueDatetime` < '$dateToday' "
                . "AND `latest` = 1 "
                . "AND customers.id = cstatuses.customerid "
                . "AND customers.btype = types.id "
                . "AND users.id = customers.account_manager "
                . "AND cstatuses.statusId = statuses.id "
                . "ORDER BY `cstatuses`.`userId` ASC";
        $customers = DB::select($query);
        $users = User::all();
//        foreach ($customers as $customer):
//            $customer->AdExpiry = Customer::GetAdExpiry($customer->id);
//        endforeach;
//        echo json_encode($customers);
        $expired = true;
        return view('expiringtasks', compact('customers', 'users', 'expired'));
    }

}
