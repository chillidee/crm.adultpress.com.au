<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        //$users = DB::table('users')->get();
        $users = User::where('status', '!=', 'deleted')->orderby('name')->get();
        return view('admin.admin', compact('users'));
        //return $users;
    }

    public function edit($id) {
        $user = User::find($id);
        $users = User::where('status', '!=', 'deleted')->orderby('name')->get();
        return view('admin.users.edit', compact('user', 'users'));
    }

    public function update(Request $request, $id) {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->status = $request->status;
        $user->group_id = $request->group_id;

        if (isset($request->assignedto) && $request->assignedto != 0 && $request->assignedto != '')
            $user->assignedto = $request->assignedto;
        else
            $user->assignedto = $user->id;

        if (isset($request->password) && $request->password != '')
            $user->password = Hash::make($request->password);

        $user->update();

        if ($user->id != $user->assignedto) {
            $query = "UPDATE users SET  assignedto = $user->assignedto WHERE  assignedto = $user->id";
            DB::update($query);
        }
        Session::flash('status', 'User details updated successfully');
        return redirect('admin/users');
    }

    public function store(Request $request) {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->status = $request->status;
        $user->group_id = $request->group_id;
        $user->password = Hash::make($_REQUEST['password']);
        $user->save();
        Session::flash('success', 'New user was created successfully');

        return back();
    }

    public function destroy() {

        $user = User::find($_REQUEST['userid']);
        $user->email = Hash::make('DELETED-'.$user->name.'-'.$user->email);
        $user->status = 'Deleted';
        $user->update();

        // Change the account managers to other user
        $query = "UPDATE customers SET account_manager = $user->assignedto  WHERE account_manager = $user->id";
        $result = DB::update($query);


        Session::flash('error', 'User deleted succefully');
        return redirect('admin/users');
    }

    public function changeacc(Request $request) {


        $query = "UPDATE customers SET customers.account_manager = '" . $request->accTo . " ' WHERE customers.account_manager='" . $request->accFrom . "'";
        DB::update($query);
        Session::flash('success', "All account managers Updated Succesfully");

        return redirect('admin/users');
    }

}
