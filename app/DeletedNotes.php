<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class DeletedNotes extends Model {

    private $tableName = 'deleted_notes';
    protected $fillable = ['datetime', 'note', 'userId', 'customerId'];

    public function __construct() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`datetime` timestamp NOT NULL , "
                    . "`note` text NULL , "
                    . "`userId` int NULL , "
                    . "`customerId` int NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
    }

    public function insertNotes($customer_id) {
        $notes = DB::select("SELECT * FROM `notes` WHERE customerId = $customer_id");
        foreach ($notes as $note):
            $deletedNote = new DeletedNotes();
            $deletedNote->datetime = $note->datetime;
            $deletedNote->userId = $note->userId;
            $deletedNote->note = $note->note;
            $deletedNote->customerId = $customer_id;
            $deletedNote->save();
        endforeach;
    }

}
