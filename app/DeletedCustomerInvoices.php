<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class DeletedCustomerInvoices extends Model {

    private $tableName = 'deleted_customer_invoices';
    protected $fillable = ['addabnid', 'invoiceid', 'customerid', 'userid', 'myob', 'startdate', 'expirydate', 'duedate', 'frequency', 'description', 'payment_method'];

    public function __construct() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`invoiceid` int NOT NULL , "
                    . "`addabnid` int NOT NULL , "
                    . "`customerid` int NULL , "
                    . "`userid` int NULL , "
                    . "`myob` text NULL , "
                    . "`startdate` date NULL , "
                    . "`expirydate` date NULL , "
                    . "`duedate` date NULL , "
                    . "`frequency` text NULL , "
                    . "`description` text NULL , "
                    . "`payment_method` text NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
    }

    public function insertInvoice($invoice_id) {
        $invoice = Invoice::find($invoice_id);
        $deletedInvoice = new DeletedCustomerInvoices();
        $deletedInvoice->invoiceid = $invoice_id;
        $deletedInvoice->customerid = $invoice->customerid;
        $deletedInvoice->addabnid = $invoice->addabnid;
        $deletedInvoice->userid = $invoice->userid;
        $deletedInvoice->myob = $invoice->myob;
        $deletedInvoice->startdate = $invoice->startdate;
        $deletedInvoice->expirydate = $invoice->expirydate;
        $deletedInvoice->duedate = $invoice->duedate;
        $deletedInvoice->frequency = $invoice->frequency;
        $deletedInvoice->description = $invoice->description;
        $deletedInvoice->payment_method = $invoice->payment_method;
        $deletedInvoice->save();
    }

}
