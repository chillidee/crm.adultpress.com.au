<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\Customer;

class AdminPassword extends Model {

    private $tableName = 'admin_passwords';
    protected $fillable = ['password', 'changedBy'];

    public function __construct() {
        $this->setupDatabase();
    }

    private function setupDatabase() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`password` text NOT NULL , "
                    . "`changedBy` text NOT NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
    }

    public function checkPassword($password) {
        $passwordRow = $this->getPasswordRowFromDB();
        if (isset($passwordRow[0]) && !empty($passwordRow[0])):
            return Hash::check($password, $passwordRow[0]->password);
        endif;
    }

    private function getPasswordRowFromDB() {
        return DB::select("SELECT * FROM $this->tableName");
    }

    public function updatePassword($newPassword, $user) {
        $passwordRow = $this->getPasswordRowFromDB();
        if (isset($passwordRow[0]) && !empty($passwordRow[0])):
            $id = $passwordRow[0]->ID;
            DB::delete("DELETE FROM `$this->tableName` WHERE `$this->tableName`.`ID` = $id");
        endif;
        $adminPassword = new AdminPassword();
        $adminPassword->password = Hash::make($newPassword);
        $adminPassword->changedBy = $user;
        $adminPassword->save();
        echo $this->getPasswordDetails();
    }

    public function getPasswordDetails() {
        $passwordRow = $this->getPasswordRowFromDB();
        if (isset($passwordRow[0]) && !empty($passwordRow[0])):
            $passwordUpdated = $passwordRow[0]->updated_at;
            $user = User::find($passwordRow[0]->changedBy);
            if (isset($user) && !empty($user)):
                $username = $user->name;
                $returnContent = "Password last changed by $username on $passwordUpdated";
            endif;
        endif;
        return (isset($returnContent) && !empty($returnContent)) ? $returnContent : false;
    }

}
