<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class DeletedAddAbns extends Model {

    private $tableName = 'deleted_add_abns';
    protected $fillable = ['customerid', 'unit', 'number', 'street', 'sttype', 'suburb', 'state', 'postcode', 'invname', 'abn',
        'invemail', 'invpoboxno', 'invpoboxsuburb', 'invpoboxstate', 'invpoboxpostcode', 'emailmethod', 'addressmethod', 'poboxmethod', 'latest', 'myobIdNumber'];

    public function __construct() {
        if (empty(DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . env('DB_DATABASE') . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`customerid` int NOT NULL , "
                    . "`unit` text NULL , "
                    . "`number` text NULL , "
                    . "`street` text NULL , "
                    . "`sttype` text NULL , "
                    . "`suburb` text NULL , "
                    . "`state` text NULL , "
                    . "`postcode` text NULL , "
                    . "`invname` text NULL , "
                    . "`abn` text NULL , "
                    . "`invemail` text NULL , "
                    . "`invpoboxno` text NULL , "
                    . "`invpoboxsuburb` text NULL , "
                    . "`invpoboxstate` text NULL , "
                    . "`invpoboxpostcode` text NULL , "
                    . "`emailmethod` text NULL , "
                    . "`addressmethod` text NULL , "
                    . "`poboxmethod` text NULL , "
                    . "`latest` text NULL , "
                    . "`myobIdNumber` text NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            DB::statement($query);
        endif;
    }

    public function insertAddAbns($customer_id) {
        $addabns = DB::select("SELECT * FROM `addabns` WHERE customerId = $customer_id");
        foreach ($addabns as $addabn):
            $deletedAddabns = new DeletedAddAbns();
            $deletedAddabns->customerid = $customer_id;
            $deletedAddabns->unit = $addabn->unit;
            $deletedAddabns->number = $addabn->number;
            $deletedAddabns->street = $addabn->street;
            $deletedAddabns->sttype = $addabn->sttype;
            $deletedAddabns->suburb = $addabn->suburb;
            $deletedAddabns->state = $addabn->state;
            $deletedAddabns->postcode = $addabn->postcode;
            $deletedAddabns->invname = $addabn->invname;
            $deletedAddabns->abn = $addabn->abn;
            $deletedAddabns->invemail = $addabn->invemail;
            $deletedAddabns->invpoboxno = $addabn->invpoboxno;
            $deletedAddabns->invpoboxsuburb = $addabn->invpoboxsuburb;
            $deletedAddabns->invpoboxstate = $addabn->invpoboxstate;
            $deletedAddabns->invpoboxpostcode = $addabn->invpoboxpostcode;
            $deletedAddabns->emailmethod = $addabn->emailmethod;
            $deletedAddabns->addressmethod = $addabn->addressmethod;
            $deletedAddabns->poboxmethod = $addabn->poboxmethod;
            $deletedAddabns->latest = $addabn->latest;
            $deletedAddabns->myobIdNumber = $addabn->myobIdNumber;
            $deletedAddabns->save();
        endforeach;
    }

}
