<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class position extends Model {

    protected $fillable = [
        'position', 'status'
    ];

    public function countit() {
        $count = DB::select("SELECT count(id) as countid FROM `customers` WHERE position=$this->id");
        if (isset($count) && $count != '')
            return $count[0]->countid;
        return 0;
    }

}
