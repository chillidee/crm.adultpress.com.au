<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Status extends Model {

    protected $fillable = [
        'orderid', 'name', 'status', 'expday', 'created_at', 'updated_at'
    ];

    public function countit() {
        $counts = DB::select("SELECT count(*)counts FROM `cstatuses` WHERE cstatuses.latest = 1 and statusId = $this->id");
        if (isset($counts) && $counts != '')
        //dd($counts[0]->counts);
            return $counts[0]->counts;
        return 0;
    }

}

//       select last statuses groupo by customerid 
//   
//      
//      SELECT * FROM cstatuses WHERE datetime = (SELECT max(datetime) as last FROM cstatuses as laststatus WHERE laststatus.customerid=cstatuses.customerid) GROUP BY customerid