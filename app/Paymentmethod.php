<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paymentmethod extends Model {

    protected $fillable = [
        'method', 'created_at', 'updated_at'
    ];

}
