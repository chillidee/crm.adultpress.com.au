<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'assignedto', 'group_id'
    ];

    public function assignedto($id) {
        //return $this->hasOne($user, $assignedto);
        return User::findOrFail($id)->name;

        //var_dump(User::find($id)->name);
        //return "user";
    }
    public function numberoftasks(){
        
    }
    

}
